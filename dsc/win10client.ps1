﻿Configuration Win10Client {
    param($computername)
Import-DscResource -ModuleName 'PSDesiredStateConfiguration'

    Node $Computername {
     File DirectoryCopy
        {
            Ensure = "Present"  # You can also set Ensure to "Absent"
            Type = "Directory" # Default is "File".
            Recurse = $true # Ensure presence of subdirectories, too
            SourcePath = "C:\bkpsdev"
            DestinationPath = "C:\bkpsdestination"
        }
    }
}

Win10Client -computername USWWKS699