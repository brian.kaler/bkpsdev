﻿
$ConfigurationData = @{
    AllNodes = @(
        @{
            # The "*" means "all nodes named in ConfigData" so we don't have to repeat ourselves
            NodeName="*"
            PSDscAllowPlainTextPassword = $true
        },
        #however, each node still needs to be explicitly defined for "*" to have meaning
        @{
            NodeName = "localhost"
        },
        #we can also use a property to define node-specific passwords, although this is no more secure
        @{
            Nodename = "USTEPO001";
        }
        @{
            Nodename = "USTMGT001";
        }
        @{
            Nodename = "USTIMP001";
        }
        @{
            Nodename = "ustdcs001r";
        }
        @{
            Nodename = "USTMSQ001";
        }
        @{
            Nodename = "USTMDB001";
        }
        @{
            Nodename = "USTMWB001";
        }
        @{
            Nodename = "USTMEX001";
        }
        @{
            Nodename = "USTRDP001";
        }
        @{
            Nodename = "USTDCS001";
        }
        @{
            Nodename = "USTIMX001";
        }
        @{
            Nodename = "USTMSQ002";
        }
        @{
            Nodename = "USTMBL001";
        }
        @{
            Nodename = "USTMSQ003";
        }
        @{
            Nodename = "USTMSP001";
        }
        @{
            Nodename = "USTMSP002";
        }
        @{
            Nodename = "USTMRP002";
        }
        @{
            Nodename = "USTFTL001";
        }
        @{
            Nodename = "USTACC002";
        }
        @{
            Nodename = "USTIMX002";
        }
        @{
            Nodename = "USTACC001";
        }
        @{
            Nodename = "ustavi001";
        }
        @{
            Nodename = "USTMGT002";
        }
        @{
            Nodename = "USTTMF001";
        }
        @{
            Nodename = "USTMSQ004";
        }
        @{
            Nodename = "ustvct001";
        }
        @{
            Nodename = "USTMRP001";
        }
        @{
            Nodename = "USTVHV001";
        }
        @{
            Nodename = "USTSUS001";
        }
        @{
            Nodename = "ustssql003";
        }
        @{
            Nodename = "USTFTL002";
        }
        @{
            Nodename = "USTIMX003";
        }
        @{
            Nodename = "USTIWS001";
        }
        @{
            Nodename = "USTADS001";
        }
        @{
            Nodename = "USTRDP002";
        }
        @{
            Nodename = "NAPTPRX01";
        }
        @{
            Nodename = "USTDPS001";
        }
        )
        NonNodeData = ""
}



configuration USOCPushConfig
{
    Import-DscResource -Module 'xPSDesiredStateConfiguration';'xNetworking'

    Node $AllNodes.NodeName
    {
     replace hre

    }

}

USOCPushConfig -ConfigurationData $ConfigurationData