﻿@{

    AllNodes = @(

        @{
            NodeName        = "*"
        },

        @{
            NodeName        = "USTTMP200"
            Role            = "Hyper-V"
        },

        @{
            NodeName        = "USTTMP201"
            Role            = "Hyper-V"
        },

        @{
            NodeName         = "USTTMP202"
            Role            = "Hyper-V"
        }
    )
}