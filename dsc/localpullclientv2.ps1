﻿

[DSCLocalConfigurationManager()]
configuration MetaConfigurationToRegisterWithLessSecurePullServer
{
    param
    (
        [ValidateNotNullOrEmpty()]
        [string] $NodeName = 'localhost',

        [ValidateNotNullOrEmpty()]
        [string] $RegistrationKey, #same as the one used to setup pull server in previous configuration

        [ValidateNotNullOrEmpty()]
        [string] $ServerName = 'localhost' #node name of the pull server, same as $NodeName used in previous configuration
    )

    Node $NodeName
    {
        Settings
        {
            RefreshMode        = 'Pull'
        }

        ConfigurationRepositoryWeb CONTOSO-PullSrv
        {
            ServerURL          = "https://$ServerName`:8080/PSDSCPullServer.svc" # notice it is https
            RegistrationKey    = $RegistrationKey
            ConfigurationNames = @('Win10Client')
        }   

        ReportServerWeb CONTOSO-PullSrv
        {
            ServerURL       = "https://$ServerName`:8080/PSDSCPullServer.svc" # notice it is https
            RegistrationKey = $RegistrationKey
        }
    }
}

# Sample use (please change values of parameters according to your scenario):
# Sample_MetaConfigurationToRegisterWithLessSecurePullServer -RegistrationKey $registrationkey

# =================================== Section DSC Client =================================== #

$guid = "d230e8b8-53ba-4bc6-a288-0db1be936c83"

#uncomment line below to 
#xDscWebServiceRegistration -certificateThumbPrint ‎8E9703F6660FBB708468716912BB056CF35E81E1 -RegistrationKey $guid

MetaConfigurationToRegisterWithLessSecurePullServer -registrationKey $guid -servername "dscpull01.us.pinkertons.com"