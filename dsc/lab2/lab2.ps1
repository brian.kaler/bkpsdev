﻿<#
  Create a Hyper-V virtual machine from an 
  ISO file and template
  This script requires the Convert-WindowsImage.ps1 script which you can
  download from Microsoft:
  http://gallery.technet.microsoft.com/scriptcenter/Convert-WindowsImageps1-0fe23a8f#content
#>

 
[cmdletbinding(SupportsShouldProcess)]
Param(
[Parameter (Position = 0,Mandatory,
HelpMessage = "Enter the name of the new virtual machine")]
[ValidateNotNullorEmpty()]
[string]$Name,
 
[ValidateScript({Test-Path $_ })]
[string]$ISOPath = 'c:\iso\en_windows_10_multi-edition_vl_version_1709_updated_dec_2017_x64_dvd_100406172.iso',
[ValidateNotNullorEmpty()]
[string]$Edition = "Enterprise",
 
[ValidateScript({$_ -ge 10GB})]
[int64]$Size = 20GB,
 
[ValidateScript({Test-Path $_ })]
[string]$Unattend = "C:\VHD\unattend.xml",
 
[ValidateNotNullorEmpty()]
[string]$VHDPath = "C:\VHD\$name-servervhd.vhdx",
 
[ValidateScript({$_ -ge 256MB})]
[int64]$Memory = 1GB,
 
[ValidateNotNullorEmpty()]
[string]$Switch = "Internal",
 
[ValidateScript({$_ -ge 1})]
[int]$ProcessorCount = 2
)

#.\convert-windowsimage.ps1

#import-module ".\convert-windowsimage.ps1"
#region Convert ISO to VHD
Write-Verbose "Converting ISO to VHD(X)"
 
#parse the format from the VHDPath parameter value
[regex]$rx = "\.VHD$|\.VHDX$"
#regex pattern is case-sensitive
if ($vhdpath.ToUpper() -match $rx) {
    #get the match and strip off the period
    $Format = $rx.Match($vhdpath.toUpper()).Value.Replace(".","")
}
else {
    Throw "The extension for VHDPath is invalid. It must be .VHD or .VHDX"
    Return
}
 
#define a hashtable of parameters and values for the Convert-WindowsImage
 
$convertParams = @{
SourcePath = $ISOPath
SizeBytes = $size
UnattendPath = $Unattend 
Edition = $Edition 
VHDFormat = $Format
VHDPath = $VHDPath
ErrorAction = 'Stop'
#DiskLayout = 'BIOS'
}
 
Write-Verbose ($convertParams | Out-String)
 
#define a variable with information to be displayed in WhatIf messages
$Should = "VM $Name from $ISOPath to $VHDPath"
 
#region -Whatif processing
If ($pscmdlet.ShouldProcess($Should)) {
    Try {
        #call the convert script splatting the parameter hashtable
        Convert-WindowsImage @convertParams
    }
    Catch {
        Write-Warning "Failed to convert $ISOPath to $VHDPath"
        Write-Warning $_.Exception.Message 
    }
} #should process
#endregion
 
#endregion
 
#region Creating the virtual machine
Write-Verbose "Creating virtual machine $Name"
Write-Verbose "VHDPath = $VHDPath"
Write-Verbose "MemoryStartup = $Memory"
Write-Verbose "Switch = $Switch"
Write-Verbose "ProcessorCount = $ProcessorCount"
 
New-VM -Name $Name -VHDPath $VHDPath -MemoryStartupBytes $Memory -SwitchName $Switch |
Set-VM -DynamicMemory -ProcessorCount $ProcessorCount -Passthru
 
Write-Verbose "New VM from ISO complete"
#endregion