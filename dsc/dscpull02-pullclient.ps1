﻿[DSCLocalConfigurationManager()]

Configuration LCM_Pull {

    Node Pull {

        Settings {
            ConfigurationMode = 'ApplyAndAutoCorrect'
            RefreshMode = 'Pull'
        }

        ConfigurationRepositoryWeb PullServer {
            ServerURL = 'https://dscpull02.us.pinkertons.com:8080/PsDscPullserver.svc'
            AllowUnsecureConnection = $false
            RegistrationKey = '4dabe2b4-3df0-41a0-a25f-5ed5722d9343'
            ConfigurationNames = @('WebServerConfig')
        }

        ResourceRepositoryWeb PullServerModules {
            ServerURL = 'https://dscpull02.us.pinkertons.com:8080/PsDscPullserver.svc'
            AllowUnsecureConnection = $false
            RegistrationKey = '4dabe2b4-3df0-41a0-a25f-5ed5722d9343'
        }
    }
}