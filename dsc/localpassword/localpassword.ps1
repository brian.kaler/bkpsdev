﻿#param(
#[Parameter(Position=0,Mandatory)]
#[string[]]$Computername
#)

#BEGIN CONFIGURATION
 
Configuration LocalUserAccounts {
 #Import-DscResource -module 'xPSDesiredStateConfiguration'

Param(
[Parameter(Position=0,Mandatory)]
[string[]]$Computername
)

Node $Computername {
$pw = foreach($item in $node.getenumerator() | where {$_.key -eq "password"}){$item.value}
User SocAdmin {
    UserName="socadmin"
    Description="Local administrator account"
    Disabled=$False
    Ensure="Present"
    password=New-Object System.Management.Automation.PSCredential("socadmin",(convertto-securestring -AsPlainText -force ($pw)))
}
 
#add the account to the Administrators group
Group Administrators {
    GroupName="Administrators"
    DependsOn="[User]socadmin"
    Ensure="Present"
    MembersToInclude="SocAdmin"
}
 
} #node
 
} #end configuration
 

#ROT13 func
function ConvertFrom-Rot13
{
     [CmdletBinding()]
     param(
          [Parameter(
              Mandatory = $true,
              ValueFromPipeline = $true
          )]
          [String]
          $rot13string
     )
    
     [String] $string = $null;
     $rot13string.ToCharArray() |
     ForEach-Object {
          Write-Verbose "$($_): $([int] $_)"
          if((([int] $_ -ge 97) -and ([int] $_ -le 109)) -or (([int] $_ -ge 65) -and ([int] $_ -le 77)))
          {
              $string += [char] ([int] $_ + 13);
          }
          elseif((([int] $_ -ge 110) -and ([int] $_ -le 122)) -or (([int] $_ -ge 78) -and ([int] $_ -le 90)))
          {
              $string += [char] ([int] $_ - 13);
          }
          else
          {
              $string += $_
          }
     }
     $string
}

#find the host number, and host string
function findhostnumber()
{
    param(
    [Parameter(Mandatory=$true)]
    [String]$hostname
    )
    $hsplit = $hostname.toCharArray()
    foreach($letter in $hsplit)
        {
        if($letter -match "[0-9]")
        {
        $hostnumber = $hostnumber + $letter
        }
        else
        {
        $hoststring = $hoststring + $letter
        }
        }
    return $hostnumber,$hoststring
}

#Function to split hostname
function splithostname(){
    param(
    [Parameter(Mandatory=$true)]
    [String]$hoststring
    )
    #is it odd length? subtract 1 to make it even
    If($hoststring.length % 2 -eq 1){$count = ($hoststring.length) - 1}else{$count = $hoststring.length}
    #get x,y - designed to look confusing.
    $x = $count / 2
    $y = $hoststring.Length - $count / 2
    #split string portion
    $split1 = $hoststring.Substring(0,$x)
    $split2 = $hoststring.Substring($x,$y)
    Return $split1.toupper(),$split2.tolower()
}

function obfuscatepassword(){
    Param(
    [Parameter(mandatory=$true)]
    [String]$computer,
    [Parameter(mandatory=$true)]
    [String]$username
    )
    #uncomment line below for local PC only execution
    #$hostname = gci -path env:computername | select -ExpandProperty value
    #process the hostname
    $hoststring = (findhostnumber($computer))[1]
    #obfuscate the hostnumber
    $number = ((findhostnumber($computer))[0])
    #split the hostname
    $split = splithostname($hoststring)
    $osplit1 = Convertfrom-rot13($split[0])
    $osplit2 = Convertfrom-rot13($split[1])
    #construct the final string
    $rejoin = ($osplit1 + $number + $osplit2) + "!"
    Return $rejoin
    }
function ConvertHashtableTo-Object {
    [CmdletBinding()]
    Param([Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)]
        [hashtable]$ht
    )
    PROCESS {
        $results = @()
 
        $ht | %{
            $result = New-Object psobject;
            foreach ($key in $_.keys) {
                $result | Add-Member -MemberType NoteProperty -Name $key -Value $_[$key]
             }
             $results += $result;
         }
        return $results
    }
}

#YOU MUST EDIT THIS - COMPUTERS TO BE TARGETED FOR PUSH CONFIG!!
$computername = get-content -path ".\targets-05142018.txt"
#YOU MUST EDIT THIS - COMPUTERS TO BE TARGETED FOR PUSH CONFIG!!

#initialize/reset variables
$hoststring = ""
$number = ""
#create config data to allow plain text passwords
$ConfigData=@{AllNodes=$Null}
#initialize an array for node information
$nodes=@()
foreach ($computer in $computername) {
  #write-host "Adding $computer" -foreground green
  #define a hashtable for each computername and add to the nodes array
  $nodes+=@{
          NodeName = "$computer"
          PSDscAllowPlainTextPassword=$true
          username = "socadmin"
          password = (Obfuscatepassword -username "socadmin" -computer $computer)
        }
}
#add the nodes to AllNodes
$ConfigData.AllNodes = $nodes 
##### Begin DSC MOF process
$csvsource = $nodes | ConvertHashtableTo-Object
$csvsource.GetEnumerator() | out-file D:\passwordlist.txt


#create the configurations
localuseraccounts -computername $targets -configurationdata $configdata