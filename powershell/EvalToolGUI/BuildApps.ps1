$arg1 = Read-Host -prompt "Provide a computer name"
$invocation = (Get-Variable MyInvocation).Value
$path = Split-Path $invocation.MyCommand.Path
$file = $path + "\evaltool.ini"
Remove-Item $file
$ini = New-Item $file -type file



function writeSoftware
{
$i = 0
Out-File $ini -inputobject "[software]"
Get-WmiObject -Class Win32Reg_AddRemovePrograms -computername $arg1| Select-Object -property DisplayName | ForEach-Object {
++$i
if($_.displayname -notmatch "KB\d{6,8}"){
$line = "software" + $i + "=" + $_.displayname
Out-File $ini -inputobject $line -append
}
}
}

writesoftware
