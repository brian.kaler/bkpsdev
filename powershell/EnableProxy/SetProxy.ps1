$regKey="HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
$proxyServer = "uswipw001"
$proxyServerToDefine = "uswipw001:80"
Write-Host "Retrieve the proxy server ..."
$proxyServer = Get-ItemProperty -path $regKey AutoConfigU -ErrorAction SilentlyContinue
Write-Host $proxyServer
if([string]::IsNullOrEmpty($proxyServer))
{
    Write-Host "Proxy is actually disabled"
    Set-ItemProperty -path $regKey ProxyEnable -value 1
    Set-ItemProperty -path $regKey ProxyServer -value $proxyServerToDefine
    Write-Host "Proxy is now enabled"
	$wshell = New-Object -ComObject Wscript.Shell

$wshell.Popup("Proxy is now enabled",0,"Done",0x1)
}
else
{
    Write-Host "Proxy is actually enabled"
    Set-ItemProperty -path $regKey ProxyEnable -value 0
    Remove-ItemProperty -path $regKey -name ProxyServer
    Write-Host "Proxy is now disabled"
		$wshell = New-Object -ComObject Wscript.Shell

$wshell.Popup("Proxy is now disabled",0,"Done",0x1)
}