#========================================================================
# Created with: SAPIEN Technologies, Inc., PowerShell Studio 2012 v3.1.26
# Created on:   11/26/2013 3:52 PM
# Created by:   bkaler
# Organization: 
# Filename:     
#========================================================================


[string]$dir="c:\"

$act="Encryption Status"
$status="Unknown"
$reg = "HKLM:\Software\Microsoft\MBAM"
$current = "Encryption Status"

Write-Progress -Activity $act -Status $status -CurrentOperation $current

#create if doesn't exist
If ((Get-Item -Path $reg) -eq "null")
{
New-Item -Path $reg -Name "MBAM" -Type "dword" -value "1" 
}

Set-ItemProperty -Path $reg -Name "KeyRecoveryOptions" -type "dword" -Value "1"
Set-ItemProperty -Path $reg -Name "UseKeyRecoveryService" -type "dword" -Value "1"
Set-ItemProperty -Path $reg -Name "KeyRecoveryServiceEndPoint" -type "ExpandString" -Value "http://uswvbde801.us.pinkertons.com/MBAMRecoveryAndHardwareService/CoreService.svc"
Set-ItemProperty -Path $reg -Name "DeploymentTime" -type "dword" -Value "1"
Set-ItemProperty -Path $reg -Name "NoStartupDelay" -type "dword" -Value "1"

$service = Get-Service | Where-Object {$_.Name -eq "MBAMAgent"}
restart-Service -ServiceName "MBAMAgent"
sleep -Seconds 15

$services = Get-Service -Name "MBAMAgent" | Select-Object -ExpandProperty "Status"

#terminate script if service fails to restart
If ($services -eq "Stopped")
{
Out-Host -InputObject "Service Failed to Restart"
Break
}

while ($num -ne 100)
{
$status = (manage-bde -status C:) -match '^\s+Conversion Status:' -replace "Conversion Status:\s+" | out-string -stream
$current = (manage-bde -status C:) -match '^\s+Protection Status:' -replace 'Protection Status:\s+' | out-string -Stream
$num = (manage-bde -status C:) -match '^\s+Percentage Encrypted:' -replace '\D+(\d+)%','$1'  | out-string -stream
$sccmtext = "Percentage Encrypted:   " + $num + "%" + "   " + $status + "   " + $current
Write-Progress -Activity $num -Status $sccmtext -PercentComplete $num -CurrentOperation $current
}

while ($drivestatus -ne 1) {
#check drive protection status
$drivestatus = Get-WmiObject -Namespace 'Root\cimv2\Security\MicrosoftVolumeEncryption' -Query 'Select * From Win32_EncryptableVolume' | where {$_.DriveLetter -eq "C:"} | Select-Object -ExpandProperty "ProtectionStatus"
#wait until drive status is good.
	sleep -Seconds 3
}

Set-ItemProperty -Path $reg -Name "DeploymentTime" -type "dword" -Value "0"

