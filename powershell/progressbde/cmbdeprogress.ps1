[string]$dir="c:\"

$act="Encryption Status"
$status="Unknown"
$reg = "HKLM:\Software\Microsoft\MBAM"
$current = "Encryption Status"

Write-Progress -Activity $act -Status $status -CurrentOperation $current

$startenc = (manage-bde -protectors -add C: -TPMandPIN "Crossmatch1")


while ($num -ne 100)
{
$status = (manage-bde -status C:) -match '^\s+Conversion Status:' -replace "Conversion Status:\s+" | out-string -stream
$current = (manage-bde -status C:) -match '^\s+Protection Status:' -replace 'Protection Status:\s+' | out-string -Stream
$num = (manage-bde -status C:) -match '^\s+Percentage Encrypted:' -replace '\D+(\d+)%','$1'  | out-string -stream
$sccmtext = "Percentage Encrypted:   " + $num + "%" + "   " + $status + "   " + $current
Write-Progress -Activity $num -Status $sccmtext -PercentComplete $num -CurrentOperation $current
}

while ($drivestatus -ne 1) {
#check drive protection status
$drivestatus = Get-WmiObject -Namespace 'Root\cimv2\Security\MicrosoftVolumeEncryption' -Query 'Select * From Win32_EncryptableVolume' | where {$_.DriveLetter -eq "C:"} | Select-Object -ExpandProperty "ProtectionStatus"
#wait until drive status is good.
	sleep -Seconds 3
}

Set-ItemProperty -Path $reg -Name "DeploymentTime" -type "dword" -Value "0"

