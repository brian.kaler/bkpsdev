﻿#determine where the mozilla.cfg file is located
function is64bit() {
  return ([IntPtr]::Size -eq 8)
}
function get-programfilesdir() {
  if (is64bit -eq $true) {
    (Get-Item "Env:ProgramFiles(x86)").Value
  }
  else {
    (Get-Item "Env:ProgramFiles").Value
  }
}
$a = get-programfilesdir
$filename = $a + "\Mozilla Firefox\Mozilla.cfg"
$length = Get-Item -path $filename | Select-Object -Property Length
If ([int]$length.length -eq 783)
{return $true}
else
{return $false}