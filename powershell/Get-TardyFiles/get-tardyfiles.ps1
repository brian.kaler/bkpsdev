﻿#base variables
$ltstamp = get-date -format hh:MM:ss

#folders to monitor
$foldersource = Get-Content ".\folderlist.txt"
#computers to monitor (if file isn't present, will run on parameter)
#$computers = gci ".\computerlist.txt"
#range in hours before alert fires (for additional days (ie 3, 72 hours)
$tardyrange = 48


#scans a folder and reports files that are older than specified tardy range
function scan_folder($folder)
{
$timestamps = Get-ChildItem $folder -file
$threshold = $now.addhours(-($tardyrange))
$tardyfiles = @()
    foreach($i in $timestamps)
    {
        $j = $threshold - $i.LastWriteTime
        if($j.TotalHours -ge $tardyrange)
        {
        $tardyfiles += $i
        }
    }
    if($tardyfiles -ne $null)
    {
    Return ,$tardyfiles
    }
}


# SCRIPT BEGINS HERE
$alerts = [system.collections.arraylist]@()
foreach($i in $foldersource)
{
$j = scan_folder($i)
    #if a file was found, raise a flag.
    if($j -ne $null)
    {
        foreach($k in $j)
        {
        $filestamp = $k.LastWriteTime
        $line = "$ltstamp File $k is old - TIMESTAMP: $filestamp"
        $alerts.add($line)
        }
    }
    #if a folder is empty, raise a flag
    if((get-childitem $i).count -lt 1)
    {
    $alerts.add("Folder $i was found to be EMPTY")
    }
}


$alerts
