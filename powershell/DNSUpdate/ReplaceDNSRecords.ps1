Function ReverseIP($ip)
{
# build array
$arr = $ip.split('.')
# build strings
$revipstr = $arr[2] + '.' + $arr[1] + '.' + $arr[0] + '.in-addr.arpa'
# return values
return $revipstr
}

Function ReverseIPName($ip)
{
# build array
$arr = $ip.split('.')
# build strings
$revipname = $arr[3]
# return values
return $revipname
}

# Environment Setup
$DNSServer = "10.1.16.67"
$DNSZone = "us.pinkertons.com"
$InputFile = "dnsrecords.csv"

# Read the input file which is formatted as name,type,address with a header row
$records = Import-CSV $InputFile

# Now we loop through the file to delete and re-create records
# DNSCMD does not have a modify option so we must use /RecordDelete first followed by a /RecordAdd 

ForEach ($record in $records) {

	# Capture the record contents as variables
	$recordName = $record.name
	$recordType = $record.type
	$recordAddress = $record.address
    
    $revip = ReverseIP($recordAddress) + '.'
    $revipname = ReverseIPName($recordaddress)

	# Build our DNSCMD DELETE command syntax
	$cmdDelete = "dnscmd $DNSServer /RecordDelete $DNSZone $recordName $recordType /f"
    $cmdDeletePTR = "dnscmd $DNSServer /RecordDelete $revip $revipname PTR /f"

	# Build our DNSCMD ADD command syntax
	$cmdAdd = "dnscmd $DNSServer /RecordAdd $DNSZone $recordName /createPTR $recordType $recordAddress"

    # Build the PTR ADD command
    #$ptrAdd = "dnscmd $DNSServer /RecordAdd $"

	# Now we execute the command
	Write-Host "Running the following command: $cmdDelete"
	Invoke-Expression $cmdDelete
    Write-Host "Running the following command: $cmdDeletePTR"
    Invoke-Expression $cmdDeletePTR

	Write-Host "Running the following command: $cmdAdd"
    Write-Host "The Reverse ip was: $revip"
	Invoke-Expression $cmdAdd

}

