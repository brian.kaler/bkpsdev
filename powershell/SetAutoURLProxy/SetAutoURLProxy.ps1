$regKey="HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
$configurl = "http://uswdhcp001/westproxyfieldMPLS.pac"
Write-Host "Retrieve the AutoConfigURL server ..."
$autoURL = Get-ItemProperty -path $regKey AutoConfigURL -ErrorAction SilentlyContinue
Write-Host $autoURL
if([string]::IsNullOrEmpty($autoURL))
{
    Write-Host "PAC File is disabled"
    Set-ItemProperty -path $regKey AutoConfigURL -value $configurl
	Set-ItemProperty -path $regKey ProxyEnable -Value 0
    Write-Host "PAC File is now enabled"
	$wshell = New-Object -ComObject Wscript.Shell

$wshell.Popup("PAC File is now enabled",0,"Done",0x1)
}
else
{
    Write-Host "PAC File is actually enabled"
    Set-ItemProperty -path $regKey AutoConfigURL -value ""
	Set-ItemProperty -path $regKey ProxyEnable -Value 1
    Remove-ItemProperty -path $regKey -name AutoConfigURL
    Write-Host "PAC File is now disabled"
		$wshell = New-Object -ComObject Wscript.Shell

$wshell.Popup("PAC File is now disabled",0,"Done",0x1)
}