﻿param(
[string]$machinelist,
[string]$query,
[int]$interval = 300
)

Function LogWrite
{
   Param ([string]$logstring)

   Add-content $Logfile -value $logstring
}


function CheckServices($machines){

foreach($computer in $machines){

$arrServices = get-service -computername $computer | where {$_.DisplayName -like $query}
Logwrite($arrServices)

foreach($svc in $arrServices){
$status = $svc.Status

if($svc.status -eq "Stopped"){
$prompt = Read-Host -prompt ("The following service on host: " + $hostname + " servicename: " + $svc.DisplayName + " is in status: " + $svc.Status + ", do you wish to start it? (y/n)")
if($prompt -eq "y"){Restart-Service $svc}
}


if($status -eq "Stopped"){
write-host ($svc.DisplayName + " is in status " + $svc.status) -foregroundcolor Magenta
}
elseif($status -eq "Running"){
write-host ($computer + " :: " + $svc.DisplayName + " is in status " + $svc.status) -foregroundcolor Green
}

}
}

}



#create current directory environment
$MyInvocation = (Get-Variable MyInvocation).Value
$dir = split-path $MyInvocation.MyCommand.path
#set Logfile
$Logfile = ($dir + "\logfile.log")
#make sure it's empty to start
set-content $logfile -value ""
$hostname = hostname
#build machine list
$machines = get-content ($dir + "\" + $machinelist)


While($true){

CheckServices($machines)
Start-Sleep -seconds $interval
}
