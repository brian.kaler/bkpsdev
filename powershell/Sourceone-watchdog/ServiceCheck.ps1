﻿Function LogWrite
{
   Param ([string]$logstring)

   Add-content $Logfile -value $logstring
}

$MyInvocation = (Get-Variable MyInvocation).Value
$dir = split-path $MyInvocation.MyCommand.path
$Logfile = ($dir + "\logfile.log")

$hostname = [System.Net.Dns]::GetHostName()
$arrServices = Get-Service | Where {$_.DisplayName -like "*SourceOne*"}
logwrite("Services Found on " + $hostname + ":" + $arrServices)


foreach($svc in $arrServices){
logwrite($svc.displayname + " in status: " + $svc.status)
if($svc.status -eq "Stopped"){
$prompt = Read-Host -prompt ("The following service on host: " + $hostname + " servicename: " + $svc.DisplayName + " is in status: " + $svc.Status + ", do you wish to start it? (y/n)")
if($prompt -eq "y"){Restart-Service $svc}
}
}
