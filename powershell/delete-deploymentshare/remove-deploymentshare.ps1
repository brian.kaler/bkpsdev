﻿#for debug mostly
remove-psdrive hku
#set up hives to search
$hives = new-psdrive -psprovider Registry -name hku -root hkey_users
#find all guids
set-location ($hives.name + ':')
$base = get-childitem
#get subkeys
set-variable -name userhives
foreach($b in $base){
    if($b.name -match '(.*S-1-5-21.*(?!_Classes))'){
            $a = Get-ChildItem ("hku:\" + $b.pschildname + "\Network") -recurse -ErrorAction SilentlyContinue | get-itemproperty -name RemotePath | where-object {$_.RemotePath -match "*uswvwds100*"}
            remove-item $a.pspath -force -recurse
            }

      }

$drive = get-psdrive Z | Remove-PSDrive


set-location c:

# saving for later
