#eventlog logging
if(([System.Diagnostics.EventLog]::Exists($logname)) -and ([System.Diagnostics.EventLog]::SourceExists($source)))
    {

    }
    Else
    {
    New-EventLog -logname $logname -source $source
    }



function add_loginfo($logmsg)
{
$params = @{
    LogName = $logname
    Source = $source
    EntryType = 'Information'
    EventId = '1000'
    Message = $logmsg
}
write-eventlog @params
}
function add_logwarn($logmsg)
{
$params = @{
    LogName = $logname
    Source = $source
    EntryType = 'Warning'
    EventId = '1000'
    Message = $logmsg
}
write-eventlog @params
}
function add_logerror($logmsg)
{
$params = @{
    LogName = $logname
    Source = $source
    EntryType = 'Error'
    EventId = '1000'
    Message = $logmsg
}
write-eventlog @params
}
