#------------------------------------------------------------------------
# Source File Information (DO NOT MODIFY)
# Source ID: 17a2891a-4ad8-422f-afda-89eb1e505d83
# Source File: D:\Mydocs\SAPIEN\PowerShell Studio 2012\Files\EvalToolGUI.pff
#------------------------------------------------------------------------
#region File Recovery Data (DO NOT MODIFY)
<#RecoveryData:
UBgAAB+LCAAAAAAABADsvQdgHEmWJSYvbcp7f0r1StfgdKEIgGATJNiQQBDswYjN5pLsHWlHIymr
KoHKZVZlXWYWQMztnbz33nvvvffee++997o7nU4n99//P1xmZAFs9s5K2smeIYCqyB8/fnwfPyIe
v8qn1WVeXz/N2uzoN07S9PGXdXFRLLPyWVHmL7JFfnR6mZVvqqr8/Kuz8er8/PHdXgt+D3/9ZF43
RbU82h3vPb7rfyCQJz+dT9u0vV7ln330+rpp88X4u8VyVl0142dVvZB/R2nsq1GqoD7bH+/gv1F6
si7bdZ1/tszXbZ2Vo/TlelIW098rv35Tvc2Xn00ePMjuT+9/uvvw3n6+c/Dwo3RJyH720TnB+yIr
lh+l03lRzmpq+tFJtWzrqmw+YkwJ15d1tcrr9lrfOSmLfNm+Ln6Qf3R0/2B/lN77lEZoGg28BNp8
dGS6u7H56zar25dVU7Q0zI+OTqjDvH49rfP85nff5O/aj44+r4tZv+npJUHSds+rbPbRESjKn/7+
+PvxXf7dNL95lsAr6Ooni/zqZ322ZtTZBXV2SZ29yhuC0myauD5pjsuyuvqqyes31fFs9opwJAJk
ZZP3KLXx3ad5mbf5e76+nM6r+qOjN9VqlD6p2rYi7n6en7ej9FVxMW9vAeKkKteL5bfzbEYEfZqf
Z0SAk7wsX7fXZe4GTS+eLZs2W07zp3kzrYtVW9Wv8vOciDTNDYPxTP3+OlO/P8/U7+9PpgX8++9+
lN51ON0WTW8WgBB1dls2Av8+qd4JmJ89nvLQIwS79JnO8+nbsmhaf/AYPkby/+mREWe062bjsG4x
yT889tt7T/Z7Xk0z0Zq7e6N0t6+ah5RzRLvc4t1XJI5fLstrkux6fRtFQGrjhy7C996ThmrcPiXu
u7fz8BYvvMkmZ8tZ/u6joyi9fbMDrKCp6ROypNO3UcL//t1WoWHqgvQU4xfVusk3AY627di9u2L4
bm8Gn6xJof8sirQOdMLdnL4r2ve0e2p8jN25rcVxsrT/8AHxwt6DW7wk0uRQvcUrwm8P7o/SvXu3
aO64bfc2rdkfOv2F2WJ1eDt0yMr/ZNGsM5GeJ9n0LXENG+8BEQ+YUXjPjf/3//8eh7Fr+EEcBs/m
FpQOlfX785f4rDe+8rX5a+c2rZm/mL1uic43x1/o8Eb+Cv+4mdV+DlwWHZvzusw4OpQTvS0UPzGN
UzgvN4ZFwja2hxvb32DYu82/W8za+UdH93a6LPP/HeqrZ3gL0r/mljfSREje3K7x16P33g30Pm6a
fEGjzxsDRz+5PlJqfpEts4t8QbIzPl6T7mJ95Ki6e1uq3tudnN87uP9pNrv36X5+7/7ju7aneM+u
j73b9tGduRv7GOKVn8Uen9bZVbG8+Dp97dw7v3/+4Hx3d3Z/J7uXbehr0Uyruiwm3wD332JEJJFf
Zzjv3dHvvSh/OAMqahKQqr5+ndeXxTT/Wkrk9pOlvZ5Udf51Onrv4emoSCvQ0H4Wxmb/NFrl8Usy
4aREaoPR5/kyJ8CeAyANoBDdh3ejn+qHZ1NNlQZNw09JdRXnedOe1DlrLbjDvc9s65N1Q+rNfO/B
HvriZZm1yFgewWcyv9tvX62Xr98cw0PS39x71RUlK+cUwZmUL4LC3odKq7shsR6/zqfruiA9byLW
x+ELaTe9zJPaTzELKnU1W0/bXuPO59323XmKfWoC8+D9u9FPT6rFKlt6/tzd3icn1eq6RlQWNOp9
doY8MKXbOwjGP47m5uWF4a8Is0UQaN8NP3l81yeaZUywPnGCv4Lw/wQAAP//GTmQr1AYAAA=#>
#endregion
#========================================================================
# Code Generated By: SAPIEN Technologies, Inc., PowerShell Studio 2012 v3.1.26
# Generated On: 12/23/2013 10:32 AM
# Generated By: bkaler
#========================================================================
#----------------------------------------------
#region Application Functions
#----------------------------------------------

function OnApplicationLoad {
	#Note: This function is not called in Projects
	#Note: This function runs before the form is created
	#Note: To get the script directory in the Packager use: Split-Path $hostinvocation.MyCommand.path
	#Note: To get the console output in the Packager (Windows Mode) use: $ConsoleOutput (Type: System.Collections.ArrayList)
	#Important: Form controls cannot be accessed in this function
	#TODO: Add snapins and custom code to validate the application load
	
	return $true #return true for success or false for failure
}

function OnApplicationExit {
	#Note: This function is not called in Projects
	#Note: This function runs after the form is closed
	#TODO: Add custom code to clean up and unload snapins when the application exits
	
	$script:ExitCode = 0 #Set the exit code for the Packager
}

#endregion Application Functions

#----------------------------------------------
# Generated Form Function
#----------------------------------------------
function Call-EvalToolGUI_pff {

	#----------------------------------------------
	#region Import the Assemblies
	#----------------------------------------------
	[void][reflection.assembly]::Load("System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
	[void][reflection.assembly]::Load("System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
	[void][reflection.assembly]::Load("System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")
	[void][reflection.assembly]::Load("mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
	[void][reflection.assembly]::Load("System.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
	[void][reflection.assembly]::Load("System.Xml, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
	[void][reflection.assembly]::Load("System.DirectoryServices, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")
	[void][reflection.assembly]::Load("System.Core, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
	[void][reflection.assembly]::Load("System.ServiceProcess, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")
	#endregion Import Assemblies

	#----------------------------------------------
	#region Generated Form Objects
	#----------------------------------------------
	[System.Windows.Forms.Application]::EnableVisualStyles()
	$formMain = New-Object 'System.Windows.Forms.Form'
	$datagridviewResults = New-Object 'System.Windows.Forms.DataGridView'
	$buttonExit = New-Object 'System.Windows.Forms.Button'
	$buttonLoad = New-Object 'System.Windows.Forms.Button'
	$checklist = New-Object 'System.Windows.Forms.DataGridViewTextBoxColumn'
	$status = New-Object 'System.Windows.Forms.DataGridViewTextBoxColumn'
	$InitialFormWindowState = New-Object 'System.Windows.Forms.FormWindowState'
	#endregion Generated Form Objects

	#----------------------------------------------
	# User Generated Script
	#----------------------------------------------
	
	
	
	
	
	
	
	#region Control Helper Functions
	function Load-DataGridView
	{
		<#
		.SYNOPSIS
			This functions helps you load items into a DataGridView.
	
		.DESCRIPTION
			Use this function to dynamically load items into the DataGridView control.
	
		.PARAMETER  DataGridView
			The ComboBox control you want to add items to.
	
		.PARAMETER  Item
			The object or objects you wish to load into the ComboBox's items collection.
		
		.PARAMETER  DataMember
			Sets the name of the list or table in the data source for which the DataGridView is displaying data.
	
		#>
		Param (
			[ValidateNotNull()]
			[Parameter(Mandatory=$true)]
			[System.Windows.Forms.DataGridView]$DataGridView,
			[ValidateNotNull()]
			[Parameter(Mandatory=$true)]
			$Item,
		    [Parameter(Mandatory=$false)]
			[string]$DataMember
		)
		$DataGridView.SuspendLayout()
		$DataGridView.DataMember = $DataMember
		
		if ($Item -is [System.ComponentModel.IListSource]`
		-or $Item -is [System.ComponentModel.IBindingList] -or $Item -is [System.ComponentModel.IBindingListView] )
		{
			$DataGridView.DataSource = $Item
		}
		else
		{
			$array = New-Object System.Collections.ArrayList
			
			if ($Item -is [System.Collections.IList])
			{
				$array.AddRange($Item)
			}
			else
			{	
				$array.Add($Item)	
			}
			$DataGridView.DataSource = $array
		}
		
		$DataGridView.ResumeLayout()
	}
	
	function ConvertTo-DataTable
	{
		<#
			.SYNOPSIS
				Converts objects into a DataTable.
		
			.DESCRIPTION
				Converts objects into a DataTable, which are used for DataBinding.
		
			.PARAMETER  InputObject
				The input to convert into a DataTable.
		
			.PARAMETER  Table
				The DataTable you wish to load the input into.
		
			.PARAMETER RetainColumns
				This switch tells the function to keep the DataTable's existing columns.
			
			.PARAMETER FilterWMIProperties
				This switch removes WMI properties that start with an underline.
		
			.EXAMPLE
				$DataTable = ConvertTo-DataTable -InputObject (Get-Process)
		#>
		[OutputType([System.Data.DataTable])]
		param(
		[ValidateNotNull()]
		$InputObject, 
		[ValidateNotNull()]
		[System.Data.DataTable]$Table,
		[switch]$RetainColumns,
		[switch]$FilterWMIProperties)
		
		if($Table -eq $null)
		{
			$Table = New-Object System.Data.DataTable
		}
	
		if($InputObject-is [System.Data.DataTable])
		{
			$Table = $InputObject
		}
		else
		{
			if(-not $RetainColumns -or $Table.Columns.Count -eq 0)
			{
				#Clear out the Table Contents
				$Table.Clear()
	
				if($InputObject -eq $null){ return } #Empty Data
				
				$object = $null
				#find the first non null value
				foreach($item in $InputObject)
				{
					if($item -ne $null)
					{
						$object = $item
						break	
					}
				}
	
				if($object -eq $null) { return } #All null then empty
				
				#Get all the properties in order to create the columns
				foreach ($prop in $object.PSObject.Get_Properties())
				{
					if(-not $FilterWMIProperties -or -not $prop.Name.StartsWith('__'))#filter out WMI properties
					{
						#Get the type from the Definition string
						$type = $null
						
						if($prop.Value -ne $null)
						{
							try{ $type = $prop.Value.GetType() } catch {}
						}
	
						if($type -ne $null) # -and [System.Type]::GetTypeCode($type) -ne 'Object')
						{
			      			[void]$table.Columns.Add($prop.Name, $type) 
						}
						else #Type info not found
						{ 
							[void]$table.Columns.Add($prop.Name) 	
						}
					}
			    }
				
				if($object -is [System.Data.DataRow])
				{
					foreach($item in $InputObject)
					{	
						$Table.Rows.Add($item)
					}
					return  @(,$Table)
				}
			}
			else
			{
				$Table.Rows.Clear()	
			}
			
			foreach($item in $InputObject)
			{		
				$row = $table.NewRow()
				
				if($item)
				{
					foreach ($prop in $item.PSObject.Get_Properties())
					{
						if($table.Columns.Contains($prop.Name))
						{
							$row.Item($prop.Name) = $prop.Value
						}
					}
				}
				[void]$table.Rows.Add($row)
			}
		}
	
		return @(,$Table)	
	}
	#endregion
	
	$FormEvent_Load={
		#TODO: Initialize Form Controls here
	
	}
	
	$buttonExit_Click={
		#TODO: Place custom script here
		$formMain.Close()
	}
	
	$buttonLoad_Click={
		#TODO: Place custom script here
	#	---------------------------------
	#	Sample Code to Load Grid
	#	---------------------------------
	#	$processes = Get-WmiObject Win32_Process -Namespace "Root\CIMV2"
	#	Load-DataGridView -DataGridView $datagridviewResults -Item $processes
	#	---------------------------------
	#	Sample Code to Load Sortable Data
	#	---------------------------------
	# 	$processes = Get-WmiObject Win32_Process -Namespace "Root\CIMV2"
	#	$table = ConvertTo-DataTable -InputObject $processes -FilterWMIProperties
	#	Load-DataGridView -DataGridView $datagridviewResults -Item $table
		
		
		$zlist = "7-Zip","Winzip","ThiswillBreak","Microsoft"
		foreach ($sw in $zlist)
			{
			$vsoft = EvalSoftware($sw)
			$zarray = @(($sw),($vsoft))
			$datagridviewResults.Rows.Add($zarray)
		}
		
	#EvalFiles("C:\","cmtrace.exe") 
		parseINI
		
	}
	
	$datagridviewResults_ColumnHeaderMouseClick=[System.Windows.Forms.DataGridViewCellMouseEventHandler]{
	#Event Argument: $_ = [System.Windows.Forms.DataGridViewCellMouseEventArgs]
		if($datagridviewResults.DataSource -is [System.Data.DataTable])
		{
			$column = $datagridviewResults.Columns[$_.ColumnIndex]
			$direction = [System.ComponentModel.ListSortDirection]::Ascending
			
			if($column.HeaderCell.SortGlyphDirection -eq 'Descending')
			{
				$direction = [System.ComponentModel.ListSortDirection]::Descending
			}
	
			$datagridviewResults.Sort($datagridviewResults.Columns[$_.ColumnIndex], $direction)
		}
	}
	
	$datagridviewResults_CellContentClick=[System.Windows.Forms.DataGridViewCellEventHandler]{
	#Event Argument: $_ = [System.Windows.Forms.DataGridViewCellEventArgs]
		#TODO: Place custom script here
		
	}
	
	
	function parseINI()
	{
	Get-Content evaltool.ini | ForEach-Object{
			out-host
			#$values = $_.name,$_.value
			#New-Variable -Name $values[1] -Value $values[2]
		}
	Out-Host -InputObject $values
	}
	
	function EvalSoftware($dn)
	{
		$i = 0
	$query = '.*' + $dn + '.*'
		if ((Get-WmiObject Win32_OperatingSystem).OSArchitecture = "32-bit")
		{$uninstall = "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall"}
		elseif ((Get-WmiObject Win32_OperatingSystem).OSArchitecture = "64-bit")
		{$uninstall = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall"}
	$zreg = gci -path $uninstall| Select-Object -Property Name
	foreach($_ in $zreg)
	{
			
		if ($_.name -match "HKEY_LOCAL_MACHINE")
		{ $fixed = $_.name -replace "HKEY_LOCAL_MACHINE","HKLM:"}
	$doquery = Get-ItemProperty -path $fixed | select DisplayName | Select-String -pattern $query -quiet
	if($doquery)
		{
		$i++
				}
	}	
		if ($i -ge 1)
		{return "true"}
		else
		{return "false"}
	}
	
	function EvalFiles($df,$file)
	{
	
		Get-ChildItem -Path $df | Where-Object {$_.name -like '*' + $file + '*'} | out-host
		#Measure-Object | Select-Object -Property count
		#if (Get-ChildItem -Path $df | Where-Object {$_.name -match $file} | Measure-Object | Select-Object -Property count)
		#{return "true"}
		#else
		#{return "false"}
	}
	
		
	
	
	
	# --End User Generated Script--
	#----------------------------------------------
	#region Generated Events
	#----------------------------------------------
	
	$Form_StateCorrection_Load=
	{
		#Correct the initial state of the form to prevent the .Net maximized form issue
		$formMain.WindowState = $InitialFormWindowState
	}
	
	$Form_Cleanup_FormClosed=
	{
		#Remove all event handlers from the controls
		try
		{
			$datagridviewResults.remove_CellContentClick($datagridviewResults_CellContentClick)
			$datagridviewResults.remove_ColumnHeaderMouseClick($datagridviewResults_ColumnHeaderMouseClick)
			$buttonExit.remove_Click($buttonExit_Click)
			$buttonLoad.remove_Click($buttonLoad_Click)
			$formMain.remove_Load($FormEvent_Load)
			$formMain.remove_Load($Form_StateCorrection_Load)
			$formMain.remove_FormClosed($Form_Cleanup_FormClosed)
		}
		catch [Exception]
		{ }
	}
	#endregion Generated Events

	#----------------------------------------------
	#region Generated Form Code
	#----------------------------------------------
	#
	# formMain
	#
	$formMain.Controls.Add($datagridviewResults)
	$formMain.Controls.Add($buttonExit)
	$formMain.Controls.Add($buttonLoad)
	$formMain.ClientSize = '584, 362'
	$formMain.Name = "formMain"
	$formMain.StartPosition = 'CenterScreen'
	$formMain.Text = "Grid"
	$formMain.add_Load($FormEvent_Load)
	#
	# datagridviewResults
	#
	$datagridviewResults.AllowUserToAddRows = $False
	$datagridviewResults.AllowUserToDeleteRows = $False
	$datagridviewResults.Anchor = 'Top, Bottom, Left, Right'
	$System_Windows_Forms_DataGridViewCellStyle_1 = New-Object 'System.Windows.Forms.DataGridViewCellStyle'
	$System_Windows_Forms_DataGridViewCellStyle_1.Alignment = 'MiddleLeft'
	$System_Windows_Forms_DataGridViewCellStyle_1.BackColor = 'Control'
	$System_Windows_Forms_DataGridViewCellStyle_1.Font = "Microsoft Sans Serif, 8.25pt"
	$System_Windows_Forms_DataGridViewCellStyle_1.ForeColor = 'WindowText'
	$System_Windows_Forms_DataGridViewCellStyle_1.SelectionBackColor = 'Highlight'
	$System_Windows_Forms_DataGridViewCellStyle_1.SelectionForeColor = 'HighlightText'
	$System_Windows_Forms_DataGridViewCellStyle_1.WrapMode = 'True'
	$datagridviewResults.ColumnHeadersDefaultCellStyle = $System_Windows_Forms_DataGridViewCellStyle_1
	[void]$datagridviewResults.Columns.Add($checklist)
	[void]$datagridviewResults.Columns.Add($status)
	$System_Windows_Forms_DataGridViewCellStyle_2 = New-Object 'System.Windows.Forms.DataGridViewCellStyle'
	$System_Windows_Forms_DataGridViewCellStyle_2.Alignment = 'MiddleLeft'
	$System_Windows_Forms_DataGridViewCellStyle_2.BackColor = 'Window'
	$System_Windows_Forms_DataGridViewCellStyle_2.Font = "Microsoft Sans Serif, 8.25pt"
	$System_Windows_Forms_DataGridViewCellStyle_2.ForeColor = 'ControlText'
	$System_Windows_Forms_DataGridViewCellStyle_2.SelectionBackColor = 'Highlight'
	$System_Windows_Forms_DataGridViewCellStyle_2.SelectionForeColor = 'HighlightText'
	$System_Windows_Forms_DataGridViewCellStyle_2.WrapMode = 'False'
	$datagridviewResults.DefaultCellStyle = $System_Windows_Forms_DataGridViewCellStyle_2
	$datagridviewResults.Location = '12, 12'
	$datagridviewResults.Name = "datagridviewResults"
	$datagridviewResults.ReadOnly = $True
	$System_Windows_Forms_DataGridViewCellStyle_3 = New-Object 'System.Windows.Forms.DataGridViewCellStyle'
	$System_Windows_Forms_DataGridViewCellStyle_3.Alignment = 'MiddleLeft'
	$System_Windows_Forms_DataGridViewCellStyle_3.BackColor = 'Control'
	$System_Windows_Forms_DataGridViewCellStyle_3.Font = "Microsoft Sans Serif, 8.25pt"
	$System_Windows_Forms_DataGridViewCellStyle_3.ForeColor = 'WindowText'
	$System_Windows_Forms_DataGridViewCellStyle_3.SelectionBackColor = 'Highlight'
	$System_Windows_Forms_DataGridViewCellStyle_3.SelectionForeColor = 'HighlightText'
	$System_Windows_Forms_DataGridViewCellStyle_3.WrapMode = 'True'
	$datagridviewResults.RowHeadersDefaultCellStyle = $System_Windows_Forms_DataGridViewCellStyle_3
	$datagridviewResults.Size = '560, 309'
	$datagridviewResults.TabIndex = 2
	$datagridviewResults.add_CellContentClick($datagridviewResults_CellContentClick)
	$datagridviewResults.add_ColumnHeaderMouseClick($datagridviewResults_ColumnHeaderMouseClick)
	#
	# buttonExit
	#
	$buttonExit.Anchor = 'Bottom, Right'
	$buttonExit.Location = '497, 327'
	$buttonExit.Name = "buttonExit"
	$buttonExit.Size = '75, 23'
	$buttonExit.TabIndex = 1
	$buttonExit.Text = "E&xit"
	$buttonExit.UseVisualStyleBackColor = $True
	$buttonExit.add_Click($buttonExit_Click)
	#
	# buttonLoad
	#
	$buttonLoad.Anchor = 'Bottom, Left'
	$buttonLoad.Location = '12, 327'
	$buttonLoad.Name = "buttonLoad"
	$buttonLoad.Size = '75, 23'
	$buttonLoad.TabIndex = 0
	$buttonLoad.Text = "&Load"
	$buttonLoad.UseVisualStyleBackColor = $True
	$buttonLoad.add_Click($buttonLoad_Click)
	#
	# checklist
	#
	$checklist.HeaderText = "Checklist Item"
	$checklist.Name = "checklist"
	$checklist.ReadOnly = $True
	$checklist.Width = 300
	#
	# status
	#
	$status.HeaderText = "Status"
	$status.Name = "status"
	$status.ReadOnly = $True
	$status.Width = 200
	#endregion Generated Form Code

	#----------------------------------------------

	#Save the initial state of the form
	$InitialFormWindowState = $formMain.WindowState
	#Init the OnLoad event to correct the initial state of the form
	$formMain.add_Load($Form_StateCorrection_Load)
	#Clean up the control events
	$formMain.add_FormClosed($Form_Cleanup_FormClosed)
	#Show the Form
	return $formMain.ShowDialog()

} #End Function

#Call OnApplicationLoad to initialize
if((OnApplicationLoad) -eq $true)
{
	#Call the form
	Call-EvalToolGUI_pff | Out-Null
	#Perform cleanup
	OnApplicationExit
}
