#========================================================================
# Created with: SAPIEN Technologies, Inc., PowerShell Studio 2012 v3.1.26
# Created on:   12/10/2013 1:34 PM
# Created by:   bkaler
# Organization: 
# Filename:     
#========================================================================



$pcname = Read-Host "Provide a computer name:"

$tsenv = New-Object -ComObject Microsoft.SMS.TSEnvironment

Set-ItemProperty -InputObject $tsenv -name "OSDComputerName" -value $pcname

