﻿#are office apps running?#
$plist = Get-Process | ForEach-Object -Process {$_.processname}
$count = 0

ForEach-object -InputObject $plist -Process {
#is excel running#
if ($_ -eq "excel") {$count++}
#is word running#
if ($_ -eq "winword") {$count++}
#is outlook running#
if ($_ -eq "outlook") {$count++}
#is onenote running#
if ($_ -eq "onenote") {$count++}
#is project running#
if ($_ -eq "winproj") {$count++}
#is visio running#
if ($_ -eq "visio") {$count++}
#is publisher running#
if ($_ -eq "publisher") {$count++}
#is powerpoint running#
if ($_ -eq "powerpnt") {$count++}
#is access running#
if ($_ -eq "msaccess") {$count++}
}

Write-Host $count
