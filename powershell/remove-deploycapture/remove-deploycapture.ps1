﻿$a = (Get-ItemProperty "hkcu:\network\z" -erroraction silentlycontinue).remotepath
$b = Get-PSDrive Z -ErrorAction SilentlyContinue

If ($a -eq "\\uswvwds100\deploymentcapture$")
{
Remove-Item -path "hkcu:\network\z"
}

If ($b -ne $NULL) 
{
Get-PSDrive Z | Remove-PSDrive
}

#also clean up HKU hives
remove-psdrive hku -ErrorAction SilentlyContinue
#set up hives to search
$hives = new-psdrive -psprovider Registry -name hku -root hkey_users
#find all guids
set-location ($hives.name + ':')
$base = get-childitem
#get subkeys
set-variable -name userhives
foreach($b in $base){
    if($b.name -match '(.*S-1-5-21.*(?!_Classes))'){
            $a = Get-ChildItem ("hku:\" + $b.pschildname + "\Network") -recurse -ErrorAction SilentlyContinue | get-itemproperty -name RemotePath | where-object {$_.RemotePath -match ".*uswvwds100.*"}
            remove-item $a.pspath -force -recurse
            }

      }


set-location c: