class Dog():
	"""define a dog"""
	def __init__(self,name,age):
		"""intialize a name and age"""
		self.name = name
		self.age = age

	def sit(self):
		"""simulate a sit"""
		print(self.name.title() + " is now sitting")
	
	def roll_over(self):
		"""simulate rolling over"""
		print(self.name.title() + " rolled over")
	
mydog = Dog('sam',12)

