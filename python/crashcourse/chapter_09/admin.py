from users import User
from privileges import Privileges

class Admin(User):
	def __init__(self,first_name,last_name,phone,email):
		"""
		define an admin user
		"""
		super().__init__(first_name,last_name,phone,email)
		self.privileges = Privileges()
	

			

			
myadmin = Admin('Brian','Kaler','888-8888','brian.kaler@gmail.com')

myadmin.describe_user()

myadmin.privileges.show_privileges()
