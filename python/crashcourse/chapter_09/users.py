class User():
	def __init__(self,first_name,last_name,phone,email):
		"""
		define a user
		"""
		self.first_name = first_name
		self.last_name = last_name
		self.phone = phone
		self.email = email
	
	def describe_user(self):
		"""
		print out the details of a user
		"""
		desc = 'User Details:' + '\n' + self.first_name + '\n' + self.last_name + '\n' + self.phone + '\n' + self.email
		print(desc)
		
	def greet_user(self):
		"""
		greet the user
		"""
		print("Hello " + self.first_name)
	
