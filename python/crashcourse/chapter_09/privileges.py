class Privileges():
	def __init__(self):
		"""
		Initialize properties of privileges
		"""
		self.privileges = ['can add post','can delete post','can ban user']
	
	def show_privileges(self):
		"""
		Show privileges available
		"""
		print('User Privileges:' + '\n ---------------')
		for p in self.privileges:
			print("- " + p)