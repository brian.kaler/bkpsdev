﻿#reimage script
#shut down host
vboxmanage controlvm "Win32-bittester1" poweroff
#sleep 2 secs
ping 127.0.0.1 -n 2 -w 1000 > NUL
#modify boot order
vboxmanage modifyvm "Win32-bittester1" --boot1 net
#sleep 2 secs
ping 127.0.0.1 -n 2 -w 1000 > NUL
#poweron host
vboxmanage controlvm "Win32-bittester1" poweron
#reset boot order
vboxmanage modifyvm "Win32-bittester1" --boot1 disk
