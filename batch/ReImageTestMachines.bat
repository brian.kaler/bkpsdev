﻿REM Host1 32-bit
REM shut down host
vboxmanage controlvm "Win32-bittester1" poweroff
REM sleep 2 secs
ping 127.0.0.1 -n 2 -w 1000 > NUL
REM modify boot order
vboxmanage modifyvm "Win32-bittester1" --boot1 net
REM sleep 2 secs
ping 127.0.0.1 -n 2 -w 1000 > NUL
REM poweron host
vboxmanage startvm "Win32-bittester1"

REM Host2 32-bit
REM shut down host
vboxmanage controlvm "Win32-bittester2" poweroff
REM sleep 2 secs
ping 127.0.0.1 -n 2 -w 1000 > NUL
REM modify boot order
vboxmanage modifyvm "Win32-bittester2" --boot1 net
REM sleep 2 secs
ping 127.0.0.1 -n 2 -w 1000 > NUL
REM poweron host
vboxmanage startvm "Win32-bittester2"

REM Host1 62-bit
REM shut down host
vboxmanage controlvm "Win64-bittester1" poweroff
REM sleep 2 secs
ping 127.0.0.1 -n 2 -w 1000 > NUL
REM modify boot order
vboxmanage modifyvm "Win64-bittester1" --boot1 net
REM sleep 2 secs
ping 127.0.0.1 -n 2 -w 1000 > NUL
REM poweron host
vboxmanage startvm "Win64-bittester1"

REM Host1 62-bit
REM shut down host
vboxmanage controlvm "Win64-bittester2" poweroff
REM sleep 2 secs
ping 127.0.0.1 -n 2 -w 1000 > NUL
REM modify boot order
vboxmanage modifyvm "Win64-bittester2" --boot1 net
REM sleep 2 secs
ping 127.0.0.1 -n 2 -w 1000 > NUL
REM poweron host
vboxmanage startvm "Win64-bittester2"

REM reset boot order (can't be done while running)
REM boxmanage modifyvm "Win32-bittester1" --boot1 disk