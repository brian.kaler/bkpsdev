'==========================================================================
' NAME:     <CHD.vbs>
' AUTHOR:   Daniel D. Jones
' CREATED:  1:40 PM 3/8/2012
' COMMENT:  Changes all the users of a file list or OUs Home Directory
'==========================================================================

Option Explicit
'On Error Resume Next

' Checks for appropriate amount of arguments
If WScript.Arguments.Count <> 3 Then
   WScript.Echo "Invalid arguments"
   WScript.Quit
End If

'---------------------------------------------------------
' Function to create an empty log file
'---------------------------------------------------------
Function NewLog(FileName)
   Dim objFSO,objList
   Const ForWriting = 2
   Set objFSO = CreateObject("Scripting.FileSystemObject")
   Set objList = objFSO.OpenTextFile(FileName,ForWriting,True)
   objList.Close
End Function

'---------------------------------------------------------
' Function to write string to file, one line
'---------------------------------------------------------
Function WriteLog(FileName, Line)
   Dim objFSO,objList
   Const ForAppending = 8
   Set objFSO = CreateObject("Scripting.FileSystemObject")
   Set objList = objFSO.OpenTextFile(FileName,ForAppending,True)
   objList.WriteLine Line
   objList.Close
End Function

'---------------------------------------------------------
' Function to return an array of users from a text file.
' Reads each line of text file as a user.
'---------------------------------------------------------
Function ReadUserList(FileName)
   Dim objFSO, objTextFile
   Dim arrUsers()
   Dim count
   Const ForReading = 1
   count = 0
   Set objFSO = CreateObject("Scripting.FileSystemObject")
   Set objTextFile = objFSO.OpenTextFile(FileName, ForReading)
   Do Until objTextFile.AtEndOfStream
      ReDim Preserve arrUsers(count)
      arrUsers(count) = objTextFile.Readline
      count = count + 1
   Loop
   ReadUserList = arrUsers
End Function

'---------------------------------------------------------
' Function to return an array of users in an OU
'---------------------------------------------------------
Function ListUsersInOU(OU)
   Dim qQuery, strFilter, strLDAPDomain, strFields
   Dim objConnection, objCommand, objRecordSet
   Dim intSize
   Dim arrUserDNs()
   intSize = 0
   ' Setup AD query to return user info
   strFilter = "(&(objectCategory=person) (objectClass=user))"
   ' AD String to contain AD domain info
   strLDAPDomain = "<LDAP://" & OU & ",dc=us,dc=pinkertons,dc=com>"
   ' AD Fields to return
   strFields = "givenName,sn,DistinguishedName"
   'qQuery = strLDAPDomain & ";" & strFilter & ";" & strFields & ";subtree" ' Search returns all objects below container
   qQuery = strLDAPDomain & ";" & strFilter & ";" & strFields & ";onelevel" ' Search returns only objects below the container
   Set objConnection = CreateObject("ADODB.Connection")
   Set objCommand = CreateObject("ADODB.Command")
   objConnection.Open "Provider=ADsDSOObject;"
   objCommand.ActiveConnection = objConnection
   objCommand.CommandText = qQuery
   Set objRecordSet = objCommand.Execute
   While Not objRecordSet.EOF
      ReDim Preserve arrUserDNs(intSize)
      arrUserDNs(intSize) = objRecordSet.Fields("DistinguishedName") & "|" & objRecordSet.Fields("givenName") & "|" & objRecordSet.Fields("sn")
      intSize = intSize + 1
      objRecordSet.MoveNext
   Wend
   objConnection.Close
   ListUsersInOU = arrUserDNs
End Function

'---------------------------------------------------------
' Function to return DN if user exists, else return
' "NOTFOUND"
'---------------------------------------------------------
Function SearchUser(First,Last)
   Dim strFilter, strLDAPDomain, strFields
   Dim oQuery, objConnection, objCommand, objRecordSet
   Dim intRecords
   ' Setup AD query to return user info
   strFilter = "(&(objectCategory=person) (objectClass=user) (givenName=" & First & ") (sn=" & Last & "))"
   ' AD String to contain AD domain info
   strLDAPDomain = "<LDAP://dc=us,dc=pinkertons,dc=com>"
   ' AD Fields to return
   strFields = "givenName,sn,DistinguishedName"
   oQuery = strLDAPDomain & ";" & strFilter & ";" & strFields & ";subtree"
   Set objConnection = CreateObject("ADODB.Connection")
   Set objCommand = CreateObject("ADODB.Command")
   objConnection.Open "Provider=ADsDSOObject;"
   ' Set AD query options
   objCommand.ActiveConnection = objConnection
   objCommand.CommandText = oQuery
   objCommand.Properties("Page Size") = 100
   objCommand.Properties("Timeout") = 30
   objCommand.Properties("Cache Results") = False
   ' Execute query
   Set objRecordSet = objCommand.Execute
   intRecords = objRecordSet.RecordCount
   objRecordSet.Requery
   If intRecords = 1 Then
      SearchUser = objRecordSet.Fields("DistinguishedName")
   Else
      SearchUser = "NOTFOUND"
   End If
   objConnection.Close
End Function

'---------------------------------------------------------
' Function to return current login script path
'---------------------------------------------------------
Function GetHomeDirectory(DN)
   Dim objUser
   Set objUser = GetObject("LDAP://" & DN)
   GetHomeDirectory = LCase(objUser.homeDirectory)
End Function

'---------------------------------------------------------
' Function to set login script path
'---------------------------------------------------------
Function SetHomeDirectory(DN,HomePath)
   Dim objUser
   Const ADS_PROPERTY_CLEAR = 1
   Set objUser = GetObject("LDAP://" & DN)
   objUser.Put "homeDirectory", HomePath
   objUser.SetInfo
End Function

'---------------------------------------------------------
' Function to return new Home Directory path
'---------------------------------------------------------
Function ReplaceHomeDir(CurrentHomeDir,OldServ,NewServ)
   Dim strNewHomeDir
   strNewHomeDir = Replace(CurrentHomeDir,OldServ,NewServ)
   ReplaceHomeDir = strNewHomeDir
End Function

'---------------------------------------------------------
' MAIN PROGRAM
'---------------------------------------------------------
Dim strOU, strDN, strCurrentHomeDir, strHowTo, strWhere, strServer
Dim arrUsers, arrUser
Dim colArgs
Dim strUser, strLog
Dim objUser
Dim strNewServer
Dim strNewHomeDir

' WOC F: Drive NetApp Filer
'strNewServer = "napwsys01\usr_home01"

' EOC F: Drive NetApp Filer
strNewServer = "napwcfs01"

' assign command line arguments to variables for readability
Set colArgs = WScript.Arguments.Named
strHowTo = colArgs("howto")
strWhere = colArgs("where")
strServer = colArgs("server")

strLog = "HomeDirLog.txt"

' Create new log file
NewLog(strLog)

Select Case strHowTo
   Case "list"
      arrUsers = ReadUserList(strWhere)
      For Each strUser In arrUsers
         arrUser = Split(strUser,",")
         strDN = SearchUser(arrUser(0),arrUser(1))
		 WriteLog strLog, "Search Result for user:" & arrUser(0) & " " & arrUser(1) & " was: " & strDN
         If strDN <> "NOTFOUND" Then
            strCurrentHomeDir = GetHomeDirectory(strDN)
			strNewHomeDir = ReplaceHomeDir(strCurrentHomeDir,strServer,strNewServer)
            If strCurrentHomeDir <> strNewHomeDir Then
               SetHomeDirectory strDN, strNewHomeDir
               WriteLog strLog, Chr(34) & arrUser(0) & " " & arrUser(1) & Chr(34) & "," & Chr(34) & "Old Home Dir: " & strCurrentHomeDir & Chr(34)
               WriteLog strLog, Chr(34) & arrUser(0) & " " & arrUser(1) & Chr(34) & "," & Chr(34) & "New Home Dir: " & strNewHomeDir & Chr(34)
            Else
               WriteLog strLog, Chr(34) & arrUser(0) & " " & arrUser(1) & Chr(34) & "," & Chr(34) & "NONEED" & Chr(34)
            End If
         Else
            WriteLog strLog, Chr(34) & arrUser(0) & " " & arrUser(1) & Chr(34) & "," & Chr(34) & "SKIPPED" & Chr(34)
         End If
      Next
   Case "ou"
      arrUsers = ListUsersInOU(strWhere)
      For Each strUser In arrUsers
         arrUser = Split(strUser,"|")
         strCurrentHomeDir = GetHomeDirectory(arrUser(0))
         strNewHomeDir = ReplaceHomeDir(strCurrentHomeDir,strServer,strNewServer)
         If strCurrentHomeDir <> strHomeDir Then
            SetHomeDirectory arrUser(0), strHomeDir
            WriteLog strLog, Chr(34) & arrUser(1) & " " & arrUser(2) & Chr(34) & "," & Chr(34) & "Old Home Dir: " & strCurrentHomeDir & Chr(34)
            WriteLog strLog, Chr(34) & arrUser(1) & " " & arrUser(2) & Chr(34) & "," & Chr(34) & "New Home Dir: " & strNewHomeDir & Chr(34)
         Else
            WriteLog strLog, Chr(34) & arrUser(1) & " " & arrUser(2) & Chr(34) & "," & Chr(34) & "NONEED" & Chr(34)
         End If
      Next
End Select
