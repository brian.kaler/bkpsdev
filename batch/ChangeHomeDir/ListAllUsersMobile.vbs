'==========================================================================
' NAME:     <ListAllUsers.vbs>
' AUTHOR:   Daniel D. Jones
' CREATED:  11/16/2005 11:29:35 AM
' UPDATED:  1/26/2006 8:46:58 AM
' COMMENT:  List All users in AD and write to CSV. Also verify User object
'           User ID meets User ID Policy and will flag accordingly.
'==========================================================================
Option Explicit
'On Error Resume Next

'---------------------------------------------------------
' Function to List All User accounts and write to CSV
'---------------------------------------------------------
Function ListAllUsers(FileName)
   Dim objQuery, objConnection, objCommand, objRecordSet, objFSO, objFile
   Dim strComputer, strLDAPDomain, strFilter, strFields, strUserInfo, strMobile
   Dim intRecords
   Dim intUAC
   Dim colArgs
   Dim tmpArr
   Dim strAccountType, strAccountStatus
   Dim strUserIDValid
   Dim intCount
   Dim intMobile

	   intCount = 0
	   intMobile = 0

   ' Constant for Account Disabled value
   Const ADS_UF_ACCOUNTDISABLE = 2

   Set objFSO = CreateObject("Scripting.FileSystemObject")
   Set objFile = objFSO.OpenTextFile(FileName, 2, True, 0)

   ' Setup AD query to return user info
   strFilter = "(&(objectCategory=person) (objectClass=user) (objectCategory=Person))"

   ' AD Strings to contain AD domain and user info
   strLDAPDomain = "<LDAP://dc=us,dc=pinkertons,dc=com>"
   strFields = "distinguishedName,sAMAccountName,cn,givenName,sn,department,title,telephoneNumber,streetAddress,l,st,postalCode,userAccountControl,mobile"
   objQuery = strLDAPDomain & ";" & strFilter & ";" & strFields & ";subtree"
   Set objConnection = CreateObject("ADODB.Connection")
   Set objCommand = CreateObject("ADODB.Command")
   objConnection.Open "Provider=ADsDSOObject;"

   ' Set AD query options
   objCommand.ActiveConnection = objConnection
   objCommand.CommandText = objQuery
   objCommand.Properties("Page Size") = 100
   objCommand.Properties("Timeout") = 30
   objCommand.Properties("Cache Results") = False

   ' Execute query
   Set objRecordSet = objCommand.Execute
   intRecords = objRecordSet.RecordCount
   objRecordSet.Requery

   WScript.Echo "==============================================="
   Do Until objRecordSet.EOF
      WScript.Echo "   Processessing: " & objRecordSet.Fields("sAMAccountName")

      intUAC = objRecordset.Fields("userAccountControl")
      If intUAC And ADS_UF_ACCOUNTDISABLE Then
         strAccountStatus = "DISABLED"
      Else
         strAccountStatus = "ENABLED"
      End If

	If Len(objRecordSet.Fields("mobile")) > 1 Then
		strMobile = "Present"
	Else
		strMobile = "None"
	End If
	  
      If InStr(objRecordSet.Fields("distinguishedName"),"Conference Room") > 0 Then
         strAccountType = "CONFERENCE ROOM"
      ElseIf InStr(objRecordSet.Fields("distinguishedName"),"Special Purpose Account") > 0 Then
         strAccountType = "SPECIAL PURPOSE ACCOUNT"
      ElseIf (InStr(objRecordSet.Fields("distinguishedName"),"Service Account") > 0) Or (InStr(objRecordSet.Fields("distinguishedName"),"IUSR_") > 0) Or (InStr(objRecordSet.Fields("distinguishedName"),"IWAM_") > 0) Then
         strAccountType = "SERVICE ACCOUNT"
      ElseIf InStr(objRecordSet.Fields("distinguishedName"),"Training User") > 0 Then
         strAccountType = "TRAINING ACCOUNT"
      ElseIf InStr(objRecordSet.Fields("distinguishedName"),"SystemMailbox") > 0 Then
         strAccountType = "SYSTEM MAILBOX"
      ElseIf (InStr(objRecordSet.Fields("distinguishedName"),"Security") > 0) Or (InStr(objRecordSet.Fields("distinguishedName"),"Dispatch") > 0) Then
         strAccountType = "SHARED FIELD ACCOUNT"
      Else
         strAccountType = "USER"
      End If


      If strAccountStatus = "ENABLED" And strAccountType = "USER" And strMobile = "Present" Then
         intCount = intCount + 1
         If Not IsNull(objRecordSet.Fields("sn")) Then
            Dim intLenSN
            Dim strMidUID
            Dim str7CharSN
            Dim intOver
            Dim strLastName

            strLastName = Replace(objRecordSet.Fields("sn")," ","")
            strLastName = Replace(strLastName,"'","")
            strLastName = Replace(strLastName,"-","")
            'strLastName = objRecordSet.Fields("sn")

            intLenSN = Len(strLastName)
            If intLenSN > 7 Then
               intLenSN = 7
            End If

            If intLenSN < 7 Then
               strMidUID = Mid(objRecordSet.Fields("sAMAccountName"),2,intLenSN)
            Else
               strMidUID = Mid(objRecordSet.Fields("sAMAccountName"),2,7)
            End If

            str7CharSN = Left(strLastName,intLenSN)

            If UCase(Left(objRecordSet.Fields("givenName"),1)) = UCase(Left(objRecordSet.Fields("sAMAccountName"),1)) Then
               If Len(objRecordSet.Fields("sAMAccountName")) <= 8 Then
                  If LCase(strMidUID) = LCase(str7CharSN) Then
                     intOver = Len(objRecordSet.Fields("sAMAccountName")) - (intLenSN + 1)
                     If intOver = 0 Then
                        strUserIDValid = "VALID"
                     ElseIf (intOver = 1) And (IsNumeric(Right(objRecordSet.Fields("sAMAccountName"),1))) Then
                        strUserIDValid = "VALID"
                     ElseIf (intOver = 2) And (IsNumeric(Right(objRecordSet.Fields("sAMAccountName"),2))) And (IsNumeric(Right(objRecordSet.Fields("sAMAccountName"),1))) Then
                        strUserIDValid = "VALID"
                     Else
                        strUserIDValid = "INVALID"
                     End If
                  Else
                     strUserIDValid = "INVALID"
                  End If
               ElseIf Len(objRecordSet.Fields("sAMAccountName")) = 9 Then
                  If (LCase(strMidUID) = LCase(str7CharSN)) And (IsNumeric(Right(objRecordSet.Fields("sAMAccountName"),1))) Then
                     strUserIDValid = "VALID"
                  Else
                     strUserIDValid = "INVALID"
                  End If
               ElseIf Len(objRecordSet.Fields("sAMAccountName")) = 10 Then
                  If (LCase(strMidUID) = LCase(str7CharSN)) And (IsNumeric(Right(objRecordSet.Fields("sAMAccountName"),2)))  And (IsNumeric(Right(objRecordSet.Fields("sAMAccountName"),1))) Then
                     strUserIDValid = "VALID"
                  Else
                     strUserIDValid = "INVALID"
                  End If
               Else
                  strUserIDValid = "INVALID"
               End If
            Else
               strUserIDValid = "INVALID"
            End If
         Else
            strUserIDValid = "INVALID"
         End If
      Else
         strUserIDValid = "NA"
      End If
	  If strMobile = "Present" Then
      strUserInfo = Chr(34) & objRecordSet.Fields("cn") & Chr(34) & Chr(44) & Chr(34) & objRecordSet.Fields("sAMAccountName") & Chr(34) & Chr(44) & Chr(34) & objRecordSet.Fields("mobile") & Chr(34) & Chr(44) & Chr(34) & objRecordSet.Fields("givenName") & Chr(34) & Chr(44) & Chr(34) & objRecordSet.Fields("sn") & Chr(34) & Chr(44) & Chr(34) & objRecordSet.Fields("title") & Chr(34) & Chr(44) & Chr(34) & objRecordSet.Fields("department") & Chr(34) & Chr(44) & Chr(34) & objRecordSet.Fields("telephoneNumber") & Chr(34) & Chr(44) & Chr(34) & objRecordSet.Fields("streetAddress") & Chr(34) & Chr(44) & Chr(34) & objRecordSet.Fields("l") & Chr(34) & Chr(44) & Chr(34) & objRecordSet.Fields("st") & Chr(34) & Chr(44) & Chr(34) & objRecordSet.Fields("postalCode") & Chr(34) & Chr(44) & Chr(34) & strAccountType & Chr(34) & Chr(44) & Chr(34) & strAccountStatus & Chr(34) & Chr(44) & Chr(34) & strUserIDValid & Chr(34) & Chr(44) & Chr(34) & objRecordSet.Fields("distinguishedName") & Chr(34)
      objFile.WriteLine strUserInfo
	  intMobile = intMobile + 1
	End If
      objRecordSet.MoveNext
   Loop
   
   objFile.Close
   objConnection.Close
   WScript.Echo "==============================================="
   WScript.Echo " PROCESSESSED " & intMobile & " USERS"
End Function

Dim strFile
strFile = "AllUsers.csv"

ListAllUsers strFile
