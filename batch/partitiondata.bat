@ECHO OFF
:: WMI query to list all properties and values of the \\root\cimv2:Win32_DiskPartition class.
:: This batch file was generated using the WMI Code Generator, Version 9.01
:: http://www.robvanderwoude.com/wmigen.php

IF "%~1"=="" (
	SET Node=%ComputerName%
) ELSE (
	SET Node=%~1
)

FOR /F %%A IN ('WMIC.EXE /Node:"%Node%" Path Win32_DiskPartition Get /Format:CSV ^| MORE /E +2 ^| FIND /C ","') DO (
	IF %%A EQU 1 (
		ECHO 1 instance:
	) ELSE (
		ECHO %%A instances:
	)
)

WMIC.EXE /Node:"%Node%" Path Win32_DiskPartition Get /Format:LIST >> Partitiondata.txt
