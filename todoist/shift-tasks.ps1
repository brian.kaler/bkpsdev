﻿#import modules
import-module todoist

#variables
$project = "Workouts"
$taskbase = "Task1"
$daystoshift = 1
$today = get-date


#set up the api
Set-TodoistAPIToken -apitoken "53378474a59cef5eccf8ea5b8495cefe9cd262b1"

#a short test below, it worked!
#New-TodoistTask -TaskContent "whatever!" -ProjectName $project -Verbose

#get all the projects
$allprojects = Get-TodoistResource -Resources projects | select projects
#if todoist returns nothing (API limit) exit
if($allprojects.projects -ne $null)
{
$projectid = $allprojects.projects | ?{$_.name -match $project} | select -ExpandProperty Id
$targettasks = Get-TodoistTask | where{$_.project_id -match $projectid}
}
else
{
Write-host "Project list returned nothing, Script halted"
exit
}

#for each task in the project, move it's due date up one.
foreach($task in $targettasks)
    {
    write-host "Now processing $($task.content)"
    $duedate = [datetime]$task.due_date_utc
    $newdate = $duedate.AddDays($daystoshift) | Get-Date -Format yyyy-M-ddThh:mm | out-string
    Update-TodoistTask -taskid $task.id -DueDate $newdate -Verbose
    $updatenote =  "Updated by Shift-Tasks on  $(Get-Date | Out-String)"
    add-TodoistNote -taskid $task.id -Content $updatenote
    }