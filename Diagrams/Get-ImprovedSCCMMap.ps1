﻿#collect some site 

# Check modules
If(-not(Get-Module -ListAvailable -Name $PSWriteHTMLModuleName)){

        Write-Error -Message "`nCould not find module $PSWriteHTMLModuleName. `nThis module needs to be installed before running this script."
        return
    
} else {
    Import-Module -Name $PSWriteHTMLModuleName
}

#$boundaries = Get-CMBoundary
$boundarygroups = Get-CMBoundaryGroup
#$components = Get-CMSiteComponent
#$site = Get-CMSite
#$distributionpoints = Get-CMDistributionPoint

$boundaryarray = @()
foreach($b in $boundarygroups){
    $boundaryprops = Get-CMBoundary -BoundaryGroupId $b.groupid
    $objlist = New-Object -TypeName PSObject
    $objlist | Add-Member -MemberType NoteProperty -Name BoundaryGroup -value $b.Name
    $objlist | Add-Member -MemberType NoteProperty -Name BoundaryName -value $boundaryprops.DisplayName
    $objlist | Add-Member -MemberType NoteProperty -Name BoundaryValue -value $boundaryprops.value
    $boundaryarray += $objlist
    
}

$HTML = "C:\temp\myimprovedmap.html"


    New-HTML -TitleText 'SCCM Infra Diagram' -UseCssLinks:$false -UseJavaScriptLinks:$false -FilePath $HTML {
        New-HTMLSection -HeaderText $HeaderText -CanCollapse {
            New-HTMLPanel {
                New-HTMLDiagram {
                    New-DiagramOptionsPhysics -Enabled $true
                    New-DiagramOptionsInteraction -Hover $true
                    New-DiagramOptionsLinks -Length '300'
                    ForEach ($b in $boundaryarray){
                        $basenode = New-DiagramNode -Label $b.boundarygroup -IconRegular building -IconColor green
                        $labels = $b.boundaryname
                        foreach($node in $boundaryarray){
                            
                            New-DiagramNode -Label $node.boundaryname -to $basenode -IconRegular bookmark -IconColor red
                        }
                    }
                   
                } -BackgroundSize '1000px' -Height '1000px' -Width '1000px'
            }
        }
    } -ShowHTML
 

