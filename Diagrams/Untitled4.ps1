﻿#collect some site 

# Check modules
If(-not(Get-Module -ListAvailable -Name $PSWriteHTMLModuleName)){

        Write-Error -Message "`nCould not find module $PSWriteHTMLModuleName. `nThis module needs to be installed before running this script."
        return
    
} else {
    Import-Module -Name $PSWriteHTMLModuleName
}

#$boundaries = Get-CMBoundary
$boundarygroups = Get-CMBoundaryGroup
#$components = Get-CMSiteComponent
#$site = Get-CMSite
#$distributionpoints = Get-CMDistributionPoint

$boundaryarray = @()
foreach($b in $boundarygroups){
    $boundaryprops = Get-CMBoundary -BoundaryGroupId $b.groupid
    $objlist = New-Object -TypeName PSObject
    $objlist | Add-Member -MemberType NoteProperty -Name BoundaryGroup -value $b.Name
    $objlist | Add-Member -MemberType NoteProperty -Name BoundaryName -value $boundaryprops.DisplayName
    $objlist | Add-Member -MemberType NoteProperty -Name BoundaryValue -value $boundaryprops.value
    $boundaryarray += $objlist
    
}

$HTML = "C:\temp\myimprovedmap.html"


    New-HTML -TitleText 'SCCM Infra Diagram' -UseCssLinks:$false -UseJavaScriptLinks:$false -FilePath $HTML {
        New-HTMLSection -HeaderText $HeaderText -CanCollapse {
            New-HTMLPanel {
                New-HTMLDiagram {
                    New-DiagramOptionsInteraction -Hover $true
                    ForEach ($b in $boundaryarray){
                        $boundaryips = $b.boundaryvalue
                        foreach($node in $boundaryips){
                            New-DiagramNode -Label $node -to $b.boundarygroup -shape box
                        }
                        New-DiagramNode -Label $b.boundarygroup -IconRegular building -IconColor green -id $b.boundarygroup -fontsize 52 -X 300 -Y 300 -HeightConstraintVAlign middle -HeightConstraintMinimum 100
                    }
                   
                } -BackgroundSize '2000px' -Height '1000px' -Width '1000px'
            }
        }
    } -ShowHTML
 

