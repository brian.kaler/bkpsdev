﻿$sqlservers = get-content "C:\temp\sqlservers.txt"

foreach($s in $sqlservers){
If(Test-Connection $s -quiet -count 1){

$serverpath = "\\"+$s+"\c$"
$item = Get-ChildItem -path $serverpath -Filter {*ERRORLOG*} -Recurse -ErrorAction SilentlyContinue | select -first 1

$contents = get-content $item.fullname -TotalCount 1

write-host "$s : $contents"
}
Else
{
write-host "Failed to contact server: $s"
}

}