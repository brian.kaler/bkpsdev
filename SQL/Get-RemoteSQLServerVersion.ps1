﻿$sqlservers = get-content "C:\temp\sqlservers.txt"

foreach($s in $sqlservers){
If(Test-Connection $s -quiet -count 1){


#where does sql live
$mssqldir = Invoke-command -computername $s -scriptblock {((((Get-item -ErrorAction SilentlyContinue ((Get-childitem -path "C:\Program Files" -recurse -ErrorAction SilentlyContinue | Where {$_.name -eq "sqlservr.exe"}).DirectoryName)).Parent).Parent).Parent).FullName}

#get the contents of the errorlog file, first line.
$contents = Invoke-command -computername $s -scriptblock {param($mssqldir)(Get-ChildItem $mssqldir -Filter {*ERRORLOG*} -Recurse -verbose -ErrorAction SilentlyContinue | select -first 1 | get-content -totalcount 1)} -ArgumentList $mssqldir

#determine output based on results.
IF($contents -eq $null){
write-host "Output unavailable from server: $s"
}
else
{
write-host "Output from server $s : $contents"
}

}
Else
{
write-host "Failed to contact server: $s"
}

}