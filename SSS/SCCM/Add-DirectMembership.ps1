﻿#add-directmembership
Function Add-DirectMembership{
Param(
[String]$dmcollection,
[String]$dmdevicename
)

$mydev = Get-CMDevice -Name $dmdevicename
if($mydev -eq $null)
{write-host "Device $s was not found in SCCM"}
Else
{
$device = $mydev.name
Write-Host "Attempting to add $dmdevicename to $dmcollection"
Add-CMDeviceCollectionDirectMembershipRule -CollectionName $dmcollection -Resource $mydev
}
}

