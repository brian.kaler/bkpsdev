#add-userdirectmembership
#brian kaler
#4/22/2019
#adds a user to a SCCM collection through direct membership

Function Add-UserDirectMembership{
Param(
[String]$dmusercollection,
[String]$dmusername
)

$myuser = Get-CMUser -Name $dmusername
if($myuser -eq $null)
{write-host "Device $s was not found in SCCM"}
Else
{
$user = $myuser.resourceid
Write-Host "Attempting to add $dmusername to $dmusercollection"
Add-CMUserCollectionDirectMembershipRule -CollectionName $dmusercollection -ResourceID $user
}
}

