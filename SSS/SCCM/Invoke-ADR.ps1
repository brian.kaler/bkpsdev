﻿#Invoke-ADR
#Written by Brian Kaler
#2/22/2019
#Modifies and Executes ADR creating both SUG and Package
#Must be run on SCCM Server or through a workstation with the SCCM module installed

#Parameters
Param(
[Parameter(Mandatory)]
[String]$appname,
[Parameter(Mandatory)]
[String]$ADRRoot,
[Parameter(Mandatory)]
[String]$ADRName,
[Parameter(Mandatory)]
[String]$adrrulename,
[Parameter(Mandatory)]
[String]$adrruledesc,
[Parameter(Mandatory)]
[String]$adrpackagename,
[Parameter(Mandatory)]
[String]$adrpackagedesc,
[Parameter(Mandatory)]
[String]$deploymentdesc,
[Parameter(Mandatory)]
[String[]]$distributionpts,
[Parameter(Mandatory)]
[bool]$deploy
)

#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1"  
# Set the current location to be the site code. 
Set-Location $SiteCode":\"  
#set up logfile
$global:pslogfile = $ENV:TEMP+"\"+$appname+".log"

function Set-ADRDeploymentPackage
{
    [CMdletbinding()]
    Param
    (
        [Parameter(Mandatory = $true)]
        [wmi]$ADRObject,
        [Parameter(Mandatory = $true)]
        [string]$PackageID
        
    )
    try {
        [xml]$ContentTemplateXML = $ADRObject.ContentTemplate
        $ContentTemplateXML.ContentActionXML.PackageID = $PackageID
        $ADRObject.ContentTemplate = $ContentTemplateXML.OuterXml
        $ADRObject.Put() | Out-Null
        Write-Output "Succesfully commited updated PackageID" >> $global:pslogfile
    }
    catch {
        throw "Something went wrong setting the value" >> $global:pslogfile
    }
}

function Get-ADRInfo
{
    [CMdletbinding()]
    Param
    (
        [Parameter(Mandatory = $true)]
        [string]$ADRName,
        [Parameter(Mandatory = $true)]
        [string]$SiteCode,
        [Parameter(Mandatory = $true)]
        [string]$SiteServer
    )
    try 
    {
        $Namespace = "root/sms/site_" + $siteCode
        [wmi]$ADR = (Get-WmiObject -computername $siteserver -Class SMS_AutoDeployment -Namespace $Namespace | Where-Object -FilterScript {$_.Name -eq $ADRName}).__PATH
        return $ADR 
    }
    catch 
    {
        throw 'Failed to Get ADRInfo' >> $global:pslogfile
    }
}

function CreateFoldersbyDate(){
Param(
[Parameter(Mandatory=$true)]
[DateTime]$Date,
[Parameter(Mandatory=$true)]
[String]$path
)
$FolderYear = ($date | Get-Date -uformat "%Y")
$FolderMonth = ($date | Get-Date -UFormat "%m")
$FolderDay = ($date | Get-date -uformat "%d")
#create folder structure
$fyear = New-Item -ItemType Directory -name $folderyear -path filesystem::$path -Force -ErrorAction SilentlyContinue
$fmonth = New-Item -ItemType Directory -name $foldermonth -path filesystem::$path\$folderyear -Force -ErrorAction SilentlyContinue
$fday = New-Item -ItemType Directory -name $FolderDay -path filesystem::$path\$folderyear\$foldermonth  -Force -ErrorAction SilentlyContinue
return $fday
}

Function IsPatchDownloadTime(){
#set up some of our needed dates
$today = (Get-Date)
$maxtuesday = 13
$mintuesday = 7
$firstday = [DateTime]::new($today.year, $today.month, 1)
$cycle = $firstday

#Find the first tuesday
$i = 0
Do{
    $i = 1
    $cycle = $cycle.adddays($i)
    $firsttuesday = $cycle.day
    }While($cycle.dayofweek -ne "Tuesday")
#find the second tuesday
Do{
    $i = 1
    $cycle = $cycle.adddays($i)
    $secondtuesday = $cycle.day
    }While($cycle.dayofweek -ne "Tuesday")
#find the thirdtuesday
Do{
    $i = 1
    $cycle = $cycle.adddays($i)
    $thirdtuesday = $cycle.day
    }While($cycle.dayofweek -ne "Tuesday")


#If the second tuesday has passed and the third tuesday has not yet arrived, it's patch downloading time
if(($today.day -ge $secondtuesday) -and ($today.day -lt $thirdtuesday)){
Return $true
}
else
{
Return $false
}
}

#If((IsPatchDownloadTime) -eq $false){
#Exit
#}

#init our logfile
If((Test-Path $global:pslogfile) -eq $false){
New-Item -Path $global:pslogfile
}

#create our folder structure for deployment package
$timestamp = get-date
$packagepath = Createfoldersbydate -date $timestamp -path $adrroot

#set up our package
$pkgobj = New-CMSoftwareUpdateDeploymentPackage -Name $adrpackagename -Description $adrpackagedesc -Path $packagepath.FullName

#add the package to our existing ADR
$adrobj = Get-ADRInfo -ADRName $adrname -SiteCode $sitecode -SiteServer $SiteServer
Set-ADRDeploymentPackage -ADRObject $adrobj -PackageID $pkgobj.packageid

#run our ADR 
Invoke-CMSoftwareUpdateAutoDeploymentRule -Name $ADRObj.name -verbose

#fetch our SUG
$sugname = $ADRname 

#Were the sugs created by the ADR recently otherwise clean up the package and exit.
$i = 0
Do{
        $i++
        Write-Output "WARNING: No SUG created yet, retry attempt $i of 5"  >> $global:pslogfile
        sleep -seconds 180
        $latestsug = Get-CMSoftwareUpdateGroup | where {($_.localizeddisplayname -match $sugname)} | Sort-Object DateCreated -Descending | select -first 1
        if($latestsug.DateCreated -ge (Get-date).addminutes(-15)){
        Break
        }
} While ($i -le 5)
if($i -eq 5){
    Write-Output "ERROR: Software update group was NOT created, this may indicate an internal SCCM error, packages will be removed."  >> $global:pslogfile
    Remove-CMSoftwareUpdateDeploymentPackage -name $adrpackagename -force -confirm:$false
    Exit
}

#distribute our content
foreach($dp in $distributionpts){
    Start-CMContentDistribution -DeploymentPackageName $pkgobj.name -DistributionPointName $dp -confirm:$false -Verbose
    }