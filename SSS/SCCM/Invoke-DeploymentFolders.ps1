function CreateFoldersbyDate(){
Param(
[Parameter(Mandatory=$true)]
[DateTime]$Date,
[Parameter(Mandatory=$true)]
[String]$path
)
$FolderYear = ($date | Get-Date -uformat "%Y")
$FolderMonth = ($date | Get-Date -UFormat "%m")
$FolderDay = ($date | Get-date -uformat "%d")
#create folder structure
$fyear = New-Item -ItemType Directory -name $folderyear -path filesystem::$path -Force -ErrorAction SilentlyContinue
$fmonth = New-Item -ItemType Directory -name $foldermonth -path filesystem::$path\$folderyear -Force -ErrorAction SilentlyContinue
$fday = New-Item -ItemType Directory -name $FolderDay -path filesystem::$path\$folderyear\$foldermonth  -Force -ErrorAction SilentlyContinue
return $fday
}

CreateFoldersbyDate -Date (Get-Date) -path "C:\scripts\deployment"