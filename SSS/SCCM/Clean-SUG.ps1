﻿#Clean-SUG
#Written by Brian Kaler
#2/18/2019
#Removes inactive SUG based on 
#Must be run on SCCM Server or through a workstation with the SCCM module installed.

$120daysinthepast = (get-date).adddays(-120)

$oldsugs = Get-CMSoftwareUpdateGroup | Where {$_.Datecreated -ge $120daysinthepast}



