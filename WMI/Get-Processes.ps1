$sysprocs = Get-WmiObject -Class Win32_Process 
$limitedprocs = $sysprocs | select ProcessId,Name,HandleCount,Usermodetime,kernelmodetime


$report = @()
foreach($proc in $sysprocs){
    $obj = New-Object -TypeName psobject
    $obj = $limitedprocs | Where {$_.processid -eq $proc.processid}
    $obj | Add-Member -MemberType NoteProperty -Name "CPUTime" -Value (($proc.usermodetime / 10000000)+($proc.kernelmodetime / 10000000))
    $report += $obj
}

