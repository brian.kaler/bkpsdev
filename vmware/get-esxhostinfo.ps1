﻿#param(
#[Parameter(Mandatory=$true)]
#[String]$vcenter
#)
$vcenter = "catvct001.us.pinkertons.com"


#Load the VMware Powershell Module
get-module -listavailable VMware* | import-module

connect-viserver $vcenter

$vmhosts = get-vmhost


#create a report structure
$repprops = @{'VMHost'=$hostinfo.VMHOST;


}



foreach($v in $vmhosts){
$hostinfo = get-vmhosthardware -VMHost $v
$iscsi = Get-IScsiHbaTarget
$hba = Get-VMHostHba -VMHost $v | select * | FL

$storagereport = New-Object -TypeName PSObject -Property $repprops
$path = "d:\mydocs\temp\" + $hostinfo.vmhost.name + "-report.csv"
Export-Csv -InputObject $storagereport -LiteralPath $path
}