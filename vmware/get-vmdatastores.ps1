﻿#param(
#[Parameter(Mandatory=$true)]
#[String]$vcenter
#)
$vcenter = "catvct001.us.pinkertons.com"


#Load the VMware Powershell Module
get-module -listavailable VMware* | import-module

connect-viserver $vcenter


$list = Get-VM |
Select Name,
@{N="Datastore";E={[string]::Join(',',(Get-Datastore -Id $_.DatastoreIdList | Select -ExpandProperty Name))}},
@{N="Folder";E={$_.Folder.Name}},@{N="Notes";E={$_.Notes}}

$list | Export-Csv "d:\CSOCVMDatastoreList.csv"