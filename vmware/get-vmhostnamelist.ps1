﻿Get-Module -ListAvailable VMware* | Import-Module | Out-Null
$vcenter = connect-viserver ustvct001.us.pinkertons.com

get-vm | get-vmguest | select Hostname | ft -AutoSize
