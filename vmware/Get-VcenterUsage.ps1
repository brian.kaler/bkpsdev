﻿#Get-VcenterUsage
#Brian Kaler
#9/27/2018

#Load the VMware Powershell Module
get-module -listavailable VMware* | import-module

#connect vcenter
$vcenter = Read-Host "Please provide the vcenter server"

Connect-VIServer -Server $vcenter

$cluster = get-cluster
$timestamp = (get-date -f MM-dd-yyyy-hh-mm-ss)
$fileName = 'C:\Temp\cluster-stats-' + $timestamp + '.csv'

$stat = 'cpu.usagemhz.average','mem.usage.average'

$start = (Get-Date).AddDays(-30)

foreach($clus in $cluster){
    Get-Stat -Entity $clus -Stat $stat -Start $start -MaxSamples 30 -IntervalMins 1440 |

    Group-Object -Property Timestamp |

    Sort-Object -Property Name |

    Select @{N='Cluster';E={$clus.Name}},

        @{N='Time';E={$_.Group[0].Timestamp}},

        @{N='CPU GHz Capacity';E={$script:capacity = [int]($clus.ExtensionData.Summary.TotalCPU/1000); $script:capacity}},

        @{N='CPU GHz Used';E={$script:used = [int](($_.Group | where{$_.MetricId -eq 'cpu.usagemhz.average'} | select -ExpandProperty Value)/1000); $script:used}},

        @{N='CPU % Free';E={[int]($script:cpufree = 100 - $script:used/$script:capacity*100)}},

        @{N='Mem Capacity GB';E={$script:mcapacity = [int]($clus.ExtensionData.Summary.TotalMemory/1GB); $script:mcapacity}},

        @{N='Mem Used GB';E={$script:mused = [int](($_.Group | where{$_.MetricId -eq 'mem.usage.average'} | select -ExpandProperty Value) * $script:mcapacity/100); $script:mused}},

        @{N='Mem % Free';E={[int](100 - $script:mused/$script:mcapacity*100)}},
        
        @{N='CPU % Used';E={[int][math]::abs($script:cpufree - 100)}}, 
        
        @{N='Mem % Used';E={[math]::abs([int](100 - $script:mused/$script:mcapacity*100) - 100)}} |

    Export-csv -Path $fileName
    }