﻿. .\Deploy-AVOVF.ps1

$myvmname = ("AVUSMSENSOR-{0:yyyy-MM-dd-HH-mm-ss}" -f (Get-Date))
Deploy-AVOVF -vCenterInstance "ustvct001.us.pinkertons.com" -TargetCluster "UCS" -ISOPath "[VMISO] deploy_config.iso" -VMDatastore "esx_vmdata01" -VMNetwork "VM_PROD" -VMName $myvmname -OVFFile "\\ustdps901\d$\alienvault-usm\USM_sensor-node.ovf"