﻿. .\Deploy-AVOVF.ps1

$myvmname = ("AVUSMSENSOR-{0:yyyy-MM-dd-HH-mm-ss}" -f (Get-Date))
Deploy-AVOVF -vCenterInstance "evcenter1.us.pinkertons.com" -TargetCluster "Production_T1" -ISOPath "[esx_iso01] usmsensor-12122018\deploy_config.iso" -VMDatastore "Prod NFS 91" -VMNetwork "VLAN 64 - SERVERS 2 (EOC-vSW-VMDC-03)" -VMName $myvmname -OVFFile "\\napecfs01\itn_home01\Apps\alienvault-usm\USM_sensor-node.ovf"