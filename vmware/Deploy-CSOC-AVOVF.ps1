﻿. .\Deploy-AVOVF.ps1

$myvmname = ("AVUSMSENSOR-{0:yyyy-MM-dd-HH-mm-ss}" -f (Get-Date))
Deploy-AVOVF -vCenterInstance "catvct001.us.pinkertons.com" -TargetCluster "UCS" -ISOPath "[esx_vmlcl01] ISO/deploy_config.iso" -VMDatastore "esx_vmlcl01" -VMNetwork "VM_Prod_2" -VMName $myvmname -OVFFile "\\catdps901\d$\alienvault-usm\USM_sensor-node.ovf"