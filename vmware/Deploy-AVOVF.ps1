﻿#Deploy-AVOVF
#Written by Brian Kaler
#12/12/2018
#Custom script to deploy the Alien Vault Sensor in OVF format
Function Deploy-AVOVF(){
Param(
[Parameter(Mandatory)]
[String]$vCenterInstance,
[Parameter(Mandatory)]
[String]$TargetCluster,
[Parameter(Mandatory=$true)]
[String]$ISOPath,
[Parameter(Mandatory=$false)]
[String]$VMName = ("AVUSMSENSOR-{0:yyyy-MM-dd-HH-mm-ss}" -f (Get-Date)),
[Parameter(Mandatory)]
[String]$VMDatastore,
[Parameter(Mandatory=$false)]
[String]$VMCPU = 4,
[Parameter(Mandatory=$false)]
[String]$VMMemory = 12288,
[Parameter(Mandatory=$false)]
[String]$VMDiskSize = 150,
[Parameter(Mandatory=$false)]
[String]$GuestOS = "otherGuest64",
[Parameter(Mandatory=$false)]
[String]$VMNetwork,
[Parameter(Mandatory)]
[String]$OVFFile
)

# -----Import Modules-----
Write-host "$(Get-Date -Format G) Importing Vmware Module"
Get-Module -ListAvailable VMware* | Import-Module | Out-Null
#remove clobbering cmdlets if they exist
remove-module hyper-v | out-null

# ------Connect vcenter------
Connect-VIServer -Server $vCenterInstance
#get a host
$vmhost = Get-Cluster $TargetCluster | Get-VMHost | get-random
#import the configuration from the filex
$ovfConfig = Get-OvfConfiguration $OVFFile
#set up the values specific to this ovf
$vdPortGroup = Get-VDPortgroup $vmnetwork -ErrorAction SilentlyContinue
if($vdPortGroup -ne $null)
    {
    $ovfConfig.NetworkMapping.ethernet0.value = $vdPortGroup
    $ovfConfig.NetworkMapping.ethernet1.value = $vdPortGroup
    $ovfConfig.NetworkMapping.ethernet2.value = $vdPortGroup
    $ovfConfig.NetworkMapping.ethernet3.value = $vdPortGroup
    $ovfConfig.NetworkMapping.ethernet4.value = $vdPortGroup
    }
    Else
    {
    $ovfConfig.NetworkMapping.ethernet0.value = $vmnetwork
    $ovfConfig.NetworkMapping.ethernet1.value = $vmnetwork
    $ovfConfig.NetworkMapping.ethernet2.value = $vmnetwork
    $ovfConfig.NetworkMapping.ethernet3.value = $vmnetwork
    $ovfConfig.NetworkMapping.ethernet4.value = $vmnetwork
    }

#get the datastore object
$mydatastore = get-Datastore $VMDatastore

#import the ovf
Import-VApp -Source $OVFFile -Datastore $mydatastore -Name $vmname -vmhost $vmhost -OvfConfiguration $ovfconfig

#customize the VM
$myvm = Get-vm -Name $VMName
#abort if the vm creation failed
if($myvm -eq $null){
Write-Host "Import OVF has failed, exiting"
exit
}
Set-VM -vm $myvm -MemoryGB $vmmemory -ProcessorCount $vmcpu 

#attach our config iso
While ((get-vm -name $vmname).Powerstate -eq "PoweredOff") {
$cddrive = Get-CDDrive -VM $vmname
Set-CDDrive -cd $cddrive -IsoPath $ISOPath -StartConnected:$true -confirm:$false -ErrorAction:SilentlyContinue
# ------Power VM------
Start-VM $myvm
}

}
