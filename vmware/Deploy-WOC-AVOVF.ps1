﻿. .\Deploy-AVOVF.ps1

$myvmname = ("AVUSMSENSOR-{0:yyyy-MM-dd-HH-mm-ss}" -f (Get-Date))
Deploy-AVOVF -vCenterInstance "wvc.us.pinkertons.com" -TargetCluster "Production" -ISOPath "[esx_iso01] usmsensor-12122018\deploy_config.iso" -VMDatastore "NFnS_NR-02" -VMNetwork "VLAN_16 - Servers" -VMName $myvmname -OVFFile "d:\Downloads\usm-sensor12122018\alienvault-usm\USM_sensor-node.ovf"