﻿param(
[Parameter(Mandatory=$true)]
[String]$vcenter
)



#Load the VMware Powershell Module
get-module -listavailable VMware* | import-module

connect-viserver $vcenter

$targets = get-vm

foreach($target in $targets)
    {
    $guest = get-vm -name $target
    get-snapshot -vm $guest | remove-snapshot -confirm:$false
    }