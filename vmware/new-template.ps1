﻿#New-Template
#Uses MDT to provision a new VM in vmware that is syspreped and readily converted to a template
#written by Brian Kaler
#8/1/2018


Param(
[Parameter(Mandatory)]
[String]$vCenterInstance,
[Parameter(Mandatory)]
[String]$TargetCluster,
[Parameter(Mandatory=$true)]
[String]$ISOPath,
[Parameter(Mandatory)]
[String]$Id,
[Parameter(Mandatory)]
[String]$MDTBuildPath,
[Parameter(Mandatory=$false)]
[String]$VMName = ("vmwarebuild-{0:yyyy-MM-dd-HH-mm-ss}" -f (Get-Date)),
[Parameter(Mandatory=$false)]
[String]$TemplateName = ("template-{0:yyyy-MM-dd-HH-mm-ss}" -f (Get-Date)),
[Parameter(Mandatory)]
[String]$VMDatastore,
[Parameter(Mandatory=$false)]
[String]$VMCPU = 2,
[Parameter(Mandatory=$false)]
[String]$VMMemory = 4096,
[Parameter(Mandatory=$false)]
[String]$VMDiskSize = 40,
[Parameter(Mandatory=$false)]
[String]$GuestOS,
[Parameter(Mandatory=$false)]
[String]$VMNetwork,
[Parameter(Mandatory=$true)]
[String]$Templatelocation = "VM Templates"
)

# -----Import Modules-----
Write-Host "$(Get-Date -Format G) Importing Hyper-V 1.1 PowerShell Module"
Import-Module $env:windir\System32\WindowsPowerShell\v1.0\Modules\Hyper-V\1.1\Hyper-V.psd1
Write-Host "$(Get-Date -Format G) Importing MDT PowerShell Module"
Import-Module "$env:programfiles\Microsoft Deployment Toolkit\bin\MicrosoftDeploymentToolkit.psd1"
Write-Host "$(Get-Date -Format G) Importing Vmware Module"
Get-Module -ListAvailable VMware* | Import-Module | Out-Null

# ------Prep the MDT-------
    ## Setup MDT custom settings for VM auto deploy
    
    Write-Host ""
    Write-Host "$(Get-Date -Format G) Starting process for $Id"
    Write-Host ""
    Write-Host "$(Get-Date -Format G) Backing up current MDT CustomSettings.ini"

    Copy-Item $MdtBuildPath\Control\CustomSettings.ini $MdtBuildPath\Control\CustomSettings-backup.ini
    Start-Sleep -s 5

    Write-Host "$(Get-Date -Format G) Setting MDT CustomSettings.ini for Task Sequence ID: $Id"

    Add-Content $MdtBuildPath\Control\CustomSettings.ini ""
    Add-Content $MdtBuildPath\Control\CustomSettings.ini ""
    Add-Content $MdtBuildPath\Control\CustomSettings.ini "TaskSequenceID=$Id"
    Add-Content $MdtBuildPath\Control\CustomSettings.ini "SkipTaskSequence=YES"
    Add-Content $MdtBuildPath\Control\CustomSettings.ini "SkipComputerName=YES"

# ------Connect vcenter------
Connect-VIServer -Server $vCenterInstance -user $vCenterUser -Password $vCenterPass
# ------Set up the objects for a new vm-----
$newvmdatastore = get-datastore $VMDatastore
$vmhost = Get-Cluster $TargetCluster | Get-VMHost | get-random
$newvmnetwork = Get-VDPortgroup $vmnetwork -ErrorAction SilentlyContinue
# ------Validate our data before we create a new VM-----
if($newvmdatastore -eq $null){
    Write-Host "Datastore for new template was unavailable, script will exit"
    Exit
    }
if($vmhost -eq $null){
    Write-Host "VM Host for new template was unavailable, script will exit"
    Exit
    }
if($newvmnetwork -eq $null){
    Write-Host "VM Host for new template was unavailable, script will exit"
    Exit
    }
# ------Create the new VM------
New-VM -Name $VMName -Datastore $newvmdatastore -vmhost $vmhost -MemoryMB $VMMemory -NumCpu $VMCPU -DiskGB $VMDiskSize -CD -NetworkName $newvmnetwork.name -guestid $GuestOS
$newvm = Get-VM -Name $VMName
if($newvm -eq $null){
    Write-Host "New VM used for template does not appear to exist, script will exit"
    Exit
    }
# ------Validate the ISO Path-----
$isopathregex = $isopath -match "(?!(\[)).*(?=(\]))"
$isopathdatastorename = $matches.0
If((Get-Datastore -name $isopathdatastorename) -eq $null){
    Write-Host "ISO path or ISO file for new template was unavailable, script will exit"
    Exit
}
# ------Attach the boot iso------
$cddrive = Get-CDDrive -VM $newvm
Set-CDDrive -cd $cddrive -IsoPath $ISOPath -StartConnected:$true -confirm:$false -ErrorAction:SilentlyContinue
# ------Set BIOS Type------
$spec = new-object VMware.Vim.VirtualMachineConfigSpec
$spec.Firmware = New-Object VMware.Vim.GuestOsDescriptor
$spec.Firmware = $vmbiostype
$task = $newvm.ExtensionData.ReconfigVM_Task($spec)
# ------Power VM------
Start-VM $newvm
# ------Wait for VM to stop imaging -----
Write-Host "$(Get-Date -Format G) Waiting for $VmName to build"
While ((get-vm -name $vmname).Powerstate -eq "PoweredOn") {Start-Sleep -s 10}
# ------Make the VM a template------
$template = New-Template -vm $newvm -name $TemplateName -Datastore $VMDatastore -Location $templatelocation
# ------Cleanup MDT ------
  Remove-Item $MdtBuildPath\Control\CustomSettings.ini
  Copy-Item $MdtBuildPath\Control\CustomSettings-backup.ini $MdtBuildPath\Control\CustomSettings.ini


