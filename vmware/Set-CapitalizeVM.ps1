﻿#CAPITALIZE all the vms
#written by Brian Kaler
#9/26/2018 9:05AM

#Load the VMware Powershell Module
get-module -listavailable VMware* | import-module

#connect vcenter
$vcenter = Read-Host "Please provide the vcenter server"
Connect-VIServer -Server $vcenter

#get vm inventory
$allvms = Get-Cluster | Get-VM

foreach($vm in $allvms){
    Set-VM -VM $vm -Name ($vm.name.ToUpper()) -Confirm:$false
    }