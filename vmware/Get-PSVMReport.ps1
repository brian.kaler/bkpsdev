﻿
Function Get-PSVMReport(){
Param(
[Parameter(Mandatory=$true)]
[STRING]$vcenter
)

Get-Module -ListAvailable VMware* | Import-Module | Out-Null
connect-viserver -Server $vcenter


#Create vmInfo object
$vmInfo = @()
    $vmInfoTemp = New-Object "PSCustomObject"
    $vmInfoTemp | Add-Member -MemberType NoteProperty -Name VMName -Value ""
    $vmInfoTemp | Add-Member -MemberType NoteProperty -Name ToolsVersion  -Value ""
    $vmInfoTemp | Add-Member -MemberType NoteProperty -Name HWVersion  -Value ""
    $vmCategories  = Get-TagCategory
    $vmCategories | %{$vmInfoTemp | Add-Member -MemberType NoteProperty -Name $_.Name  -Value "" }
    $vmInfo += $vmInfoTemp

get-vm | %{
   $vmInfoTemp = New-Object "PSCustomObject"
   $toolsVersion = Get-VMGuest $_ | select -ExpandProperty ToolsVersion
   $ipaddresses = Get-VMGuest $_ | select -ExpandProperty IPAddress
   $vmdisks = Get-VMGuest $_ | select -ExpandProperty Disks
   $osfullname = Get-VMGuest $_ | select -ExpandProperty OSFullName
   $oshostname = Get-VMGuest $_ | select -ExpandProperty HostName
   $vmInfoTemp | Add-Member -MemberType NoteProperty -Name HostName -Value $oshostname
   $vmInfoTemp | Add-Member -MemberType NoteProperty -Name OS -Value $osfullname
   $vmInfoTemp | Add-Member -MemberType NoteProperty -Name VMLabel -value $_.Name
   $vmInfoTemp | Add-Member -MemberType NoteProperty -Name PowerState -value $_.PowerState
   $vmInfoTemp | Add-Member -MemberType NoteProperty -Name NumCPUs -value $_.NumCPU
   $vmInfoTemp | Add-Member -MemberType NoteProperty -Name Memory -value $_.MemoryGB
   $vmInfoTemp | Add-Member -MemberType NoteProperty -Name Notes -value $_.Notes
   $vmInfoTemp | Add-Member -MemberType NoteProperty -Name IPAddresses  -Value ($ipaddresses -join ",")
   $vmInfoTemp | Add-Member -MemberType NoteProperty -Name Disks  -Value ($vmdisks -join ",")
   $vmInfoTemp | Add-Member -MemberType NoteProperty -Name ToolsVersion  -Value $toolsVersion
   $vmInfoTemp | Add-Member -MemberType NoteProperty -Name HWVersion  -Value $_.Version
   $vmtags = ""
   $vmtags = Get-TagAssignment -Entity $_ 
   if($vmtags){
       $vmCategories | %{ 
           $tempVMtag = ""
            $tempCategroy = $_.Name
            $tempVMtag = $vmtags | Where-Object {$_.tag.category.name -match $tempCategroy}
            if($tempVMtag)
            {
                           $vmInfoTemp | Add-Member -MemberType NoteProperty -Name $tempCategroy -Value ($tempVMtag.tag.name -join ",")
            }else {
                $vmInfoTemp | Add-Member -MemberType NoteProperty -Name $tempCategroy -Value ""
            }
       }
   }else{
       $vmCategories | %{
           $vmInfoTemp | Add-Member -MemberType NoteProperty -Name $_.name -Value ""
        }
   }
    $vmInfo += $vmInfoTemp
}


$timestamp = (get-date -Format ddMMyyyy-hhmmss)
$path = "c:\temp\"
$filename = "vcenter-report-($vcenter.name)"
$filetype = ".csv"
$vmInfo | select * -Skip 1 | Export-Csv ($path+$filename+$timestamp+$filetype) -NoTypeInformation -UseCulture 
}

Get-PSVMReport