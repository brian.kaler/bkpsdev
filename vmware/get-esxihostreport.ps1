﻿If ($PSVersionTable.PSVersion.Major -lt 5) {
    iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    choco install powershell -y
    choco upgrade powershell -y
}

if((get-module -name vDocumentation).name -ne "vDocumentation"){

Install-Module -Name VMware.PowerCLI -Scope CurrentUser
Install-Module ImportExcel -scope CurrentUser
Install-Module vDocumentation -Scope CurrentUser
}

get-module -name VMware* | import-module
import-module vDocumentation
import-module ImportExcel

$vcenter = Read-Host -Prompt "Please provide the name of the vcenter server to run reports on:"
if((get-powercliconfiguration).InvalidCertificateAction -ne "Ignore"){
Set-PowerCLIConfiguration -InvalidCertificateAction ignore
}

Connect-VIServer -Server $vcenter

#Get-ESXInventory -ExportExcel
#Get-ESXStorage -ExportExcel
#get-esxiodevice -exportexcel

get-esxiodevice