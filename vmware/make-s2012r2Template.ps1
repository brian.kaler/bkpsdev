﻿.\new-template -vCenterInstance "catvct001.us.pinkertons.com" -TargetCluster "UCS" -id "S2012R2SQLD" -VMDatastore "esx_vmlcl01" -MDTBuildPath "\\napccfs01\stas_home01\deploymentshare" -vmnetwork "VM_PROD_2" -guestos "windows8_64Guest" -isopath "[esx_vmlcl01] ISO\LiteTouchPE_x64.iso"

#guestos reference
#-----------------
#win95Guest
#win98Guest
#winMeGuest
#winNTGuest
#win2000ProGuest
#win2000ServGuest
#win2000AdvServGuest
#winXPHomeGuest
#winXPProGuest
#winXPPro64Guest
#winNetWebGuest
#winNetStandardGuest
#winNetEnterpriseGuest
#winNetDatacenterGuest
#winNetBusinessGuest
#winNetStandard64Guest
#winNetEnterprise64Guest
#winLonghornGuest
#winLonghorn64Guest
#winNetDatacenter64Guest
#winVistaGuest
#winVista64Guest
#windows7Guest
#windows7_64Guest
#windows7Server64Guest
#windows8Guest
#windows8_64Guest ----- SERVER2012R2/Windows8
#windows8Server64Guest
#windows9Guest
#windows9_64Guest ----- Windows 10
#windows9Server64Guest ----- SERVER 2016
