﻿#Tag Search
#Written by Brian Kaler
#9/25/2018 2:55 PM 


#Load the VMware Powershell Module
get-module -listavailable VMware* | import-module

$vcenter = "catvct001.us.pinkertons.com"

Connect-VIServer -Server $vcenter

$allvm = Get-Cluster | get-vm

$criteria = "Production","Domino"




Foreach($vm in $allvm){
    $tags = Get-TagAssignment -Entity $vm
    write-host "Now Processing: " ($tags.entity.name[0])
    foreach($tag in $tags){
        if($tag.Tag -match $criteria[1]){
            write-host "Found! " ($tag.entity.name)
            write-host $tag.tag
            if($tag.Tag -match $criteria[0]){
                write-host "Found! " ($tag.entity.name)
                }
            }
        }
    }