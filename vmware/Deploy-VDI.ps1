﻿#code starts here

Param(
[Parameter(Mandatory)]
[string]$vcenterinstance="wvc.us.pinkertons.com",
[Parameter(Mandatory)]
[String]$TargetCluster="XEN",
[Parameter(Mandatory)]
[String]$VMName,
[Parameter(Mandatory)]
[String]$SourceTemplate = "SWIN101803VDI-2019-02-22-10-57-59",
[Parameter(Mandatory)]
[String]$VMDatastore = "ESX_XEN01",
[Parameter(Mandatory)]
[String]$CustomSpec = "VDI Template - XEN VLAN",
[Parameter(mandatory=$false)]
[String]$PoweronafterCustomization = $true
)



Write-Host "$(Get-Date -Format G) Importing Vmware Module"
Get-Module -ListAvailable VMware* | Import-Module | Out-Null

#connect
Connect-VIServer -server $vcenterinstance 
$vmhost = Get-Cluster $TargetCluster | Get-VMHost | get-random

$mytemplate = Get-Template -Name $SourceTemplate
$mycustomspec = Get-OSCustomizationSpec $CustomSpec
$mydatastore = Get-Datastore -Name $VMDatastore

$mynewvm = New-VM -name $VMName -Template $mytemplate -VMHost $vmhost -OSCustomizationSpec $mycustomspec -Datastore $mydatastore

If($PoweronafterCustomization){
Start-VM $mynewvm 
}


