﻿#CAPITALIZE all the vms
#written by Brian Kaler
#9/26/2018 9:05AM

#Load the VMware Powershell Module
get-module -listavailable VMware* | import-module

#connect vcenter
#$vcenter = Read-Host "Please provide the vcenter server"
$vcenter = "ustvct001.us.pinkertons.com"
Connect-VIServer -Server $vcenter

#get vm inventory
$allvms = Get-Cluster | Get-VM

foreach($vm in $allvms){
    if($vm.name.IndexOf("-") -ge 1){
        $newname = ($vm.name.trimend($vm.Name.Substring($vm.name.IndexOf("-"))))
        write-host $newname
        Set-VM -VM $vm -Name $newname -confirm:$false
        }
    
    }