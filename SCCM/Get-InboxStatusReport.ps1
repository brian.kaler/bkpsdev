﻿#Get-InboxStatusReport
#Written by Brian Kaler
#9/9/2020
#Monitors SCCM Inbox via HTML Report
#SCCM MODULE NOT REQUIRED, psreport module however is
Import-module PsWritehtml
#Report parmeters
$ReportTitle = "SCCM Inbox Report"


#get our inboxes
$local_home = "D:\Program Files\Microsoft Configuration Manager\inboxes"
$inboxes = Get-ChildItem -Path $local_home -filter *.* | ?{$_.Psiscontainer -eq $true}
#Measure our inboxes, gather file details.
$report = @()
foreach($ib in $inboxes){
    $filecount = (Get-ChildItem -Path $ib.fullname -Filter * | measure).count
    $repitem = New-Object -TypeName psobject 
    $repitem | Add-Member -MemberType NoteProperty -Name "Inbox Name" -Value $ib.name
    $repitem | Add-Member -MemberType NoteProperty -Name "Inbox Filecount" -Value $filecount
    $repitem | Add-Member -MemberType
    $report += $repitem
    }

#create our html report
New-HTML -TitleText $ReportTitle -UseCssLinks -UseJavaScriptLinks -FilePath $reportpath {
    New-HTMLTab -TabName 'Gridview' {
        New-HTMLContent -HeaderText "Groups" {
            New-HTMLContent -HeaderText 'Deployment' -CanCollapse {
                New-HTMLTable -DataTable $report -Verbose
            }
        }
        New-HTMLContent -HeaderText 'Test 2' {

        }
    }

} -ShowHTML