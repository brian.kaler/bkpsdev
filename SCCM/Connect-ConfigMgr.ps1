﻿##      ADDS A Connect-ConfigMgr ITEM TO THE ISE ADD-ONS MENU          ##
## Created by Matt Shadbolt - http://blogs.technet.com/b/ConfigMgrDogs ##

Function Connect-ConfigMgr {

$CustomError = $null 

If ($Console64 = Get-ItemProperty -Path HKLM:\SOFTWARE\Wow6432Node\Microsoft\ConfigMgr10\Setup -Name ProductCode -ErrorAction SilentlyContinue) {
    
    # 64-bit system
    $ModulePath = (Get-ItemProperty HKLM:\SOFTWARE\Wow6432Node\Microsoft\ConfigMgr10\Setup -Name "UI Installation Directory").'UI Installation Directory'
    $SiteServerName = "uswscm901.us.pinkertons.com"
    $ProviderLocation = gcim -ComputerName $SiteServerName -Namespace root\sms SMS_ProviderLocation -filter "ProviderForLocalSite='True'"
    $ProviderMachine = $ProviderLocation.Machine
    $SiteCode = $ProviderLocation.SiteCode
    Import-Module $ModulePath\bin\ConfigurationManager.psd1
    Set-Location $SiteCode":\" 
    }

ElseIf ($Console32 = Get-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\ConfigMgr10\Setup -Name ProductCode -ErrorAction SilentlyContinue) {
    
    # 32-bit system
    $ModulePath = (Get-ItemProperty HKLM:\SOFTWARE\Microsoft\ConfigMgr10\Setup -Name "UI Installation Directory").'UI Installation Directory'
    $SiteServerName = (Get-ItemProperty HKLM:\SOFTWARE\Microsoft\ConfigMgr10\AdminUI\Connection -Name Server).Server 
    $ProviderLocation = gcim -ComputerName $SiteServerName -Namespace root\sms SMS_ProviderLocation -filter "ProviderForLocalSite='True'"
    $ProviderMachine = $ProviderLocation.Machine
    $SiteCode = $ProviderLocation.SiteCode
    Import-Module $ModulePath\bin\ConfigurationManager.psd1
    Set-Location $SiteCode":\" 

    }

Else { 
    $CustomError = [String]"Error: The required registry keys cannot be found. Please ensure the console has been installed on this computer" 
    Throw $CustomError
    }
}

$psISE.CurrentPowerShellTab.AddOnsMenu.Submenus.Add("Connect-ConfigMgr", 
{
    Connect-ConfigMgr
},"ALT+F1") | out-Null