﻿#Get-CurrentDeploymentStatus
#Brian Kaler
#11/18/2019
#Returns objects based on deploymentID and a deploymentcutoff date - 
#!!!! Works only on Task Sequences and Packages!!!!

#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = "C:\bkpsdev\sccm"
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
set-location SSS:\

$deploymentname = ""
$deploymentid = "SSS200DA"
#some examples for date selection, uncomment to use.
#$deploycutoff = (Get-Date).adddays(-15)
[datetime]$deploycutoff = "1/9/2019"

$deploymentobj = Get-CMDeployment -DeploymentId $deploymentid
$deploymentstatusobj = Get-CMDeploymentStatus | where {$_.DeploymentId -eq $deploymentid}


$report = @()
foreach($obj in $deploymentstatusobj){
$details = Get-CMDeploymentStatusDetails -InputObject $obj
    Foreach($deet in $details){
        If($deet.SummarizationTime -ge $deploycutoff){
        $report += $deet | select DeviceName,PackageName,SummarizationTime,StatusType,StatusDescription
        }
    }
}
$report