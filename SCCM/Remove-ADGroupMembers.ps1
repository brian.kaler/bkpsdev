﻿$expcs = @("MAINT-SERVER-ONDEMAND"
"MAINT-SERVER-CVE_20188653_PROD")

foreach($ex in $expcs){
    $mygroup = Get-ADGroup -Identity $ex
    $pcs = Get-ADGroupMember -Identity $mygroup
    if($pcs -eq $null){
    Write-host "no members were found, exiting"
    Exit
    }
    Remove-ADGroupMember -Members $pcs -Identity $mygroup -confirm:$false -verbose

    }