﻿#Get-CMUserPrimaryDevice
#Brian Kaler
#4/2/2019
#Gets all primary devices based on the provided username

#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = (Get-Item -path ".\").FullName
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
set-location SSS:\

Function Get-CMDevicePrimaryUser{
    Param(
        [Parameter(Mandatory,ValueFromPipeline)]
        [ValidateNotNullorEmpty()]
        [Object]$cmdevice
    )
    $device = Get-CMDevice -Name $cmdevice
    $affinity = Get-CMUserDeviceAffinity -devicename $cmdevice
    if($affinity -ne $null){
        $user =  Get-CMUser -Name $affinity.uniqueusername -ErrorAction SilentlyContinue
        IF($user.resourceid -ne $null){
            $resourceid = $user.resourceid
        }
        Else{
            Write-Host "The User $user.DisplayName has no ResourceID in SCCM!"
        }
    }
    If($user -ne $null){
        $userobj = Get-CMResource -ResourceId $resourceid -ErrorAction SilentlyContinue -fast
    }
    If($userobj -ne $null){
        $outobj = New-Object -TypeName PSObject
        $outobj | Add-Member -MemberType NoteProperty -Name Hostname -Value $cmdevice
        $outobj | Add-Member -MemberType NoteProperty -Name FullName -Value $userobj.FullUserName
        $outobj | Add-Member -MemberType NoteProperty -Name UniqueUserName -Value $userobj.UniqueUserName
        $outobj | Add-Member -MemberType NoteProperty -Name Mail -Value $userobj.Mail
        $outobj | Add-Member -MemberType NoteProperty -Name CurrentLoggedOnUser -Value $device.currentlogonuser
        $outobj | Add-Member -MemberType NoteProperty -Name LastLoggedOnUser -Value $device.lastlogonuser
    }
    Else{
        $outobj = New-Object -TypeName PSObject
        $outobj | Add-Member -MemberType NoteProperty -Name Hostname -Value $cmdevice
        $outobj | Add-Member -MemberType NoteProperty -Name FullName -Value "No Primary User"
        $outobj | Add-Member -MemberType NoteProperty -Name UniqueUserName -Value "No Primary User"
        $outobj | Add-Member -MemberType NoteProperty -Name Mail -Value "No Primary User"
        $outobj | Add-Member -MemberType NoteProperty -Name CurrentLoggedOnUser -Value $device.currentlogonuser
        $outobj | Add-Member -MemberType NoteProperty -Name LastLoggedOnUser -Value $device.lastlogonuser 
    }   
    Return $outobj
}

#examples
$allcollections = Get-CMCollection -name "PHASED-Windows10-1809-1709ORLOWER2"

$report = @()
$dloop = 0
foreach($col in $allcollections){
    $dloop += 1
    Write-Host "Processing $dloop of ($allcollections.count)"
    $devices = Get-CMCollectionMember -CollectionName $col.name
    foreach($dev in $devices){
        $devloop += 1
        $devicename = $dev.name
        write-host "Now Processing $devloop of ($devices.count) Now ($devicename)"
        $deviceuser = Get-CMDevicePrimaryUser -cmdevice $dev.name
        $report += $deviceuser
    }
}


