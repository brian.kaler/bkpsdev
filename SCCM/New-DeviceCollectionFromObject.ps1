﻿


$scriptname = "New-SCCMDeviceCollection"
#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = (Get-Item -path ".\").FullName
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
#import ActiveDirectory
$adsmod = Import-Module ActiveDirectory -PassThru
If($scmod -eq $null){
    Write-Host "ERROR: Could not find Configuration Manager Module, please confirm SCCM Admin Console is installed on this machine"
    Sleep -seconds 15
    exit
    }
If($adsmod -eq $null){
    Write-Host "ERROR: Could not find Active Directory Module, please confirm it is installed on the machine, `ntry running 'install-module activedirectory' as a potential fix"
    Sleep -seconds 15
    exit
    }
# Set the current location to be the site code. 
$siteloc = Set-Location $SiteCode":\"  
CLEAR

$limitingcollectionid = "SMSDM003"
$source = get-content $filename


#Create a collection to contain each unique primary service name
$primaryservices = $sourcexls | group-object -Property "Primary Service"
#Eliminate errors in the data.
$unsupportedcharacters = @("/",")","(","\","[","]",":",";","`,","|","=")

#strip out the users' bs data.
$sanitizedprimaryservices = @()
$primaryservices | foreach-object {
    foreach($char in $unsupportedcharacters){
        Do
        {
        $_ = $_.replace($char,"")
        } Until($_ -notcontains $char)    
    }
    $sanitizedprimaryservices += $_
}


#TEMP COMMENT, WILL NOT WORK UNTIL UNCOMMENT

#Foreach($col in $sourcexls){
#IF($COL."Primary Service" -ne $null){
#$groupname = $COL."Primary Service"
##set our collection name
#$collectionname = $groupname
##Domain prefix - double backslash is intentional, do not modify
#$domainpfx = "PNKUS\\"
##group folder set
#$groupfolder = "Servers"
##AD Group name - Must include the domain prefix and groupname.
#$adgroupname = $domainpfx+$groupname
#Add-ComputerstoAdGroup -groupname $adgroupname -members $col.Hostname -groupfolder $groupfolder | out-null
#Add-CollectionFromAd -collectionname $collectionname -queryname $collectionname -limitingcollectionid $limitingcollectionid -adgroupname $col."Primary Service" | out-null
#}
#}




