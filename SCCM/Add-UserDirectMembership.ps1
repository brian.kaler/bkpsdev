#add-userdirectmembership
#brian kaler
#4/22/2019
#adds a user to a SCCM collection through direct membership

If((get-module -Name ConfigurationManager)-eq $null){
    #define the directory the scripts reside in
    Write-host "INFO: Loading subscripts and modules, please wait..."
    $scriptroot = "C:\bkpsdev\sccm"
    #Load the SCCM Module
    $SiteCode = "SSS" 
    $SiteServer = "uswscm901.us.pinkertons.com" 
    # Import the ConfigurationManager.psd1 module  
    $scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
    set-location SSS:\
    }
    Function Add-UserDirectMembership{
    Param(
    [String]$dmusercollection,
    [String]$dmusername
    )
    
    $myuser = Get-CMUser -Name $dmusername
    if($myuser -eq $null)
    {write-host "Device $s was not found in SCCM"}
    Else
    {
    $user = $myuser.resourceid
    Write-Host "Attempting to add $dmusername to $dmusercollection"
    Add-CMUserCollectionDirectMembershipRule -CollectionName $dmusercollection -ResourceID $user
    }
    }
    
    #EXAMPLES! 
    
    
    #$myusers = Get-ADGroup -Identity "Safes_Admin_Domain-Global" | Get-ADGroupMember | select -ExpandProperty samaccountname
    #cd SSS:\
    #foreach($user in $myusers){
    #    write-host "Adding PNKUS\$user"
    #    Add-UserDirectMembership -dmusercollection "Citrix Receiver 4.9" -dmusername "PNKUS\$user"
    #
    #}
    