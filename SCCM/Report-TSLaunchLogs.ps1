﻿$tslaunchlogs = gci .

#get file counts for specific entries in the log, record the pc name
$report = @()
foreach($log in $tslaunchlogs){
    $scheduledupgrade = get-content $log -Tail 500 | select-string "User scheduled a forced upgrade"
    $remainingdays = get-content $log -Tail 500 | select-string "Remaining days left to upgrade this computer"
    $readinessdown = get-content $log -Tail 500 | select-string "Readiness test(s) unsuccesful"
    $readinessfail = get-content $log -Tail 500 | select-string "Failed readiness test"
    #do more stuff based on presence of data
    $boolschedupgrade = IF($scheduledupgrade -ne $null){$true}else{$false}
    If($remainingdays -ne $null){
        $remainingdayscount = $remainingdays | select -Last 1
        }else{
        $remainingdayscount = 0
        }

    $obj = New-Object -TypeName psobject
    $obj | Add-Member -MemberType NoteProperty -Name Logname -Value $log.fullname
    $obj | Add-Member -MemberType NoteProperty -Name scheduledupgrade -Value $boolschedupgrade
    $obj | Add-Member -MemberType NoteProperty -Name remainingdays -Value $remainingdayscount
    $obj | Add-Member -MemberType NoteProperty -Name readiness -Value ($readinessdown | select -last 1)
    $obj | Add-Member -MemberType NoteProperty -Name readinessfail -Value ($readinessfail | select -Last 1)
    
    $report += $obj


}