#Populate-FITDevices
#Written by Brian Kaler
#8/20/2019
#Updates an SCCM Collection with a list of devices based on Primary User 

#parameters

#Source AD Users (group) for gathering a list of users, comment out if you already have a list of groups
$adgroups = @("All IT Associates (WOC)",
            "All IT Associates (EOC)")


#import ad module
Import-Module ActiveDirectory -force | out-null
#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = (Get-Item -path ".\").FullName
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
set-location SSS:\

Function Add-DirectMembership{
    Param(
    [String]$dmcollection,
    [String]$dmdevicename
    )
    
    $mydev = Get-CMDevice -Name $dmdevicename
    if($mydev -eq $null)
    {write-host "Device $s was not found in SCCM"}
    Else
    {
    $device = $mydev.name
    Write-Host "Attempting to add $dmdevicename to $dmcollection"
    Add-CMDeviceCollectionDirectMembershipRule -CollectionName $dmcollection -Resource $mydev
    }
    }


$fulllist = @()
$names = @()
foreach($adgrp in $adgroups){
$names += Get-ADGroupMember -Identity $adgrp | select -ExpandProperty samaccountname
}
#format names so that SCCM cmdlet works
foreach($name in $names){
$fulllist += $name.insert(0,"PNKUS\")
}


$alldevices = @()
foreach($user in $fulllist){
$alldevices += Get-CMUserDeviceAffinity -UserName $user
}
#add direct membership to the SCCM collection for FIT Devices
Foreach($pc in $alldevices){
    Add-DirectMembership -dmdevicename $pc.ResourceName -dmcollection "Friends of IT Devices"
}

