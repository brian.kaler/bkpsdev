﻿#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = (Get-Item -path ".\").FullName
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
Set-Location SSS:\

$mybootimage = Get-CMBootImage -id SSS000D0
$mydps = Get-CMDistributionPoint -DistributionPointGroupName "OSD Centric Distribution Points"
$mymps = Get-CMManagementPoint -SiteSystemServerName "uswmps901.us.pinkertons.com"

New-CMBootableMedia -AllowUnknownMachine `
    -BootImage $mybootimage `
    -DistributionPoint $mydps `
    -ManagementPoint $mymps `
    -MediaMode SiteBased `
    -MediaType CdDvd `
    -path "C:\temp\sccmboot03192018.iso" 