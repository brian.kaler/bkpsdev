#Get-CMUserPrimaryDevice
#Brian Kaler
#4/2/2019
#Uses Get-CMUserPrimaryDevice and 

#add in any subscripts here
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = (Get-Item -path ".\").FullName
..\SCCM\Get-CMUserPrimarydevice.ps1
..\AD\Add-ComputerstoAdgroup.ps1
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
set-location SSS:\



$users = Get-CMCollectionMember -CollectionName "Citrix Receiver 4.9" | select -ExpandProperty SMSID
$report = @()
foreach($u in $users){
    $report += Get-CMUserPrimaryDevice -cmuser $u
}

Add-ComputerstoAdgroup -members $report -groupname "SAFES_BETA_COMPUTERS"
