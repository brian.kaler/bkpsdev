
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1"  
# Set the current location to be the site code. 
Set-Location $SiteCode":\"  

$adrname = "*Windows Server*"

$myadr = Get-CMSoftwareUpdateGroup | Where {$_.LocalizedDisplayname -like $adrname} | sort DateCreated -Descending | select -first 1 
$myupdates = $myadr | Get-CMSoftwareUpdate

    
foreach($update in $myupdates){
    $pattern = "KB\d*"
    $mysummary = New-Object -TypeName psobject
    #$mysummary | add-member -MemberType NoteProperty -name KBID -value ($update.LocalizedDisplayName | Select-String -Pattern $pattern -AllMatches)
    $mysummary | add-member -MemberType NoteProperty -name Name -value ($update.LocalizedDisplayName)
    $mysummary | add-member -MemberType NoteProperty -name Severity -Value ($update.SeverityName)
    $mysummary | add-member -MemberType NoteProperty -name URL -Value ($update.LocalizedInformativeURL)

    write-host $mysummary
    }