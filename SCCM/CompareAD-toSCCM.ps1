

#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = "C:\bkpsdev\sccm"
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
set-location SSS:\

$mylist = Get-ADComputer -filter * -SearchBase "OU=Servers,DC=us,DC=pinkertons,DC=com" -Server USWDCS003 -Properties Name,WsiServerEnvironment | select Name,WsiServerEnvironment

$PrimaryReport = @()


foreach($pc in $mylist){
    If(Get-CMDevice -Name $pc.name -fast){   
        $primaryreport += [PSCustomObject]@{
        Name = $pc.name
        SCCMStatus = "Found"
        }
        Write-Host "Found: "($pc.name)
    }
    else
    {
        $primaryreport += [PSCustomObject]@{
        Name = $pc.name
        SCCMStatus = "Not found"
    }
    Write-Host "Not Found: "($pc.name)
    }
    
    
}