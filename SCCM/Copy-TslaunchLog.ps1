﻿$source = "c:\windows\temp\tslaunch.log"
$destinationlog = "\\napwcfs01\itn_home01\SCCM\Logs\TSLaunch\"+$env:computername+"-TSLaunch.log"

Try{
$copieditem = Copy-Item -path $source -Destination $destinationlog -ErrorAction Stop
}
Catch{
$result = $_.Exception.Message
}
if($result -ne $null){
Write-Error -Message $result
}
Else{
    if($copieditem -ne $null){
    Write-Verbose -Message "TSLaunch was copied:"$copieditem    
    }else
    {
    Write-Verbose -Message "TSLaunch may have not copied"
    }

}
