#New-PhasedCollection
#Written by Brian Kaler
#3/7/2019
#Description
#Creates multiple collections based off an existing system center collection


$scriptname = "New-PhasedCollection"
#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = "C:\bkpsdev\sccm"
$scriptroot = "\\SNA-H5BPCV2\c$\bkpsdev\sccm"
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 

Function Add-DeviceDirectMembership{
    Param(
    [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [String]$dmcollection,
    [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [String]$dmdevicename
    )
    $mydev = Get-CMDevice -Name $dmdevicename
    if($mydev -eq $null)
    {write-host "Device $s was not found in SCCM"}
    Else
    {
    $device = $mydev.name
    Write-Host "Attempting to add $device to $dmcollection"
    Add-CMDeviceCollectionDirectMembershipRule -CollectionName $dmcollection -Resource $mydev
    }
}

Function Add-UserDirectMembership{
    Param(
    [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [String]$dmcollection,
    [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [String]$dmusername
    )
    $myuser = Get-CMUser -Name $dmusername
    if($myuser -eq $null)
    {write-host "User $s was not found in SCCM"}
    Else
    {
    $user = $myuser.resourceid
    $username = $myuser.smsid
    Write-Host "Attempting to add $username to $dmcollection"
    Add-CMUserCollectionDirectMembershipRule -CollectionName $dmcollection -ResourceID $user
    }
    }
    

Function Add-DirectMembership{
    Param(
    [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [ValidateSet("User","Device")]
    [String]$dmtype,
    [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [String]$dmcollectionname,
    [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [String]$dmidentity
    )
    If($dmtype -eq "Device"){
        Add-DeviceDirectMembership -dmcollection $dmcollectionname -dmdevicename $dmidentity
    }
    If($dmtype -eq "User"){
        Add-UserDirectMembership -dmcollection $dmcollectionname -dmusername $dmidentity
    }
}

Function New-PhasedCollection(){
Param(
[Parameter(Mandatory=$true)]
[Int]$count,
[Parameter(Mandatory=$true)]
[string]$collectionname,
[Parameter(Mandatory=$true)]
[string]$newcollectionname,
[Parameter(Mandatory=$true)]
[ValidateSet("User","Device")]
[string]$coltype
)
    #Gather our specificed collection members

    [system.collections.arraylist]$allmembers = Get-CMCollectionMember -CollectionName $collectionname

    #Based on collection type create collections
    $allsplitmembers = @()
    $loopcnt = 0
    Do{
        $thesplit = ($allmembers | Get-Random -Count $count)
        foreach($i in $thesplit){
                $allsplitmembers += [PSCustomObject]@{
                    Name = "SplitCollection"
                    Computer = $i.name
                    User = $i.SMSID
                    Group = $loopcnt
                }
                $allmembers.Remove($i)
        }
        $loopcnt++
    } Until (($allmembers | measure).count -eq 0) 
    #get the limits of that collection.
    $groupcount = [Math]::Floor(($allsplitmembers.count / $count)) + 1
    Switch($coltype){
        #for devices
        "Device" {
                #Create our collections
                Foreach($i in 1..$groupcount){
                    #generate our collection groups by groupcount
                    $cmname = $newcollectionname+$i
                    $cmname = $newcollectionname+'{0:00}' -f [int]$i
                    if((Get-CMCollection -name $cmname) -eq $null){
                    New-CMCollection -name $cmname -CollectionType Device -Comment "Autogeneratedby: New-PhasedCollection" -LimitingCollectionName "All Systems"
                    }
                }
                #take each group of X counted machines and add them to our collections
                foreach($g in $allsplitmembers){
                    $mysplitgrp = $newcollectionname +'{0:00}' -f [int]$g.group
                    add-directmembership -dmtype Device -dmcollection $mysplitgrp -dmidentity $g.computer
                }
            }
        "User" {
        #for users
            #Create our collections
            Foreach($i in 1..$groupcount){
                #generate our collection groups by groupcount
                $cmname = $newcollectionname+$i
                $cmname = $newcollectionname+'{0:00}' -f [int]$i
                if((Get-CMCollection -name $cmname) -eq $null){
                New-CMCollection -name $cmname -CollectionType User -Comment "Autogeneratedby: New-PhasedCollection" -LimitingCollectionName "All Users"
                }
            }
            #take each group of X counted machines and add them to our collections
            foreach($g in $allsplitmembers){
                $mysplitgrp = $newcollectionname +'{0:00}' -f [int]$g.group
                add-directmembership -dmtype User -dmcollection $mysplitgrp -dmidentity $g.computer
            }
        }
    }
}


#examples
$usercollectionname = "New users since (11/19/2019)"
$mygroupofcollections = @("PNKCAN Workstations")


foreach($group in $mygroupofcollections){
    $mycollectionname = $group
    $mynewcollectionname = "PHASED-ZScaler_CAN_"
    New-PhasedCollection -coltype device -count 100 -collectionname $mycollectionname -newcollectionname $mynewcollectionname 
}


#foreach($group in $mygroupofcollections){
#    $mycollectionname = $group
#    $mynewcollectionname = "Friends of IT-Devices-11.20.2019-"
#    New-PhasedCollection -coltype device -count 25 -collectionname $mycollectionname -newcollectionname $mynewcollectionname 
#}

#$newfituserscol = "NewUsers-11.21.2019-"
#New-PhasedCollection -coltype user -count 25 -collectionname $usercollectionname -newcollectionname $newfituserscol

