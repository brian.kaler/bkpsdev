﻿$mygroup = get-adgroup -filter * | where {$_.name -match "MAINT-SERVER-ONDEMAND"}
#FOR MANUAL ENTRY - edit the below, otherwise use CSV
#$members = "CATTMF001","CATFTL001","CATFTL002","CATIMX001","CATIWS001","CATMEX001","CATMWB001","CATMSQ001","CATMSQ002","CATMSQ003","CATMSQ004","CATMSP001","CATMSP002","CATNAP001","CATMRP001","CATMRP002","CATDOM012","CATQST001","CATQST002","ustacc001","USTACC002","USTADS001","ustavi001","USTEPO001","ustftl001","ustftl002","ustimx001","USTIMX005"

#create a CSV with "machine" for the header and one col for import.
$members = get-content -Path ".\sourcecomputers.txt"



foreach($m in $members){
    write-host $m
    $pc = get-adcomputer -filter * | where {$_.name -match $m}
    add-ADGroupMember -identity $mygroup -members $pc -ErrorAction Inquire -server uswdcs001.us.pinkertons.com
    }



Get-ADGroupMember -identity $mygroup