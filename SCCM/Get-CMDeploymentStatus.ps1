#Get-CMDeploymentUsers
#Brian Kaler
#4/2/2019
#Returns an object with the usernames involved in a deployment

#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = "C:\bkpsdev\sccm"
#Include the subscripts
. $scriptroot\Add-Directmembership.ps1
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
set-location SSS:\

$deployments = Get-CMSoftwareUpdateDeployment | where {$_.EnforcementDeadline -gt (get-date).adddays(-1)}

foreach($dp in $deployments){
    Get-CMSoftwareUpdateDeploymentStatus -InputObject $dp
}