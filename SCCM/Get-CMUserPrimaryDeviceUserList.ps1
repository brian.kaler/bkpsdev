#Get-CMUserPrimaryDeviceUserList
#Brian Kaler
#4/2/2019
#Gets all primary devices based on the provided username

#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = (Get-Item -path ".\").FullName
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
set-location SSS:\

$mydevicelist = "C:\temp\myprimarydevicelist.txt"
Function Get-CMUserPrimaryDeviceName(){
    [Parameter(Mandatory)]
    [string[]]$usernames
    $report = @()
    foreach($targetuser in $users){
        $report += Get-CMUserPrimaryDeviceName -username $targetuser | select
    }
    Return $report
}

