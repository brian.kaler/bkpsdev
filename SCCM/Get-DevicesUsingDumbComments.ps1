﻿#add-directmembership

#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = "C:\bkpsdev\sccm"
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
set-location SSS:\




Function ConvertTo-PSCustomObject{
    Param($InputObject)
    $out = "[PSCustomObject]@{`n"
    $Properties = $InputObject | Get-Member | Where MemberType -eq Property
    ForEach ($prop in $Properties){
        $name = $prop.Name
        if ([String]::IsNullOrEmpty($InputObject.$name)){
            $value = $null
        }
        else {
            $value = $InputObject.$name
        }
 
        $out += "`t$name = '$value'`n"
    }
 
    $out += "}"
    $out
}

$myobj = @()
foreach($u in $moreusers){
    $firstname = $u.split(" ")[0]
    $lastname = $u.split(" ")[1]

$myobj += get-aduser -filter "(surname -eq '$lastname') -and (givenname -eq '$firstname')" -properties givenname,surname,samaccountname,mail
}



Function Add-DirectMembership{
Param(
[String]$dmcollection,
[String]$dmdevicename
)

$mydev = Get-CMDevice -Name $dmdevicename
if($mydev -eq $null)
{write-host "Device $s was not found in SCCM"}
Else
{
$device = $mydev.name
Write-Host "Attempting to add $dmdevicename to $dmcollection"
Add-CMDeviceCollectionDirectMembershipRule -CollectionName $dmcollection -Resource $mydev
}
}





foreach($dev in $mydevices){
    foreach($u in $myobj){
        if($dev.primaryuser -ne $null){
        if($u.samaccountname -like $dev.primaryuser.split("\")[1])
    {Add-DirectMembership -dmcollection "OSU-W10-1809-REQUIRED-UPGRADE" -dmdevicename $dev.name}
    }
    }
}
