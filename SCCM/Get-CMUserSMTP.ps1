#Get-CMUserColSMTP
#Brian Kaler
#4/2/2019
#Gets all users from a SCCM Collection and returns their smtp address from AD.

$mycollectionname = "DEPLOYMENT-CITRIX-SOCAL-GROUP1-2to11"

#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = (Get-Item -path ".\").FullName
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
set-location SSS:\

Function Get-CMUserColSMTP(){
    param(
        [Parameter(Mandatory=$true)]
        [string]$CollectionName
    )
    #Get the list of users from the SCCM collection, return an attribute we can use to lookup
    $cmusers = Get-CMUser -CollectionName $collectionName | select Name,Smsid
    #Get all AD Users
    $adusers = Get-ADUser -filter * -Properties Samaccountname,mail | select SamAccountname,mail
    #Custom object returned
    $deployusers = @()
    #Query against the AD users
    foreach($cmuser in $cmusers){
        foreach($aduser in $adusers){
            $myuser = "pnkus\"+$aduser.samaccountname
            If($cmuser.smsid -like $myuser){
                write-host "Processing: " $aduser.samaccountname
                $deployusers += [PSCustomObject]@{
                    Name = "SMTPUsers"
                    User = $cmuser.SMSID
                    FullName = $cmuser.Name
                    EmailAddress = $aduser.mail
                }
            }
        }
    }
    Return $deployusers
}

$report = Get-CMUserColSMTP -CollectionName $mycollectionname

If($report -ne $null){  
    $fullpath = "C:\temp\cmusercolsmtp\"+(Get-Date -format MMddyyyyhhmmss)+".xls"
    $report | Export-Excel -Path $fullpath 
    } 