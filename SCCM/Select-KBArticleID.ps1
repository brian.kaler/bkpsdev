﻿#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1"  
# Set the current location to be the site code. 
Set-Location $SiteCode":\"  

$adrname = "ADR - Windows Server 2019-02-15 11:13:11"
$myupdates = Get-CMSoftwareUpdateGroup -Name $adrname | Get-CMSoftwareUpdate

foreach($update in $myupdates){
    $pattern = "KB\d*"
    $kbstring = $update.LocalizedDisplayName | Select-String -Pattern $pattern -AllMatches
    write-host $kbstring.matches | ForEach-Object {$_.value}
    }