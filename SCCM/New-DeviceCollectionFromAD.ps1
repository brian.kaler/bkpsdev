﻿#New-CollectionFromAD
#Written by Brian Kaler
#10/22/2018
#Description
#Creates a SCCM Device collection based off of AD Membership
#Returns new collection

Function Add-CollectionFromAD(){
Param(
    [Parameter(Mandatory=$true)]
    [String]$collectionname,
    [Parameter(Mandatory=$true)]
    [String]$queryname,
    [Parameter(Mandatory=$true)]
    [String]$limitingcollectionid,
    [Parameter(Mandatory=$true)]
    [String]$adgroupname
    )
$Scriptname = "Add-CollectionFromAD"
#get our default AD Group template and modify it accordingly
$querytemplate = (Get-CMQuery -name "Machines in AD Group XXX" | select -expandproperty Expression).Replace("YOURDOMAIN\\GROUPNAME",$adgroupname)

#create our query 
If(Get-cmquery -Name $queryname){
    Set-CMQuery -name $queryname -Expression $querytemplate -Comment $comment -LimitToCollectionId $limitingcollectionid
    $query = Get-cmquery -Name $queryname
    }
    else
    {
    $query = New-CMQuery -name $queryname -Comment $comment -LimitToCollectionId $limitingcollectionid -Expression $querytemplate
    }

#add the collection if it does already exist
If(Get-CMCollection -Name $collectionname){
    $collection = Get-CMCollection -Name $collectionname
    }
    Else
    {
    New-CMCollection -name $collectionname -CollectionType Device -LimitingCollectionid $limitingcollectionid -RefreshType Periodic -RefreshSchedule (Get-CMCollection -id $limitingcollectionid).RefreshSchedule[0] -Comment $comment -verbose:$false | Out-Null
    $collection = Get-CMCollection -Name $collectionname
    }

#populate the collection, if the query already exists remove it and add again to keep queries from growing out of control
$expression = $query.expression
If(Get-CMDeviceCollectionQueryMembershipRule -Collection $collection -rulename $queryname){
    Write-Host "WARNING: A Query rule matching the name $queryname already exists, existing query will be updated"
    Remove-CMDeviceCollectionQueryMembershipRule -Collection $collection -rulename $queryname -force -confirm:$false
    Add-CMDeviceCollectionQueryMembershipRule -Collection $collection -QueryExpression $expression -RuleName $queryname
    }
Else{
    Add-CMDeviceCollectionQueryMembershipRule -Collection $collection -QueryExpression $expression -RuleName $queryname
    }

#write some output
write-host "INFO: $scriptname completed, Collection: $collectionname will be updated with the contents of AD periodically"
write-host "INFO: Please note that your Collection: $collectionname is created in the root directory of `nDevice Collections and must be moved into it's appropriate folder structure in SCCM"

Return $collection
}

