﻿
$alldeployments = Get-CMDeployment
$cutoff = (get-date).AddDays(-180)
$report = @()
foreach($deploy in $alldeployments){
    If($deploy.deploymenttime -lt $cutoff){
        $report += $deploy.applicationname
    }
}

$report | unique