﻿#Add-CollectionFromAD
#Written by Brian Kaler
#10/22/2018
#Description
#Creates a SCCM User/Device collection based off of AD Membership

#initial setup of some other sccm parameters
$Scriptname = "Add-CollectionFromAD"
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
Write-host "INFO: Loading subscripts and modules, please wait..."
# Import the ConfigurationManager.psd1 module  
Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1"
new-psdrive -Name "SSS" -PSProvider CMSite -Root "uswscm901.us.pinkertons.com" -Description "SSS Primary Site"  
# Set the current location to be the site code. 
Set-Location $SiteCode":\"  
#Import ad module
import-module ActiveDirectory

Function Add-CollectionFromAD(){
Param(
    [Parameter(Mandatory=$true)]
    [String]$collectionname,
    [Parameter(Mandatory=$true)]
    [String]$queryname,
    [Parameter(Mandatory=$true)]
    [String]$querytemplate,
    [Parameter(Mandatory=$true)]
    [String]$limitingcollectionid,
    [Parameter(Mandatory=$true)]
    [String]$adgroupname,
    [Parameter(Mandatory=$true)]
    [String]$UserorDevice
    )


Switch($UserorDevice){
    Device{
        #create our query if it does not already exist
        If(Get-cmquery -Name $queryname){
            Set-CMQuery -name $queryname -Expression $querytemplate -Comment $comment -LimitToCollectionId $limitingcollectionid -targetclassname "System Resource"
            $query = Get-cmquery -Name $queryname
            }
            else
            {
            $query = New-CMQuery -name $queryname -Comment $comment -LimitToCollectionId $limitingcollectionid -Expression $querytemplate -targetclassname "System Resource"
            }
            #add the collection if it does not already exist
            If(Get-CMCollection -Name $collectionname){
                $collection = Get-CMCollection -Name $collectionname
                }
                Else
                {
                    New-CMCollection -name $collectionname -CollectionType device -LimitingCollectionid $limitingcollectionid -RefreshType Periodic -RefreshSchedule (Get-CMCollection -id $limitingcollectionid).RefreshSchedule[0] -Comment $comment
                }
        $collection = Get-CMCollection -Name $collectionname
        #populate the collection
        $expression = $query.expression
        If(Get-CMDeviceCollectionQueryMembershipRule -Collection $collection -rulename $queryname){
            Write-Host "Warning: A Query rule matching the name $queryname already exists, existing query will be updated"
            Remove-CMDeviceCollectionQueryMembershipRule -Collection $collection -rulename $queryname -force -confirm:$false
            Add-CMDeviceCollectionQueryMembershipRule -Collection $collection -QueryExpression $expression -RuleName $queryname
            }
        Else{
            Add-CMDeviceCollectionQueryMembershipRule -Collection $collection -QueryExpression $expression -RuleName $queryname
            }

        }

    User{
        #create our query if it does not already exist
        If(Get-cmquery -Name $queryname){
            Set-CMQuery -name $queryname -Expression $querytemplate -Comment $comment -LimitToCollectionId $limitingcollectionid -targetclassname "User Resource"
            $query = Get-cmquery -Name $queryname
            }
            else
            {
            $query = New-CMQuery -name $queryname -Comment $comment -LimitToCollectionId $limitingcollectionid -Expression $querytemplate -targetclassname "User Resource"
            }
            #add the collection if it does not already exist
            If(Get-CMCollection -Name $collectionname){
                $collection = Get-CMCollection -Name $collectionname
                }
                Else
                {
                    New-CMCollection -name $collectionname -CollectionType user -LimitingCollectionid $limitingcollectionid -RefreshType Periodic -RefreshSchedule (Get-CMCollection -id $limitingcollectionid).RefreshSchedule[0] -Comment $comment
                }
            $collection = Get-CMCollection -Name $collectionname
            #populate the collection
            $expression = $query.expression
            If(Get-CMUserCollectionQueryMembershipRule -Collection $collection -rulename $queryname){
                Write-Host "Warning: A Query rule matching the name $queryname already exists, existing query will be updated"
                Remove-CMUserCollectionQueryMembershipRule -Collection $collection -rulename $queryname -force -confirm:$false
                Add-CMUserCollectionQueryMembershipRule -Collection $collection -QueryExpression $expression -RuleName $queryname
                }
            Else{
                Add-CMUserCollectionQueryMembershipRule -Collection $collection -QueryExpression $expression -RuleName $queryname
                }
        }


}

#write some output
write-host "$scriptname completed, Collection: $collectionname will be updated with the contents of AD periodically"

}

#examples
$myadgroups = Get-ADGroup -filter * | ?{$_.name -like "*Infrastructure*"} | ?{$_.name -notlike "*LOCAL*"} | select -expandproperty name


foreach($group in $myadgroups){
    $myadgroupname = "PNKUS\\"+$group
    $myquerytemplate = (Get-CMQuery -name "Users in AD Group" | select -expandproperty Expression).Replace("<<REPLACEME>>",$myadgroupname)
    Add-CollectionFromAD -collectionname $group -queryname ($group+"-Users in AD Group") -limitingcollectionid "SMS00002" -adgroupname $myadgroupname -userordevice user -querytemplate $myquerytemplate
}


#maybe useful for some accidental extra rules.
#foreach($col in $mysafescols){
#    Remove-CMDeviceCollectionQueryMembershipRule -CollectionName $col.name -Rulename "Users in AD Group" -force
#}


