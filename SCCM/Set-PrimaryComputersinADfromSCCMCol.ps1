#Set-PrimaryComputersinAD
#Brian Kaler
#4/3/2019
#Takes a source AD Group of Users, determines their primary computers, adds them to a destination AD Group of computers

#inputs
$SourceCollection = "Citrix Receiver 4.9"
$DestinationDeviceGroup = "SAFES_Beta_Computers"
#initial setup of some other sccm parameters
$Scriptname = "Set-PrimaryComputersinAD"
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
Write-host "INFO: Loading subscripts and modules, please wait..."
# Import the ConfigurationManager.psd1 module  
Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1"
new-psdrive -Name "SSS" -PSProvider CMSite -Root "uswscm901.us.pinkertons.com" -Description "SSS Primary Site"  
# Set the current location to be the site code. 
Set-Location $SiteCode":\"  
#Import ad module
import-module ActiveDirectory
#get a list of users from a security group
$users = (Get-CMUser -Collectionname $SourceCollection).smsid
#identify) the group we will import devices into
$devicegroup = (get-adgroup -filter * | where {$_.name -match $DestinationDeviceGroup}).name
#get each member of the group and collect their primary devices
$pedevices = @()
foreach($u in $users){
    $sccmusername = $u
    $pdevices = (Get-CMUserDeviceAffinity -username $sccmusername).resourcename
    #add each device individually into the DestinationDeviceGroup
    foreach($dev in $pdevices){
        $adcomputer = Get-ADComputer -identity $dev
        Write-Host "Attempting to add $dev to $devicegroup"
        Add-ADGroupMember -identity $devicegroup -members $adcomputer
    }
    
}

