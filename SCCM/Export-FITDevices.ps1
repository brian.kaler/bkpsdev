#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = (Get-Item -path ".\").FullName
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
set-location SSS:\

#load the excel module
import-module importexcel

$reportsdir = "\\napwcfs01\itn_home01\SCCM\Reports"
$timestamp = (get-date -Format ddMMyyyy-hhmmss)

$fot1607 = Get-CMCollection -Name "Friends of IT Devices - Windows 10 1607" | Get-CMCollectionMember
$fot1803 = Get-CMCollection -Name "Friends of IT Devices - Windows 10 1803" | Get-CMCollectionMember
$fot1809 = Get-CMCollection -Name "Friends of IT Devices - Windows 10 1809" | Get-CMCollectionMember

Function ReturnPrimaryUserwithEmail(){
Param(
    [Parameter(Mandatory)]
    [Object]$CMCollection
    )
    $devices = Get-CMCollection -name $CMCollection | Get-CMCollectionMember | `
Select Name,PrimaryUser,Lastlogonuser,LastDDR

$finalreport = @()
    foreach($dev in $devices){
    $report = New-Object "PSCustomObject"
    $userident =  (($dev.primaryuser).split("\")[1])
    $user = Get-ADUser -identity $userident -Properties Name,givenname,surname,Mail
    $report | Add-Member -MemberType NoteProperty -Name ComputerName -Value $dev.Name -Verbose
    $report | Add-Member -MemberType NoteProperty -Name PrimaryUser -Value $dev.PrimaryUser -Verbose
    $report | Add-Member -MemberType NoteProperty -Name ADUsername -Value $user.name -Verbose
    $report | Add-Member -MemberType NoteProperty -Name EmailAddress -Value $user.Mail -Verbose
    $report | Add-Member -MemberType NoteProperty -Name LastLogonUser -Value $dev.LastLogonUser -Verbose
    $report | Add-Member -MemberType NoteProperty -Name LastDDR -Value $dev.LastDDR -Verbose
    $finalreport += $report
    }
    Return $finalreport
}

$1607report = ReturnPrimaryUserwithEmail -CMCollection "Friends of IT Devices - Windows 10 1607"
$1803report = ReturnPrimaryUserwithEmail -CMCollection "Friends of IT Devices - Windows 10 1803"
$1809report = ReturnPrimaryUserwithEmail -CMCollection "Friends of IT Devices - Windows 10 1809"

$1607report | Export-excel -path "c:\temp\FOT1607-($timestamp).xls"
$1803report | Export-excel -path "c:\temp\FOT1803-($timestamp).xls"
$1809report | Export-excel -path "c:\temp\FOT1809-($timestamp).xls"

