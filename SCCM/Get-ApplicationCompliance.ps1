<#
.Synopsis 
     Script used for application detection methods
.Notes
    FileName:    SoftwareDetectionScript.PS1
    Author:      PatchMyPC
    Contact:     support@patchmypc.com
    Created:     2019-07-24
    Updated:     2019-10-02
    License:     Copyright Patch My PC, LLC all rights reserved

    Version 1.0
        - Initial release
    Version 1.1
        - Get-InstalledSoftwares reads only relevant registry values to workaround Get-ItemProperty bug on corrupted registry data
    Version 1.2
        - Enhancement on versions comparison
    Version 1.3
        - Fix compatibility issue with PowerShell v2
#>

#Set variables#
$ScriptVersion = '1.3'
$AppToSearch = '*Malwarebytes Anti-Malware*'  # A pattern used to search for displayName in the uninstall registry key.
$AppToAvoid = ''                # A pattern used to reject similar applications.
$AppMSICodeToSearch = '1'	# A MSI code used to search for in the uninstall registry key.
$AppVersionToSearch = '1'     # Version >= check to determine if application is installed and greater than or equal to this version.

$ScriptLogFilePath = 'C:\Windows\Temp\PatchMyPC-SoftwareDetectionScript.log'

Function Test-Administrator
{
	$currentUser = [Security.Principal.WindowsIdentity]::GetCurrent()
	(New-Object Security.Principal.WindowsPrincipal $currentUser).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}

Function Set-LogPath
# Configures the full path to the log file depending on whether or not the CCM folder exists.
{
    $LogFile = 'PatchMyPC-SoftwareDetectionScript.log'
	$LogPath = $env:temp
	
	if(Test-Administrator)
	{
		# Script is running Administrator privileges
		if(Test-Path -Path 'C:\Windows\CCM\Logs') 
        {
            $LogPath = 'C:\Windows\CCM\Logs'
        }
	}    
    
    $global:ScriptLogFilePath = "$LogPath\$LogFile"
}

Function Write-Log
#Write the log file if the global variable is set
{
    param (
    [Parameter(Mandatory = $true)]
    [string]$Message,
    [Parameter()]
    [ValidateSet(1, 2, 3)]
    [string]$LogLevel=1 )
   
    $TimeGenerated = "$(Get-Date -Format HH:mm:ss).$((Get-Date).Millisecond)+000"
    $Line = '<![LOG[{0}]LOG]!><time="{1}" date="{2}" component="{3}" context="" type="{4}" thread="" file="">'
    $LineFormat = $Message, $TimeGenerated, (Get-Date -Format MM-dd-yyyy), "$($MyInvocation.ScriptName | Split-Path -Leaf):$($MyInvocation.ScriptLineNumber)", $LogLevel
    $Line = $Line -f $LineFormat
    Add-Content -Value $Line -Path $global:ScriptLogFilePath -ErrorAction SilentlyContinue
}

Function Clear-Log
# Delete the log file if bigger than $maxSize
{
    param (
        [Parameter(Mandatory = $true)][string]$maxSize
    )
    try 
    {
		if(Test-Path -Path $global:ScriptLogFilePath) 
        {
            if ((Get-Item $global:ScriptLogFilePath).length -gt $maxSize)
            {
                Remove-Item -Path $global:ScriptLogFilePath
                Start-Sleep -Seconds 1
            }				
        }
    }
    catch {Write-Log -Message "Unable to delete log file."}    
}

Function GetVersionFromName
{
    param ([Parameter(Mandatory = $true)] [string]$name)
    if ($name -match "\d+\.\d+(\.\d+)?(\.\d+)?")
    {
        return $Matches[0]
    }
    return ""
}

Function Get-VersionFromString
{
  param([string]$versionString)
  try
  {
    if($versionString -notlike '*.*')
    {
      $versionString += '.0.0.0'
    }
    [System.Version]$version = [System.Version]($versionString)
    $major = if($version.Major -eq -1){0}else{$version.Major}
    $minor = if($version.Minor -eq -1){0}else{$version.Minor}
    $build = if($version.Build -eq -1){0}else{$version.Build}
    $revision = if($version.Revision -eq -1){0}else{$version.Revision}
    
    return [System.Version]("$major.$minor.$build.$revision")
  }
  catch
  {
    return [System.Version]('0.0.0.0')
  }
}

Function Compare-Version
{
   param([string]$versionString1,[string]$versionString2)
   
   [System.Version]$version1 = Get-VersionFromString($versionString1)
   [System.Version]$version2 = Get-VersionFromString($versionString2)

   return $version1.CompareTo($version2)
}

Function Get-InstalledSoftwares{
    $regpath = @('HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*')
    $regpath64 = @('HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*')
    $propertyNames = 'DisplayName','UninstallString','DisplayVersion','PSChildName','Publisher','InstallDate'

    if (-not ([IntPtr]::Size -eq 4)) 
    {
        $regpath += 'HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*'
    }
    $32bit = Get-ItemProperty $regpath -Name $propertyNames -ErrorAction SilentlyContinue | .{process{if($_.DisplayName -and $_.UninstallString) { $_ } }} | Select-Object DisplayName, DisplayVersion, UninstallString, PSChildName, Publisher, InstallDate | Sort DisplayName
    $64bit = Get-ItemProperty $regpath64 -Name $propertyNames -ErrorAction SilentlyContinue | .{process{if($_.DisplayName -and $_.UninstallString) { $_ } }} | Select-Object DisplayName, DisplayVersion, UninstallString, PSChildName, Publisher, InstallDate | Sort DisplayName
    $result = $32bit + $64bit
    Return $result
}

Function IsInstalled
{
    param([string]$appName, [string]$except, [string]$appVersion, [string]$msiCode)

    $InstalledSoftwares = Get-InstalledSoftwares | Where-Object {
        $version = GetVersionFromName $_.DisplayName 
        ($_.DisplayName -like $appName -and -not($_.DisplayName -like $except) -and (($_.DisplayVersion -ne $null -and (Compare-Version $_.DisplayVersion.Trim() $appVersion) -ge 0) -or (Compare-Version $version $appVersion) -ge 0)) -or ($_.PSChildname -eq $msiCode)
    } #Search
    If ($InstalledSoftwares -eq $null) # No match found for DisplayName and DisplayVersion check
    {
        Write-Log -Message "No detection for $($appName) with version $($appVersion)"
        Return $false
    }
    Else
    {    
        foreach($Software in $InstalledSoftwares) # Found match
        {
            Write-Log -Message "Found $($Software.DisplayName) version $($Software.DisplayVersion) installed on $($Software.InstallDate)" -LogLevel 2
        }
        Return $true
    }
}

# Main program
Set-LogPath
Clear-Log 2mb
Write-Log -Message "*** Starting detection for $($AppToSearch) $(if($AppToAvoid -ne """") {"except $AppToAvoid"}) with version $($AppVersionToSearch)"
Write-Log -Message "Detection script version $ScriptVersion"
Write-Log -Message "Running as $env:username $(if(Test-Administrator) {"[Administrator]"} Else {"[Not Administrator]"}) on $env:computername"

$detectionResult = IsInstalled $AppToSearch $AppToAvoid $AppVersionToSearch $AppMSICodeToSearch

if($detectionResult -eq $true)
{            
	return $true
}
else{
    return $false
}
Exit 0
# SIG # Begin signature block
# MIIU6gYJKoZIhvcNAQcCoIIU2zCCFNcCAQExDzANBglghkgBZQMEAgEFADB5Bgor
# BgEEAYI3AgEEoGswaTA0BgorBgEEAYI3AgEeMCYCAwEAAAQQH8w7YFlLCE63JNLG
# KX7zUQIBAAIBAAIBAAIBAAIBADAxMA0GCWCGSAFlAwQCAQUABCCR2zEPFVqqVZAt
# uT+JRv1Vthfe133uFbs8za44yYVWbKCCEEswggMIMIIB8KADAgECAhAUtnZ5jdCE
# qEk+0MycPrYkMA0GCSqGSIb3DQEBDQUAMBwxGjAYBgNVBAMMEVBhdGNoTXlQQyBT
# ZXJ2aWNlMB4XDTE5MTExODIyNDU0NFoXDTI0MTExODIyNDU0NFowHDEaMBgGA1UE
# AwwRUGF0Y2hNeVBDIFNlcnZpY2UwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK
# AoIBAQCzDzdorS+ew3HbYeVTMzgBpWfG9z+ZftrgC65556MSLZzTUeMk5Cld6uNi
# bAdFIsyWzWrbO0shYc1RTNtcRTAdqexhRoH/MyONoUKraqNcXKZ7XKNV1c1jP45r
# wHU4/Tf9JxSNwRFld1fYFRI620u0YyaYLKZfvrhXucP5zpoH2oaFc43tKKhfv+AX
# XKc08qFN3WsjiSdWopjosCdrJc2oGKxlfjFUn3NO5KvhnVDU33yHxPptX9NbokCP
# QxzS6WbOmciQjRZ/pRpbpic+dAkWlhR0EQ4oyWQbj1868tzAnBNqROhUDLnD3eQl
# uHD85/OhdML2ot0WHiSvxkRdAuS9AgMBAAGjRjBEMBMGA1UdJQQMMAoGCCsGAQUF
# BwMDMB0GA1UdDgQWBBQVZe9FFtuHqzxF4jYPd7NfT5M1ODAOBgNVHQ8BAf8EBAMC
# B4AwDQYJKoZIhvcNAQENBQADggEBACcFqsy6m55Ni4PRZ0Uv6v4aJ8G1RSa+svul
# NmH6K3nMpdYOhX/IYV8yUzZTVa+0rpad6EO0/TWYjT4lB6/BnKliD2PTtmi/00CO
# lKbLmX2DkWY1Gz5o86E8n+7LW8poGJCaCea5nXjtsyITKGsV2JVl8tuzJK0sAi4A
# 77H8lQOwXOiJ+9tdRLZxmEjXld7adrwAgWZSXEgacgaEoyadhAyIfSutPobk6NJP
# jLg1wi6Jw6kgXxkUmMxvbAo/MwGRPJRAhga6QpwtBN/zwb0GXJAxV+8Dp3ZE3XxK
# R+lxuezNIj4/NUEfzq3jhoMvzlZCh344Hf888ioSVvPVczRP/ZkwggZqMIIFUqAD
# AgECAhADAZoCOv9YsWvW1ermF/BmMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYT
# AlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2Vy
# dC5jb20xITAfBgNVBAMTGERpZ2lDZXJ0IEFzc3VyZWQgSUQgQ0EtMTAeFw0xNDEw
# MjIwMDAwMDBaFw0yNDEwMjIwMDAwMDBaMEcxCzAJBgNVBAYTAlVTMREwDwYDVQQK
# EwhEaWdpQ2VydDElMCMGA1UEAxMcRGlnaUNlcnQgVGltZXN0YW1wIFJlc3BvbmRl
# cjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKNkXfx8s+CCNeDg9sYq
# 5kl1O8xu4FOpnx9kWeZ8a39rjJ1V+JLjntVaY1sCSVDZg85vZu7dy4XpX6X51Id0
# iEQ7Gcnl9ZGfxhQ5rCTqqEsskYnMXij0ZLZQt/USs3OWCmejvmGfrvP9Enh1DqZb
# FP1FI46GRFV9GIYFjFWHeUhG98oOjafeTl/iqLYtWQJhiGFyGGi5uHzu5uc0LzF3
# gTAfuzYBje8n4/ea8EwxZI3j6/oZh6h+z+yMDDZbesF6uHjHyQYuRhDIjegEYNu8
# c3T6Ttj+qkDxss5wRoPp2kChWTrZFQlXmVYwk/PJYczQCMxr7GJCkawCwO+k8IkR
# j3cCAwEAAaOCAzUwggMxMA4GA1UdDwEB/wQEAwIHgDAMBgNVHRMBAf8EAjAAMBYG
# A1UdJQEB/wQMMAoGCCsGAQUFBwMIMIIBvwYDVR0gBIIBtjCCAbIwggGhBglghkgB
# hv1sBwEwggGSMCgGCCsGAQUFBwIBFhxodHRwczovL3d3dy5kaWdpY2VydC5jb20v
# Q1BTMIIBZAYIKwYBBQUHAgIwggFWHoIBUgBBAG4AeQAgAHUAcwBlACAAbwBmACAA
# dABoAGkAcwAgAEMAZQByAHQAaQBmAGkAYwBhAHQAZQAgAGMAbwBuAHMAdABpAHQA
# dQB0AGUAcwAgAGEAYwBjAGUAcAB0AGEAbgBjAGUAIABvAGYAIAB0AGgAZQAgAEQA
# aQBnAGkAQwBlAHIAdAAgAEMAUAAvAEMAUABTACAAYQBuAGQAIAB0AGgAZQAgAFIA
# ZQBsAHkAaQBuAGcAIABQAGEAcgB0AHkAIABBAGcAcgBlAGUAbQBlAG4AdAAgAHcA
# aABpAGMAaAAgAGwAaQBtAGkAdAAgAGwAaQBhAGIAaQBsAGkAdAB5ACAAYQBuAGQA
# IABhAHIAZQAgAGkAbgBjAG8AcgBwAG8AcgBhAHQAZQBkACAAaABlAHIAZQBpAG4A
# IABiAHkAIAByAGUAZgBlAHIAZQBuAGMAZQAuMAsGCWCGSAGG/WwDFTAfBgNVHSME
# GDAWgBQVABIrE5iymQftHt+ivlcNK2cCzTAdBgNVHQ4EFgQUYVpNJLZJMp1KKnka
# g0v0HonByn0wfQYDVR0fBHYwdDA4oDagNIYyaHR0cDovL2NybDMuZGlnaWNlcnQu
# Y29tL0RpZ2lDZXJ0QXNzdXJlZElEQ0EtMS5jcmwwOKA2oDSGMmh0dHA6Ly9jcmw0
# LmRpZ2ljZXJ0LmNvbS9EaWdpQ2VydEFzc3VyZWRJRENBLTEuY3JsMHcGCCsGAQUF
# BwEBBGswaTAkBggrBgEFBQcwAYYYaHR0cDovL29jc3AuZGlnaWNlcnQuY29tMEEG
# CCsGAQUFBzAChjVodHRwOi8vY2FjZXJ0cy5kaWdpY2VydC5jb20vRGlnaUNlcnRB
# c3N1cmVkSURDQS0xLmNydDANBgkqhkiG9w0BAQUFAAOCAQEAnSV+GzNNsiaBXJuG
# ziMgD4CH5Yj//7HUaiwx7ToXGXEXzakbvFoWOQCd42yE5FpA+94GAYw3+puxnSR+
# /iCkV61bt5qwYCbqaVchXTQvH3Gwg5QZBWs1kBCge5fH9j/n4hFBpr1i2fAnPTgd
# KG86Ugnw7HBi02JLsOBzppLA044x2C/jbRcTBu7kA7YUq/OPQ6dxnSHdFMoVXZJB
# 2vkPgdGZdA0mxA5/G7X1oPHGdwYoFenYk+VVFvC7Cqsc21xIJ2bIo4sKHOWV2q7E
# LlmgYd3a822iYemKC23sEhi991VUQAOSK2vCUcIKSK+w1G7g9BQKOhvjjz3Kr2qN
# e9zYRDCCBs0wggW1oAMCAQICEAb9+QOWA63qAArrPye7uhswDQYJKoZIhvcNAQEF
# BQAwZTELMAkGA1UEBhMCVVMxFTATBgNVBAoTDERpZ2lDZXJ0IEluYzEZMBcGA1UE
# CxMQd3d3LmRpZ2ljZXJ0LmNvbTEkMCIGA1UEAxMbRGlnaUNlcnQgQXNzdXJlZCBJ
# RCBSb290IENBMB4XDTA2MTExMDAwMDAwMFoXDTIxMTExMDAwMDAwMFowYjELMAkG
# A1UEBhMCVVMxFTATBgNVBAoTDERpZ2lDZXJ0IEluYzEZMBcGA1UECxMQd3d3LmRp
# Z2ljZXJ0LmNvbTEhMB8GA1UEAxMYRGlnaUNlcnQgQXNzdXJlZCBJRCBDQS0xMIIB
# IjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA6IItmfnKwkKVpYBzQHDSnlZU
# XKnE0kEGj8kz/E1FkVyBn+0snPgWWd+etSQVwpi5tHdJ3InECtqvy15r7a2wcTHr
# zzpADEZNk+yLejYIA6sMNP4YSYL+x8cxSIB8HqIPkg5QycaH6zY/2DDD/6b3+6LN
# b3Mj/qxWBZDwMiEWicZwiPkFl32jx0PdAug7Pe2xQaPtP77blUjE7h6z8rwMK5nQ
# xl0SQoHhg26Ccz8mSxSQrllmCsSNvtLOBq6thG9IhJtPQLnxTPKvmPv2zkBdXPao
# 8S+v7Iki8msYZbHBc63X8djPHgp0XEK4aH631XcKJ1Z8D2KkPzIUYJX9BwSiCQID
# AQABo4IDejCCA3YwDgYDVR0PAQH/BAQDAgGGMDsGA1UdJQQ0MDIGCCsGAQUFBwMB
# BggrBgEFBQcDAgYIKwYBBQUHAwMGCCsGAQUFBwMEBggrBgEFBQcDCDCCAdIGA1Ud
# IASCAckwggHFMIIBtAYKYIZIAYb9bAABBDCCAaQwOgYIKwYBBQUHAgEWLmh0dHA6
# Ly93d3cuZGlnaWNlcnQuY29tL3NzbC1jcHMtcmVwb3NpdG9yeS5odG0wggFkBggr
# BgEFBQcCAjCCAVYeggFSAEEAbgB5ACAAdQBzAGUAIABvAGYAIAB0AGgAaQBzACAA
# QwBlAHIAdABpAGYAaQBjAGEAdABlACAAYwBvAG4AcwB0AGkAdAB1AHQAZQBzACAA
# YQBjAGMAZQBwAHQAYQBuAGMAZQAgAG8AZgAgAHQAaABlACAARABpAGcAaQBDAGUA
# cgB0ACAAQwBQAC8AQwBQAFMAIABhAG4AZAAgAHQAaABlACAAUgBlAGwAeQBpAG4A
# ZwAgAFAAYQByAHQAeQAgAEEAZwByAGUAZQBtAGUAbgB0ACAAdwBoAGkAYwBoACAA
# bABpAG0AaQB0ACAAbABpAGEAYgBpAGwAaQB0AHkAIABhAG4AZAAgAGEAcgBlACAA
# aQBuAGMAbwByAHAAbwByAGEAdABlAGQAIABoAGUAcgBlAGkAbgAgAGIAeQAgAHIA
# ZQBmAGUAcgBlAG4AYwBlAC4wCwYJYIZIAYb9bAMVMBIGA1UdEwEB/wQIMAYBAf8C
# AQAweQYIKwYBBQUHAQEEbTBrMCQGCCsGAQUFBzABhhhodHRwOi8vb2NzcC5kaWdp
# Y2VydC5jb20wQwYIKwYBBQUHMAKGN2h0dHA6Ly9jYWNlcnRzLmRpZ2ljZXJ0LmNv
# bS9EaWdpQ2VydEFzc3VyZWRJRFJvb3RDQS5jcnQwgYEGA1UdHwR6MHgwOqA4oDaG
# NGh0dHA6Ly9jcmwzLmRpZ2ljZXJ0LmNvbS9EaWdpQ2VydEFzc3VyZWRJRFJvb3RD
# QS5jcmwwOqA4oDaGNGh0dHA6Ly9jcmw0LmRpZ2ljZXJ0LmNvbS9EaWdpQ2VydEFz
# c3VyZWRJRFJvb3RDQS5jcmwwHQYDVR0OBBYEFBUAEisTmLKZB+0e36K+Vw0rZwLN
# MB8GA1UdIwQYMBaAFEXroq/0ksuCMS1Ri6enIZ3zbcgPMA0GCSqGSIb3DQEBBQUA
# A4IBAQBGUD7Jtygkpzgdtlspr1LPUukxR6tWXHvVDQtBs+/sdR90OPKyXGGinJXD
# UOSCuSPRujqGcq04eKx1XRcXNHJHhZRW0eu7NoR3zCSl8wQZVann4+erYs37iy2Q
# wsDStZS9Xk+xBdIOPRqpFFumhjFiqKgz5Js5p8T1zh14dpQlc+Qqq8+cdkvtX8JL
# FuRLcEwAiR78xXm8TBJX/l/hHrwCXaj++wc4Tw3GXZG5D2dFzdaD7eeSDY2xaYxP
# +1ngIw/Sqq4AfO6cQg7PkdcntxbuD8O9fAqg7iwIVYUiuOsYGk38KiGtSTGDR5V3
# cdyxG0tLHBCcdxTBnU8vWpUIKRAmMYID9TCCA/ECAQEwMDAcMRowGAYDVQQDDBFQ
# YXRjaE15UEMgU2VydmljZQIQFLZ2eY3QhKhJPtDMnD62JDANBglghkgBZQMEAgEF
# AKCBhDAYBgorBgEEAYI3AgEMMQowCKACgAChAoAAMBkGCSqGSIb3DQEJAzEMBgor
# BgEEAYI3AgEEMBwGCisGAQQBgjcCAQsxDjAMBgorBgEEAYI3AgEVMC8GCSqGSIb3
# DQEJBDEiBCBeNsoeHpPVMLwySDhiU1nxWSLX40tyO16FXAEeEyhqXDANBgkqhkiG
# 9w0BAQEFAASCAQBIj9hjir9OHsFhNtjRfowL8COjPDig0r4mfDg+OT16Dji6J1el
# C3dl0TZ2WUEh7eDIiPrrcAVdEqSBiojRRowYxHd9gdemgs6lnxtng285WzxSAum0
# PZeRYvXTMlr8fHW7YWi5INohtiWnRcraLEf86L4mwePtCWDJReELzjNE7A73kr2I
# hEaG80CQDxcqLx8dg5HQcXg2i3fTXQcwCxKunMCwSq85GC0gmYb0ab7Bpak8WyFY
# wX9AfVjicti6vbDvUQoBBGsyxhd7NmDN0AJKXmDWsWmMMr4g8yoIi9g+VL/RHhpu
# l5Wq4eB8A9ehBYIt6I9wsflY/Vyae2c+zjaeoYICDzCCAgsGCSqGSIb3DQEJBjGC
# AfwwggH4AgEBMHYwYjELMAkGA1UEBhMCVVMxFTATBgNVBAoTDERpZ2lDZXJ0IElu
# YzEZMBcGA1UECxMQd3d3LmRpZ2ljZXJ0LmNvbTEhMB8GA1UEAxMYRGlnaUNlcnQg
# QXNzdXJlZCBJRCBDQS0xAhADAZoCOv9YsWvW1ermF/BmMAkGBSsOAwIaBQCgXTAY
# BgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0xOTExMTkx
# NjI2MDVaMCMGCSqGSIb3DQEJBDEWBBRaD6nqIOh0SAPobZKPcsKWJk8gcDANBgkq
# hkiG9w0BAQEFAASCAQAidxZtNgQfockKQqhblytEK6mgU8DABkwNJBtAqxzMiwLw
# wlCSqD6s+SLEZZ7Kb0arn9+DRu6cjtR5EAYbwZ/tjVPoifNNjqrNoQJKeV82jrk9
# mg0aIq7cMuAhi7cMyaTcK0Cs46FOv9BweuB7TFUMKWq0hJ+wK2moRQ8DpuBf6l8Y
# +RRStUHoRy3i+5jeTiSZKi6El8ptoWil5GXFgrQpm0m5zUzjV8bzNBdHdteZiG4v
# DVt9BZNEnfcZtXzv6EHvmhOAo4EMYapDvP9Tg0Y7v6ceJZBFMtoox5c7IWa1JUvg
# 4fR/2T5O5O4sq5VnVuCJncESkxClr/xB8FAqU0Fb
# SIG # End signature block
