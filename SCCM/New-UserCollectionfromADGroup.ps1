#New-UserCollectionfromADGroup
#Creates a SCCM Collection based on a query of the AD domain
#Writtenby Brian Kaler
#5/13/2019


#Import ad module
import-module ActiveDirectory
#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = ".\"
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
set-location SSS:\



#Parameter examples
#$myadgroups = Get-ADGroup -filter * | ?{$_.name -like "*SAFES*"} | select -expandproperty name
$myadgroups = get-content "C:\temp\availableapplicationssccmad11202019.txt"
$limitingcollectionid = "SMS00002"
$queryname = "Users in AD Group"
$collectionname =

foreach($adgroupname in $myadgroups){
    $querytemplate = (Get-CMQuery -name "Users in AD Group" | select -expandproperty Expression).Replace("<<REPLACEME>>",$adgroupname)
    $newqueryname = $queryname+"-"+$adgroupname
  #create our query 
    If(Get-cmquery -Name $newqueryname){
        Set-CMQuery -name $newqueryname -Expression $querytemplate -Comment $comment -LimitToCollectionId $limitingcollectionid
        $query = Get-cmquery -Name $newqueryname
        }
        else
        {
        $query = New-CMQuery -name $newqueryname -Comment $comment -LimitToCollectionId $limitingcollectionid -Expression $querytemplate
        }
  
    #add the collection if it does already exist
    If(Get-CMCollection -Name $adgroupname){
      $collection = Get-CMCollection -Name $adgroupname
      }
      Else
      {
      New-CMCollection -name $adgroupname -CollectionType User -LimitingCollectionid $limitingcollectionid -RefreshType Periodic -RefreshSchedule (Get-CMCollection -id $limitingcollectionid).RefreshSchedule[0] -Comment $comment

      $collection = Get-CMCollection -Name $adgroupname
      }
    #populate the collection, if the query already exists remove it and add again to keep queries from growing out of control
    $expression = $query.expression
    $newqueryobj = get-cmquery -name $newqueryname
    IF($newqueryobj -ne $null){
    If(Get-CMUserCollectionQueryMembershipRule -Collection $adgroupname -rulename $newqueryobj){
        Write-Host "WARNING: A Query rule matching the name $queryname already exists, existing query will be updated"
        Remove-CMUserCollectionQueryMembershipRule -Collection $adgroupname -rulename $newqueryobj  -force -confirm:$false
        Add-CMUserCollectionQueryMembershipRule -Collection $adgroupname -QueryExpression $expression -RuleName $newqueryobj
        }
    Else{
        Add-CMUserCollectionQueryMembershipRule -Collection $adgroupname -QueryExpression $expression -RuleName $newqueryobj
        }
      }
    else {
      Write-Host "No Query was found, please examine your variables"
    }
}
