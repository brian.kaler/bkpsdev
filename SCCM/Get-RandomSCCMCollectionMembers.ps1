﻿#Get-RandomSCCMCollectionMembers
#Written By Brian Kaler
#12/10/2018
#Small function to take a number of SCCM collection members at random
#todo: Get it to output as either SCCM collection or String (string currently done)

$scriptname = "Get-RandomSCCMCollectionMembers"
#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = (Get-Item -path ".\").FullName
#Include the subscripts
. $scriptroot\Add-computerstoadgroup.ps1
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
#import ActiveDirectory
$adsmod = Import-Module ActiveDirectory -PassThru
If($scmod -eq $null){
    Write-Host "ERROR: Could not find Configuration Manager Module, please confirm SCCM Admin Console is installed on this machine"
    Sleep -seconds 15
    exit
    }
If($adsmod -eq $null){
    Write-Host "ERROR: Could not find Active Directory Module, please confirm it is installed on the machine, `ntry running 'install-module activedirectory' as a potential fix"
    Sleep -seconds 15
    exit
    }
# Set the current location to be the site code. 
$siteloc = Set-Location $SiteCode":\"  
CLEAR

#grab from my current channel
$dripdrip = Get-CMCollectionMember -CollectionId SSS000C0 | random -Count 20 | select -ExpandProperty Name
#get my adgroup for scripting the channel change
$mygroup = Get-ADGroup -filter * -SearchBase "OU=SCCM,DC=US,DC=PINKERTONS,DC=COM" | WHERE {$_.name -eq "MAINT-OFFICE365-FASTRING"}

#Cheatkey for folders:
#SoftwareUpdates
#Applications
#ClientPush
$myfolder = "SoftwareUpdates"


Add-ComputerstoADGroup -groupname $mygroup.name -groupfolder $myfolder -members $dripdrip



$mygroupcount = Get-ADGroup -filter * -SearchBase "OU=SCCM,DC=US,DC=PINKERTONS,DC=COM" | WHERE {$_.name -eq "MAINT-OFFICE365-FASTRING"} | Get-ADGroupMember | Measure-Object | selecT count
$mygroupcount