﻿#Create MAINT-SERVER-GROUPS
#Written by Brian Kaler
#10/18/2018


#create a CSV with " and one col for import.
$groups = import-csv -Path ".\sourcegroups.csv"

#for manual entry uncomment below.
#$groups = "MAINT-SERVER-PRODD1W1","MAINT-SERVER-PRODD1W2","MAINT-SERVER-PRODD1W3","MAINT-SERVER-PRODD2W1","MAINT-SERVER-PRODD2W2","MAINT-SERVER-PRODD2W3","MAINT-SERVER-PRODD3W1","MAINT-SERVER-PRODD3W2","MAINT-SERVER-PRODD3W3","MAINT-SERVER-PRODD4W1","MAINT-SERVER-PRODD4W2","MAINT-SERVER-PRODD4W3","MAINT-SERVER-PRODD5W1","MAINT-SERVER-PRODD5W2","MAINT-SERVER-PRODD5W3"


foreach($g in $groups){
    New-ADGroup -Name $g.group -Path "OU=Servers,DC=US,DC=PINKERTONS,DC=COM" -groupscope DomainLocal -Verbose
    }