﻿#add-directmembership

#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = ".\"
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
set-location SSS:\
Function Add-DeviceDirectMembership{
    Param(
    [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [String]$dmcollection,
    [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [String]$dmdevicename
    )
    $mydev = Get-CMDevice -Name $dmdevicename
    if($mydev -eq $null)
    {write-host "Device $s was not found in SCCM"}
    Else
    {
    $device = $mydev.name
    Write-Host "Attempting to add $device to $dmcollection"
    Add-CMDeviceCollectionDirectMembershipRule -CollectionName $dmcollection -Resource $mydev
    }
}

Function Add-UserDirectMembership{
    Param(
    [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [String]$dmcollection,
    [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [String]$dmusername
    )
    $myuser = Get-CMUser -Name $dmusername
    if($myuser -eq $null)
    {write-host "User $s was not found in SCCM"}
    Else
    {
    $user = $myuser.resourceid
    $username = $myuser.smsid
    Write-Host "Attempting to add $username to $dmcollection"
    Add-CMUserCollectionDirectMembershipRule -CollectionName $dmcollection -ResourceID $user
    }
    }
    

Function Add-DirectMembership{
    Param(
    [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [ValidateSet("User","Device")]
    [String]$dmtype,
    [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [String]$dmcollectionname,
    [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [String]$dmidentity
    )
    If($dmtype -eq "Device"){
        Add-DeviceDirectMembership -dmcollection $dmcollectionname -dmdevicename $dmidentity
    }
    If($dmtype -eq "User"){
        Add-UserDirectMembership -dmcollection $dmcollectionname -dmusername $dmidentity
    }
}

#Examples

#Add-directmembership -dmtype Device -dmcollectionname "BK Test TS Deploys" -dmidentity "SNA-H5BPCV2"

#Add-directmembership -dmtype User -dmcollectionname "BK Test Users" -dmidentity "pnkus\bkaler"

#$myusers = get-aduser -filter * -Properties * | where {$_.City -like "*Westlake Village*"}

foreach($user in $myusers){
    Add-DirectMembership -dmtype User -dmcollectionname "WOC Staff" -dmidentity $user
}
