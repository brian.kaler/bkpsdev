﻿
If($allapps -eq $null){
    $allapps = Get-CMApplication -Fast | select *
    }
$matchapps = get-content C:\temp\matchapps.txt

$finalreport = @()
foreach($str in $matchapps){
    $matchedapps = $allapps.localizeddisplayname | Select-FuzzyString $str -verbose | Where {$_.score -gt 300} | select -ExpandProperty Result
    foreach($match in $matchedapps){
        $app = Get-CMApplication -name $match
        If($app -ne $null){
            $appproperties = $app.properties
            $appreport = New-Object -TypeName PSObject
            $appreport | Add-Member -MemberType NoteProperty -Name "Application Name" -value $app.localizeddisplayname
            $appreport | Add-Member -MemberType NoteProperty -Name "NumberofDeviceswithApp" -value $appproperties.NumberofDeviceswithApp
            $appreport | Add-Member -MemberType NoteProperty -Name "NumberOfUsersWithApp" -value $appproperties.NumberOfUsersWithApp
            $appreport | Add-Member -MemberType NoteProperty -Name "FuzzyMatch" -value $matchedapp
            $finalreport += $appreport
        }
    }
}

