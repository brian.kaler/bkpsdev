#Get-CMUserPrimaryDeviceReport
#Brian Kaler
#4/2/2019
#Gets all primary devices based on the provided username

#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = "C:\bkpsdev\sccm"
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
set-location SSS:\

$adusers = get-aduser -filter * -properties name,samaccountname

Function Get-CMUserPrimaryDevice{
    Param(
        [Parameter(Mandatory)]
        [ValidateNotNullorEmpty()]
        [string[]]$cmusers
    )
    $report = @()
    $primaryreport = @()
    $nonprimaryreport = @()
    foreach($user in $adusers){
        
        #write-host "Processing: "$fulladusername
        foreach($user in $cmusers){
        $fulladusername = "pnkus\"+$user.samaccountname
        $devices = Get-CMUserPrimaryDevice -cmuser $fulladusername

        If($Devices -ne $null){
                $primaryreport += [PSCustomObject]@{
                Name = "Primary User"
                SAMaccountname = $fulladusername
                Username = $user.name
                }
            }
            else {
                $nonprimaryreport += [PSCustomObject]@{
                    Name = "Non-Primary User"
                    SAMaccountname = $fulladusername
                    Username = $user.name
            }
        }
    }
$report += $primaryreport
$report += $nonprimaryreport
Return $report
}
}


#some situational thing, remove me later
#$adgroupname = "SAFES_SOCAL-Users_Domain-Global"
#$members = get-adgroupmember -Identity $adgroupname

Get-CMUserPrimaryDevice -cmusers $mymembers

