<# This form was created using POSHGUI.com  a free online gui designer for PowerShell
.NAME
    Untitled
#>

Add-Type -AssemblyName System.Windows.Forms
[System.Windows.Forms.Application]::EnableVisualStyles()

$Form                            = New-Object system.Windows.Forms.Form
$Form.ClientSize                 = '856,161'
$Form.text                       = "Windows 10 Feature Upgrade"
$Form.TopMost                    = $false

$TextBox1                        = New-Object system.Windows.Forms.TextBox
$TextBox1.multiline              = $true
$TextBox1.text                   = "This is some placeholder text, a warning will prompt the user to continue *YES* or decline *NO*"
$TextBox1.width                  = 830
$TextBox1.height                 = 81
$TextBox1.location               = New-Object System.Drawing.Point(12,29)
$TextBox1.Font                   = 'Microsoft Sans Serif,10'

$Label1                          = New-Object system.Windows.Forms.Label
$Label1.text                     = "Status Message:"
$Label1.AutoSize                 = $true
$Label1.width                    = 25
$Label1.height                   = 10
$Label1.location                 = New-Object System.Drawing.Point(12,11)
$Label1.Font                     = 'Microsoft Sans Serif,10'

$ButtonOK                        = New-Object system.Windows.Forms.Button
$ButtonOK.text                   = "Yes"
$ButtonOK.width                  = 125
$ButtonOK.height                 = 30
$ButtonOK.location               = New-Object System.Drawing.Point(12,118)
$ButtonOK.Font                   = 'Microsoft Sans Serif,10'

$ButtonCancel                    = New-Object system.Windows.Forms.Button
$ButtonCancel.text               = "No"
$ButtonCancel.width              = 125
$ButtonCancel.height             = 30
$ButtonCancel.location           = New-Object System.Drawing.Point(717,118)
$ButtonCancel.Font               = 'Microsoft Sans Serif,10'

$Form.controls.AddRange(@($TextBox1,$Label1,$ButtonOK,$ButtonCancel))


$ButtonOK.Add_Click(
    {
        Write-Host 'ContinuingOK' -Fore green
        $script:Canceling=$false
        [System.Windows.Forms.Application]::DoEvents()
        $Form.Close()
        exit 0
    }
)

$ButtonCancel.Add_Click(
    {
        Write-Host 'Cancelling' -Fore green
        $script:Canceling=$true
        [System.Windows.Forms.Application]::DoEvents()
        $Form.Close()
        exit 1
    }
)
#show our form
[void]$form.ShowDialog()
