#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = (Get-Item -path ".\").FullName
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
Set-Location SSS:\

$mybootimage = Get-CMBootImage -id SSS000D0
$mydpsgroup = Get-CMDistributionPointgroup -name "OSD Centric Distribution Points"
$mydps = Get-CMDistributionPoint -DistributionPointGroup $mydpsgroup | Where {$_ -like "*USW*"} | select -first 1
$mymps = Get-CMManagementPoint -SiteSystemServerName "uswmps901.us.pinkertons.com"
$prodimage = Get-CMTaskSequence | Where {$_.Description -eq "Production Image"} | Select -first 1
$timestamp = (Get-date -Format "MMddyyyyhhmm")

New-CMStandaloneMedia -DistributionPoint $mydps `
    -MediaType CdDvd `
    -path "C:\temp\sccmboot-$timestamp.iso" `
    -tasksequence $prodimage `
    -mediasize SizeUnlimited `
    -verbose
    