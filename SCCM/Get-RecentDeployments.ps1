#get-recentdeployments
#brian kaler
#12/9/2019
#Deployments created in the last 30 days


$alldeployments = Get-CMDeployment
$cutoff = (get-date).AddDays(-30)
$report = @()
foreach($deploy in $alldeployments){
    If($deploy.deploymenttime -gt $cutoff){
        $report += $deploy.applicationname
    }
}

$report | unique