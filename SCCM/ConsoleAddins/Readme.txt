________
Install:
________
Run in Powershell (run as administrator if you have UAC enabled):
install.ps1

To run silently (for example, as an SCCM package), run in Powershell:
Install.ps1 -SiteCode ABC -CMProvider Server1 -Silent

______
Usage:
______
Right-click a package, collection, task sequence or query and choose [Object] Path
(example: for a collection, it will say Collection Path).  Popup will occur with
the object's folder path.
 
__________
Uninstall:
__________
Run in Powershell (run as administrator if you have UAC enabled):
Uninstall.ps1

Uninstall.ps1 also has a -Silent switch to surpress dialogs.

_______
Author:
_______
Duncan Russell
http://www.sysadmintechnotes.com