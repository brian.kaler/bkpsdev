﻿#Get-ChromeExtensionDir
#Returns compliance negative *NO* if any extension folder exists given the targets defined
#Brian Kaler
#3/6/2020

#USER EDITABLE OPTIONS
#DEFINE THE TARGETS
$targets = @("Kbfnbcaeplbcioakkpcpgfkobkghlhen")


#SCRIPT BELOW
$strCurrentUser = (Get-WmiObject Win32_ComputerSystem -Computer ".").UserName
$objCurrentUser = New-Object System.Security.Principal.NTAccount($strCurrentUser)
$strCurrrentUserSID = ($objCurrentUser.Translate([System.Security.Principal.SecurityIdentifier])).Value
New-PSDrive HKU Registry HKEY_USERS -ErrorAction SilentlyContinue | out-null
$strKeyPath = "HKU:\$strCurrrentUserSID\Volatile Environment"
$userhomedir = (Get-ItemProperty $strKeyPath).USERPROFILE
$chromeuserdata = ($userhomedir+"\appdata\Local\Google\Chrome\User Data") 
$chromeextdirs = Get-ChildItem -path $chromeuserdata -filter "Extensions" -Recurse -ErrorAction SilentlyContinue | Where {$_.Psiscontainer -eq $true}

Foreach($target in $targets){
    foreach($dir in $chromeextdirs){
        cd $dir.fullname
        $targetfolderscount = 0
        If(test-path -Path .\$target){
            $targetfolders = get-Item -Path $target
            $targetfolderscount++
        }
    }
    If($targetfolderscount -ge 1){
        $compliance = "No"
    }else{
        $compliance = "Yes"
    }
}

Return $compliance