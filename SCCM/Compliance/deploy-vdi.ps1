#code starts here

$mylist = get-content C:\temp\mypredeterminednames.txt


[string]$vcenterinstance="wvc.us.pinkertons.com"
[String]$TargetCluster="Production"
#[String]$VMName = ("vdi-{0:HH-mm-ss}" -f (Get-Date))
[String]$SourceTemplate = "SWIN101809D-2019-05-17-16-11-39"
[String]$VMDatastore = "NFS_NR-02"
[String]$CustomSpec = "Server Template - Imaging VLAN"
[Bool]$PoweronafterCustomization = $true

Write-Host "$(Get-Date -Format G) Importing Vmware Module"
Get-Module -ListAvailable VMware* | Import-Module | Out-Null

#connect
Connect-VIServer -server $vcenterinstance 
$vmhost = Get-Cluster $TargetCluster | Get-VMHost | get-random

$mytemplate = Get-Template -Name $SourceTemplate
$mycustomspec = Get-OSCustomizationSpec $CustomSpec
$mydatastore = Get-Datastore -Name $VMDatastore


foreach($vm in $mylist){
    $mynewvm = New-VM -name $VM -Template $mytemplate -VMHost $vmhost -OSCustomizationSpec $mycustomspec -Datastore $mydatastore
}

If($PoweronafterCustomization){
Get-VM $vmname | Start-VM
}
