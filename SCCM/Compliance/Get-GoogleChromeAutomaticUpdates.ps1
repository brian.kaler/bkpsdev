<#
  This script will check if automatic updates is disabled and return a Compliant/Non-Compliant string.
 
  Created:     04.08.2014
  Version:     1.0
  Author:      Odd-Magne Kristoffersen
  Homepage:    https://sccmguru.wordpress.com/
     
  References:
  - Turning Off Auto Updates in Google Chrome
    http://www.chromium.org/administrators/turning-off-auto-updates
#>
 
$UpdateCheck = Get-ItemProperty 'HKLM:\SOFTWARE\Google\Update'
    if (($UpdateCheck.AutoUpdateCheckPeriodMinutes -eq 0) –and ($UpdateCheck.DisableAutoUpdateChecksCheckboxValue -eq 1) `
    -and ($UpdateCheck.'Update{4DC8B4CA-1BDA-483E-B5FA-D3C12E15B62D}' -eq 0) -and ($UpdateCheck.'Update{4EA16AC7-FD5A-47C3-875B-DBF4A2008C20}' -eq 0) `
    -and ($UpdateCheck.'Update{8A69D345-D564-463C-AFF1-A69D9E530F96}' -eq 0) -and ($UpdateCheck.'Update{8BA986DA-5100-405E-AA35-86F34A02ACBF}' -eq 0) `
    -and ($UpdateCheck.UpdateDefault -eq 0))
    {Write-Host 'Compliant'}
    else
    {Write-Host 'Non-Compliant'}