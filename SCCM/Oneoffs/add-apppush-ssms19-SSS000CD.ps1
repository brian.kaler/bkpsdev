﻿#create a new group name or existing group name, format should be APPPUSH-%APP%-%PACKAGEORAPPID%
$groupname = "APPPUSH-SSMS19-SSS000CD"
#members for manual entry, use AD computer name
$members = @(
        "catimx001"
        "catiws001"
        )
#template for file-based membership, uncomment and edit accordingly.
#$members = get-content ".\apppushmembers.txt"

#create our group if it does not exist.
If(($mygroup = get-adgroup -filter * | where {$_.name -match $groupname}) -eq $null){
    $mygroup = New-ADGroup -Name $groupname -Path "OU=SCCM,DC=US,DC=PINKERTONS,DC=COM" -groupscope DomainLocal -Verbose
    }

$pcs = get-adcomputer -filter *

foreach($m in $members){
    foreach($pc in $pcs)
        {
        If($m -eq $pc.Name)
            {
            add-ADGroupMember -identity $mygroup -members $pc -ErrorAction inquire -server uswdcs001.us.pinkertons.com -ErrorVariable $badpc
            write-host "$m was found and added to the group"
            if($badpc -ne $null){"$m was NOT found!"}
            }

    
        }
    }


write-host "Total count in group"
Get-ADGroupMember -identity $mygroup | measure-object | select count