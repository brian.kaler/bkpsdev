﻿#create a new group name or existing group name, format should be APPPUSH-%APP%-%PACKAGEORAPPID%
$groupname = "APPPUSH-SSMS19-SSS000CD"
#members for manual entry.
$members = @(
        "catimx002.us.pinkertons.com"
        )
#template for file-based membership, uncomment and edit accordingly.
#$members = get-content ".\apppushmembers.txt"

#create our group if it does not exist.
If(($mygroup = get-adgroup -filter * | where {$_.name -match $groupname}) -eq $null){
    New-ADGroup -Name  -Path "OU=Servers,DC=US,DC=PINKERTONS,DC=COM" -groupscope DomainLocal -Verbose
    }


$pcs = get-adcomputer -filter *

foreach($m in $members){
    foreach($pc in $pcs)
        {
        If($m -eq $pc.Name)
            {
            add-ADGroupMember -identity $mygroup -members $pc -ErrorAction inquire -server uswdcs001.us.pinkertons.com -ErrorVariable $badpc
            write-host "$m was found and added to the group"
            if($badpc -ne $null){"$m was NOT found!"}
            }

    
        }
    }


write-host "Total count in group"
Get-ADGroupMember -identity $mygroup | measure-object | select count