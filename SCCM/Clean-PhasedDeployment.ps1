﻿
$collectionpattern = "*PHASED-ZScaler*"
$deploymentcollectionname = "ZScaler App USA/CANADA"


#gather the collections
$phasedcollections = Get-CMCollection -Name $collectionpattern
$deploymentcollection = Get-CMCollection -Name $deploymentcollectionname
foreach($z in $phasedcollections){
    
    $membershiprule = Get-CMCollectionDirectMembershipRule -CollectionName $z.name
    If($membershiprule -ne $null){
        Remove-CMCollectionDirectMembershipRule -CollectionName $z.name -ResourceId $membershiprule.resourceid -Confirm:$false -Force
    }
}

#remove our included phased collections from the deployment collection.
$includecollectionsrule = Get-CMCollectionIncludeMembershipRule -CollectionId $deploymentcollection.CollectionID | ? {$_.rulename -like $collectionpattern}
foreach($icol in $includecollectionsrule){
    Remove-CMCollectionIncludeMembershipRule -CollectionId $deploymentcollection.CollectionID -IncludeCollectionId $icol.IncludeCollectionID -Confirm:$false -Force
}

#finally remove our phased collections entirely
If((Read-Host -Prompt "Press y to delete all phased collections") -eq "y"){
    foreach($col in $phasedcollections){
        Remove-CMCollection -CollectionId $col.collectionid -Force -Confirm:$false
    }
} 