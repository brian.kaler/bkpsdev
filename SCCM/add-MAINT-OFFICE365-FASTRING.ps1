﻿$mygroup = get-adgroup -filter * | where {$_.name -match "MAINT-OFFICE365-FASTRING"}
#FOR MANUAL ENTRY - edit the below, otherwise use CSV
#$members = "THISPCHERE","THATPCTHERE"

#create a CSV with "machine" for the header and one col for import.
$members = get-content "..\input\pcserversupportcomputers.txt"


$pcs = get-adcomputer -filter *

foreach($m in $members){
    foreach($pc in $pcs)
        {
        If($m -eq $pc.Name)
            {
            add-ADGroupMember -identity $mygroup -members $pc -ErrorAction inquire -server uswdcs001.us.pinkertons.com -ErrorVariable $badpc
            write-host "$m was found and added to the group"
            if($badpc -ne $null){"$m was NOT found!"}
            }

    
        }
    }


Get-ADGroupMember -identity $mygroup