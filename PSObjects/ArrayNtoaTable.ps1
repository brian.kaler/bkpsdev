﻿$TrackedOU = @("OU=Workstations,DC=us,DC=pinkertons,DC=com",
"OU=Windows,DC=us,DC=pinkertons,DC=com",
"CN=Computers,DC=us,DC=pinkertons,DC=com",
"OU=Servers,DC=us,DC=pinkertons,DC=com",
"OU=Citrix Xen 7,DC=us,DC=pinkertons,DC=com",
"OU=SCCM,DC=us,DC=pinkertons,DC=com",
"OU=Securitas,DC=local,DC=securitas,DC=ca",
"OU=Servers,DC=local,DC=securitas,DC=ca",
"OU=STAS,DC=local,DC=securitas,DC=ca",
"OU=TEST,DC=local,DC=securitas,DC=ca",
"OU=VDI,DC=us,DC=pinkertons,DC=com",
"OU=VM Workstations,DC=us,DC=pinkertons,DC=com",
"CN=Computers,DC=local,DC=securitas,DC=ca")

#used to pull the discovery methods, not clean data
$mydiscover = (Get-CMDiscoveryMethod -name ActiveDirectorySystemDiscovery).proplists
$included = ($mydiscover | where {$_.propertylistname -eq "AD Containers"}).Values | fl
$excluded = ($mydiscover | where {$_.propertylistname -eq "AD Containers Exclusions"}).VAlues | fl


#just in case this is run multiple times
$valuesiwant = @()
$skipcount = 3
$i = 0
Do{
    $valuesiwant += $included[$i]
    $i = $i + $skipcount
}Until($i -gt $included.count)

$valuesiwant
