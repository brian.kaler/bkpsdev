﻿#USMTUSBBackup
#Written by Brian Kaler
#5/28/2019
#GUI Based USMT Scanstate and Loadstate, to and from the same USB Media.

#All USMT FILES MUST RESIDE ON THE SCRIPTPATH, with ARCH folders at the ROOT.
$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition
$usmtlogpath = $Env:TEMP
#for some future checks, turn to $false after production
$debug = $false
#our defaults for return values (no status)
$ret_backup     =   -1
$ret_restore    =   -1
$ret_estimate   =   -1



##FORM CONTROLS

    Add-Type -AssemblyName System.Windows.Forms
    [System.Windows.Forms.Application]::EnableVisualStyles()

    $mainusbform                    = New-Object system.Windows.Forms.Form
    $mainusbform.ClientSize         = '785,210'
    $mainusbform.text               = "USB Data Backup Tool"
    $mainusbform.TopMost            = $false
    $mainusbform.MaximizeBox        = $false
    $mainusbform.MinimizeBox        = $false
    $mainusbform.FormBorderStyle    = 'Fixed3d'

    $toplabel1                      = New-Object system.Windows.Forms.Label
    $toplabel1.text                 = "Status Message:"
    $toplabel1.AutoSize             = $true
    $toplabel1.width                = 25
    $toplabel1.height               = 10
    $toplabel1.location             = New-Object System.Drawing.Point(12,8)
    $toplabel1.Font                 = 'Microsoft Sans Serif,10'

    $ButtonA                        = New-Object system.Windows.Forms.Button
    $ButtonA.text                   = "Backup FROM Old PC"
    $ButtonA.width                  = 130
    $ButtonA.height                 = 30
    $ButtonA.location               = New-Object System.Drawing.Point(12,99)
    $ButtonA.Font                   = 'Microsoft Sans Serif,8'

    $ComboUSBPicker1                = New-Object system.Windows.Forms.ComboBox
    $ComboUSBPicker1.text           = "comboBox"
    $ComboUSBPicker1.width          = 761
    $ComboUSBPicker1.height         = 20
    $ComboUSBPicker1.location       = New-Object System.Drawing.Point(12,175)
    $ComboUSBPicker1.Font           = 'Microsoft Sans Serif,10'
    $ComboUSBPicker1.DropDownStyle  = [System.Windows.Forms.ComboBoxStyle]::DropDownList;

    $ButtonB                        = New-Object system.Windows.Forms.Button
    $ButtonB.text                   = "Exit"
    $ButtonB.width                  = 60
    $ButtonB.height                 = 30
    $ButtonB.location               = New-Object System.Drawing.Point(713,99)
    $ButtonB.Font                   = 'Microsoft Sans Serif,8'

    $ButtonC                        = New-Object system.Windows.Forms.Button
    $ButtonC.text                   = "Restore TO New PC"
    $ButtonC.width                  = 130
    $ButtonC.height                 = 30
    $ButtonC.location               = New-Object System.Drawing.Point(145,99)
    $ButtonC.Font                   = 'Microsoft Sans Serif,8'

    $ButtonD                        = New-Object system.Windows.Forms.Button
    $ButtonD.text                   = "Refresh USB"
    $ButtonD.width                  = 130
    $ButtonD.height                 = 30
    $ButtonD.location               = New-Object System.Drawing.Point(415,99)
    $ButtonD.Font                   = 'Microsoft Sans Serif,8'

    $ButtonE                        = New-Object system.Windows.Forms.Button
    $ButtonE.text                   = "Estimate Size"
    $ButtonE.width                  = 130
    $ButtonE.height                 = 30
    $ButtonE.location               = New-Object System.Drawing.Point(280,99)
    $ButtonE.Font                   = 'Microsoft Sans Serif,8'

    $ProgressBar1                   = New-Object system.Windows.Forms.ProgressBar
    $ProgressBar1.width             = 758
    $ProgressBar1.height            = 60
    $ProgressBar1.location          = New-Object System.Drawing.Point(9,230)

    $l_progressbar                  = New-Object system.Windows.Forms.Label
    $l_progressbar.text             = "You should not see this"
    $l_progressbar.AutoSize         = $true
    $l_progressbar.width            = 25
    $l_progressbar.height           = 10
    $l_progressbar.location         = New-Object System.Drawing.Point(12,210)
    $l_progressbar.Font             = 'Microsoft Sans Serif,10'

    $Question1                      = New-Object system.Windows.Forms.TextBox
    $Question1.multiline            = $true
    $Question1.ReadOnly             = $true
    $Question1.foreColor = 'White'
    $question1.BackColor = 'Black'
    $Question1.width                = 761
    $Question1.height               = 58
    $Question1.location             = New-Object System.Drawing.Point(12,29)
    $Question1.Font                 = 'Microsoft Sans Serif,10'
    $Question1.text                 = "Click [Backup FROM Old PC] to save your data to USB. `
--- OR --- `
Click [Restore TO New PC]  if you have a previously saved backup to Restore."

    $BackupEstimateLabel            = New-Object system.Windows.Forms.Label
    $BackupEstimateLabel.text       = "Estimated Backup Size: [PLEASE PRESS ESTIMATE TO GATHER]"
    $BackupEstimateLabel.AutoSize   = $true
    $BackupEstimateLabel.width      = 600
    $BackupEstimateLabel.height     = 10
    $BackupEstimateLabel.location   = New-Object System.Drawing.Point(12,150)
    $BackupEstimateLabel.Font       = 'Microsoft Sans Serif,10'


    
    $mainusbform.controls.AddRange(@($toplabel1,$ButtonA,$ButtonB,$ButtonD,$ProgressBar1,$ComboUSBPicker1,$ButtonC,$l_progressbar,$Question1))


#END POSHGIT online forms stuff

#some useful tidbits with descriptions below:

#below can be used to collect the date time, log files are a little unreliable with it
#$prlogdate = [DateTime]::ParseExact(($prloglastline.substring(0,19)))

##If the job is taking longer than ~30 minutes
#If($sleepcount -gt 200){
#    $TextboxBackupOK1.text = "Backing up your files has taken longer than expected and may be stalled, please contact Securitas Service Desk."
#    $TextboxBackupOK1.refresh()
#    [void]$FormBackupOK.ShowDialog()
#    break
#} 
                

#end tidbits

##Functions!

    <#
    .Synopsis
    Write-Log writes a message to a specified log file with the current time stamp.
    .DESCRIPTION
    The Write-Log function is designed to add logging capability to other scripts.
    In addition to writing output and/or verbose you can write to a log file for
    later debugging.
    .NOTES
    Created by: Jason Wasser @wasserja
    Modified: 11/24/2015 09:30:19 AM  

    Changelog:
        * Code simplification and clarification - thanks to @juneb_get_help
        * Added documentation.
        * Renamed LogPath parameter to Path to keep it standard - thanks to @JeffHicks
        * Revised the Force switch to work as it should - thanks to @JeffHicks

    To Do:
        * Add error handling if trying to create a log file in a inaccessible location.
        * Add ability to write $Message to $Verbose or $Error pipelines to eliminate
        duplicates.
    .PARAMETER Message
    Message is the content that you wish to add to the log file. 
    .PARAMETER Path
    The path to the log file to which you would like to write. By default the function will 
    create the path and file if it does not exist. 
    .PARAMETER Level
    Specify the criticality of the log information being written to the log (i.e. Error, Warning, Informational)
    .PARAMETER NoClobber
    Use NoClobber if you do not wish to overwrite an existing file.
    .EXAMPLE
    Write-Log -Message 'Log message' 
    Writes the message to c:\Logs\PowerShellLog.log.
    .EXAMPLE
    Write-Log -Message 'Restarting Server.' -Path c:\Logs\Scriptoutput.log
    Writes the content to the specified log file and creates the path and file specified. 
    .EXAMPLE
    Write-Log -Message 'Folder does not exist.' -Path c:\Logs\Script.log -Level Error
    Writes the message to the specified log file as an error message, and writes the message to the error pipeline.
    .LINK
    https://gallery.technet.microsoft.com/scriptcenter/Write-Log-PowerShell-999c32d0
    #>
    function Write-Log
    {
        [CmdletBinding()]
        Param
        (
            [Parameter(Mandatory=$true,
                    ValueFromPipelineByPropertyName=$true)]
            [ValidateNotNullOrEmpty()]
            [Alias("LogContent")]
            [string]$Message,

            [Parameter(Mandatory=$false)]
            [Alias('LogPath')]
            [string]$Path='$usmtlogpath\USMTUSBBackup.log',
            
            [Parameter(Mandatory=$false)]
            [ValidateSet("Error","Warn","Info")]
            [string]$Level="Info",
            
            [Parameter(Mandatory=$false)]
            [switch]$NoClobber
        )

        Begin
        {
            # Set VerbosePreference to Continue so that verbose messages are displayed.
            $VerbosePreference = 'Continue'
        }
        Process
        {
            
            # If the file already exists and NoClobber was specified, do not write to the log.
            if ((Test-Path $Path) -AND $NoClobber) {
                Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name."
                Return
                }

            # If attempting to write to a log file in a folder/path that doesn't exist create the file including the path.
            elseif (!(Test-Path $Path)) {
                Write-Verbose "Creating $Path."
                $NewLogFile = New-Item $Path -Force -ItemType File
                }

            else {
                # Nothing to see here yet.
                }

            # Format Date for our Log File
            $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss"

            # Write message to error, warning, or verbose pipeline and specify $LevelText
            switch ($Level) {
                'Error' {
                    Write-Error $Message
                    $LevelText = 'ERROR:'
                    }
                'Warn' {
                    Write-Warning $Message
                    $LevelText = 'WARNING:'
                    }
                'Info' {
                    Write-Verbose $Message
                    $LevelText = 'INFO:'
                    }
                }
            
            # Write log entry to $Path
            "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append
        }
        End
        {
        }
    }
        Function Cleanup_Logs{
            $logfiles = get-childitem -filter *.log
            foreach($log in $logfiles)
            {
                remove-item $log -force -erroraction SilentlyContinue
                remove-item "Miglog.xml" -force -erroraction SilentlyContinue
            }

        }
        Function Copy_Logs{
            $logfiles = get-childitem -filter *.log
            foreach($log in $logfiles)
            {
                Copy-Item $log -force -Destination  -erroraction SilentlyContinue
                remove-item "Miglog.xml" -force -erroraction SilentlyContinue
            }
        }

    Function USMT_Estimate{
        Param(
        $username,
        $destinationusbdrive
        )
                #Disable our buttons while the operation is running
                $ButtonA.Enabled = $false
                $buttonC.Enabled = $false    
                $arch = $env:PROCESSOR_ARCHITECTURE
                $exe = "$scriptpath\$arch\scanstate.exe"
                $arglist = "$destinationusbdrive /l:$usmtlogpath\estimate.log /p:$usmtlogpath\estimatelog.xml /progress:$usmtlogpath\estimateprogress.log /localonly /c /ue:* /o /v:13 /vsc /ui:$username /i:.\$arch\MigUser.xml /i:.\$arch\ExcludeSystemFolders.xml /i:.\$arch\MigApp.xml /i:.\$arch\ExcludeDrives_D_to_Z.xml"
                #for me mostly.
                If($debug){
                    $myusmtstring = "$exe $arglist"
                    $question1.Text = $myusmtstring
                    }
                #set up a process
                $pinfo = New-Object System.Diagnostics.ProcessStartInfo
                $pinfo.FileName = $exe
                #This never worked well, scanstate would freeze, commented in case it becomes useful
                #$pinfo.RedirectStandardError = $true
                #$pinfo.RedirectStandardOutput = $true
                $pinfo.UseShellExecute = $false
                $pinfo.Arguments = $arglist
                $pinfo.WorkingDirectory = "$scriptpath\"
                $p = New-Object System.Diagnostics.Process
                $p.StartInfo = $pinfo
                $p.Start() | Out-Null
                $sleepcount = 0
                #satisfy the user that stuff is going on 
                $progressbar1.Value = 1
                $progressbar1.refresh() 
                #initial sleep before loadstate/scanstate needs to be long
                sleep -seconds 30
                While($p.HasExited -eq $false){
                    sleep -seconds 5
                    $peloglastline = (Get-Content $usmtlogpath\estimateprogress.log -ErrorAction SilentlyContinue | select -Last 1)
                    if($peloglastline -ne $null){
                        $percentcomplete = ($peloglastline | ? {$_ -match "totalPercentageCompleted"})
                        IF($percentcomplete -ne $null){
                            $progressbar1.Value = $percentcomplete.split()[-1]
                            $progressbar1.refresh() 
                        }    
                    }
                    $sleepcount++
                }
                Write-Host "USMT Estimate Exited"
                #This is a dirty way to update but I want to keep the return clean on all 3 functions
                [xml]$ResultXML = Get-Content $scriptpath\estimatelog.xml
                $BackupSizeinMegabytes = ([MATH]::Floor(($ResultXML.PreMigration.storeSize.size.'#text')/1024/1024))
                $BackupSize = "Estimated Backup Size: $BackupSizeinMegabytes Megabytes"
                $BackupEstimateLabel.text = $BackupSize
                $BackupEstimateLabel.refresh()
                $selecteddrivesize = ([string]($ComboUSBPicker1.SelectedItem) | select-string -Pattern "[0-9]+").matches[0].value
                write-host "total size calculated: $backupsizeinmegabytes"
                write-host "selected drive size $selecteddrivesize"
                if([int]$selecteddrivesize -lt [int]$BackupSizeinMegabytes){
                    Return [int]255
                }
                #return our exit code for our final notifications
                Return $p.Exitcode
                #can be used to collect the date time, log files are a little unreliable with it
                #$pblogdate = [DateTime]::ParseExact(($pbloglastline.substring(0,19)))
            }

    Function USMT_Scanstate{
        Param(
        $destinationusbdrive,
        $username
        )
                #Disable our buttons while the operation is running
                $ButtonA.Enabled = $false
                $buttonC.Enabled = $false
                $arch = $env:PROCESSOR_ARCHITECTURE
                $exe = "$scriptpath\$arch\scanstate.exe"
                $arglist = "$destinationusbdrive /l:$usmtlogpath\scanstate.log /progress:$usmtlogpath\backupprogress.log /localonly /c /ue:* /o /v:13 /vsc /ui:$username /i:.\$arch\MigUser.xml /i:.\$arch\ExcludeSystemFolders.xml /i:.\$arch\MigApp.xml /i:.\$arch\ExcludeDrives_D_to_Z.xml"
                #for me mostly.
                If($debug){
                    $myusmtstring = "$exe $arglist"
                    $question1.Text = $myusmtstring
                    }
                #set up a process
                $pinfo = New-Object System.Diagnostics.ProcessStartInfo
                $pinfo.FileName = $exe
                #This never worked well, scanstate would freeze, commented in case it becomes useful
                #$pinfo.RedirectStandardError = $true
                #$pinfo.RedirectStandardOutput = $true
                $pinfo.UseShellExecute = $false
                $pinfo.Arguments = $arglist
                $pinfo.WorkingDirectory = "$scriptpath\"
                $p = New-Object System.Diagnostics.Process
                $p.StartInfo = $pinfo
                $p.Start() | Out-Null
                $sleepcount = 0
                #satisfy the user that stuff is going on 
                $progressbar1.Value = 1
                $progressbar1.refresh() 
                #initial sleep before loadstate/scanstate needs to be long
                sleep -seconds 30
                While($p.HasExited -eq $false){
                    sleep -seconds 5
                    $pbloglastline = (Get-Content $usmtlogpath\backupprogress.log -ErrorAction SilentlyContinue | select -Last 1)
                    if($pbloglastline -ne $null){
                        $percentcomplete = ($pbloglastline | ? {$_ -match "totalPercentageCompleted"})
                        IF($percentcomplete -ne $null){
                            $progressbar1.Value = $percentcomplete.split()[-1]
                            $progressbar1.refresh() 
                        }
                    $sleepcount++
                    }
                }
                Write-Host "USMT Scanstate Exited"
                #return our exit code for our final notifications
                Return $p.Exitcode
                #can be used to collect the date time, log files are a little unreliable with it
                #$pblogdate = [DateTime]::ParseExact(($pbloglastline.substring(0,19)))
            }
    Function USMT_LoadState{
        Param(
        $sourceusbdrive
        )
            $arch = $env:PROCESSOR_ARCHITECTURE
            $exe = "$scriptpath\$arch\loadstate.exe"
            $usmtstore = "$sourceusbdrive\"
            $arglist = "$usmtstore /l:$usmtlogpath\loadstate.log /progress:$usmtlogpath\restoreprogress.log /c /lac /i:.\$arch\MigUser.xml /i:.\$arch\ExcludeSystemFolders.xml /i:.\$arch\MigApp.xml /i:.\$arch\ExcludeDrives_D_to_Z.xml"
            If($debug){
                $myusmtstring = "$exe $arglist"
                $question1.Text = $myusmtstring
            }
            #Disable our buttons while the operation is running
            $ButtonA.Enabled = $false
            $buttonC.Enabled = $false
            #set up a process
            $pinfo = New-Object System.Diagnostics.ProcessStartInfo
            $pinfo.FileName = $exe
            #Redirection never worked well, scanstate would freeze, commented in case it becomes useful
            #$pinfo.RedirectStandardError = $true
            #$pinfo.RedirectStandardOutput = $true
            $pinfo.UseShellExecute = $false
            $pinfo.Arguments = $arglist
            $pinfo.WorkingDirectory = "$scriptpath\"
            $p = New-Object System.Diagnostics.Process
            $p.StartInfo = $pinfo
            $p.Start() | Out-Null
            #[void]$FormBackupOK.ShowDialog()
            #Uncomment the below for updating the progress bar once I figure it out
                $sleepcount = 0
                #satisfy the user that stuff is going on 
                $progressbar1.Value = 1
                $progressbar1.refresh() 
                #initial sleep before loadstate/scanstate needs to be long
                sleep -seconds 30
                While($p.HasExited -eq $false){
                    sleep -seconds 5
                    $prloglastline = (Get-Content $usmtlogpath\restoreprogress.log -ErrorAction SilentlyContinue | select -Last 1)
                    if($prloglastline -ne $null){
                        $prloglastline = (Get-Content $usmtlogpath\restoreprogress.log -ErrorAction SilentlyContinue | select -Last 1)
                        $percentcomplete = ($prloglastline | ? {$_ -match "totalPercentageCompleted"})
                        IF($percentcomplete -ne $null){    
                            $progressbar1.Value = $percentcomplete.split()[-1]
                            $progressbar1.refresh() 
                        }   
                    }
                    $sleepcount++
                }
                Write-Host "USMT Loadstate Exited"
                #return our exit code for our final notifications
                Return $p.Exitcode
            }

    Function Refresh_USB_Drives{
        #Remove any existing drives (it's a refresh)
        $ComboUSBPicker1.Items.clear()
        #Refresh the drive list
        $mydrives = gwmi win32_diskdrive | ?{$_.interfacetype -eq "USB"} | %{gwmi -Query "ASSOCIATORS OF {Win32_DiskDrive.DeviceID=`"$($_.DeviceID.replace('\','\\'))`"} WHERE AssocClass = Win32_DiskDriveToDiskPartition"} |  %{gwmi -Query "ASSOCIATORS OF {Win32_DiskPartition.DeviceID=`"$($_.DeviceID)`"} WHERE AssocClass = Win32_LogicalDiskToPartition"} | select deviceid,size
        foreach($drive in $mydrives){
            $driveid = $drive.deviceid
            $drivesize = [MATH]::Floor(($drive.size / 1024 / 1024))
            $drivestring = "Drive Letter: $driveid Size: $drivesize Megabytes"
            $ComboUSBPicker1.Items.Add($drivestring)
            $ButtonA.Enabled = $true
			$buttonC.Enabled = $true
			$ButtonE.Enabled = $true
            $mainusbform.Refresh()
        }
        If($ComboUSBPicker.selected -eq $null){
            $ComboUSBPicker1.SelectedIndex = 0
            $ComboUSBPicker1.Refresh()
            }
        If($debug -eq $true){
            $drivestring = "C:\USMTdebug"
            $ComboUSBPicker1.Items.Add($drivestring)
        }
    }

##SAVESTATE/LOADSTATE and ESTIMATE Exit Status Messages
    $OperationEstimateOutput =
    {
        #If our estimate succeeds
        If(($ret_estimate) -le 3){
            $progressbar1.Value = 100
            $progressbar1.refresh()
            $Question1.text = "Your Backup Estimation operations have completed successfully"
            $Question1.foreColor = 'White'
            $question1.BackColor = 'Green'
            $ButtonA.Enabled = $true
            $buttonC.Enabled = $false
            $mainusbform.Refresh()
            $Question1.refresh()
        #or fails...
        }Else{  
            $Question1.text = "The tool could not calculate your backup size. 
You may still attempt to backup your data, contact Securitas Service Desk with any issues."
            $Question1.foreColor = 'White'
            $question1.BackColor = 'Red'
            $Question1.refresh()
        }
        #If our drive is not large enough to fit the backup.
        If(($ret_estimate) -eq 255){
            $Question1.text = "Your estimated backup size exceeds the total size of your USB Thumbdrive.  `
please start the process again with a larger drive."
            $Question1.foreColor = 'White'
            $question1.BackColor = 'Red'
            $Question1.refresh()
            $ButtonA.Enabled = $false
            $buttonC.Enabled = $false
            $mainusbform.Refresh()
        }
    }
    $OperationBackupOutput = 
    {
        #if either scanstate or loadstate fail (less than 3) Notify the user the backup or restore have failed.
        #success conditions
        Write-host "Backup returned code $ret_backup"
        If(($ret_backup) -le 3){
            $progressbar1.Value = 100
            $progressbar1.refresh()
            $Question1.text = "Your backup operations have completed successfully, please remove your USB drive."
            $Question1.foreColor = 'White'
            $question1.BackColor = 'Green'
            $Question1.refresh()
        }Else{
            If($ret_backup -ne -1){
                $Question1.text = "Your backup operations have failed, please contact Securitas Service Desk. `
                
Exit code: $ret_backup"
                $Question1.foreColor = 'White'
                $question1.BackColor = 'Red'
                $Question1.refresh()
                }
        }
    }
    $OperationRestoreOutput = 
    {
        #success
        If(($ret_restore) -le 3){
            $progressbar1.Value = 100
            $progressbar1.refresh() 
            $Question1.text = "Your restore operations have completed successfully, please verify your files and contact Securitas Service Desk with any issues."
            $Question1.foreColor = 'White'
            $question1.BackColor = 'Green'
            $Question1.refresh()       
        }
        Else{
        #Failure
            If($ret_restore -ne -1){
                $Question1.text = "Your restore operations have failed, please contact Securitas Service Desk. `

Exit code: $ret_restore"
                $Question1.foreColor = 'White'
                $question1.BackColor = 'Red'
                $Question1.refresh()
            }
        }
    }

##STARTUP
    #PRE-REQUISITE CHECKS - is the user an admin? (SAVESTATE/LOADSTATE REQ.)
    $currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
    #If the script is run without administrator privileges
    If($currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator) -eq $false){
        $question1.Text = "This script was not launched as Administrator, all functions disabled."
        $ButtonA.Enabled = $false
        $buttonC.Enabled = $false
        $buttonE.Enabled = $false
        $mainusbform.Refresh()
    }
    #Previous executions cleared
    Cleanup_Logs
    #get some parameters from the local system
    $myusername = (gwmi -class Win32_ComputerSystem | select -ExpandProperty Username)
    $mydrives = gwmi win32_diskdrive | ?{$_.interfacetype -eq "USB"} | %{gwmi -Query "ASSOCIATORS OF {Win32_DiskDrive.DeviceID=`"$($_.DeviceID.replace('\','\\'))`"} WHERE AssocClass = Win32_DiskDriveToDiskPartition"} |  %{gwmi -Query "ASSOCIATORS OF {Win32_DiskPartition.DeviceID=`"$($_.DeviceID)`"} WHERE AssocClass = Win32_LogicalDiskToPartition"} | select deviceid,size
    #if no drives are found notify the user accordingly.
    If($null -eq $mydrives){
        $question1.Text = "No compatible USB Drives were found, please insert your USB thumbdrive and restart this application."
    }
    #report on each drive and notify in the combobox in the GUI
    foreach($drive in $mydrives){
            $driveid = $drive.deviceid
            $drivesize = [MATH]::Floor(($drive.size / 1024 / 1024))
            $drivestring = "Drive Letter: $driveid Size: $drivesize Megabytes"
            $ComboUSBPicker1.Items.Add($drivestring)
    }
    #Initially pick a USB drive if available.
    If($ComboUSBPicker.selected -eq $null){
            $ComboUSBPicker1.SelectedIndex = 0
            $ComboUSBPicker1.Refresh()
            }

##GUI LOGIC
    #exit button
    $ButtonB.Add_Click(
        {    
		$mainusbform.close()
        }
    )
    #Estimate Button
    $ButtonE.Add_Click(
        {
        $l_progressbar.text = "Data ESTIMATE Started - This may take a while, please be patient..."
        $mainusbform.ClientSize = '785,300'
        $mainusbform.Refresh()
        $Question1.text = "Now estimating backup size, please wait..."
        $Question1.foreColor = 'White'
        $question1.BackColor = 'Black'
        $driveletter = $ComboUSBPicker1.SelectedItem
        If($debug){
            $ret_estimate = USMT_Estimate -destinationusbdrive $driveletter -username $myusername 
        }
        else {
            $ret_estimate = USMT_Estimate -destinationusbdrive ($driveletter).Substring(14,2) -username $myusername 
        }
        &$OperationEstimate
        }
        )
    #Backup button
    $ButtonA.Add_Click(
    {
        $l_progressbar.text = "Data BACKUP Started - This may take a while, please be patient..."
        $mainusbform.ClientSize = '785,300'
        $mainusbform.Refresh()
        $Question1.text = "Files and Settings are now being backed up, please wait."
        $Question1.foreColor = 'White'
        $question1.BackColor = 'Black'
        $driveletter = $ComboUSBPicker1.SelectedItem
        If($debug){
            $ret_backup = USMT_Scanstate -destinationusbdrive $driveletter -username $myusername
        }
        else {
            $ret_backup = USMT_Scanstate -destinationusbdrive ($driveletter).Substring(14,2) -username $myusername
        }
        &$OperationBackupOutput
        }
    )
    #Restore button
    $ButtonC.Add_Click(
    {
        $l_progressbar.text = "Data RESTORE Started - This may take a while, please be patient..."
        $mainusbform.ClientSize = '785,300'
        $mainusbform.Refresh()
        $Question1.text = "Files and Settings are now being restored, please wait."
        $Question1.foreColor = 'White'
        $question1.BackColor = 'Black'
        $driveletter = $ComboUSBPicker1.SelectedItem
        $ret_restore = USMT_Loadstate -sourceusbdrive ($driveletter).Substring(14,2) -username $myusername
        &$OperationRestoreOutput
    }
    )
    #Refresh USB Drive list
    $ButtonD.Add_Click(
        {
        Refresh_USB_Drives
        $driveletter = $ComboUSBPicker1.SelectedItem
        }
        ) 
    #Strip out the progressbar at startup.
        $mainusbform.ClientSize = '785,210'
        $mainusbform.Refresh()
    #for debug
        if($debug){
            $Question1.text = "DEBUG MODE - Please turn `$debug in the script to `$false"
            $Question1.foreColor = 'White'
            $question1.BackColor = 'Red'
            $Question1.refresh()
        }
        #Hide Task Sequence Dialog.
        Try
        {
        $TSProgressUI = new-object -comobject Microsoft.SMS.TSProgressUI
        $TSProgressUI.CloseProgressDialog()
        $TSProgressUI = $null
        }
        Catch
        {
        Write-Host "Could not find the TSProgressUI"
        }
        #Show our main form to our user. Good idea to keep this as close to the bottom of the script as possible except rare occasions.
        [void]$mainusbform.ShowDialog()





# SIG # Begin signature block
# MIIMMQYJKoZIhvcNAQcCoIIMIjCCDB4CAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUexNu1syuaGfxaG+c8oHxHD+m
# bE+gggmaMIIEmTCCA4GgAwIBAgIQcaC3NpXdsa/COyuaGO5UyzANBgkqhkiG9w0B
# AQsFADCBqTELMAkGA1UEBhMCVVMxFTATBgNVBAoTDHRoYXd0ZSwgSW5jLjEoMCYG
# A1UECxMfQ2VydGlmaWNhdGlvbiBTZXJ2aWNlcyBEaXZpc2lvbjE4MDYGA1UECxMv
# KGMpIDIwMDYgdGhhd3RlLCBJbmMuIC0gRm9yIGF1dGhvcml6ZWQgdXNlIG9ubHkx
# HzAdBgNVBAMTFnRoYXd0ZSBQcmltYXJ5IFJvb3QgQ0EwHhcNMTMxMjEwMDAwMDAw
# WhcNMjMxMjA5MjM1OTU5WjBMMQswCQYDVQQGEwJVUzEVMBMGA1UEChMMdGhhd3Rl
# LCBJbmMuMSYwJAYDVQQDEx10aGF3dGUgU0hBMjU2IENvZGUgU2lnbmluZyBDQTCC
# ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJtVAkwXBenQZsP8KK3TwP7v
# 4Ol+1B72qhuRRv31Fu2YB1P6uocbfZ4fASerudJnyrcQJVP0476bkLjtI1xC72Ql
# WOWIIhq+9ceu9b6KsRERkxoiqXRpwXS2aIengzD5ZPGx4zg+9NbB/BL+c1cXNVeK
# 3VCNA/hmzcp2gxPI1w5xHeRjyboX+NG55IjSLCjIISANQbcL4i/CgOaIe1Nsw0Rj
# gX9oR4wrKs9b9IxJYbpphf1rAHgFJmkTMIA4TvFaVcnFUNaqOIlHQ1z+TXOlScWT
# af53lpqv84wOV7oz2Q7GQtMDd8S7Oa2R+fP3llw6ZKbtJ1fB6EDzU/K+KTT+X/kC
# AwEAAaOCARcwggETMC8GCCsGAQUFBwEBBCMwITAfBggrBgEFBQcwAYYTaHR0cDov
# L3QyLnN5bWNiLmNvbTASBgNVHRMBAf8ECDAGAQH/AgEAMDIGA1UdHwQrMCkwJ6Al
# oCOGIWh0dHA6Ly90MS5zeW1jYi5jb20vVGhhd3RlUENBLmNybDAdBgNVHSUEFjAU
# BggrBgEFBQcDAgYIKwYBBQUHAwMwDgYDVR0PAQH/BAQDAgEGMCkGA1UdEQQiMCCk
# HjAcMRowGAYDVQQDExFTeW1hbnRlY1BLSS0xLTU2ODAdBgNVHQ4EFgQUV4abVLi+
# pimK5PbC4hMYiYXN3LcwHwYDVR0jBBgwFoAUe1tFz6/Oy3r9MZIaarbzRutXSFAw
# DQYJKoZIhvcNAQELBQADggEBACQ79degNhPHQ/7wCYdo0ZgxbhLkPx4flntrTB6H
# novFbKOxDHtQktWBnLGPLCm37vmRBbmOQfEs9tBZLZjgueqAAUdAlbg9nQO9ebs1
# tq2cTCf2Z0UQycW8h05Ve9KHu93cMO/G1GzMmTVtHOBg081ojylZS4mWCEbJjvx1
# T8XcCcxOJ4tEzQe8rATgtTOlh5/03XMMkeoSgW/jdfAetZNsRBfVPpfJvQcsVncf
# hd1G6L/eLIGUo/flt6fBN591ylV3TV42KcqF2EVBcld1wHlb+jQQBm1kIEK3Osgf
# HUZkAl/GR77wxDooVNr2Hk+aohlDpG9J+PxeQiAohItHIG4wggT5MIID4aADAgEC
# AhAbh1EfIADX1Rys6/EZSQGmMA0GCSqGSIb3DQEBCwUAMEwxCzAJBgNVBAYTAlVT
# MRUwEwYDVQQKEwx0aGF3dGUsIEluYy4xJjAkBgNVBAMTHXRoYXd0ZSBTSEEyNTYg
# Q29kZSBTaWduaW5nIENBMB4XDTE5MDUzMTAwMDAwMFoXDTIwMDUzMDIzNTk1OVow
# gbYxCzAJBgNVBAYTAnVzMRMwEQYDVQQIDApDYWxpZm9ybmlhMRkwFwYDVQQHDBBX
# ZXN0bGFrZSBWaWxsYWdlMS4wLAYDVQQKDCVTRUNVUklUQVMgU0VDVVJJVFkgU0VS
# VklDRVMgVVNBLCBJTkMuMRcwFQYDVQQLDA5JbmZyYXN0cnVjdHVyZTEuMCwGA1UE
# AwwlU0VDVVJJVEFTIFNFQ1VSSVRZIFNFUlZJQ0VTIFVTQSwgSU5DLjCCASIwDQYJ
# KoZIhvcNAQEBBQADggEPADCCAQoCggEBANuWllaPey78uTo97pHtjT5Amgbe42rX
# eBH0lAAxOi91PTooBluEfP+s4ZFf1QNmIq3NB0qhQIVPWnxX54fGEHPkqaxXpXux
# CvEspw6POFpVex7Lt7u1B/ltef+c7C5A0RL47WvgJ/S5wUBEFrN1jN4muaUTwDzL
# WQs7CMXoErR1AxG6K8yFRY+1wIHlyG5Z14Cr5SkCBK6HuCFRGAo95zjoe+thtCGG
# XX4l3HvxNXL5Iyeo+ZWadyeIjnPmAUfBdrP01CEmd0f7ldofXkqpVzaGfI4L4CHB
# t4jm23Ai/tC0adl1bOBdlQcp6mxR3cLW2BtIgVxAfwC0VkPwC0GulWUCAwEAAaOC
# AWowggFmMAkGA1UdEwQCMAAwHwYDVR0jBBgwFoAUV4abVLi+pimK5PbC4hMYiYXN
# 3LcwHQYDVR0OBBYEFNLKl+hsIYgni+yFhswLzuw7xxCsMCsGA1UdHwQkMCIwIKAe
# oByGGmh0dHA6Ly90bC5zeW1jYi5jb20vdGwuY3JsMA4GA1UdDwEB/wQEAwIHgDAT
# BgNVHSUEDDAKBggrBgEFBQcDAzBuBgNVHSAEZzBlMGMGBmeBDAEEATBZMCYGCCsG
# AQUFBwIBFhpodHRwczovL3d3dy50aGF3dGUuY29tL2NwczAvBggrBgEFBQcCAjAj
# DCFodHRwczovL3d3dy50aGF3dGUuY29tL3JlcG9zaXRvcnkwVwYIKwYBBQUHAQEE
# SzBJMB8GCCsGAQUFBzABhhNodHRwOi8vdGwuc3ltY2QuY29tMCYGCCsGAQUFBzAC
# hhpodHRwOi8vdGwuc3ltY2IuY29tL3RsLmNydDANBgkqhkiG9w0BAQsFAAOCAQEA
# PlmMzVnjmpM3ZCcAUiY4YlRrgfBf6bV7plAT78tWFU9xn+hi2w6iM5YGxWfVBCwf
# 5S9e2/Du3WMgMBL6E9F4l7pWHIYut7e8f/jBJypsLP8iEjvw389ATHBF/f8RCekS
# wKowa+Ff7R2A8IWqTyETNt707aMArZtAtbyshd/0UmCqisTIHaU/6o+oZfN0W1AD
# XozqmjYGEH6txkr6HW16jRD/GNa5UaaIEdrtSrldfQiuI0xC4QmgeGG82lne0HRO
# lAC33gdWw+6DRF6vI2bOOtrBX+xSkSjQNoFHaGB+1RFV1gb0NlcQSeIE6fIRRimc
# f6uqFqYig+PLAnDXl2rpSDGCAgEwggH9AgEBMGAwTDELMAkGA1UEBhMCVVMxFTAT
# BgNVBAoTDHRoYXd0ZSwgSW5jLjEmMCQGA1UEAxMddGhhd3RlIFNIQTI1NiBDb2Rl
# IFNpZ25pbmcgQ0ECEBuHUR8gANfVHKzr8RlJAaYwCQYFKw4DAhoFAKB4MBgGCisG
# AQQBgjcCAQwxCjAIoAKAAKECgAAwGQYJKoZIhvcNAQkDMQwGCisGAQQBgjcCAQQw
# HAYKKwYBBAGCNwIBCzEOMAwGCisGAQQBgjcCARUwIwYJKoZIhvcNAQkEMRYEFDw7
# 70pYj8mXjjmTnMri4Ax01oxMMA0GCSqGSIb3DQEBAQUABIIBAA/mWCaR139zcU2b
# QN0vPuTdv3f8HfO8LBlQhtDziaFVc8ccRo0taC2TMWKb5XAA8HYkySxfqz8gaULG
# 3LcBm6mt9xYXm5MnrorDnF4P853OEyos4lQ6xZkQN6VlwVuv/JUrE4PCa01KloOD
# Teoy6XfDwD0NwN/kazoyZ9Mhji+5xxUJ3WmkploKnvneOt9HdoQAj80aIhIaBeo+
# 3+QfjCVw1V6ItqB4uFYF343r7gMWHFLkTWFfyTS/H71CkfmD0P/WynQVYqkMSHJx
# 1kc5J7ToSHKAPtCa6SJy8AU1QbL7SMNxCmiwnkIFOn9BrP/d2cymmXXYDXyMdAzV
# O6GP+Ko=
# SIG # End signature block
