﻿$currentuser = (get-item env:\\username).value
$localpath = ".\"
$remotelogpath = "\\napwcfs01\itn_home01\SCCM\Logs\USB_UserBackupTool\"+$currentuser
If((Test-Path $remotelogpath) -eq $false){
    New-Item -Path $remotelogpath -ItemType Directory
}
$logs = get-childitem -Path $localpath -filter *.log -recurse
$xmls = get-childitem -path $localpath -filter *.xml -recurse
$logfiles = $logs + $xmls
$logfiles | Copy-Item -Destination $remotelogpath -Verbose -ErrorAction SilentlyContinue