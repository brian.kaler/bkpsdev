Import-Module ActiveDirectory
$users = Get-ADUser -Filter *
foreach ($user in $users){
$email = $user.GivenName + '.' + $user.Surname + '@astera.com'
$legacyemail = $user.SamAccountName + '@astera.com'
$newemail = "SMTP:"+$email
$oldemail = "smtp:"+$legacyemail
Set-ADUser $user -Add @{proxyAddresses = ($newemail)}
Set-ADUser $user -Add @{proxyAddresses = ($oldemail)}
}