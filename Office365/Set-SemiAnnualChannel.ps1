﻿
if($drive -eq $null){
$drive = new-psdrive -PSProvider Registry -root HKLM:\ -name Regdrive
}

$myroot = set-location -path "regdrive:\SOFTWARE\Microsoft\Office\ClickToRun\Configuration"

$channelcdn = Get-ItemProperty -path . | select CDNbaseUrl

#if not in the semi-annual channel make the necessary registry edits
If($channelcdn -ne "http://officecdn.microsoft.com/pr/7ffbc6bf-bc32-4f92-8982-f9dd17fd3114"){
  Set-ItemProperty -path . -Name CDNBaseURL -value "http://officecdn.microsoft.com/pr/7ffbc6bf-bc32-4f92-8982-f9dd17fd3114" -force
  Remove-ItemProperty UpdateUrl -path . -Force -ErrorAction SilentlyContinue
  Remove-ItemProperty UpdateToVersion -path . -Force -ErrorAction SilentlyContinue
  Set-Location ..\Updates
  Remove-ItemProperty UpdateToVersion -path . -Force -ErrorAction SilentlyContinue
  Set-Location Regdrive:\SOFTWARE\Policies\Microsoft\Office\16.0\Common\OfficeUpdate
  Remove-Item -path . -Force -ErrorAction SilentlyContinue
}

#run click2run configure
$parameters = "/update user displaylevel=False forceappshutdown=True"
$updateexe = "C:\Program Files\Common Files\microsoft shared\ClickToRun\OfficeC2RClient.exe"


$processinfo = new-object System.Diagnostics.ProcessStartInfo
$processinfo.filename = $updateexe
$processinfo.Arguments = $parameters
$processinfo.RedirectStandardError = $true
$processinfo.RedirectStandardOutput = $true
$processinfo.UseShellExecute = $false
$processinfo.Arguments = "localhost"
$process = New-Object System.Diagnostics.Process
$process.startinfo = $processinfo
$process.start() | Out-Null
$process.waitforexit()
$output = $process.standardoutput.readtoend()

If($output = "0"){exit 0} else {exit 1}
