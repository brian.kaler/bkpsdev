#eventlog - LOGS Script events to Custom Windows Event Log
function add_eventlog
{
param(
[Parameter(Mandatory=$true)]
[String]$logname,
[Parameter(Mandatory=$true)]
[String]$source
)
    
    if(([System.Diagnostics.EventLog]::Exists($logname)) -and ([System.Diagnostics.EventLog]::SourceExists($source)))
        {

        }
        Else
        {
        New-EventLog -logname $logname -source $source
        }
}

#eventlog - LOGS MESSAGE AS A INFORMATION EVENT
function add_loginfo($logmsg)
{
$params = @{
    LogName = $logname
    Source = $source
    EntryType = 'Information'
    EventId = '1000'
    Message = $logmsg
}
write-eventlog @params
write-host "INFO: $logmsg"
}

#eventlog - LOGS MESSAGE AS A WARNING EVENT
function add_logwarn($logmsg)
{
$params = @{
    LogName = $logname
    Source = $source
    EntryType = 'Warning'
    EventId = '1000'
    Message = $logmsg
}
write-eventlog @params
write-host "Warning: $logmsg"
}
#eventlog - LOGS MESSAGE AS A ERROR EVENT
function add_logerror($logmsg)
{
$params = @{
    LogName = $logname
    Source = $source
    EntryType = 'Error'
    EventId = '1000'
    Message = $logmsg
}
write-eventlog @params
write-host "Error: $logmsg"
}

#scans a remote folder on another computer for files
function scan_remote_folder
{
param(
[String]$folder,
[String]$computername         
)
#session cleanup of existing sessions from previous runs
Remove-PSSession -computername $computername -ErrorAction SilentlyContinue
#create a pssession to the computer, not sure if required, test without sometime
$session = New-PSSession -ComputerName $computername 

$files = invoke-command -computername $computername -scriptblock {param($folder) `
        get-childitem -path $folder | where-object{$_.psiscontainer -eq $false}} -ArgumentList $folder

Return $files
}


function get-adhosts
{
param(
[Parameter(Mandatory=$True)]
[String]$adcn, 
[Parameter(Mandatory=$True)]
[String]$adsite
)
#modules
import-module activedirectory

#objects
$adsite =  'DC=US,DC=PINKERTONS,DC=COM'
$adcn = "Computers"


#active directory init
$adpath = $adcn + "," + $adsite

#run the query
$adcomputers = get-adcomputer -SearchBase $adpath -filter * -Errorvariable $query -ErrorAction SilentlyContinue
#validate query and return with error if invalid or empty
if($query -eq $false)
    {
    write-error "Invalid query or other error"
    }
    elseif( ($adcomputers | measure-object | select -expandproperty Count) -eq 0 )
    {
    write-error "No Computers were returned from that query"
    }


#build a computer collection from ad
$computernames = @()
$computernames += $adcomputers | select -ExpandProperty Name
#return 
Return $computernames
}

Function Get-IISBinding
    {
        Get-WebBinding | % {
            $name = $_.ItemXPath -replace '(?:.*?)name=''([^'']*)(?:.*)', '$1'
            New-Object psobject -Property @{
                Name = $name
                Binding = $_.bindinginformation.Split(":")[-1]
            }
        } | Group-Object -Property Name | 
        Format-Table Name, @{n="Bindings";e={$_.Group.Binding -join "`n"}} -Wrap
    }