﻿Function IsPatchDownloadTime(){
#set up some of our needed dates
$today = (Get-Date)
$maxtuesday = 13
$mintuesday = 7
$firstday = [DateTime]::new($today.year, $today.month, 1)
$cycle = $firstday

#Find the first tuesday
$i = 0
Do{
    $i = 1
    $cycle = $cycle.adddays($i)
    $firsttuesday = $cycle.day
    }While($cycle.dayofweek -ne "Tuesday")
#find the second tuesday
Do{
    $i = 1
    $cycle = $cycle.adddays($i)
    $secondtuesday = $cycle.day
    }While($cycle.dayofweek -ne "Tuesday")
#find the thirdtuesday
Do{
    $i = 1
    $cycle = $cycle.adddays($i)
    $thirdtuesday = $cycle.day
    }While($cycle.dayofweek -ne "Tuesday")


#If the second tuesday has passed and the third tuesday has not yet arrived, it's patch downloading time
if(($today.day -ge $secondtuesday) -and ($today.day -lt $thirdtuesday)){
Return $true
}
else
{
Return $false
}
}

ispatchdownloadtime