#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseUpx=n
#AutoIt3Wrapper_Compile_Both=y
#AutoIt3Wrapper_UseX64=y
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
;Script enables Compatibility mode for Intranet Sites

$1 = RegWrite("HKEY_CURRENT_USER\Software\Microsoft\Internet Explorer\BrowserEmulation","IntranetCompatibilityMode","REG_DWORD","1")

DllCall('WININET.DLL', 'long', 'InternetSetOption', 'int', 0, 'long', 39, 'str', 0, 'long', 0)
$erv = $1
if $erv <> 1 then Exit(1000)