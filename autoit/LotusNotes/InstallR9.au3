#RequireAdmin
#include <FileConstants.au3>

$installpath = "c:\software\Lotus Notes R9\"
$logdir = "c:\software\Lotus Notes R9\instlog\"
dircreate("c:\software\Lotus Notes R9")
dircreate("c:\software\Lotus Notes R9\instlog")

$logfilepath = $logdir
$logfilename = @ComputerName & "_Log_" & @MDAY & @MON & @YEAR & ".log"

$log = FileOpen($logfilepath & $logfilename,1)
$hoststring = @ComputerName & " - " & @HOUR & ":" & @MIN & ":" & @SEC & " "
FileWriteLine($log,$hoststring & "Started Lotus Notes R9 Install script")

FileWriteLine($log,$hoststring & "Unpacking files")

FileInstall("NOTES_9.0.1_WIN_EN.exe", @ScriptDir)
FileInstall("notes901FP7_basic_win.exe", @ScriptDir)


$fc1 = FileCopy(@ScriptDir & "notes901FP7_basic_win.exe", "c:\software\Lotus Notes R9\", 8)
$fc2 = FileCopy(@ScriptDir & "NOTES_9.0.1_WIN_EN.exe", "c:\software\Lotus Notes R9\", 8)

FileWriteLine($log,$hoststring & "Starting client install")
FileWriteLine($log,$hoststring & "Unpack Install complete, exit code: " & $fc1)
FileWriteLine($log,$hoststring & "Unpack FP Install complete, exit code: " & $fc2)

$exit1 = runwait($installpath & 'NOTES_9.0.1_WIN_EN.exe /s /v"/qb!+"')
FileWriteLine($log,$hoststring & "Client Install complete, exit code: " & $exit1)

$exit2 = runwait($installpath & 'notes901FP7_basic_win.exe /s /v"/qb!+"')
FileWriteLine($log,$hoststring & "FP Install complete, exit code: " & $exit2)