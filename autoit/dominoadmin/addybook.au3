#include <GuiTab.au3>
#requireadmin

; Window to Process
; Now making Window name and executables/PIDs interchangable!
; Credit to Cynagen
func _Win2Process($wintitle)
    if isstring($wintitle) = 0 then return -1
    $wproc = WinGetProcess($wintitle)
    return _ProcessName($wproc)
endfunc
func _Process2Win($pid)
    if isstring($pid) then $pid = processexists($pid)
    if $pid = 0 then return -1
    $list = WinList()
    for $i = 1 to $list[0][0]
        if $list[$i][0] <> "" AND BitAnd(WinGetState($list[$i][1]),2) then
            $wpid = WinGetProcess($list[$i][0])
            if $wpid = $pid then return $list[$i][0]
        EndIf
    next
    return -1
endfunc
func _ProcessName($pid)
    if isstring($pid) then $pid = processexists($pid)
    if not isnumber($pid) then return -1
    $proc = ProcessList()
    for $p = 1 to $proc[0][0]
        if $proc[$p][1] = $pid then return $proc[$p][0]
    Next
    return -1
 EndFunc

func _ReNotes()
$client = _Process2Win("nlnotes.exe")
Global $hWnd = WinActivate($client)
EndFunc

_ReNotes()

func _ContextGAL()
   WinActivate($hWnd)
   MouseMove(91,107)
   mouseclick("left")
   sleep(500)
   MouseMove(77,131)
   mouseclick("left")
   ControlClick($hWnd,"","NotesSubprog11","left",1,95,43)
   $GAL = ControlFocus($hWnd,"","NotesLineView1")
   $a = ControlGetText($hWnd,"",$GAL)
   ConsoleWrite($GAL)
EndFunc

_ContextGAL()




