
#include <FileConstants.au3>

$installpath = "c:\software\mapfdrivedr\"
$logdir = "c:\software\mapfdrivedr\instlog\"
dircreate("c:\software\mapfdrivedr")
dircreate("c:\software\mapfdrivedr\instlog")

$logfilepath = $logdir
$logfilename = @ComputerName & "_Log_" & @MDAY & @MON & @YEAR & ".log"

$log = FileOpen($logfilepath & $logfilename,1)
$hoststring = @ComputerName & " - " & @HOUR & ":" & @MIN & ":" & @SEC & " "
FileWriteLine($log,$hoststring & "Started mapfdrive install")

FileWriteLine($log,$hoststring & "Unpacking files")

FileInstall("adusershome.csv", @ScriptDir)
FileInstall("Run-script.bat", @ScriptDir)
FileInstall("map-UsersnewF.ps1",@ScriptDir)


$fc1 = FileCopy(@ScriptDir & "adusershome.csv", "c:\software\mapfdrivedr\", 8)
$fc2 = FileCopy(@ScriptDir & "run-script.bat", "c:\software\mapfdrivedr\", 8)
$fc3 = FileCopy(@ScriptDir & "map-UsersnewF.ps1", "c:\software\mapfdrivedr\", 8)

FileWriteLine($log,$hoststring & "Starting client install")
FileWriteLine($log,$hoststring & "Unpack csv complete, exit code: " & $fc1)
FileWriteLine($log,$hoststring & "Unpack batch complete, exit code: " & $fc2)
FileWriteLine($log,$hoststring & "Unpack ps1 complete, exit code: " & $fc3)


$exit1 = runwait($installpath & 'cmd /k run-script.bat')
FileWriteLine($log,$hoststring & "Client package complete, exit code: " & $exit1)
