#Region
#AutoIt3Wrapper_UseUpx=n
#EndRegion
;**** Directives created by AutoIt3Wrapper_GUI ****
;Fix BITS Version
;Brian Kaler - 11/14/13

#include <Date.au3>

;;VARIABLES
$clientpath = "\\uswscm901\SMS_SSS\Client\"
$logfilepath = "C:\windows\temp\"
$logfilename = @ComputerName & "_Log_" & @MDAY & @MON & @YEAR & ".log"

;;Create/Append Log
$log = FileOpen($logfilepath & $logfilename,1)
$hoststring = @ComputerName & " - " & @HOUR & ":" & @MIN & ":" & @SEC & " "
FileWriteLine($log,$hoststring & "Started fixBITS script")


;;install files here
fileinstall("./BITS/2.5/WindowsServer2003.WindowsXP-KB923845-x64-ENU.exe",@ScriptDir)
fileinstall("./BITS/2.5/WindowsServer2003-KB923845-x86-ENU.exe",@ScriptDir)
fileinstall("./BITS/2.5/WindowsXP-KB923845-x86-ENU.exe",@ScriptDir)
$BITS200364 = "WindowsServer2003.WindowsXP-KB923845-x64-ENU.exe"
$BITS2003 =  "WindowsServer2003-KB923845-x86-ENU.exe"
$BITSXP = "WindowsXP-KB923845-x86-ENU.exe"


;;determine BITS version


$BITSver = FileGetVersion("c:\windows\system32\qmgr.dll")
If $BITSver < 6.6 Then
   FileWriteLine($log,$hoststring & "Version Found: " & $BITSver & "- Determined BITS Version too low, will install 2.5")
Else
   FileWriteLine($log,$hoststring & "Version Found: " & $BITSver & "- Determined BITS Version 2.5 or higher, will not install")
   $exit2 = RunWait($clientpath & "ccmsetup.exe /MP:USWMPS901 /accept SMSSITECODE=SSS SMSMP=USWMPS901 FSP=USWSCM901")
   FileWriteLine($log,$hoststring & "SCCM Agent Install RAN, Exit code " & $exit2)
   Exit
EndIf

;;determine version of windows, xp, 2003, 2003x64

$OS = "undefined"

Select
Case @OSArch = "X64"
   $install = $BITS200364
   FileWriteLine($log,$hoststring & "Server 2003 X64 found")
Case @OSArch = "X86" and @OSVersion = "WIN_XP"
   $install = $BITSXP
   FileWriteLine($log,$hoststring & "Windows XP found")
Case @OSArch = "X86" and @OSVersion = "WIN_2003"
   $install = $BITS2003
   FileWriteLine($log,$hoststring & "Server 2003 X86 found")

EndSelect
;;If you can't determine the version of the OS, do not attempt the install.
If $install = "undefined" Then
   FileWriteLine($log,$hoststring & "No Matching OS has been found, script will terminate")
   exit
EndIf
;;install BITS
$exit = RunWait($install & " /qn",@Scriptdir)
FileWriteLine($log,$hoststring & "BITS Install RAN, Exit code " & $exit)
FileDelete($BITS200364)
FileDelete($BITS2003)
FileDelete($BITSXP)
;;install SCCM Agent
$exit2 = RunWait($clientpath & "ccmsetup.exe /MP:USWMPS901 /accept SMSSITECODE=SSS SMSMP=USWMPS901 FSP=USWSCM901")
FileWriteLine($log,$hoststring & "SCCM Agent Install RAN, Exit code " & $exit2)



