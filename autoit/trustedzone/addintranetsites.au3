;Script adds 2 sites (client.securitasinc.com, cloud.securitasinc.com) to trusted sites for the Intranet Zone for the Current User.


$1 = RegWrite("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\securitasinc.com")
$2 = RegWrite("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\securitasinc.com\client")
$3 = RegWrite("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\securitasinc.com\client","https","REG_DWORD","1")
$4 = RegWrite("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\securitasinc.com")
$5 = RegWrite("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\securitasinc.com\cloud")
$6 = RegWrite("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\securitasinc.com\cloud","https","REG_DWORD","1")
DllCall('WININET.DLL', 'long', 'InternetSetOption', 'int', 0, 'long', 39, 'str', 0, 'long', 0)
$erv = $1 + $2 + $3 + $4 + $5 + $6
if $erv <> 6 then Exit(1000)