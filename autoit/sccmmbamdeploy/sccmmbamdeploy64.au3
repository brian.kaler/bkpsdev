;MBAM "DeploymentTime" registry settings 64 bit version
;used to roll out MBAM during an SCCM process, keys need to be set during deployment.

#include <Array.au3>
#include <Constants.au3>
#include <Process.au3>

FileInstall("setup.ico",@scriptdir & "\setup.ico")
$Form1_1 = GUICreate("Encryption Progress", 365, 85, 0, 0)
GUISetBkColor(0xFFFFFF)
$icon = GUISetIcon(@ScriptDir & "\Setup.ico")
$progressbar = GUICtrlCreateProgress(5,55,350,25)
$progresslabel1 = GUICtrlCreateLabel("Precentage Complete:",5,5)
$progresslabel2 = GUICtrlCreateLabel("Disk Size:",5,18)
$progresslabel3 = GUICtrlCreateLabel("Current Status:",5,31)
$progresstext1 = GUICtrlCreateLabel("0%",250,5,300)
$progresstext2 = GUICtrlCreateLabel("0 GB",250,18,300)
$progresstext3 = GUICtrlCreateLabel("No Status",250,31,300)



Global $ConnInfo[1][3]
$ConnInfo[0][0] = 0
;_ArrayDisplay($ConnInfo)
dim $status
dim $encrypts
$status = 0
$wbemFlagReturnImmediately = 0x10
$wbemFlagForwardOnly = 0x20
$colItems = ""
$strComputer = "localhost"

RegWrite("HKEY_LOCAL_MACHINE64\SOFTWARE\Microsoft\MBAM", "KeyRecoveryOptions", "REG_DWORD", "00000001")
RegWrite("HKEY_LOCAL_MACHINE64SOFTWARE\Microsoft\MBAM", "UseKeyRecoveryService", "REG_DWORD", "00000001")
RegWrite("HKEY_LOCAL_MACHINE64\SOFTWARE\Microsoft\MBAM", "KeyRecoveryServiceEndPoint", "REG_EXPAND_SZ", "http://uswvbde801.us.pinkertons.com/MBAMRecoveryAndHardwareService/CoreService.svc")
RegWrite("HKEY_LOCAL_MACHINE64\SOFTWARE\Microsoft\MBAM"   , "DeploymentTime", "REG_DWORD", "00000001")
RegWrite("HKEY_LOCAL_MACHINE64\SOFTWARE\Microsoft\MBAM", "NoStartupDelay", "REG_DWORD", "00000001")

;Restart MBAM Service and start encrypting
Run("sc stop MBAMAgent")
Sleep(15000)
Run("sc start MBAMAgent")


GUISetState(@SW_SHOW)

;Monitor Encryption Progress, update progress bar gui
Do
	;run command
$command = _GetDOSOutput("manage-bde -status c:")

	;parse each line of output into array
$aOutput = StringSplit($command,@CRLF,1)
	;look at each line
for $i = 1 to Ubound($aOutput) - 1
	;depending on the contents of the line, report some values
Select
	;if you find Percentage line
	Case StringInStr($aOutput[$i],"Percentage Encrypted:") > 0
		;store percent string
		$percent = $aOutput[$i]
		;trim to only the data needed
		$trimper = StringinStr($percent,":") + 2
		;return data
		$encrypts = int(StringMid($percent,$trimper))
		;store values in GUI
		GUICtrlSetData($progressbar,$encrypts)
		GUICtrlSetData($progresstext1,$encrypts & "%")
	Case StringInStr($aOutput[$i],"Conversion Status:") > 0
		$status = $aOutput[$i]
		$trimsta = StringinStr($status,":") + 2
		$statuss = StringMid($status,$trimsta)
		GUICtrlSetData($progresstext3,StringStripWS($statuss,3))
	Case StringInStr($aOutput[$i],"Size:") > 0
		$disk = $aOutput[$i]
		$trimdis = StringinStr($disk,":") + 2
		$disksizes = StringMid($disk,$trimdis)
		GUICtrlSetData($progresstext2,StringStripWS($disksizes,3))
   EndSelect

   Next
sleep(5000)

Until $encrypts = 100
;pause for a length after reaching 100%
sleep(15000)


;Query if Encryption has Completed
Do
$Output = ""
$objWMIService = ObjGet("winmgmts:\\" & $strComputer & "\root\cimv2\security\MicrosoftVolumeEncryption")
$colItems = $objWMIService.ExecQuery('SELECT * FROM Win32_EncryptableVolume', "WQL", _
        $wbemFlagReturnImmediately + $wbemFlagForwardOnly)


If IsObj($colItems) Then
    $i = 1
    For $objItem In $colItems
        ReDim $ConnInfo[$i + 1][3]
        ;$ConnInfo[$i][0] = $objItem.DriveLetter
        ;$ConnInfo[$i][1] = $objItem.ProtectionStatus
		;$ConnInfo[$i][2] = $objItem.ConversionStatus
        ;$i = $i + 1
        ;$ConnInfo[0][0] += 1
		If $objItem.DriveLetter = "C:" Then
		   $status = $objItem.ProtectionStatus
		   ConsoleWrite("Found C:!" & @CRLF)
		EndIf
		If $status = 1 Then
		   ConsoleWrite("Encrypted!")
		   ExitLoop
		   EndIf
    Next
 EndIf
 Sleep(15000)
Until $status = 1

;Turn DeploymentTime Off
RegWrite("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\MBAM", "DeploymentTime", "REG_DWORD", "00000000")


Func _GetDOSOutput($sCommand)
    Local $iPID, $sOutput = ""

    $iPID = Run('"' & @ComSpec & '" /c ' & $sCommand, "", @SW_HIDE, $STDERR_CHILD + $STDOUT_CHILD)
    While 1
        $sOutput &= StdoutRead($iPID, False, False)
        If @error Then
            ExitLoop
        EndIf
        Sleep(10)
    WEnd
    Return $sOutput
EndFunc   ;==>_GetDOSOutput