﻿ipmo ScheduledTasks
#how times a day should it run? (every 30 minutes)
$intervals = 24*2-1
$basetime = get-date


$action = New-ScheduledTaskAction -Execute 'Powershell.exe' -Argument '-NonInteractive -NoLogo -NoProfile -WindowStyle Hidden -file "C:\bkpsdev\unlock-aduser.ps1"'
$trigger = @()
for ($i=1; $i -le $intervals; $i++)
    {
    $time = ($basetime).addminutes(30*$i)
    $format = (get-date $time -format "H:mm")
    $trigger +=  New-ScheduledTaskTrigger -Daily -at $format
    }
Register-ScheduledTask -Action $action -Trigger $trigger -TaskName "unlock-bkaler" -Description "unlock my account if locked"  