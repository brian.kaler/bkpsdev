$DomainControllers = Get-WinADDomainControllers -TestAvailability



New-HTML -UseCssLinks -UseJavaScriptLinks {
    New-HTMLTable -DataTable $DomainControllers {
        New-HTMLTableHeader -Names 'IPV4Address', 'IPV6Address' -Title 'Ip Addresses' -Alignment center -Color White -BackGroundColor Gray
        New-HTMLTableHeader -Names 'SchemaMaster', 'PDCEmulator', 'RIDMaster', 'DomainNamingMasterMaster', 'InfrastructureMaster' -Title 'Roles' -Alignment center -Color White -BackGroundColor Gray
        New-HTMLTableHeader -Names 'LdapPort', 'SslPort' -Title 'Ports' -Alignment center -Color White -BackGroundColor Gray
        New-HTMLTableCondition -ComparisonType 'string' -Name 'SchemaMaster' -Operator eq -Value 'True' -BackgroundColor Green -Color White -Inline
        New-HTMLTableCondition -ComparisonType 'string' -Name 'PDCEmulator' -Operator eq -Value 'True' -BackgroundColor Green -Color White -Inline
        New-HTMLTableCondition -ComparisonType 'string' -Name 'RIDMaster' -Operator eq -Value 'True' -BackgroundColor Green -Color White -Inline
        New-HTMLTableCondition -ComparisonType 'string' -Name 'IsReadOnly' -Operator eq -Value 'True' -BackgroundColor Yellow -Color Black -Inline
        New-HTMLTableCondition -ComparisonType 'string' -Name 'Pingable' -Operator eq -Value 'True' -BackgroundColor Green -Color White -Inline
        New-HTMLTableCondition -ComparisonType 'string' -Name 'Pingable' -Operator ne -Value 'True' -BackgroundColor Red -Color White -Inline
    } -HideFooter
} -FilePath $PSScriptRoot\TestMyHTML.html -ShowHTML -TitleText 'My test'