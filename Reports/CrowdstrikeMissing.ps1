﻿import-module PSWriteHTML

$agentsystems = "Crowdstrike Falcon Agent_All Systems_Noncompliant"
$computers = Get-CMCollectionMember -CollectionName $agentsystems

$workstations = $computers | where {$_.deviceos -like "*Workstation*"} | Select Name,DeviceOS,LastPolicyRequest,LastClientCheckTime,LastDDR
$servers = $computers | where {$_.deviceos -like "*Server*"} | Select Name,DeviceOS,LastPolicyRequest,LastClientCheckTime,LastDDR
$timestamp = (get-date -Format ddMMyyyyhhmmss)
$reportpath = 'C:\temp\CrowdstrikeMissing'+$timestamp+'.html'

New-HTML -TitleText $ReportTitle -UseCssLinks -UseJavaScriptLinks -FilePath $reportpath {
    New-HTMLTab -TabName 'Gridview' {
        New-HTMLContent -HeaderText "Workstations" {
            New-HTMLContent -HeaderText 'Workstations' -CanCollapse {
                New-HTMLTable -DataTable $workstations -Verbose
            }
        }
        New-HTMLContent -HeaderText 'Servers' {
        New-HTMLContent -HeaderText 'Servers' -CanCollapse {
                New-HTMLTable -DataTable $servers -Verbose
        }
    }
}
} -ShowHTML