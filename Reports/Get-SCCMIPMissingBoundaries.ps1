﻿#$OSUUpdateDP = Get-CMDistributionPointGroup | Where {$_.Name -eq "OSU Centric Distribution Points"} | Get-CMDistributionPoint

#($OSUUpdateDP | select -ExpandProperty NetworkOSPath).substring(2)
function IsIpAddressInRange {
param(
        [string] $ipAddress,
        [string] $fromAddress,
        [string] $toAddress
    )

    $ip = [system.net.ipaddress]::Parse($ipAddress).GetAddressBytes()
    [array]::Reverse($ip)
    $ip = [system.BitConverter]::ToUInt32($ip, 0)

    $from = [system.net.ipaddress]::Parse($fromAddress).GetAddressBytes()
    [array]::Reverse($from)
    $from = [system.BitConverter]::ToUInt32($from, 0)

    $to = [system.net.ipaddress]::Parse($toAddress).GetAddressBytes()
    [array]::Reverse($to)
    $to = [system.BitConverter]::ToUInt32($to, 0)

    $from -le $ip -and $ip -le $to
}

If($ipquery -eq $null){
    $ipquery = Invoke-CMQuery -Name "IP Addr and Hostname"
    Write-Host "IP Address Query collected..."
}

If($boundaries -eq $null){
    $boundaries = Get-CMBoundary | where {$_.BoundaryType -eq 3}
    Write-Host "SCCM Boundaries collected..."
}


$validippatterns = @("^10.45(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){2}$",
"^10.21(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){2}$",
"^10.1(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){2}$")

#get only the first ip collected
$ipreport = @()
foreach($pc in $ipquery){
    $obj = New-Object -TypeName psobject
    $obj | add-member -MemberType NoteProperty -Name "Hostname" -Value $pc.name
    #validate our ip addreses
    $pcips = @()
    foreach($pip in $pc.IPAddresses){
        foreach($pattern in $validippatterns){
            if($pip -match $pattern){
            $pcips += $pip
            }
            If($pcips.count -eq 0){
                $pcips | out-null
            }
        }
    }

    If($pcips -ne $null){
      $i = 1
        foreach($validip in $pcips){
                    $ipvalue = [IPADDRESS]$validip
                    $subnetvalue = [string]$ipvalue.GetAddressBytes()[0]+"."+$ipvalue.GetAddressBytes()[1]+"."+$ipvalue.GetAddressBytes()[2]+".1"
                    $obj | Add-Member -MemberType NoteProperty -Name "IPAddress$i" -Value $ipvalue -Verbose
                    $obj | Add-Member -MemberType NoteProperty -Name "Subnet$i" -Value $subnetvalue -Verbose
                    $i++
        }
    $ipreport += $obj
    }


}
If($ipreport -ne $null){
    Write-Host "IP Report Processed Successfully"
}

#start the boundary piece

$boundaryreport = @()

foreach($ip in $ipreport){
    #boundary is guilty until proven innocent
    $ipinboundary = $false
        foreach($b in $boundaries){
            $rangestring = $b.value
            $rangestart = $rangestring.substring(0,$rangestring.indexof("-"))
            $rangeend = $rangestring.substring(($rangestring.indexof("-")+1))
            
            IF(IsIpAddressInRange -ipAddress $ip.ipaddress1 -fromAddress $rangestart -toAddress $rangeend){
                $ipinboundary = $true
                $ipinquestion = $ip.ipaddress1
                $obj = New-Object -TypeName PSObject
                $obj | Add-Member -MemberType NoteProperty -Name BoundaryName -Value $b.DisplayName
                $obj | Add-Member -MemberType NoteProperty -Name BoundaryRange -Value $b.value
                $obj | Add-Member -MemberType NoteProperty -Name IpTested -Value $ipinquestion
                $obj | Add-Member -MemberType NoteProperty -Name Validity -Value "Valid"
                }
            }
        If($ipinboundary -eq $false){
            Write-Host $ip.IPAddress1" was not found in a boundary"
            $ipinquestion = $ip.ipaddress1
            $obj = New-Object -TypeName PSObject
            $obj | Add-Member -MemberType NoteProperty -Name BoundaryName -Value "No Boundary"
            $obj | Add-Member -MemberType NoteProperty -Name BoundaryRange -Value "No Range"
            $obj | Add-Member -MemberType NoteProperty -Name IpTested -Value $ipinquestion
            $obj | Add-Member -MemberType NoteProperty -Name Validity -Value "Invalid"
            }
        $boundaryreport += $obj
        }
        

