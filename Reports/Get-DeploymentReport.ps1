﻿#Get-1809CurrentDeploymentReport
#Brian Kaler
#3/10/2020
#Returns HTML report of the current 1809 deployment


#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = "C:\bkpsdev\sccm"
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
set-location SSS:\

Function Get-CMDevicePrimaryUser{
    Param(
        [Parameter(Mandatory,ValueFromPipeline)]
        [ValidateNotNullorEmpty()]
        [Object]$cmdevice
    )
    $device = Get-CMDevice -Name $cmdevice
    $affinity = Get-CMUserDeviceAffinity -devicename $cmdevice
    if($affinity -ne $null){
        $user =  Get-CMUser -Name $affinity.uniqueusername -ErrorAction SilentlyContinue
    }
    If($user -ne $null){
        $userobj = Get-CMResource -ResourceId $user.resourceid -ErrorAction SilentlyContinue -fast
    }
    If($userobj -ne $null){
        $outobj = New-Object -TypeName PSObject
        $outobj | Add-Member -MemberType NoteProperty -Name Hostname -Value $cmdevice
        $outobj | Add-Member -MemberType NoteProperty -Name FullName -Value $userobj.FullUserName
        $outobj | Add-Member -MemberType NoteProperty -Name UniqueUserName -Value $userobj.UniqueUserName
        $outobj | Add-Member -MemberType NoteProperty -Name Mail -Value $userobj.Mail
        $outobj | Add-Member -MemberType NoteProperty -Name CurrentLoggedOnUser -Value $device.currentlogonuser
        $outobj | Add-Member -MemberType NoteProperty -Name LastLoggedOnUser -Value $device.lastlogonuser
    }
    Else{
        $outobj = New-Object -TypeName PSObject
        $outobj | Add-Member -MemberType NoteProperty -Name Hostname -Value $cmdevice
        $outobj | Add-Member -MemberType NoteProperty -Name FullName -Value "No Primary User"
        $outobj | Add-Member -MemberType NoteProperty -Name UniqueUserName -Value "No Primary User"
        $outobj | Add-Member -MemberType NoteProperty -Name Mail -Value "No Primary User"
        $outobj | Add-Member -MemberType NoteProperty -Name CurrentLoggedOnUser -Value $device.currentlogonuser
        $outobj | Add-Member -MemberType NoteProperty -Name LastLoggedOnUser -Value $device.lastlogonuser 
    }   
    Return $outobj
}

$ReportTitle = 'Windows 10 1809 - Deployment Report'

$1809clients = Get-CMCollectionMember -CollectionName "Windows 10 (1809 Only)"
$1709orlower = Get-CMCollectionMember -CollectionName "Windows 10 (1709 or Lower)"
$timestamp = (get-date -Format ddMMyyyyhhmmss)
$reportpath = 'C:\temp\mynextreportcopy'+$timestamp+'.html'

$deploymentname = ""
$deploymentid = "{1CA2A94E-0393-415B-83B0-3D99707CC842}"
#some examples for date selection, uncomment to use.
#$deploycutoff = (Get-Date).adddays(-15)
[datetime]$deploycutoff = "1/9/2019"

$deploymentobj = Get-CMDeployment -DeploymentId $deploymentid
$deploymentstatusobj = Get-CMDeploymentStatus | where {$_.DeploymentId -eq $deploymentid}


$deploymentreport = @()
foreach($obj in $deploymentstatusobj){
$details = Get-CMDeploymentStatusDetails -InputObject $obj
    Foreach($deet in $details){
        If($deet.SummarizationTime -ge $deploycutoff){
        $deploymentreport += $deet | select DeviceName,PackageName,SummarizationTime,StatusType,StatusDescription
        }
    }
}


$statustypelist = @{
1='Success'
2='InProgress'
3='RequirementsNotMet'
4='Unknown'
5='Error'
}

$report = @()
$i = 0
$total = $deploymentreport.count
foreach($dep in $deploymentreport){
    $i++
    $primaryuser = Get-CMDevicePrimaryUser -cmdevice $dep.devicename
    $statustypedescription = $statustypelist.item([int]$dep.statustype)
    #create our object
    $mrep = New-Object -TypeName PSObject
    $mrep | Add-Member -MemberType NoteProperty -Name DeviceName -Value $dep.devicename
    $mrep | Add-Member -MemberType NoteProperty -Name "Primary User" -Value $primaryuser.CurrentLoggedOnUser
    $mrep | Add-Member -MemberType NoteProperty -Name PackageName -Value $dep.PackageName
    $mrep | Add-Member -MemberType NoteProperty -Name SummarizationTime -Value $dep.SummarizationTime
    $mrep | Add-Member -MemberType NoteProperty -Name StatusType -Value $dep.StatusType
    $mrep | Add-Member -MemberType NoteProperty -Name StatusTypeDescription -Value $statustypedescription
    $mrep | Add-Member -MemberType NoteProperty -Name StatusDescription -Value $dep.StatusDescription
    #add it to the report
    $report += $mrep
    Write-Host "Processed:"$dep.DeviceName"($i of $total)"

}

New-HTML -TitleText $ReportTitle -UseCssLinks -UseJavaScriptLinks -FilePath $reportpath {
    New-HTMLTab -TabName 'Gridview' {
        New-HTMLContent -HeaderText "Groups" {
            New-HTMLContent -HeaderText 'Deployment' -CanCollapse {
                New-HTMLTable -DataTable $report -Verbose
            }
        }
        New-HTMLContent -HeaderText 'Test 2' {

        }
    }
    New-HTMLTab -TabName 'Charts' {
        New-HTMLChart -Title "Feature Upgrade Counts" -Gradient {
            New-ChartPie -Name "Windows 10 (1809 Only)" -value ($1809clients).count
            New-ChartPie -Name "Windows 10 (1709 or Lower)" -value $1709orlower.count
        }
        New-HTMLChart -Title "Deployment Status" -Gradient {
            New-ChartPie -Name "Success" -value ($report.statustypedescription | Where {$_ -eq "Success"}).count
            New-ChartPie -Name "In Progress" -value ($report.statustypedescription | Where {$_ -eq "InProgress"}).count
            New-ChartPie -Name "Unknown" -value ($report.statustypedescription | Where {$_ -eq "Unknown"}).count
            New-ChartPie -Name "Error" -value ($report.statustypedescription | Where {$_ -eq "Error"}).count
        }


    }
} -ShowHTML