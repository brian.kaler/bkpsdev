﻿#Get-1809CurrentDeploymentReport
#Brian Kaler
#3/10/2020
#Returns HTML report of the current 1809 deployment


#define the directory the scripts reside in
Write-host "INFO: Loading subscripts and modules, please wait..."
$scriptroot = "C:\bkpsdev\sccm"
#Load the SCCM Module
$SiteCode = "SSS" 
$SiteServer = "uswscm901.us.pinkertons.com" 
# Import the ConfigurationManager.psd1 module  
$scmod = Import-Module "$($ENV:SMS_ADMIN_UI_PATH)\..\ConfigurationManager.psd1" -PassThru
set-location SSS:\

Function Get-CMDevicePrimaryUser{
    Param(
        [Parameter(Mandatory,ValueFromPipeline)]
        [ValidateNotNullorEmpty()]
        [Object]$cmdevice
    )
    $device = Get-CMDevice -Name $cmdevice
    $affinity = Get-CMUserDeviceAffinity -devicename $cmdevice
    if($affinity -ne $null){
        $user =  Get-CMUser -Name $affinity.uniqueusername -ErrorAction SilentlyContinue
    }
    If($user -ne $null){
        $userobj = Get-CMResource -ResourceId $user.resourceid -ErrorAction SilentlyContinue -fast
    }
    If($userobj -ne $null){
        $outobj = New-Object -TypeName PSObject
        $outobj | Add-Member -MemberType NoteProperty -Name Hostname -Value $cmdevice
        $outobj | Add-Member -MemberType NoteProperty -Name FullName -Value $userobj.FullUserName
        $outobj | Add-Member -MemberType NoteProperty -Name UniqueUserName -Value $userobj.UniqueUserName
        $outobj | Add-Member -MemberType NoteProperty -Name Mail -Value $userobj.Mail
        $outobj | Add-Member -MemberType NoteProperty -Name CurrentLoggedOnUser -Value $device.currentlogonuser
        $outobj | Add-Member -MemberType NoteProperty -Name LastLoggedOnUser -Value $device.lastlogonuser
    }
    Else{
        $outobj = New-Object -TypeName PSObject
        $outobj | Add-Member -MemberType NoteProperty -Name Hostname -Value $cmdevice
        $outobj | Add-Member -MemberType NoteProperty -Name FullName -Value "No Primary User"
        $outobj | Add-Member -MemberType NoteProperty -Name UniqueUserName -Value "No Primary User"
        $outobj | Add-Member -MemberType NoteProperty -Name Mail -Value "No Primary User"
        $outobj | Add-Member -MemberType NoteProperty -Name CurrentLoggedOnUser -Value $device.currentlogonuser
        $outobj | Add-Member -MemberType NoteProperty -Name LastLoggedOnUser -Value $device.lastlogonuser 
    }   
    Return $outobj
}

$ReportTitle = 'Windows 10 1809 - Deployment Report'
$timestamp = (get-date -Format ddMMyyyyhhmmss)
$reportpath = 'C:\temp\1809CurrentDeployment'+$timestamp+'.html'

$deploymentname = ""
$deploymentid = "SSS200DA"
$UpgradeCollection = "Windows 10 (1709 or Lower)"
#some examples for date selection, uncomment to use.
#$deploycutoff = (Get-Date).adddays(-15)
[datetime]$deploycutoff = "1/9/2019"

$deploymentobj = Get-CMDeployment -DeploymentId $deploymentid
$deploymentstatusobj = Get-CMDeploymentStatus | where {$_.DeploymentId -eq $deploymentid}

If(($deploymentobj -or $deploymentstatusobj) -eq $null){
    Write-Host "Script will exit: could not find a deployment or deployment status for the deployment with DEPLOYMENTID: "$deploymentid
    Exit
}

$deploymentreport = @()
$progress1val = 0
$progress1max = $deploymentstatusobj.count
foreach($obj in $deploymentstatusobj){
    $details = Get-CMDeploymentStatusDetails -InputObject $obj
    #lets create a custom psobject out of the deployment details.
    $myobj = @()
    $progress1val++
    Write-Progress -Activity "Processing Status Objects" -PercentComplete (($progress1val / $progress1max) * 100)
    foreach($deet in $details){
        $list = New-Object -TypeName PSobject
        $listprops = ($deet | select -Property *).propertylist
        $listcount = $listprops.Count
        for($i = 1; $i -lt $listcount; $i++){
            $name = $listprops.keys | select -Index $i
            $value = $listprops.Values | select -Index $i
            $list | Add-Member -MemberType NoteProperty -Name $name -Value $value
        }
        #mark it as upgraded if it's in the collection
        If((get-cmdevice -Name $deet.DeviceName -CollectionName $UpgradeCollection -Fast) -ne $null){
            $list | Add-Member -MemberType NoteProperty -Name "Upgrade Assigned" -Value $true
        }else{
            $list | Add-Member -MemberType NoteProperty -Name "Upgrade Assigned" -Value $false
        }
        #consider our cutoff time for the report
        If($deet.SummarizationTime -ge $deploycutoff){
            $list | Add-Member -MemberType NoteProperty -Name "Cutoff Status" -Value $false
        }else{
            $list | Add-Member -MemberType NoteProperty -Name "Cutoff Status" -Value $true
        }
        $myobj += $list
    }

    #top line will filter on upgrade/cutoff status
    #$deploymentreport += $myobj | ? { ($_.'Upgrade Status' -eq $false) -and ($_.'Cutoff Status' -eq $false) }
    $deploymentreport += $myobj

}
    Write-Progress -Activity "Processing Status Objects" -status "Ready" -Completed 

$statustypelist = @{
1='Success'
2='InProgress'
3='RequirementsNotMet'
4='Unknown'
5='Error'
}

$reportsource = @()
$i = 0
$total = $deploymentreport.count
Write-Host "Gathering Primary User and Device Data"
foreach($dep in $deploymentreport){
    $i++
    $device = Get-CMDevice -Name $dep.DeviceName -CollectionName $UpgradeCollection  -Fast
    $statustypedescription = $statustypelist.item([int]$dep.statustype)
    #create our object
    $mrep = New-Object -TypeName PSObject
    $mrep | Add-Member -MemberType NoteProperty -Name DeviceName -Value $dep.devicename
    $mrep | Add-Member -MemberType NoteProperty -Name DeviceOSBuild -Value (($device.deviceosbuild) | Select-String -Pattern '(\d+\.)(\d+\.)(\d+)').matches.Value
    $mrep | Add-Member -MemberType NoteProperty -Name DevicePrimaryUser -Value $device.PrimaryUser
    $mrep | Add-Member -MemberType NoteProperty -Name DeviceLastLogonUser -Value $device.LastLogonUser
    $mrep | Add-Member -MemberType NoteProperty -Name PackageName -Value $dep.PackageName
    $mrep | Add-Member -MemberType NoteProperty -Name SummarizationTime -Value $dep.SummarizationTime
    $mrep | Add-Member -MemberType NoteProperty -Name StatusType -Value $dep.StatusType
    $mrep | Add-Member -MemberType NoteProperty -Name StatusTypeDescription -Value $statustypedescription
    $mrep | Add-Member -MemberType NoteProperty -Name StatusDescription -Value $dep.StatusDescription
    $mrep | Add-Member -MemberType NoteProperty -Name "Upgrade Assigned" -Value $dep.'Upgrade Assigned'
    $mrep | Add-Member -MemberType NoteProperty -Name "Cutoff Status" -Value $dep.'Cutoff Status'
    #now lets determine how long the client has been offline
    #add it to the report
    $hours = ($device.CNLastOfflineTime - $device.CNLastOnlineTime).hours
    $days = ($device.CNLastOfflineTime - $device.CNLastOnlineTime).days
    If([Math]::Sign($hours) -eq -1){
        $mrep | Add-Member -MemberType NoteProperty -Name 'hours online' -Value $hours
        $mrep | Add-Member -MemberType NoteProperty -Name 'days online' -Value $days
    }
    If([Math]::Sign($hours) -eq 1){
        $mrep | Add-Member -MemberType NoteProperty -Name 'hours offline' -Value $hours
        $mrep | Add-Member -MemberType NoteProperty -Name 'days offline' -Value $days
    }
    If(($hours -or $days) -eq $null){
        $mrep | Add-Member -MemberType NoteProperty -Name 'hours offline' -Value "0"
        $mrep | Add-Member -MemberType NoteProperty -Name 'days offline' -Value "0"
    }
    $reportsource += $mrep
    Write-Host "Processed DEVICE:"($dep.devicename)"for PRIMARYUSER:"($device.PrimaryUser)"($i of $total)"

}

#gather some more statistics
$reportdeviceosversions = (($report.deviceosbuild) | group)
#Remove any system not found in the upgrade colection
$report = $reportsource | ? {$_.'Upgrade Assigned' -eq $true}

New-HTML -TitleText $ReportTitle -UseCssLinks -UseJavaScriptLinks -FilePath $reportpath {
    New-HTMLTab -TabName 'Gridview' {
        New-HTMLContent -HeaderText "Groups" {
            New-HTMLContent -HeaderText 'Deployment' -CanCollapse {
                New-HTMLTable -DataTable $report -Verbose
            }
        }
        New-HTMLContent -HeaderText 'Test 2' {

        }
    }
    New-HTMLTab -TabName 'Charts' {
        New-HTMLChart -Title "Feature Upgrade Counts" -Gradient {
            ForEach($v in $reportdeviceosversions){
                New-ChartPie -Name $v.name -value $v.count
            }  
        }
        New-HTMLChart -Title "Report Cutoff Status" -Gradient {
            New-ChartPie -Name "Within Cutuff Date" -value ($report.'cutoff status' | Where {$_ -eq $false}).count
            New-ChartPie -Name "Outside Cutoff Date" -value ($report.'cutoff status' | Where {$_ -eq "InProgress"}).count
        }
        New-HTMLChart -Title "Upgrade Assignment Status" -Gradient {
            New-ChartPie -Name "Upgrade Assigned" -value ($report.'Upgrade Assigned' | Where {$_ -eq $true}).count
            New-ChartPie -Name "Upgrade Not Assigned" -value ($report.'Upgrade Assigned' | Where {$_ -eq $false}).count
        }
        New-HTMLChart -Title "Deployment Status (from Monitor/Deployments)" -Gradient {
            New-ChartPie -Name "Success" -value ($report.statustypedescription | Where {$_ -eq "Success"}).count
            New-ChartPie -Name "In Progress" -value ($report.statustypedescription | Where {$_ -eq "InProgress"}).count
            New-ChartPie -Name "Unknown" -value ($report.statustypedescription | Where {$_ -eq "Unknown"}).count
            New-ChartPie -Name "Error" -value ($report.statustypedescription | Where {$_ -eq "Error"}).count
        }

    }
} -ShowHTML