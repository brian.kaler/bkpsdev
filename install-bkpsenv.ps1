﻿#create ps environment script
$root = "c:\"
#install chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
#install choco apps
choco install notepadplusplus -y
choco install git -y
choco install poshgit -y
choco install vscode -y
#install psget (for getting powershell modules)
(new-object Net.WebClient).DownloadString("http://psget.net/GetPsGet.ps1") | iex
#install modules
install-module posh-git
install-module importexcel
install-module pswritehtml




#setup git
git config --global user.name "brian.kaler"
git config --global user.email "bkal3r@gmail.com"
#fix zscaler issue
git config --global http.sslVerify false
#change directory and clone
cd $root
git clone https://gitlab.com/brian.kaler/bkpsdev.git
#
