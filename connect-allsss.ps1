﻿$acctName="brian.kaler@securitasinc.com"
$orgName="securitasgroup"
$mycreds = Import-Clixml C:\temp\bksecuritas.txt
#Azure Active Directory
Connect-AzureAD -Credential $mycreds
#SharePoint Online
#Connect-SPOService -Url https://$orgName-admin.sharepoint.com
#Skype for Business Online
#$sfboSession = New-CsOnlineSession -UserName $acctName
#Import-PSSession $sfboSession
#Exchange Online
Connect-ExchangeOnline -UserPrincipalName $acctName -ShowProgress $true
#Teams
Import-Module MicrosoftTeams
Connect-MicrosoftTeams -Credential $mycreds