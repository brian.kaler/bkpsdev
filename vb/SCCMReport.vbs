'SCCM Client Status Report

'This report will examine AD for computer accounts and compare that
'to information in the SCCM database.

Const cClientVersion = "5.00.7804.1000"

'List of clients that we don't care if they have SCCM installed or not
Dim arrNoClient
arrNoClient = Array( _
	 "skip.this.computer.FQDN" _
	,"skip.this.other.computer.FQDN" _
)

'Define variables
Dim objExcel, RowCtr, SO, objTrans, dicNoClient
Set SO = WScript.StdOut
Set objTrans = CreateObject("NameTranslate")
Set dicNoClient = CreateObject("Scripting.Dictionary")

'Convert the arrNoClient array into a dictionary so that it is easy to search
On Error Resume Next
For I = LBound(arrNoClient) to UBound(arrNoClient)
	S = LCase(arrNoClient(I))
	dicNoClient.Add S,S
Next
SO.WriteLine "Accumulated " & dicNoClient.Count & _
	" computer name(s) that are okay as non-clients"
On Error Goto 0

'Get TZ Bias
Dim lngBias, lngBiasKey, objShell
Set objShell = CreateObject("Wscript.Shell")
lngBiasKey = objShell.RegRead("HKLM\System\CurrentControlSet\Control\" _
    & "TimeZoneInformation\ActiveTimeBias")
If (UCase(TypeName(lngBiasKey)) = "LONG") Then
    lngBias = lngBiasKey
ElseIf (UCase(TypeName(lngBiasKey)) = "VARIANT()") Then
    lngBias = 0
    For k = 0 To UBound(lngBiasKey)
        lngBias = lngBias + (lngBiasKey(k) * 256^k)
    Next
End If
SO.WriteLine "Active Time Zone Bias is " & lngBias & " minutes"

'Launch Excel and populate the worksheet with the basic headers for the report
SO.WriteLine "Setting up Excel"
Set objExcel = CreateObject("Excel.Application")
objExcel.visible = True
Set WB = objExcel.Workbooks.Add
WB.Activate
'Modify the names of the tabs to be the top level domain
'FQDNs to be checked against.  Remove tabs that aren't
'to be used.
'Change the tab colors to whatever groupings you like.
WB.Worksheets(1).Name = "us.pinkertons.com"
WB.Worksheets(1).Tab.Color = RGB(128,255,128)
'WB.Worksheets(2).Name = "domain.root.2.local"
'WB.Worksheets(2).Tab.Color = RGB(128,255,128)
'WB.Worksheets(3).Name = "domain.root.3.local"
'WB.Worksheets(3).Tab.Color = RGB(128,255,128)
'WB.Worksheets.Add ,WB.Worksheets(3),6
'WB.Worksheets(4).Name = "domain.root.4.local"
'WB.Worksheets(4).Tab.Color = RGB(255,255,128)
'WB.Worksheets(5).Name = "domain.root.5.local"
'WB.Worksheets(5).Tab.Color = RGB(128,128,255)
'WB.Worksheets(6).Name = "domain.root.6.local"
'WB.Worksheets(6).Tab.Color = RGB(128,128,255)
'WB.Worksheets(7).Name = "domain.root.7.local"
'WB.Worksheets(7).Tab.Color = RGB(64,64,64)
'WB.Worksheets(8).Name = "domain.root.8.local"
'WB.Worksheets(8).Tab.Color = RGB(255,64,64)
'WB.Worksheets(9).Name = "NOTES"

Dim ReportTime
ReportTime = Now

SO.WriteLine "Connecting to SCCM"
Dim SCCM
Set SCCM = CreateObject("ADODB.Connection")
SCCM.Provider = "sqloledb"
				SCCM.Open "Server=USWSCD801;Database=CM_WOC;Trusted_Connection=yes"

'Modify the sheet numbers to reflect the sheets containing domains
'to be scanned and compared with SCCM.
For SheetNo = 1 to 8	'WB.Worksheets.Count
	Set WS = WB.Worksheets(SheetNo)
	With WS
		SO.WriteLine "Setting up sheet for " & .Name
		.Activate

		.Cells(1,1) = "SCCM Client Status Report for " & .Name
		.Range("A1:I1").Merge
		.Cells(2,1) = FormatDateTime(ReportTime)

		.Cells(4,1) = "DOMAIN"
		.Cells(5,1) = "Name"
		.Cells(5,2) = "Last Logon"
		.Cells(5,3) = "Days Since Logon"
		.Cells(5,4) = "Last PWD Change"
		.Cells(5,5) = "Days Since PWD"
		.Cells(5,6) = "Disabled"
		.Cells(5,7) = "OU"
		.Range("A4:G4").Merge

		.Cells(4,8) = "SCCM RESOURCE"
		.Cells(5,8) = "Name"
		.Cells(5,9) = "Client"
		.Cells(5,10) = "Resource ID"
		.Cells(5,11) = "AD Discovery"
		.Cells(5,12) = "Days Since AD"
		.Cells(5,13) = "Heartbeat"
		.Cells(5,14) = "Days Since HB"
		.Cells(5,15) = "HW Inventory"
		.Cells(5,16) = "Days Since HW"
		.Cells(5,17) = "Health"
		.Range("H4:Q4").Merge

		.Cells(4,18) = "PATCHES"
		.Cells(5,18) = "Last Successful Scan"
		.Cells(5,19) = "Days Since Scan"
		.Cells(5,20) = "Last Scan Attempt"
		.Cells(5,21) = "Days Since Attempt"
		.Cells(5,22) = "Scan Errors"
		.Cells(5,23) = "Last Apply"
		.Cells(5,24) = "Days Since Apply"
		.Cells(5,25) = "Apply Errors"
		.Cells(5,26) = "Authorized"
		.Cells(5,27) = "Installed"
		.Cells(5,28) = "Install %"
		.Cells(5,29) = "Required"
		.Cells(5,30) = "Require %"
		.Range("R4:AD4").Merge

		'Query AD
		Set AD = GetObject("LDAP://" & .Name)
		RowCtr = 6
		EnumContainer AD, WS

		'Sort our results by AD account name
		.Range("A6:ZZ" & CStr(RowCtr-1)).Sort .Range("A6"),1

		'Add icon sets
		'LastLogon Days
		.Range("C6:C" & CStr(RowCtr-1)).Select
		Set FC = objExcel.Selection.FormatConditions.AddIconSetCondition
		FC.IconSet = WB.IconSets(4)	'3 Traffic Lights 1
		FC.ReverseOrder = True
		With FC.IconCriteria(3)
			.Type = 0
			.Value = 105
			.Operator = 7
		End With
		With FC.IconCriteria(2)
			.Type = 0	'Number
			.Value = 45
			.Operator = 7	'>=
		End With

		'PWD Days
		.Range("E6:E" & CStr(RowCtr-1)).Select
		Set FC = objExcel.Selection.FormatConditions.AddIconSetCondition
		FC.IconSet = WB.IconSets(4)	'3 Traffic Lights 1
		FC.ReverseOrder = True
		With FC.IconCriteria(3)
			.Type = 0
			.Value = 91
			.Operator = 7
		End With
		With FC.IconCriteria(2)
			.Type = 0	'Number
			.Value = 31
			.Operator = 7	'>=
		End With

		'AD Discovery
		.Range("L6:L" & CStr(RowCtr-1)).Select
		Set FC = objExcel.Selection.FormatConditions.AddIconSetCondition
		FC.IconSet = WB.IconSets(4)
		FC.ReverseOrder = True
		With FC.IconCriteria(3)
			.Type = 0
			.Value = 15
			.Operator = 7
		End With
		With FC.IconCriteria(2)
			.Type = 0
			.Value = 8
			.Operator = 7
		End With

		'Heartbeat
		.Range("N6:N" & CStr(RowCtr-1)).Select
		Set FC = objExcel.Selection.FormatConditions.AddIconSetCondition
		FC.IconSet = WB.IconSets(4)
		FC.ReverseOrder = True
		With FC.IconCriteria(3)
			.Type = 0
			.Value = 15
			.Operator = 7
		End With
		With FC.IconCriteria(2)
			.Type = 0
			.Value = 8
			.Operator = 7
		End With

		'HW Inventory
		.Range("P6:P" & CStr(RowCtr-1)).Select
		Set FC = objExcel.Selection.FormatConditions.AddIconSetCondition
		FC.IconSet = WB.IconSets(4)
		FC.ReverseOrder = True
		With FC.IconCriteria(3)
			.Type = 0
			.Value = 18
			.Operator = 7
		End With
		With FC.IconCriteria(2)
			.Type = 0
			.Value = 11
			.Operator = 7
		End With

		'Scan
		.Range("S6:S" & CStr(RowCtr-1)).Select
		Set FC = objExcel.Selection.FormatConditions.AddIconSetCondition
		FC.IconSet = WB.IconSets(4)
		FC.ReverseOrder = True
		With FC.IconCriteria(3)
			.Type = 0
			.Value = 22
			.Operator = 7
		End With
		With FC.IconCriteria(2)
			.Type = 0
			.Value = 8
			.Operator = 7
		End With

		'Scan Attempt
		.Range("U6:U" & CStr(RowCtr-1)).Select
		Set FC = objExcel.Selection.FormatConditions.AddIconSetCondition
		FC.IconSet = WB.IconSets(4)
		FC.ReverseOrder = True
		With FC.IconCriteria(3)
			.Type = 0
			.Value = 22
			.Operator = 7
		End With
		With FC.IconCriteria(2)
			.Type = 0
			.Value = 8
			.Operator = 7
		End With

		'Apply
		.Range("X6:X" & CStr(RowCtr-1)).Select
		Set FC = objExcel.Selection.FormatConditions.AddIconSetCondition
		FC.IconSet = WB.IconSets(4)
		FC.ReverseOrder = True
		With FC.IconCriteria(3)
			.Type = 0
			.Value = 61
			.Operator = 7
		End With
		With FC.IconCriteria(2)
			.Type = 0
			.Value = 31
			.Operator = 7
		End With

		.Range("A1:AZ5").Font.Bold = True

		'Auto-fit the columns
		.Columns("A:AZ").AutoFit

		.Cells(3,1).Activate

	End With
Next

'This is a set of notes included in the workbook for reporting limitations
'to upper management.
Set WS = WB.Worksheets(9)
With WS
	.Cells(1,1) = "NOTES"
	.Cells(1,1).Font.Bold = True
	.Cells(3,1) = _
		"SCCM only scans certain AD OUs and Containers for computer objects."
	.Cells(5,1) = _
		"Further, SCCM only pulls in objects that have both reset their password"
	.Cells(6,1) = "in the last 180 days and logged on in the last 90 days."
	.Cells(8,1) = _
		"SCCM will also remove data from the database for objects that have"
	.Cells(9,1) = _
		"not returned any information (inventory, heartbeat, status) and have"
	.Cells(10,1) = "not been discovered by any process for at least 90 days."

	.Cells(12,1) = _
		"SCCM records are aligned based on computer name.  This may result"
	.Cells(13,1) = _
		"in the wrong record matching if the name is used across domains or"
	.Cells(14,1) = "if there are multiple records in SCCM matching the name."

	.Cells(16,1) = _
		"At this time, wm-dmz.local cannot be scanned due to restrictions."

	.Cells(1,3) = "COLUMNS"
	.Cells(1,3).Font.Bold = True
	.Range("C1:D1").Merge
	.Cells(3,3) = "Name"
	.Cells(3,4) = "Computer name from Active Directory"
	.Cells(4,3) = "Last Logon"
	.Cells(4,4) = "Date of last logon timestamp (which is only updated if the " & _
		"logon is within the last 14 days)"
	.Cells(5,3) = "Days Since Logon"
	.Cells(5,4) = "Calculation of age vs. when the report was run"
	.Cells(6,3) = "Last PWD Change"
	.Cells(6,4) = "Date of last password change"
	.Cells(7,3) = "Days Since PWD"
	.Cells(7,4) = "Calculation of age vs. when the report was run"
	.Cells(8,3) = "Disabled"
	.Cells(8,4) = "Indicates if the account is disabled or not; " & _
		"enabled is shown with a period (.)"
	.Cells(9,3) = "OU"
	.Cells(9,4) = "Location of the account within the domain"

	.Cells(11,3) = "Name"
	.Cells(11,4) = "Computer name from SCCM"
	.Cells(12,3) = "Client"
	.Cells(12,4) = "Indicates whether the computer is a client or if there " & _
		"are major issues"
	.Cells(13,3) = "Resource ID"
	.Cells(13,4) = "Numeric identifier within SCCM"
	.Cells(14,3) = "AD Discovery"
	.Cells(14,4) = "Date of last discovery within Active Directory (full or delta)"
	.Cells(15,3) = "Days Since AD"
	.Cells(15,4) = "Calculation of age vs. when the report was run"
	.Cells(16,3) = "Heartbeat"
	.Cells(16,4) = "Date of last heartbeat from client"
	.Cells(17,3) = "Days Since HB"
	.Cells(17,4) = "Calculation of age vs. when the report was run"
	.Cells(18,3) = "HW Inventory"
	.Cells(18,4) = "Date of last hardware inventory from client"
	.Cells(19,3) = "Days Since HW"
	.Cells(19,4) = "Calculation of age vs. when the report was run"
	.Cells(20,3) = "Health"
	.Cells(20,4) = "Status from the Client Health Check process; indicates " & _
		"health pass/fail and whether the client is considered active or not"

	.Cells(22,3) = "Last Successful Scan"
	.Cells(22,4) = "Date of last reported successful scan for applicable patches"
	.Cells(23,3) = "Days Since Scan"
	.Cells(23,4) = "Calculation of age vs. when the report was run"
	.Cells(24,3) = "Last Scan Attempt"
	.Cells(24,4) = "Date of last attempted scan"
	.Cells(25,3) = "Days Since Attempt"
	.Cells(25,4) = "Calculation of age vs. when the report was run"
	.Cells(26,3) = "Scan Errors"
	.Cells(26,4) = "Status or error code associated with the last scan attempt"
	.Cells(27,3) = "Last Apply"
	.Cells(27,4) = "Date of the last update installation on the client; " & _
		"NOTE: SCCM only keeps about 60 days of data"
	.Cells(28,3) = "Days Since Apply"
	.Cells(28,4) = "Calculation of age vs. when the report was run"
	.Cells(29,3) = "Apply Errors"
	.Cells(29,4) = "The most recent error code from the client after the " & _
		"most recent successful update installation"
	.Cells(30,3) = "Authorized"
	.Cells(30,4) = "Total number of updates authorized and assigned to the client"
	.Cells(31,3) = "Installed"
	.Cells(31,4) = "Number of updates installed or not applicable to the client"
	.Cells(32,3) = "Install %"
	.Cells(32,4) = "Percentage of Installed/Authorized"
	.Cells(33,3) = "Required"
	.Cells(33,4) = "Number of updates authorized but missing on the client"
	.Cells(34,3) = "Require %"
	.Cells(34,4) = "Percentage of Required/Authorized"

	.Columns("A:AZ").AutoFit
End With

WB.Worksheets(1).Activate

objExcel.Visible = True

Function EnumContainer(objContainer, WS)
	Dim objItem, RS, RS1, RS2, RID, FQDN
	objContainer.Filter=Array("computer")
	For Each objItem in objContainer
		SO.WriteLine objItem.AdsPath
		AN = objItem.Get("samAccountName")
		AN = Left(AN,Len(AN)-1)
		FQDN = LCase(AN & "." & WS.Name)
		WS.Cells(RowCtr,1) = AN
		On Error Resume Next
		Set WC = objItem.Get("lastLogonTimestamp")
		If Err.Number = 0 Then
			On Error Goto 0
			If TypeName(WC) = "Object" Then
				DD = Integer8Date(WC,lngBias)
				WS.Cells(RowCtr,2) = FormatDateTime(DD)
				WS.Cells(RowCtr,3) = DateDiff("d",DD,ReportTime)
			End If
		End If
		On Error Resume Next
		Set WC = objItem.Get("pwdLastSet")
		If Err.Number = 0 Then
			On Error Goto 0
			If TypeName(WC) = "Object" Then
				DD = Integer8Date(WC,lngBias)
				If DD <> #1/1/1601# Then
					WS.Cells(RowCtr,4) = FormatDateTime(DD)
					WS.Cells(RowCtr,5) = DateDiff("d",DD,ReportTime)
				End If
			End If
		End If
		On Error Goto 0
		If objItem.AccountDisabled Then
			WS.Cells(RowCtr,6) = "DISABLED"
		Else
			WS.Cells(RowCtr,6) = "."
		End If
		DN = objItem.Get("distinguishedName")
		WS.Cells(Rowctr,7) = Mid(DN,InStr(DN,",")+1)

		'See if there is a row in SCCM
		RID = "0"
		Set RS = SCCM.Execute("Select R.Name0,R.Client0,R.Active0,R.ResourceID," & _
		"R.Client_Version0 from v_R_System R where R.Name0='" & AN & _
		"' and R.Obsolete0 <> 1 and R.ResourceID<2000000000")
		If Not RS.EOF Then
			Do Until RS.EOF
				WS.Cells(RowCtr,8) = RS("Name0")
				If IsNull(RS("Client0")) Then
					WS.Cells(RowCtr,9) = "NOT A CLIENT"
					If dicNoClient.Exists(FQDN) Then WS.Cells(RowCtr,9) = _
						"NOT A CLIENT (ALLOWED)"
				ElseIf RS("Client0") = 0 Then
					WS.Cells(RowCtr,9) = "NOT A CLIENT"
					If dicNoClient.Exists(FQDN) Then WS.Cells(RowCtr,9) = _
						"NOT A CLIENT (ALLOWED)"
				Else
					If RS("Active0") = 1 Then
						WS.Cells(RowCtr,9) = "client"
						If RS("Client_Version0") <> cClientVersion Then _
							WS.Cells(RowCtr,9) = "WRONG VERSION"
					Else
						WS.Cells(RowCtr,9) = "INACTIVE CLIENT"
						If dicNoClient.Exists(FQDN) Then _
							WS.Cells(RowCtr,9) = "INACTIVE CLIENT (ALLOWED)"
					End If
				End If
				RID = RS("ResourceID")
				WS.Cells(RowCtr,10) = RID
				RS.MoveNext
			Loop
		Else
			Set RS = SCCM.Execute("Select R.Name0,R.Client0,R.Active0" & _
					",R.ResourceID,R.Client_Version0,R.Obsolete0 from " & _
					"v_R_System R where R.Name0='" & AN & "'")
			If Not RS.EOF Then
				Do Until RS.EOF
					WS.Cells(RowCtr,8) = RS("Name0")
					If IsNull(RS("Client0")) Then
						WS.Cells(RowCtr,9) = "NOT A CLIENT"
						If dicNoClient.Exists(FQDN) Then WS.Cells(RowCtr,9) = _
							"NOT A CLIENT (ALLOWED)"
					ElseIf RS("Client0") = 0 Then
						WS.Cells(RowCtr,9) = "NOT A CLIENT"
						If dicNoClient.Exists(FQDN) Then WS.Cells(RowCtr,9) = _
							"NOT A CLIENT (ALLOWED)"
					Else
						If RS("Active0") = 1 Then
							WS.Cells(RowCtr,9) = "client"
							If RS("Client_Version0") <> cClientVersion Then _
								WS.Cells(RowCtr,9) = "WRONG VERSION"
						Else
							WS.Cells(RowCtr,9) = "INACTIVE CLIENT"
							If dicNoClient.Exists(FQDN) Then _
								WS.Cells(RowCtr,9) = "INACTIVE CLIENT (ALLOWED)"
						End If
					End If
					If RS("Obsolete0") = 1 Then
						WS.Cells(RowCtr,9) = "OBSOLETE"
						If dicNoClient.Exists(FQDN) Then _
							WS.Cells(RowCtr,9) = "OBSOLETE (ALLOWED)"
					End If
					RID = RS("ResourceID")
					WS.Cells(RowCtr,10) = RID
					RS.MoveNext
				Loop
			End If
		End If

		'Get AD Discovery Date
		Set RS = SCCM.Execute("Select * from v_AgentDiscoveries where " & _
			"AgentName='SMS_AD_SYSTEM_DISCOVERY_AGENT' and ResourceID = " & RID)
		If Not RS.EOF Then
			WS.Cells(RowCtr,11) = CDate(RS("AgentTime"))
			WS.Cells(RowCtr,12) = DateDiff("d",CDate(RS("AgentTime")),ReportTime)
		End If

		'Get some health stats
		Set RS = _
			SCCM.Execute("Select * from v_CH_ClientSummary where ResourceID = " & _
			RID)
		If Not RS.EOF Then
			If Not IsNull(RS("LastDDR")) Then
				WS.Cells(RowCtr,13) = CDate(RS("LastDDR"))
				WS.Cells(RowCtr,14) = DateDiff("d",CDate(RS("LastDDR")),ReportTime)
			End If
			If Not IsNull(RS("LastHW")) Then
				WS.Cells(RowCtr,15) = CDate(RS("LastHW"))
				WS.Cells(RowCtr,16) = DateDiff("d",CDate(RS("LastHW")),ReportTime)
			End If
			WS.Cells(RowCtr,17) = RS("ClientStateDescription")
		End If

		'Get Patch Stats
		Set RS = _
			SCCM.Execute("Select * from v_UpdateScanStatus where ResourceID = " & _
			RID)
		If Not RS.EOF Then
			If Not IsNull(RS("ScanTime")) Then
				WS.Cells(RowCtr,18) = CDate(RS("Scantime"))
				WS.Cells(RowCtr,19) = DateDiff("d",CDate(RS("ScanTime")),ReportTime)
			End If
			If Not IsNull(RS("LastScanTime")) Then
				WS.Cells(RowCtr,20) = CDate(RS("LastScanTime"))
				WS.Cells(RowCtr,21) = _
					DateDiff("d",CDate(RS("LastScanTime")),ReportTime)
			End If
			If Not IsNull(RS("LastScanState")) Then
				Select Case RS("LastScanState")
					Case 0 : WS.Cells(RowCtr,22) = "unknown"
					Case 1 : WS.Cells(RowCtr,22) = "waiting for content"
					Case 2 : WS.Cells(RowCtr,22) = "running"
					Case 3 : WS.Cells(RowCtr,22) = "."
					Case 4 : WS.Cells(RowCtr,22) = "PENDING RETRY"
					Case 5 : WS.Cells(RowCtr,22) = _
						"FAILED (" & Hex(RS("LastErrorCode")) & ")"
					Case 6 : WS.Cells(RowCtr,22) = "Completed w/ Errors"
					Case 7 : WS.Cells(RowCtr,22) = "SCCM 2003 CLIENT"
				End Select
			End If
		End If

		Set RS = SCCM.Execute("Select MAX(statetime) from " & _
			"v_AssignmentStatePerTopic where TopicType=301 and " & _
			"StateID=4 and ResourceID=" & RID)
		LS = #1/1/2000#
		If Not RS.EOF Then
			If Not IsNull(RS(0)) Then
				LS = CDate(RS(0))
				WS.Cells(RowCtr,23) = LS
				WS.Cells(RowCtr,24) = DateDiff("d",CDate(RS(0)),ReportTime)
			End If
		End If

		If LS > #1/1/1601# Then
			Set RS = SCCM.Execute("select top 1 * from " & _
				"v_AssignmentStatePerTopic where TopicType = 301 and StateID=6 " & _
				"and LastErrorCode is not null and ResourceID=" & RID & _
				" and StateTime >= '" & FormatDateTime(LS) & _
				"' order by StateTime Desc")
			If Not RS.EOF Then
				WS.Cells(RowCtr,25) = Hex(RS("LastErrorCode"))
			End If
		End If

		'Installed vs Required
		Set RS = SCCM.Execute("select CSR.Status, COUNT(CSR.CI_ID) from " & _
			"v_Update_ComplianceStatusReported CSR join " & _
			"v_UpdateAssignmentStatus UAS on UAS.ResourceID = CSR.ResourceID " & _
			"join v_UpdateDeploymentSummary UDS on UDS.CI_ID = CSR.CI_ID and " & _
			"UDS.AssignmentID = UAS.AssignmentID and UDS.AssignmentEnabled = 1 " & _
			"where CSR.ResourceID = " & RID & " group by CSR.Status")
		Installed = 0
		Required = 0
		Do Until RS.EOF
			Select Case RS(0)
				Case 1,3 : Installed = Installed + RS(1)
				Case 2 : Required = Required + RS(1)
			End Select
			RS.MoveNext
		Loop
		Total = Installed + Required
		If Total > 0 Then
			WS.Cells(RowCtr,26) = CLng(Total)
			WS.Cells(RowCtr,27) = CLng(Installed)
			WS.Cells(RowCtr,28) = CDbl(Installed/Total)
			WS.Cells(RowCtr,28).NumberFormat = "0.000%"
			WS.Cells(RowCtr,29) = CLng(Required)
			WS.Cells(RowCtr,30) = CDbl(Required/Total)
			WS.Cells(RowCtr,30).NumberFormat = "0.000%"
		End If

		RowCtr = RowCtr + 1
	Next

	objContainer.Filter=Array("container","organizationalUnit")
	For Each objItem in objContainer
		EnumContainer objItem, WS
	Next
End Function

Function Integer8Date(ByVal objDate, ByVal lngBias)
    ' Function to convert Integer8 (64-bit) value to a date, adjusted for
    ' local time zone bias.
    Dim lngAdjust, lngDate, lngHigh, lngLow
    lngAdjust = lngBias
    lngHigh = objDate.HighPart
    lngLow = objdate.LowPart
    ' Account for error in IADsLargeInteger property methods.
    If (lngLow < 0) Then
        lngHigh = lngHigh + 1
    End If
    If (lngHigh = 0) And (lngLow = 0) Then
        lngAdjust = 0
    End If
    lngDate = #1/1/1601# + (((lngHigh * (2 ^ 32)) _
        + lngLow) / 600000000 - lngAdjust) / 1440
    ' Trap error if lngDate is ridiculously huge.
    On Error Resume Next
    Integer8Date = CDate(lngDate)
    If (Err.Number <> 0) Then
        On Error GoTo 0
SO.WriteLine "DATE OVERFLOW"
        Integer8Date = #1/1/1601#
    End If
    On Error GoTo 0
End Function