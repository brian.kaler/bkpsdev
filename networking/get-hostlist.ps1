﻿Add-PSSnapin vmware.vimautomation.core

function get-hostlist{
param(
[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
[String]$hosts,
[Parameter(Mandatory=$true)]
[String]$username,
[Parameter(Mandatory=$true)]
[String]$password
)
Begin{
if((Get-Item ".\master-vmlist.csv") -notcontains $null)
{
new-item -name ".\master-vmlist.csv" -ItemType file
}
}
Process
{
foreach($host in $hosts)
    {
    connect-viserver -server $host -user $username -password $password -Force $true
    get-vm | ft | export-csv -path ".\master-vmlist.csv" -Append
    disconnect-viserver -server $host
    }
}

}