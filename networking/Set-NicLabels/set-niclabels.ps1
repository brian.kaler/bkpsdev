	#BEGIN
	#collect hardware information, determine ports.
	$allnicinfo = Get-NetAdapterHardwareInfo | Select Name, InstanceID, InterfaceDescription, BusNumber, FunctionNumber

	#determine how many adapters exist
    $busids = ($allnicinfo | select -Unique -ExpandProperty Busnumber)
	$buscount = ($allnicinfo | select -Unique | measure).count

	#improve the bus count method
	foreach($busid in $busids)
		{
		$bus = $busid
		Write-Host $bus
		$funccount= @()
		$funccount = $allnicinfo | where-object {$_.busnumber -eq $bus} | select -expandproperty functionnumber | measure-object -Maximum | select Maximum
		for($i = 0; $i -le $funccount.Maximum; $i++)
			{
			$nic = Get-NetAdapterHardwareInfo | where {($_.busnumber -eq $bus) -and ($_.functionnumber -eq $i)}
			$port = $i + 1
			Rename-NetAdapter $nic.Name "B$bus P$port"
			}
		
		}


