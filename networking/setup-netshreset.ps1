﻿if((test-path "$env:UserProfile\Documents\netshreset.cmd") -eq $False){
$File = New-item "$env:UserProfile\Documents\netshreset.cmd" -type File 
Add-Content -path $file -Value "cmd /k netsh int ip reset"
$TargetFile = $file | select -ExpandProperty FullName
}
$ShortcutFile = "$env:Public\Desktop\Netsh_Reset.lnk"
$WScriptShell = New-Object -ComObject WScript.Shell
$Shortcut = $WScriptShell.CreateShortcut($ShortcutFile)
$Shortcut.TargetPath = "$TargetFile"
$Shortcut.Save()
