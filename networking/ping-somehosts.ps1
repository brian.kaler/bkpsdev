﻿$updates = import-csv ".\input\dnsupdates2.csv"
$dnsserver = "uswdcs002.us.pinkertons.com"
$rootdns = "us.pinkertons.com"

#get DNS records


Function ListCurrentDNS(){
    Param(
    [Parameter(Mandatory=$true)]
    [String]$dnsserver,
    [Parameter(Mandatory=$true)]
    [String]$zone,
    [Parameter(Mandatory=$true)]
    [String]$hostname
    )
    $records = dnscmd $dnsserver /zoneprint $zone
    $lines = $records | Where {$_ -match $hostname}
    
    #Write-Host "Processed $hostname :"
    Write-Host $lines
    }

Function UpdateCurrentDNS(){
    Param(
    [Parameter(Mandatory=$true)]
    [String]$dnsserver,
    [Parameter(Mandatory=$true)]
    [String]$zone,
    [Parameter(Mandatory=$true)]
    [String]$recordname,
    [Parameter(Mandatory=$true)]
    [String]$recordtype,
    [Parameter(Mandatory=$true)]
    [String]$recordAddress
    )
    # Build our DNSCMD DELETE command syntax 
    $cmdDelete = "dnscmd $DNSServer /RecordDelete $zone $recordName $recordType /f" 

    # Build our DNSCMD ADD command syntax 
    $cmdAdd = "dnscmd $DNSServer /RecordAdd $zone $recordName $recordType $recordAddress" 

    # Now we execute the command 
    Write-Host "Running the following command: $cmdDelete" 
    Invoke-Expression $cmdDelete 
 
    Write-Host "Running the following command: $cmdAdd" 
    Invoke-Expression $cmdAdd 
    }


#update DNS
#foreach($record in $records)
#    {
#    # Capture the record contents as variables 
#    $recordName = $record.dns
#    $recordType = $record.type 
#    $recordAddress = $record.address
#
#    # Build our DNSCMD DELETE command syntax 
#    $cmdDelete = "dnscmd $DNSServer /RecordDelete $DNSZone $recordName $recordType /f" 
# 
#    # Build our DNSCMD ADD command syntax 
#    $cmdAdd = "dnscmd $DNSServer /RecordAdd $DNSZone $recordName $recordType $recordAddress" 
#
#    # Now we execute the command 
#    Write-Host "Running the following command: $cmdDelete" 
#    Invoke-Expression $cmdDelete 
# 
#    Write-Host "Running the following command: $cmdAdd" 
#    Invoke-Expression $cmdAdd 
#    }


#ping all woc "failback ips" 
#foreach($ip in $updates){
#    Test-Connection $ip.ip_woc_failover -Verbose -Count 1 
#    }

#foreach($u in $updates){
#    Write-Host "Processing ($u.dns) now: "
#    ListCurrentDns -dnsserver $dnsserver -zone "securitasinc.com" -hostname $u.dns
#    ListCurrentDns -dnsserver $dnsserver -zone "us.pinkertons.com" -hostname $u.dns
#    If($u.dns -eq "Fspddbwc2"){
#    updatecurrentdns -dnsserver $dnsserver -zone $u.zone -recordtype $u.type -recordname $u.dns -recordAddress $u.ip_woc_failover
#    }
#
#    }
#
    
