﻿$updates = import-csv ".\input\dnsupdates2.csv"
$dnsserver = "uswdcs002.us.pinkertons.com"
$rootdns = "us.pinkertons.com"

#get DNS records


Function ListCurrentDNS(){
    Param(
    [Parameter(Mandatory=$true)]
    [String]$dnsserver,
    [Parameter(Mandatory=$true)]
    [String]$zone,
    [Parameter(Mandatory=$true)]
    [String]$hostname,
    [Parameter(Mandatory=$false)]
    [String]$ptrzone
    )
    $records = dnscmd $dnsserver /zoneprint $zone
    if($ptrzone -ne $null){
        $ptrs = dnscmd $dnsserver /zoneprint $ptrzone
        }
    $lines += $records | Where {$_ -match $hostname}
    $lines += "`n"
    $lines += $ptrs | where {$_ -match $hostname}

    
    Write-Host "Processed $hostname :"
    Write-Host $lines
    }

Function UpdateCurrentDNS(){
    Param(
    [Parameter(Mandatory=$true)]
    [String]$dnsserver,
    [Parameter(Mandatory=$true)]
    [String]$zone,
    [Parameter(Mandatory=$true)]
    [String]$recordname,
    [Parameter(Mandatory=$true)]
    [String]$recordtype,
    [Parameter(Mandatory=$true)]
    [String]$recordAddress,
    [Parameter(Mandatory=$true)]
    [String]$ptrzone
    )

    $fqdn = ($recordname+"."+$zone)
    $ptrip = $recordaddress.Split('.',4)[3]


    # Build our DNSCMD DELETE command syntax 
    $cmdDelete = "dnscmd $DNSServer /RecordDelete $zone $recordName $recordType /f" 

    # Build our DNSCMD ADD command syntax 
    $cmdAdd = "dnscmd $DNSServer /RecordAdd $zone $recordName $recordType $recordAddress" 
        
    # delete our ptr syntax
    $cmdDelete2 = "dnscmd $DNSServer /RecordDelete $ptrzone $ptrip PTR /f"

    # add our ptr cmd syntax
    $cmdptr = "dnscmd $DNSServer /RecordAdd $ptrzone $ptrip PTR $fqdn"

    # Now we execute the command 
    Write-Host "Running the following command: $cmdDelete" 
    Invoke-Expression $cmdDelete 

    # Now we execute the command 
    Write-Host "Running the following command: $cmdDelete2" 
    Invoke-Expression $cmdDelete2 
 
    Write-Host "Running the following command: $cmdAdd" 
    Invoke-Expression $cmdAdd 

    Write-Host "Running the following ptr command: $cmdptr" 
    Invoke-Expression $cmdptr 
    }


#update DNS
#foreach($record in $records)
#    {
#    # Capture the record contents as variables 
#    $recordName = $record.dns
#    $recordType = $record.type 
#    $recordAddress = $record.address
#
#    # Build our DNSCMD DELETE command syntax 
#    $cmdDelete = "dnscmd $DNSServer /RecordDelete $DNSZone $recordName $recordType /f" 
# 
#    # Build our DNSCMD ADD command syntax 
#    $cmdAdd = "dnscmd $DNSServer /RecordAdd $DNSZone $recordName $recordType $recordAddress" 
#
#    # Now we execute the command 
#    Write-Host "Running the following command: $cmdDelete" 
#    Invoke-Expression $cmdDelete 
# 
#    Write-Host "Running the following command: $cmdAdd" 
#    Invoke-Expression $cmdAdd 
#    }


#ping all woc "failback ips" 
#foreach($ip in $updates){
#    Test-Connection $ip.ip_woc_failover -Verbose -Count 1 
#    }


#uncomment for functions
foreach($u in $updates){
    #ListCurrentDns -dnsserver $dnsserver -hostname $u.dns -zone $u.zone -ptrzone $u.ptr
    #updatecurrentdns -dnsserver $dnsserver -zone $u.zone -recordtype $u.type -recordname $u.dns -recordAddress $u.ip_woc_failover -ptrzone $u.ptr
    }

    
