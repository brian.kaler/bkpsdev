function get-activepcs
{
param(
[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
[String]$pclist
)

$pcs = Get-Content $pclist

#collect
$activepcs = @()
#($pcs | % {test-connection -ComputerName $_ -count 1 -asjob -ThrottleLimit 128}) | wait-job | Receive-Job
$activepcs += $pcs | Test-ConnectionAsync -MaxConcurrent 500 -count 1 | where {$_.responsetime -ge 1}


#create a csv
$filename =  "winhosts" + (get-date -format MMddyyyy-HHmm)
$activepcs | select Address,ProtocolAddress | export-csv "$filename`.csv"
}