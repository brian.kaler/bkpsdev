﻿#parameters
$mypc = 
$controldir = $PSScriptRoot+"\control"


#USER SETUP
$mdtadminpw = ConvertTo-SecureString "ImageVM!" -AsPlainText -Force
$mdtuser = "mdt_admin"

$mdtlocaluser = Get-LocalUser -name $mdtuser -ErrorAction SilentlyContinue
If($mdtlocaluser -eq $null){
$mdtuser = New-LocalUser -AccountNeverExpires -FullName $mdtuser -Name $mdtuser -password $mdtadminpw
}
Add-LocalGroupMember -group "Administrators" -member $mdtlocaluser -ErrorAction SilentlyContinue

#CONTROL SETUP
Function ModCustomSettings{
Param(
[Parameter(Mandatory=$true)]
$controldir
)
$lines = get-content $controldir"\customsettings.ini"
foreach($l in $lines){
md
}

}


Function ModBootstrap{
Param(
[Parameter(Mandatory=$true)]
$controldir
)
$lines = get-content $controldir"\bootstrap.ini"
}