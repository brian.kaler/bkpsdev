﻿

if((Get-PSDrive -name REGGIE -ErrorAction SilentlyContinue) -eq $null){
$drive = New-PSDrive -name REGGIE -PSProvider registry -root "HKLM\software\Policies\Adobe\Acrobat Reader\DC\FeatureLockDown"
}


Set-Location ($drive.name+":\\")

$that = ((Get-itemproperty .\cDefaultLaunchAttachmentPerms).tBuiltInPermList)

if($that -notcontains "zip"){
$newvalue = $that+"|.zip:2|"
Set-ItemProperty -name tBuiltInPermList -Value $newvalue -Path ".\cDefaultLaunchAttachmentPerms" -force -Verbose
}