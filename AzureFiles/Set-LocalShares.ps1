$folders = @("Marketing",
"Sales",
"Testing",
"Development",
"dimitri",
"dimregrout",
"dimcustissues")


foreach($folder in $folders){
New-Item -Path C:\azureshares -name $folder -ItemType Directory -ErrorAction SilentlyContinue
New-SmbShare -Path C:\azureshares\$folder  -Name $folder -FullAccess "Authenticated Users"
}