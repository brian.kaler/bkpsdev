
If($myconnection.context -eq $null){
    $pscredential = Get-Credential
    $myconnection = Connect-AzAccount -Credential $pscredential
    }

$mystorage = Get-AzStorageAccount -Name "astfilesharestore01" -ResourceGroupName "astresgroup01"

$myshares = Get-AzStorageShare -Context $mystorage.context | where {$_.issnapshot -eq $false}
$myresourcegroups = Get-AzResourceGroup

$invalidshares = @("ali-zain","archived-shares","azfs-finance","brian","convertedshares","dfaas","dimitri2-y","dimitri2-yy","dimitri3","dimitriclone","dwa","emmanuel","fahad","faizan-ali","fiza-baig","flowfiles","hassaan-ahsan","humza-khatri","irfan","irfan-ahmed","isurani","jay","kone-acquisitions","linux","linuxoutput","mike","moquinn","mujtabakasmani","munira","nabeel","nida","rauf","restore01","rizwan","sameen","sandeep","sanober","share","siraj-shaukat","test-abid","training","wajiha")



#foreach($share in $myshares){
#    write-host $share.name
#    Get-AzStorageShare -Context $mystorage.context -Name $share.name
#}

foreach($rg in $myresourcegroups){
    Get-AzResource -Name $rg.resourcegroupname | ft
}