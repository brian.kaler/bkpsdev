#Install-Endpoint 
#Brian Kaler
#12/8/2019
#Install an endpoint in the Azure cloud

$endpointhostname = "ASTHYV11.astera.com"
$resourcegroup = "astresgroup01"
$storageaccount = "astfilesharestore01"
$storagesyncService = "asterafilesync1"
$endpointname = "05190fd6-629a-4321-9f29-966309f2eff0"
$folders = @("Marketing",
"Sales",
"Testing",
"Development",
"dimitri",
"dimregrout",
"dimcustissues")
$localroot = "D:\azureshares\"

If($myconnection.context -eq $null){
    $pscredential = Get-Credential
    $myconnection = Connect-AzAccount -Credential $pscredential
    }

$mystorage = Get-AzStorageAccount -Name $storageaccount -ResourceGroupName $resourcegroup
$myshares = Get-AzStorageShare -Context $mystorage.context | where {$_.issnapshot -eq $false}
$mysyncgroups = get-azstoragesyncgroup -resourcegroupname $resourcegroup -StorageSyncServiceName $storagesyncService
$mynewendpoint = Get-AzStorageSyncServer -StorageSyncServiceName $storagesyncService -ResourceGroupName $resourcegroup | where {$_.friendlyname -eq $endpointhostname}


foreach($sg in $mysyncgroups){
    foreach($folder in $folders){
        if($sg.SyncGroupName -like "*$folder*"){
        $localpath = $localroot+$folder
        New-AzStorageSyncServerEndpoint -ResourceGroupName $resourcegroup -StorageSyncServiceName $storagesyncService -SyncGroupName $sg.syncgroupname -Name $endpointname -ServerResourceId $mynewendpoint.ResourceId -ServerLocalPath $localpath
    }
    }
}