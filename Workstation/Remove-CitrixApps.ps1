﻿
$path = 'Registry::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\*'
$profiles = Get-ItemProperty -Path $path | Select-Object -Property PSChildName, ProfileImagePath
$watchitems = @("SAFES","Master Data")
$myuserprograms = @()

foreach($p in $profiles){
$profilepath = $p.profileimagepath+"\AppData\Roaming\Microsoft\Windows\Start Menu\Programs"
$programs = gci $profilepath

foreach($p in $programs){
    if($p.extension -eq ".lnk"){
        foreach($item in $watchitems){
            if($p.name -like "*$item*"){
                Remove-Item $p.fullname -Force -WhatIf
                }
            }
        }
    }
}