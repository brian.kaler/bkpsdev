﻿function add-remotedesktopuser
{
param(
[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
[String]$computers,
[Parameter(Mandatory=$true)]
[String]$user
)

foreach($pc in $computers)
    {
    invoke-command -ComputerName $pc -ScriptBlock{net localgroup "remote desktop users" $user /add} -OutVariable $output
    }
    write-host $output
}