﻿#ROT13 func
function ConvertFrom-Rot13
{
     [CmdletBinding()]
     param(
          [Parameter(
              Mandatory = $true,
              ValueFromPipeline = $true
          )]
          [String]
          $rot13string
     )
    
     [String] $string = $null;
     $rot13string.ToCharArray() |
     ForEach-Object {
          Write-Verbose "$($_): $([int] $_)"
          if((([int] $_ -ge 97) -and ([int] $_ -le 109)) -or (([int] $_ -ge 65) -and ([int] $_ -le 77)))
          {
              $string += [char] ([int] $_ + 13);
          }
          elseif((([int] $_ -ge 110) -and ([int] $_ -le 122)) -or (([int] $_ -ge 78) -and ([int] $_ -le 90)))
          {
              $string += [char] ([int] $_ - 13);
          }
          else
          {
              $string += $_
          }
     }
     $string
}

#find the host number, and host string
function findhostnumber()
{
    param(
    [Parameter(Mandatory=$true)]
    [String]$hostname
    )
    $hsplit = $hostname.toCharArray()
    foreach($letter in $hsplit)
        {
        if($letter -match "[0-9]")
        {
        $hostnumber = $hostnumber + $letter
        }
        else
        {
        $hoststring = $hoststring + $letter
        }
        }
    return $hostnumber,$hoststring
}

#Function to split hostname
function splithostname(){
    param(
    [Parameter(Mandatory=$true)]
    [String]$hoststring
    )
    #is it odd length? subtract 1 to make it even
    If($hoststring.length % 2 -eq 1){$count = ($hoststring.length) - 1}else{$count = $hoststring.length}
    #get x,y - designed to look confusing.
    $x = $count / 2
    $y = $hoststring.Length - $count / 2
    #split string portion
    $split1 = $hoststring.Substring(0,$x)
    $split2 = $hoststring.Substring($x,$y)
    Return $split1,$split2
}



#initialize/reset variables
$hoststring = ""
$number = ""
#get hostname to process
$hostname = gci -path env:computername | select -ExpandProperty value
#process the hostname
$hoststring = (findhostnumber($hostname))[1]
#obfuscate the hostnumber
$number = ([int](findhostnumber($hostname))[0] + 5)
#split the hostname
$hostname = splithostname($hoststring)
$osplit1 = Convertfrom-rot13($hostname[0])
$osplit2 = Convertfrom-rot13($hostname[1])
#construct the final string
$obfuscate = $osplit1 + $number + $osplit2


    
$obfuscate