﻿#cred saver
$mycred = Get-Credential
$map = "\\uswdcs001\netlogon"
$i = 0

#loop forever
While($true)
{
$i = ++$i
write-host "loop $i stated"
sleep -seconds 1800
#if the drive doesn't exist map it, otherwise disconnect it, then sleep for 30 minutes.
if((Get-PSDrive -name "U") -eq $null)
    {
    new-psdrive -name "U" -PSProvider FileSystem -root $map -Credential $mycred
    }
    Elseif($i -eq 1)
    {
    Remove-PSDrive -name "U"
    new-psdrive -name "U" -PSProvider FileSystem -root $map -Credential $mycred
    }
    Else
    {
    Remove-PSDrive -name "U"
    }

}