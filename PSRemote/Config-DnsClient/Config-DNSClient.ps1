﻿#update DNS settings on hosts
#writen by Brian Kaler
#7/11/2018

#Load the VMware Powershell Module
get-module -listavailable VMware* | import-module

Function Update-DNSClient
{
Param(
[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
[String]$hostname
)
Write-Host "--- Now Processing $hostname"
$session = New-PSSession $hostname
If($session -eq $null){
        write-host "Failed to process $hostname - likely WINRM related issue"
        }
        else
        {
        $adapter = Invoke-Command -session $session -scriptblock {Get-WmiObject Win32_NetworkAdapter -filter "Name = 'vmxnet3 Ethernet Adapter'" | select-object -ExpandProperty NetConnectionID}
        Write-Host "Found Network Adapter on $hostname identified as: $adapter"
        Invoke-Command -Session $session -ArgumentList $adapter -ScriptBlock {netsh interface ip set dns $($args[0]) static 10.1.64.135}
        Invoke-Command -Session $session -ArgumentList $adapter -ScriptBlock {netsh interface ip add dns $($args[0]) 10.1.16.67 index=2}
        Write-Host "If no output was recieved, no error has occured on $hostname ---"
        Remove-PSSession -Session $session
        }
}

Function Sort-DNS
{
Param(
[Parameter(Mandatory=$true)]
[String[]]$fqdns,
[Parameter(Mandatory=$true)]
[String]$filter
)
$matches = New-Object System.Collections.Generic.List[System.Object]
foreach($d in $fqdns)
{
if(Select-String -Pattern $filter -InputObject $d)
    {
    $matches.add($d)
    }
}
return $matches
}

#take all powered on machines and select hostname
$targets = get-vm | where {$_.powerstate -eq "PoweredOn"} | get-vmguest | select -ExpandProperty Hostname

#sort only us.pinkerton.com hosts
$output = Sort-DNS -fqdns $targets -filter "us.pinkertons.com"

#update dns on the client remotely
foreach($pc in $output){
Update-DNSClient -hostname $pc
}

