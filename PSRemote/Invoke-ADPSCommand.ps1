﻿#Query all VM
#written by Brian Kaler
#9/26/2018 9:05AM

#include core
. .\core\core.ps1


#Load Modules
import-module ActiveDirectory

#Get List of AD Computers
$mygroup = get-adgroup -filter * | where {$_.name -match "CLIENTPUSH-R-PROD11072018"}
$computers = Get-ADGroupMember $mygroup

#$credout = Store-credentials -dnsnames $allnames

foreach($pc in $computers){
    $target = $pc.Name
    Write-Host "Processing " $pc.name
    invoke-command -ComputerName $pc.name -ScriptBlock {powershell.exe -executionpolicy bypass -file "\\uswsch901\clienthealth\ConfigMgrClientHealth.ps1"}
    }

