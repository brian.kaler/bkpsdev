﻿#Query all VM
#written by Brian Kaler
#9/26/2018 9:05AM

#include core
. .\core\core.ps1


#Load the VMware Powershell Module
get-module -listavailable VMware* | import-module
import-module webadministration

#connect vcenter
#$vcenter = Read-Host "Please provide the vcenter server"
$vcenter = "catvct001.us.pinkertons.com"
Connect-VIServer -Server $vcenter

#get vm inventory
$allvms = Get-Cluster | Get-VM

Function Get-IISBinding
    {
        Get-WebBinding | % {
            $name = $_.ItemXPath -replace '(?:.*?)name=''([^'']*)(?:.*)', '$1'
            New-Object psobject -Property @{
                Name = $name
                Binding = $_.bindinginformation.Split(":")[-1]
            }
        } | Group-Object -Property Name | 
        Format-Table Name, @{n="Bindings";e={$_.Group.Binding -join "`n"}} -Wrap
    }

$allnames = New-Object "System.Collections.Generic.List[String]"
$allvms | foreach-object {$allnames.add($_.ExtensionData.Guest.Hostname)}

#$credout = Store-credentials -dnsnames $allnames

foreach($vm in $allvms){
    $target = $vm.name
    Write-Host "Processing " $target
    invoke-command -ComputerName $target -ScriptBlock {gpupdate /force}
    }

#get-vm | select Name, @{N="Operating System";E={$_.guest.osfullname}} | export-csv -Path .\csoc2-fullos.csv


