﻿#Verify-HotfixReport
#Compare what hotfixes are installed on a PC with a list of provided updates by KBID
#Written by Brian Kaler
#10/29/2018

#source files
$sourcehotfixes = get-content ".\sourcehotfixes.txt"
$sourcesystems = get-content ".\sourcesystems.txt"

$timestamp = (get-date -Format ddMMyyyy-hhmmss)
$outfile = ".\HotfixResults-$timestamp.csv"

#define our lists
$systems = $sourcesystems
$hotfixes = $sourcehotfixes




Function Verify-Hotfix(){
Param(
[Parameter(Mandatory=$true)]
[String]$computername,
[Parameter(Mandatory=$true)]
[String[]]$hotfixes
)
#capture our hotfix data
$systemhotfixes = get-hotfix -ComputerName $computername -ErrorAction

#define our object to hold our data
$hotfixpc = New-Object "PSCustomObject"
$hotfixpc | Add-Member -MemberType NoteProperty -Name HostName -Value $computername
#compare to our source, output hotfix yes or no!
foreach($targetfix in $hotfixes){
    If($systemhotfixes.HotfixId -contains $targetfix){
                $hotfixpc | Add-Member -MemberType NoteProperty -Name $targetfix -Value "Yes" -force
                }
                Else
                {
                $hotfixpc | Add-Member -MemberType NoteProperty -Name $targetfix -Value "No" -force
                }
       }
    
Return $hotfixpc

}



$results = @()
foreach($s in $systems){
$results += Verify-Hotfix -computername $s -hotfixes $hotfixes -ErrorAction SilentlyContinue
}

$results | Export-Csv $outfile