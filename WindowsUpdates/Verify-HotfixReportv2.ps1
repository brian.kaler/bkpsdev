#Verify-HotfixReport
#Compare what hotfixes are installed on a PC with a list of provided updates by KBID
#Written by Brian Kaler
#10/29/2018
#version 2 - improved function.

#source files
#$sourcehotfixes = get-content ".\sourcehotfixes.txt"
#$sourcesystems = get-content ".\sourcesystems.txt"

$timestamp = (get-date -Format ddMMyyyy-hhmmss)
$outfile = ".\HotfixResults-$timestamp.csv"





Function Verify-Hotfix(){
Param(
[Parameter(Mandatory=$true)]
[String[]]$hotfixlist,
[Parameter(Mandatory=$true)]
[String[]]$systemlist
)
#create our results object
$results = @()
#read our lists
$hotfixes = get-content $hotfixlist
$systems = get-content $systemlist

foreach($s in $systems){
#Progress tracking
write-host "Processed "$s
#capture our hotfix data
$systemhotfixes = get-hotfix -ComputerName $s

#define our object to hold our data
$hotfixpc = New-Object "PSCustomObject"
$hotfixpc | Add-Member -MemberType NoteProperty -Name HostName -Value $s
#compare to our source, output hotfix yes or no!
foreach($targetfix in $hotfixes){
    If($systemhotfixes.HotfixId -contains $targetfix){
                $hotfixpc | Add-Member -MemberType NoteProperty -Name $targetfix -Value "Yes" -force
                }
                Else
                {
                $hotfixpc | Add-Member -MemberType NoteProperty -Name $targetfix -Value "No" -force
                }
       }

$results += $hotfixpc
}    

return $results

}

Verify-Hotfix -hotfixlist .\sourcehotfixes.txt -systemlist .\sourcesystems.txt
$results | Export-Csv $outfile