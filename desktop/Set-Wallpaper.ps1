#Set-Wallpaper (Securitas 2016)
#blatently stolen from the internet and modified slightly
#Brian Kaler
#Changes the current user's wallpaper to Securitas2016.jpg (from the attached DC)


function Test-RegistryValue {

param (

 [parameter(Mandatory=$true)]
 [ValidateNotNullOrEmpty()]$Path,

[parameter(Mandatory=$true)]
 [ValidateNotNullOrEmpty()]$Value
)

try {

Get-ItemProperty -Path $Path | Select-Object -ExpandProperty $Value -ErrorAction Stop | Out-Null
 return $true
 }

catch {

return $false

}

}

function set-wallPaper ([string]$desktopImage)
{

   
     Remove-ItemProperty -path "HKCU:\Control Panel\Desktop" -name WallPaper 

#Not actually needed
<#
     for ($i = 0; $i -le 5; $i++)
     { 
         if (Test-RegistryValue -path "hkcu:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Wallpapers" -value "BackgroundHistoryPath$i" )
         {
         Remove-itemproperty -path "hkcu:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Wallpapers" -name "BackgroundHistoryPath$i" 
         }
     }
#>
     
               
     set-itemproperty -path "HKCU:\Control Panel\Desktop" -name WallPaper -value $desktopImage
     
#needed to actually change the background consistently 

Sleep -seconds 5

     RUNDLL32.EXE USER32.DLL,UpdatePerUserSystemParameters ,1 ,True

#Not needed but lets you know it was set correctly  
#         Get-ItemProperty -path "HKCU:\Control Panel\Desktop" 
}

#Set the destination for our wallpaper filename/path
$wallpaperfullpath = "C:\Software\securitas2016.jpg"
#download our wallpaper from the local domain
$domainwallpaperpath = "\\us.pinkertons.com\SYSVOL\us.pinkertons.com\scripts\securitas2016.jpg" 
#self explanatory
Copy-item -Path $domainwallpaperpath -Destination $wallpaperfullpath
Set-Wallpaper -desktopImage $domainwallpaperpath


