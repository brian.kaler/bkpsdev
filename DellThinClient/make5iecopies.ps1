$myfolder = "C:\program files\internet explorer\"
$myfile = $myfolder+"iexplore.exe"
$myfileobj = get-item $myfile

For($i=1;$i -le 5; $i++){
    If($myfileobj -ne $null){
        $filename = ([string]($myfileobj.name)).substring(0,8)
        $ext = ([string]$myfileobj.Extension)
        $dest = $myfolder + $filename + (([string]$i).padleft(2,'0')) + $ext
        Copy-Item $myfileobj.PSPath $dest
    }
}