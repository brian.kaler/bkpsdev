﻿#find folders that have had activity in the last 365 days.
function get-oldfolders{

param(
[parameter(Mandatory = $true)]
[string]$root,
[parameter(Mandatory = $false)]
[int]$age = 365
)

$folders = get-childitem -path $root | ?{$_.psiscontainer -eq $true}
$age = (get-date).adddays(-365)
$a = New-Object System.Collections.Generic.List[System.Object]
foreach($folder in $folders)
    {
    write-host "Now processing $folder"
    $files = get-childitem -path $folder.fullname
    $count = $files.count
    write-host "Found $count files"
    foreach($file in $files)
        {
        if($file.lastwritetime -gt $age.date)
            {
            $a.add($file.directory)
            }
        }
        $unique = $a | select -unique | Sort-Object
    }
return $unique
}
