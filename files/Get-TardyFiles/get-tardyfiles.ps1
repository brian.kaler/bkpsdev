﻿#Get-TardyFiles
#Written by Brian Kaler
#Circa 2018
#Collects files in a directory, creates a log with entries based on when last written to.

Param(
[Parameter(Mandatory=$true,Position=0)]
[string]$computername,
[Parameter(Mandatory=$true,Position=1)]
[string]$foldersource
)

#include core functions
. ..\core\core.ps1
#if that fails include core.psm1 file in the working directory (for task scheduler)
$scriptdir = Split-Path -Path $Myinvocation.Mycommand.Definition -Parent
Import-Module "$scriptdir\core.psm1"

#base variables
$scriptname = "tardyfiles"
$logname = "Backup-Monitor"
$source = "Check-Backup"
#range in hours before alert fires (for additional days (ie 3, 72 hours)
$warningrange = 48
$criticalrange = 120

#for debug purposes, comment out when not debugging
#$computername = "catwks001.us.pinkertons.com"
#$foldersource = "c:\backup"

# SCRIPT BEGINS HERE

#create log
add_eventlog -logname $logname -source $source


foreach($i in $foldersource)
{
    #list the files on the remote computer
    $files = scan_remote_folder -computername $computername -folder $i
    #if any files were found, calculate if older than tardyrange and alert on tardy files
    if($files -ne $null)
        {
        $tardyfiles = @()
        foreach($file in $files)
            {
            $hours = ((get-date).subtract($file.LastWriteTime)).totalhours
            $filename = $file.fullname
            $timestamp = $file.LastWriteTime
            If($hours -ge $criticalrange)
            {
            add_logerror("$filename : is extremely out of date - TIMESTAMP: $timestamp")
            }
            ElseIf($hours -ge $warningrange)
            {
            add_logwarn("$filename : is older than permitted - TIMESTAMP: $timestamp") 
            }
            Else
            {
            add_loginfo("$filename : is current, no action necessary - TIMESTAMP: $timestamp")
            }
        }
    }

    #if a folder is empty, log event
    if((measure-object -inputobject $files).count -lt 1)
    {
    add_logerror("Folder $i was found to be EMPTY")
    }

}

