#eventlog - LOGS Script events to Custom Windows Event Log
function add_eventlog
{
param(
[Parameter(Mandatory=$true)]
[String]$logname,
[Parameter(Mandatory=$true)]
[String]$source
)
    
    if(([System.Diagnostics.EventLog]::Exists($logname)) -and ([System.Diagnostics.EventLog]::SourceExists($source)))
        {

        }
        Else
        {
        New-EventLog -logname $logname -source $source
        }
}

#eventlog - LOGS MESSAGE AS A INFORMATION EVENT
function add_loginfo($logmsg)
{
$params = @{
    LogName = $logname
    Source = $source
    EntryType = 'Information'
    EventId = '1000'
    Message = $logmsg
}
write-eventlog @params
write-host "Information: $logmsg"
}

#eventlog - LOGS MESSAGE AS A WARNING EVENT
function add_logwarn($logmsg)
{
$params = @{
    LogName = $logname
    Source = $source
    EntryType = 'Warning'
    EventId = '1000'
    Message = $logmsg
}
write-eventlog @params
write-host "Warning: $logmsg"
}
#eventlog - LOGS MESSAGE AS A ERROR EVENT
function add_logerror($logmsg)
{
$params = @{
    LogName = $logname
    Source = $source
    EntryType = 'Error'
    EventId = '1000'
    Message = $logmsg
}
write-eventlog @params
write-host "Error: $logmsg"
}

#scans a remote folder on another computer for files
function scan_remote_folder
{
param(
[String]$folder,
[String]$computername         
)
#session cleanup of existing sessions from previous runs
Remove-PSSession -computername $computername -ErrorAction SilentlyContinue
#create a pssession to the computer, not sure if required, test without sometime
$session = New-PSSession -ComputerName $computername 

$files = invoke-command -computername $computername -scriptblock {param($folder) `
        get-childitem -path $folder | where-object{$_.psiscontainer -eq $false}} -ArgumentList $folder

Return $files
}
