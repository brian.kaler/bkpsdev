﻿#FAILOVER-WOC-TO-EOC-FDRIVE
#Written by Brian Kaler
#11/19/2018
#Takes queried users home drive in Active Directory and sets a new value based on DR storage paths

#make sure these parameters are changed in case of a different DR path!
$wocstring = "\\napwcfs01\usr_home01\wocusers\"
$eocstring = "\\napecfs01\DR_usr_home01\WOCUsers\"
$wocregex = "(?<=\\\\napwcfs01\\usr_home01\\WOCUsers\\).*"
$eocregex = "(?<=\\\\napecfs01\\DR_usr_home01\\WOCUsers\\).*"

#adjust searchbase parameter to target smaller groups
$myusers = get-aduser -Filter * -Properties GivenName,Sn,HomeDirectory -searchbase "OU=.WOC,DC=US,DC=PINKERTONS,DC=COM"

#set-aduser below needs to be uncommented to make the changes and remove the -whatif flag.
foreach($u in $myusers){
    if($u.HomeDirectory -match $wocregex){
    $myuserfolder = $matches[0]
        $fdrivepath = $eocstring+$myuserfolder
    #set-aduser $u -homedirectory $fdrivepath -WhatIf
    write-host "Changed user $myuserfolder to homedirectory: $fdrivepath"
    }

    }

