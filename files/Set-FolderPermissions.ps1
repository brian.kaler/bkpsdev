#Set-FullAccessFolderPermissions
#Brian Kaler
#4/17/2019
#Change Permissions

$root = "c:\temp"
$user = "pnkus\srvcSccmShareAccess"

function Set-FolderAttribs(){
    Param(
    [Parameter(Mandatory=$true)]
    [String]$root
    )
    write-host "Collecting all folders (recursive, may take a while!)"
    $allfolders = get-childitem $root -force -filter * | get-item -filter {$_.Directory} -force 

    foreach($folder in $allfolders)
        {
        write-host $folder.basename
        set-location $root
        (icacls.exe $folder.basename /grant:F $user:F)

        }
    }


Set-FolderAttribs -root $root