﻿#Watch-File
#Written by Brian Kaler
#Determines if a file is older than an established window (uses LastWriteTime)

$thisfolder =  "\\uswsus901\c$\inetpub\wwwroot\"
$thisfile =  "O365IPAddresses_Worldwide.xml"
$minutes = 60

Function Watch-File(){
Param(
[Parameter(Mandatory=$true)]
[String]$wfile,
[Parameter(Mandatory=$true)]
[String]$wfolder,
[Parameter(Mandatory=$true)]
[String]$minutes
)
$now = get-date

$lastwrite = get-date (get-childitem -Path $thisfolder | Where {$_.name -eq $thisfile} | select -ExpandProperty LastWriteTime)

If((((Get-date).addminutes(-$minutes) - $lastwrite) | select -ExpandProperty Minutes) -ge $minutes){Return $false}
Else
{Return $true}
}


if(Watch-file -wfile $thisfile -wfolder $thisfolder -minutes $minutes){Write-Host "File is still current enough"} Else {Write-Host "File is out of date!"}
