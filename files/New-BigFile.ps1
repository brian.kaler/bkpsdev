﻿$mybigfile = get-item -Path .\totalfile.tlt

$allmyfiles = Get-ChildItem -Path .\ -Filter *.txt

foreach($f in $allmyfiles){
    $content = get-content $f
    Out-File -InputObject $content -FilePath $mybigfile -append
}