﻿
$wocstring = "\\napwcfs01\usr_home01\wocusers\"


$eocstring = "\\napecfs01\DR_usr_home01\WOCUsers\"

#adjust searchbase parameter to target smaller groups
$myusers = get-aduser -Filter * -Properties GivenName,Sn,HomeDirectory -searchbase "OU=.WOC,DC=US,DC=PINKERTONS,DC=COM" | where {$_.homedirectory -like "*$wocstring*"}
measure $myusers

#for WOC Failover uncomment below and make sure to comment EOC failover
#foreach($u in $myusers){
#    
#    $myuserfolder = ($u.homedirectory).substring(32)
#    $myhomefolder = ($eocstring)+($myuserfolder)
#    
#    set-aduser $u -homedirectory $myhomefolder
#    get-aduser $u | select name
#    }

#for EOC failover uncomment below and make sure to comment WOC failover

foreach($u in $myusers){
    
    $myuserfolder = ($u.homedirectory).substring(32)
    $myhomefolder = ($wocstring)+($myuserfolder)
    
    set-aduser $u -homedirectory $myhomefolder
    get-aduser $u | select Name,HomeDirectory
    }
