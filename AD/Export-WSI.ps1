﻿#Export-WSI
#Written by Brian Kaler
#2/19/2019
#Collects contents of AD custom Windows Server Inventory attributes and exports excel format.



$ts = (get-date -Format MMddyyyyhhmmss)
#parameters for Excel export
$excelparams = @{
    worksheetname = "wsi-Inventory"
    path = $env:TEMP + "\wsi-inventoryreport" + $ts + ".xls"
    show = $true
    verbose = $true
}
$servers = get-adcomputer -searchbase "OU=Servers,DC=US,DC=PINKERTONS,DC=COM" -filter * 
`
    -properties Name,OperatingSystem,OperatingSystemServicePack,wsiProdPatchGroup,wsiServiceGroup,wsiPrimaryService,wsiServiceOwner,wsiServerEnvironment,wsiSupportContact,CanonicalName,pwdLastSet,LastLogonTimestamp | `
    Select Name,OperatingSystem,OperatingSystemServicePack,wsiProdPatchGroup,wsiServiceGroup,wsiPrimaryService,wsiServiceOwner,wsiServerEnvironment,wsiSupportContact,CanonicalName,pwdLastSet,LastLogonTimestamp

$report = @()
foreach($s in $servers){
    $s.pwdlastset = [datetime]::fromfiletime($s.pwdlastset)
    $s.LastLogontimestamp = [datetime]::fromfiletime($s.lastlogontimestamp)
    $report += $s
}

$report | Export-Excel @ExcelParams -AutoSize