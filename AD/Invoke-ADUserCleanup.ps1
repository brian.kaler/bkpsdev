#Invoke-ADUserCleanup
#Written by Brian Kaler
#9/10/2019
#Disables ADObject and users over 180 days without logon, and moves them to Disabled Computers

#editable params
$mysearchbase = "DC=us,DC=pinkertons,DC=com"
$mydisabledpcbase = "OU=Disabled Computers,DC=us,DC=pinkertons,DC=com"
$mylogpath = ".\InactiveSystemsProcessed"+(get-date -Format ddMMyyyy-hhmmss)+".log"


#Don't edit below unless you know what you're doing
$myusers = Get-ADUser -filter * -Properties Name,DistinguishedName,lastlogondate,enabled -SearchBase $mysearchbase | select Name,DistinguishedName,lastlogondate,enabled | where {$_.lastlogondate -ne $null -and $_.enabled -eq $true}
$myinactiveusers = $myusers | where {$_.lastlogondate -lt (get-date).adddays(-180)}
$newline = "`r`n"
$mylog = New-Item -Path $mylogpath



#BELOW NEEDS TO BE ALTERED FOR USERS IF EVER USED!
#foreach($p in $myinactivepcs){
#    #disable the account first
#    $disableobj = Disable-ADAccount -Identity $p.DistinguishedName -PassThru
#    If($disableobj -ne $null){
#        $l_disableobj = ($p.name)+" was successfully disabled"
#        $descriptionstr = "Computer Disabled on"+(get-date -Format ddMMyyyy-hhmmss)
#        Set-ADObject -Identity $p.DistinguishedName -Description $descriptionstr
#        Add-Content -Path $mylog -value $l_disableobj
#    }
#    else {
#        $l_disableobj = ($p.name)+" was NOT successfully disabled."
#        Add-Content -Path $mylog -value $l_disableobj
#    }
#}

$mylogoutput = Get-Content $mylog
