import-module activedirectory
$users = get-content "C:\temp\myextrausers.txt"

$allusers = get-aduser -filter * | select name,SamAccountname
$output = @()

foreach($u in $allusers){
    foreach($f in $users){
        If($u.name -match $f){
            $output += $u.SAMACCOUNTNAME
        }
}
}

$output | export-csv C:\temp\myextrausers2.csv