﻿$domainadmins = get-adgroup -Identity "Domain Admins"
$alladmins = Get-ADGroupMember -Identity $domainadmins | Get-ADUser -Properties * | select *
$filename = "DomainAdminsAll"

$ts = (get-date -Format MMddyyyyhhmmss)
#parameters for Excel export
$excelparams = @{
    worksheetname = $filename
    path = $env:TEMP + "\" + $filename + $ts + ".xls"
    show = $true
    verbose = $true
}


$alladmins | Export-Excel @ExcelParams -AutoSize