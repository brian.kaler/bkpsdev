#Import-BLKeyAD
#Written by Brian Kaler
#3/8/2019
#Escrows bitlocker recovery key into AD.

$blstatus = manage-bde -protectors -get C: -type "RecoveryPassword"
$recoverykey = (select-string -InputObject $blstatus -pattern "\{.+\}").matches[0].value
Invoke-Command -ScriptBlock {manage-bde.exe -protectors -adbackup C: -id $recoverykey}