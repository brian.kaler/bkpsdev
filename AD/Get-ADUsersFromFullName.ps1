﻿$myusers = get-content C:\temp\myusers.txt


$report = @()
foreach($user in $myusers){
    $aduser = $null
    $split = $user.split(" ")
    If($split.count -eq 2){
        $firstname = $split[0]
        $lastname = $split[1]
        $aduser = Get-ADUser -Filter {((givenname -eq $firstname) -and (Surname -like $lastname))}
        if($aduser -ne $null){
            $report += $aduser.samaccountname
        }
        Else{
            $report += "No AD User found"
        }
    }

    If($split.count -eq 3){
        $firstname = $split[0]
        $lastname = $split[1]+" "+$split[2]
        $aduser = Get-ADUser -Filter {(givenname -eq $firstname) -and (surname -like "$lastname")}
        if($aduser -ne $null){
            $report += $aduser.samaccountname
        }
        Else{
            $report += "No AD User found"
        }
    }

    }
