﻿#Add-ComputersToADGroup
#Written by Brian Kaler
#11/20/2018
#Description
#Creates a local security group in AD and populates with contents of variable or text file

Function Add-ComputerstoADGroup(){
    Param(
    [Parameter(Mandatory=$true)]
    [String]$groupname,
    [Parameter(Mandatory=$false)]
    [String]$groupfolder,
    [Parameter(Mandatory=$true)]
    [String[]]$members
    )
    $Scriptname = "Add-ComputersToADGroup"
    #create our group if it does not exist.
    If(($mygroup = get-adgroup -filter * | where {$_.name -match $groupname}) -eq $null){
        New-ADGroup -Name $groupname -Path "OU=$groupfolder,OU=SCCM,DC=US,DC=PINKERTONS,DC=COM" -groupscope DomainLocal -Verbose
            While($mygroup -eq $null){
            $mygroup = get-adgroup -filter * | where {$_.name -match $groupname}
            Write-Host "INFO: Waiting for AD to update with new group [$mygroup] - this message will repeat until ready."
            Sleep -seconds 5
            }
        }
        Else
        {
        $mygroup = get-adgroup -filter * | where {$_.name -match $groupname}
        }
    #get a random DC
    $adserver = (Get-ADDomainController | RANDOM).Hostname
    #get a list of all the computers in AD
    $pcs = get-adcomputer -filter *
    IF($pcs -eq $null){
    Write-Host "ERROR: Error reaching Active Directory for complete computer list, script failing"
    exit
    }
    foreach($m in $members){
        foreach($pc in $pcs)
            {
            If($m -eq $pc.Name)
                {
                write-host "INFO: $m was found and added to"$mygroup.name
                $badpc = add-ADGroupMember -identity $mygroup.objectGUID -Members $pc -ErrorAction SilentlyContinue -server $adserver
                }
                ELSE
                {
                #Write-Host "WARNING: $pc was NOT found and will not be added to your group"
                }
            }
        }
    
    write-host "INFO: Waiting 5 seconds for AD Group membership to update, you will have a count of your group membership shortly..."
    write-host "WARNING: Count may be inaccurate"
    sleep -seconds 5
    write-host "INFO: $scriptname completed, total member count in $groupname is: " (Get-ADGroupMember -identity $groupname | measure-object | select -ExpandProperty count)
}


    # -- manual operation, use as a function when possible.
   #$mygroup = "APPPUSH-SERVER2008SP2"
   #$mymembers = gc C:\bkpsdev\input\server2008sp2-12052018.txt
   #add-computerstoadgroup -groupname "SERVERS-RVS" -groupfolder "Servers" -members $rvs
   #Add-ComputerstoADGroup -groupname "SAFES_Beta_Computers" -members $mymembers.resourcename