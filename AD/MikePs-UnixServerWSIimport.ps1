$wsisourcexls = "c:\software\AD\Unix_Inventory.xlsx"
$wsisheet = "List"

#import ImportExcel module
$xlsmod = import-module ImportExcel -PassThru 
#import into an object 
$source = import-excel -Path $wsisourcexls -WorksheetName $wsisheet 
#make a new object to hold only the WSI attributes.
$wsiCollection = @()
foreach($col in $source){
$wsiObject = New-object system.object
$wsiobject | Add-Member -Type NoteProperty -name Hostname -value $col."ServerName"
$wsiObject | Add-Member -Type NoteProperty -name ServiceGroup -value $col."Service Group"
$wsiObject | Add-Member -Type NoteProperty -name ServerEnvironment -value $col."Environment"
$wsiObject | Add-Member -Type NoteProperty -name PrimaryService -value $col."Function"
$wsiObject | Add-Member -Type NoteProperty -name ServiceOwner -value $col."Service Owner"
$wsiObject | Add-Member -Type NoteProperty -name SupportContact -value $col."Support Contact"

$wsiCollection += $wsiobject
$wsiobject = $null
}

$proccount = 0
$batch = $wsiCollection | select *
Foreach($pc in $batch){
$adcomputer = get-adcomputer -identity $pc.hostname If($adcomputer.name -eq $null){ Write-Host "ERROR: Unable to process $pc - perhaps check if spreadsheet host has an existing AD membership"
}
Else
{
Write-Host "INFO: Now processing"($pc.hostname) 
set-adcomputer -Identity $pc.hostname -replace @{wsiServiceGroup=$pc.ServiceGroup} -ErrorAction silentlycontinue -WhatIf
set-adcomputer -Identity $pc.hostname -replace @{wsiServerEnvironment=$pc.ServerEnvironment} -ErrorAction silentlycontinue -WhatIf
set-adcomputer -Identity $pc.hostname -replace @{wsiServiceOwner=$pc.ServiceOwner} -ErrorAction silentlycontinue -WhatIf
set-adcomputer -Identity $pc.hostname -replace @{wsiSupportContact=$pc.SupportContact} -ErrorAction silentlycontinue -WhatIf
set-adcomputer -Identity $pc.hostname -replace @{wsiPrimaryService=$pc.PrimaryService} -ErrorAction silentlycontinue -WhatIf
$proccount += 1 }
}

$results = get-adcomputer -searchbase "OU=Unix,DC=US,DC=PINKERTONS,DC=COM" -filter * -properties Name,wsiPrimaryService,wsiServerEnvironment,wsiServiceOwner,wsiSupportContact | Select Name,wsiPrimaryService,wsiServerEnvironment,wsiServiceOwner,wsiSupportContact | Where {$_.wsiPrimaryService -ne $null} | ft -AutoSize 


# SIG # Begin signature block # MIIMMQYJKoZIhvcNAQcCoIIMIjCCDB4CAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUOIJ0K+pO2urmZOrOht5NGnOi
# kwSgggmaMIIEmTCCA4GgAwIBAgIQcaC3NpXdsa/COyuaGO5UyzANBgkqhkiG9w0B
# AQsFADCBqTELMAkGA1UEBhMCVVMxFTATBgNVBAoTDHRoYXd0ZSwgSW5jLjEoMCYG
# A1UECxMfQ2VydGlmaWNhdGlvbiBTZXJ2aWNlcyBEaXZpc2lvbjE4MDYGA1UECxMv
# KGMpIDIwMDYgdGhhd3RlLCBJbmMuIC0gRm9yIGF1dGhvcml6ZWQgdXNlIG9ubHkx
# HzAdBgNVBAMTFnRoYXd0ZSBQcmltYXJ5IFJvb3QgQ0EwHhcNMTMxMjEwMDAwMDAw
# WhcNMjMxMjA5MjM1OTU5WjBMMQswCQYDVQQGEwJVUzEVMBMGA1UEChMMdGhhd3Rl
# LCBJbmMuMSYwJAYDVQQDEx10aGF3dGUgU0hBMjU2IENvZGUgU2lnbmluZyBDQTCC
# ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJtVAkwXBenQZsP8KK3TwP7v
# 4Ol+1B72qhuRRv31Fu2YB1P6uocbfZ4fASerudJnyrcQJVP0476bkLjtI1xC72Ql
# WOWIIhq+9ceu9b6KsRERkxoiqXRpwXS2aIengzD5ZPGx4zg+9NbB/BL+c1cXNVeK
# 3VCNA/hmzcp2gxPI1w5xHeRjyboX+NG55IjSLCjIISANQbcL4i/CgOaIe1Nsw0Rj
# gX9oR4wrKs9b9IxJYbpphf1rAHgFJmkTMIA4TvFaVcnFUNaqOIlHQ1z+TXOlScWT
# af53lpqv84wOV7oz2Q7GQtMDd8S7Oa2R+fP3llw6ZKbtJ1fB6EDzU/K+KTT+X/kC
# AwEAAaOCARcwggETMC8GCCsGAQUFBwEBBCMwITAfBggrBgEFBQcwAYYTaHR0cDov
# L3QyLnN5bWNiLmNvbTASBgNVHRMBAf8ECDAGAQH/AgEAMDIGA1UdHwQrMCkwJ6Al
# oCOGIWh0dHA6Ly90MS5zeW1jYi5jb20vVGhhd3RlUENBLmNybDAdBgNVHSUEFjAU
# BggrBgEFBQcDAgYIKwYBBQUHAwMwDgYDVR0PAQH/BAQDAgEGMCkGA1UdEQQiMCCk
# HjAcMRowGAYDVQQDExFTeW1hbnRlY1BLSS0xLTU2ODAdBgNVHQ4EFgQUV4abVLi+
# pimK5PbC4hMYiYXN3LcwHwYDVR0jBBgwFoAUe1tFz6/Oy3r9MZIaarbzRutXSFAw
# DQYJKoZIhvcNAQELBQADggEBACQ79degNhPHQ/7wCYdo0ZgxbhLkPx4flntrTB6H
# novFbKOxDHtQktWBnLGPLCm37vmRBbmOQfEs9tBZLZjgueqAAUdAlbg9nQO9ebs1
# tq2cTCf2Z0UQycW8h05Ve9KHu93cMO/G1GzMmTVtHOBg081ojylZS4mWCEbJjvx1
# T8XcCcxOJ4tEzQe8rATgtTOlh5/03XMMkeoSgW/jdfAetZNsRBfVPpfJvQcsVncf
# hd1G6L/eLIGUo/flt6fBN591ylV3TV42KcqF2EVBcld1wHlb+jQQBm1kIEK3Osgf
# HUZkAl/GR77wxDooVNr2Hk+aohlDpG9J+PxeQiAohItHIG4wggT5MIID4aADAgEC
# AhAbh1EfIADX1Rys6/EZSQGmMA0GCSqGSIb3DQEBCwUAMEwxCzAJBgNVBAYTAlVT
# MRUwEwYDVQQKEwx0aGF3dGUsIEluYy4xJjAkBgNVBAMTHXRoYXd0ZSBTSEEyNTYg
# Q29kZSBTaWduaW5nIENBMB4XDTE5MDUzMTAwMDAwMFoXDTIwMDUzMDIzNTk1OVow
# gbYxCzAJBgNVBAYTAnVzMRMwEQYDVQQIDApDYWxpZm9ybmlhMRkwFwYDVQQHDBBX
# ZXN0bGFrZSBWaWxsYWdlMS4wLAYDVQQKDCVTRUNVUklUQVMgU0VDVVJJVFkgU0VS
# VklDRVMgVVNBLCBJTkMuMRcwFQYDVQQLDA5JbmZyYXN0cnVjdHVyZTEuMCwGA1UE
# AwwlU0VDVVJJVEFTIFNFQ1VSSVRZIFNFUlZJQ0VTIFVTQSwgSU5DLjCCASIwDQYJ
# KoZIhvcNAQEBBQADggEPADCCAQoCggEBANuWllaPey78uTo97pHtjT5Amgbe42rX
# eBH0lAAxOi91PTooBluEfP+s4ZFf1QNmIq3NB0qhQIVPWnxX54fGEHPkqaxXpXux
# CvEspw6POFpVex7Lt7u1B/ltef+c7C5A0RL47WvgJ/S5wUBEFrN1jN4muaUTwDzL
# WQs7CMXoErR1AxG6K8yFRY+1wIHlyG5Z14Cr5SkCBK6HuCFRGAo95zjoe+thtCGG
# XX4l3HvxNXL5Iyeo+ZWadyeIjnPmAUfBdrP01CEmd0f7ldofXkqpVzaGfI4L4CHB
# t4jm23Ai/tC0adl1bOBdlQcp6mxR3cLW2BtIgVxAfwC0VkPwC0GulWUCAwEAAaOC
# AWowggFmMAkGA1UdEwQCMAAwHwYDVR0jBBgwFoAUV4abVLi+pimK5PbC4hMYiYXN
# 3LcwHQYDVR0OBBYEFNLKl+hsIYgni+yFhswLzuw7xxCsMCsGA1UdHwQkMCIwIKAe
# oByGGmh0dHA6Ly90bC5zeW1jYi5jb20vdGwuY3JsMA4GA1UdDwEB/wQEAwIHgDAT
# BgNVHSUEDDAKBggrBgEFBQcDAzBuBgNVHSAEZzBlMGMGBmeBDAEEATBZMCYGCCsG
# AQUFBwIBFhpodHRwczovL3d3dy50aGF3dGUuY29tL2NwczAvBggrBgEFBQcCAjAj
# DCFodHRwczovL3d3dy50aGF3dGUuY29tL3JlcG9zaXRvcnkwVwYIKwYBBQUHAQEE
# SzBJMB8GCCsGAQUFBzABhhNodHRwOi8vdGwuc3ltY2QuY29tMCYGCCsGAQUFBzAC
# hhpodHRwOi8vdGwuc3ltY2IuY29tL3RsLmNydDANBgkqhkiG9w0BAQsFAAOCAQEA
# PlmMzVnjmpM3ZCcAUiY4YlRrgfBf6bV7plAT78tWFU9xn+hi2w6iM5YGxWfVBCwf
# 5S9e2/Du3WMgMBL6E9F4l7pWHIYut7e8f/jBJypsLP8iEjvw389ATHBF/f8RCekS
# wKowa+Ff7R2A8IWqTyETNt707aMArZtAtbyshd/0UmCqisTIHaU/6o+oZfN0W1AD
# XozqmjYGEH6txkr6HW16jRD/GNa5UaaIEdrtSrldfQiuI0xC4QmgeGG82lne0HRO
# lAC33gdWw+6DRF6vI2bOOtrBX+xSkSjQNoFHaGB+1RFV1gb0NlcQSeIE6fIRRimc
# f6uqFqYig+PLAnDXl2rpSDGCAgEwggH9AgEBMGAwTDELMAkGA1UEBhMCVVMxFTAT
# BgNVBAoTDHRoYXd0ZSwgSW5jLjEmMCQGA1UEAxMddGhhd3RlIFNIQTI1NiBDb2Rl
# IFNpZ25pbmcgQ0ECEBuHUR8gANfVHKzr8RlJAaYwCQYFKw4DAhoFAKB4MBgGCisG
# AQQBgjcCAQwxCjAIoAKAAKECgAAwGQYJKoZIhvcNAQkDMQwGCisGAQQBgjcCAQQw
# HAYKKwYBBAGCNwIBCzEOMAwGCisGAQQBgjcCARUwIwYJKoZIhvcNAQkEMRYEFA0L
# 4ZoryXYaIOD1jggfKRwrQRkZMA0GCSqGSIb3DQEBAQUABIIBAAsNdgzuq2rQmK/W
# WJ6u1Mo3oghFzFbw6u1SStD7uymSoOUWvgZYelhU8mjDozPZYDM1n/sv0wmmbEpk
# ro54CucHuPNurvOxE9ubCwYrbbUS/s/4kpseL+uyMdriTr3mjHN0aeuGcaQygfxG
# jf4XXN2M1kLb6tMAWaR4BQfum8v+ehi6L81TYzvxGZAuBBOs2Pp+w1hFmxy64QaJ
# kvqraJt0+i3mxybUTwtywAIPaDBAXErJfN2IRRIeW+pH5Gg7oob4vHk5v8F9O9oA
# Bgsgf3Zzz2o6rTvQCgrLN61sjUVbMABQ6WMDT+upEnNPCmtLFyBQWBsbrlNrHOPV
# JBivIyg=
# SIG # End signature block
