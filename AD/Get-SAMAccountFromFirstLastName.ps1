﻿import-module activedirectory
$users = get-content "C:\temp\purchaseusers07082019.txt"

$allusers = get-aduser -VERBOSE -filter * -Properties name,givenname,surname,samaccountname | ?{$_.name -ne $null} | select name,givenname,surname,samaccountname
$output = @()

foreach($u in $users){
    foreach($f in $allusers){
        $fullname = $f.givenname + " " + $f.surname
        #Write-Host "Searching name $fullname"
        $pattern = "*"+($u)+"*"
        If($fullname -like $pattern){
            Write-Host "Found Name: $f"
            $output += $f.SAMACCOUNTNAME
        }
}
}

$output | export-csv C:\temp\purchaseusers04102019.csv