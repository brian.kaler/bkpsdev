#Invoke-ADCleanup
#Written by Brian Kaler
#8/12/2019
#Disables ADObject and systems over 180 days without logon, and moves them to Disabled Computers

#editable params
$mysearchbase = "CN=Computers,DC=us,DC=pinkertons,DC=com"
$mydisabledpcbase = "OU=Disabled Computers,DC=us,DC=pinkertons,DC=com"
$mylogpath = ".\InactiveSystemsProcessed"+(get-date -Format ddMMyyyy-hhmmss)+".log"


#Don't edit below unless you know what you're doing
$mypcs = Get-ADComputer -filter * -Properties * -SearchBase $mysearchbase | select Name,DistinguishedName,lastlogondate | where {$_.lastlogondate -ne $null}
$myinactivepcs = $mypcs | where {$_.lastlogondate -lt (get-date).adddays(-180)}
$newline = "`r`n"
$mylog = New-Item -Path $mylogpath

foreach($p in $myinactivepcs){
    #disable the account first
    $disableobj = Disable-ADAccount -Identity $p.DistinguishedName -PassThru
    If($disableobj -ne $null){
        $l_disableobj = ($p.name)+" was successfully disabled"
        $descriptionstr = "Computer Disabled on"+(get-date -Format ddMMyyyy-hhmmss)
        Set-ADObject -Identity $p.DistinguishedName -Description $descriptionstr
        Add-Content -Path $mylog -value $l_disableobj
    }
    else {
        $l_disableobj = ($p.name)+" was NOT successfully disabled."
        Add-Content -Path $mylog -value $l_disableobj
    }
    #now move the account to disabled container
    $moveobj = Move-ADObject -Identity $p.DistinguishedName -TargetPath $mydisabledpcbase -passthru
        If($moveobj -ne $null){
        $l_adobjmove = ($p.name)+" was moved to Disabled Computers container."
        Add-Content -Path $mylog -value $l_adobjmove
    }
    Else{
        $l_adobjmove = ($p.name)+" was NOT successfully moved to Disabled Computers container."
        Add-Content -Path $mylog -value $l_adobjmove
    }
}

$mylogoutput = Get-Content $mylog
