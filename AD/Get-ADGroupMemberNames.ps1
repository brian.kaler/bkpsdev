﻿
#import ad module
Import-Module ActiveDirectory -force | out-null

#parameters
$mysearchbase = "DC=US,DC=PINKERTONS,DC=COM"
$mygroup = "All IT Associates (EOC)"

$eocit = Get-ADGroupMember -Identity $mygroup | select -ExpandProperty samaccountname
