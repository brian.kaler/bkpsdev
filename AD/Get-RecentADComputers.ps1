#GET-RECENTADCOMPUTERS
#7/11/2019
#Written by Brian kaler
#Gets a collection of AD Computer objects selected by creation date

Function Get-RecentADComputers{
Param(
    [Parameter(Mandatory=$true)]
    [DateTime]

)

 #get a random DC
 $adserver = (Get-ADDomainController | RANDOM).Hostname
 #get a list of all the computers in AD
 $pcs = get-adcomputer -filter *
IF($pcs -eq $null){
    Write-Host "ERROR: Error reaching Active Directory for complete computer list, script failing"
    exit
    }


}