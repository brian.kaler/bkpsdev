﻿import-module activedirectory

$me = (get-aduser -filter 'sn -like "kaler"' -properties *)[0]
$lockstatus = ($me | select -ExpandProperty lockedout)
write-host ($me.name) "was found in lock state: $lockstatus"
if($lockstatus -eq $true){
Unlock-ADAccount $me.DistinguishedName
write-host "Unlocking: " ($me.name)
sleep -seconds 15
$me = (get-aduser -filter 'sn -like "kaler"' -properties *)[0]
$lockstatus = ($me | select -ExpandProperty lockedout)
write-host ($me.name) "is now in lock state: $lockstatus" 
}

