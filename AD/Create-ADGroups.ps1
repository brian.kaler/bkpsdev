﻿#Create MAINT-SERVER-GROUPS
#Written by Brian Kaler
#10/18/2018


#create a CSV with " and one col for import.
$groups = get-content -Path "C:\temp\availableapplicationssccmad11202019.txt"

#for manual entry uncomment below.
#$groups = "MAINT-SERVER-PRODD1W1","MAINT-SERVER-PRODD1W2","MAINT-SERVER-PRODD1W3"

foreach($g in $groups){
    New-ADGroup -Name $g -Path "OU=Applications,OU=SCCM,DC=US,DC=PINKERTONS,DC=COM" -groupscope DomainLocal -Verbose
    }

