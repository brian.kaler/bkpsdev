﻿import-module activedirectory
$users = get-content "C:\temp\purchaseusers04102019.txt"

$allusers = get-aduser -filter * -Properties name,givenname,surname,samaccountname | select name,givenname,surname,samaccountname
$output = @()

foreach($u in $allusers){
    foreach($f in $users){
        $fullname = $u.givenname + " " + $u.surname
        If($u.name -match $fullname){
            $output += $u.SAMACCOUNTNAME
        }
}
}

$output | export-csv C:\temp\purchaseusers04102019.csv