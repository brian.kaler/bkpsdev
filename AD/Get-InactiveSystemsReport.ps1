
import-module ActiveDirectory


$TrackedOU = @("OU=Workstations,DC=us,DC=pinkertons,DC=com",
"OU=Windows 10,DC=us,DC=pinkertons,DC=com",
"CN=Computers,DC=us,DC=pinkertons,DC=com",
"OU=Servers,DC=us,DC=pinkertons,DC=com",
"OU=Citrix Xen 7,DC=us,DC=pinkertons,DC=com",
"OU=SCCM,DC=us,DC=pinkertons,DC=com",
"OU=Securitas,DC=local,DC=securitas,DC=ca",
"OU=Servers,DC=local,DC=securitas,DC=ca",
"OU=STAS,DC=local,DC=securitas,DC=ca",
"OU=TEST,DC=local,DC=securitas,DC=ca",
"OU=VDI,DC=us,DC=pinkertons,DC=com",
"OU=VM Workstations,DC=us,DC=pinkertons,DC=com",
"CN=Computers,DC=local,DC=securitas,DC=ca")
foreach($ou in $trackedou){
    $queryallpcs += Get-ADComputer -filter * -searchbase $ou -Properties Name,DistinguishedName,lastlogondate | select Name,DistinguishedName,lastlogondate | where {$_.lastlogondate -ne $null}
}
$listinactivepcs = $queryallpcs | where {$_.lastlogondate -lt (get-date).adddays(-180)}
$listactivepcs = $queryallpcs | where {$_.lastlogondate -ge (get-date).adddays(-180)}

