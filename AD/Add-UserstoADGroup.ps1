<# 
.Synopsis 
   Creates a local security group in AD and populates with contents of variable or text file 
.DESCRIPTION 
   Creates a local security group in AD and populates with contents of variable or text file 
.NOTES 
    Add-UsersToADGroup
    Written by Brian Kaler
    3/13/2019
.PARAMETER Members
    If you use get-content on a text file such as this, you can use  $mylist in place of the members listed above.
    Example of get-content with a valid newline list of SAMACCOUNTNAME members:
    $mylist = Get-content -path “C:\temp\mylistofsamaccountnames.txt” 
.EXAMPLE 
   .\add-userstoadgroup.ps1 -members (@(“bkaler”,”mayala”)) -groupname “USERS-SOFTWARE-CITRIXRECEIVER49”
   Adds users bkaler and mayala to USERS-SOFTWARE-CITRIXRECEIVER49
#> 
Function Add-UserstoADGroup(){
    Param(
    [Parameter(Mandatory=$true)]
    [String]$groupname,
    [Parameter(Mandatory=$true)]
    [String[]]$members
    )

    $Scriptname = "Add-UsersToADGroup"

    #create our group if it does not exist.
    If(($mygroup = get-adgroup -filter * | where {$_.name -match $groupname}) -eq $null){
        New-ADGroup -Name $groupname -Path "OU=Users,OU=SCCM,DC=US,DC=PINKERTONS,DC=COM" -groupscope DomainLocal -Verbose
            While($mygroup -eq $null){
            $mygroup = get-adgroup -filter * | where {$_.name -match $groupname}
            Write-Host "INFO: Waiting for AD to update with new group [$mygroup] - this message will repeat until ready."
            Sleep -seconds 5
            }
        }
        Else
        {
        $mygroup = get-adgroup -filter * | where {$_.name -match $groupname}
        }
    
    #get a random DC
    $adserver = (Get-ADDomainController | RANDOM).Hostname
    #get a list of all the Users in AD
    $Users = get-aduser -filter * | select Name,Samaccountname
    IF($Users -eq $null){
    Write-Host "ERROR: Error reaching Active Directory for complete user list, script will end"
    exit
    }
    
    foreach($m in $members){
        foreach($u in $Users)
            {
            If($m -eq $u.samaccountname)
                {
                $baduser = add-ADGroupMember -identity $mygroup.objectGUID -members $u.samaccountname -ErrorAction SilentlyContinue -server $adserver -passthru
                If($baduser -eq $null){
                    write-host "INFO: $m was matched but something went wrong adding to"$mygroup.name
                }
                else {
                    write-host "INFO: $m was found and added to"$mygroup.name
                }
                }
                ELSE
                {
                #Write-Host "WARNING: $pc was NOT found and will not be added to your group"
                }
            }
        }
    
    write-host "INFO: Waiting 5 seconds for AD Group membership to update, you will have a count of your group membership shortly..."
    write-host "WARNING: Count may be inaccurate"
    sleep -seconds 5
    write-host "INFO: $scriptname completed, total member count in $groupname is: " (Get-ADGroupMember -identity $groupname | measure-object | select -ExpandProperty count)
    }

