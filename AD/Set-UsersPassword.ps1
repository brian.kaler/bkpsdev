﻿#Set-UsersPassword
#Written by Brian Kaler
#12/3/2018
#sets an AD users password from a CSV
#make sure CSV has both a "UserID" field that matches AD SAMACCOUNTNAME and a new "Password" field.

$members = import-csv "..\input\indiausers12122018.csv"

$adusers = get-aduser -Filter * -SearchBase "OU=CIUS,DC=us,DC=pinkertons,DC=com" -Properties samaccountname,mail
$i = 0
foreach($m in $members){
    foreach($a in $adusers){
        if($a.samaccountname -eq $m.UserId){ 
            $i += 1
            if($a.mail -eq $null){write-host ($m.UserId) "      was matched, but no mail property found"}
            write-host ($m.UserId) "      was matched, processing now - new password:      " ($m.Password)
            $securepw = ConvertTo-SecureString -string $m.Password -AsPlainText -Force
            Set-ADAccountPassword -reset -NewPassword $securepw -Identity $a -verbose
            }
        }
    }

Write-host "Script ran and processed $i records"