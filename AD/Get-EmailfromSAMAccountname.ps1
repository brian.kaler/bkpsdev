﻿$users = @("EInkel",
"JRitchie",
"DTaha",
"FMacri",
"KSchwartz",
"RLane",
"EMercier",
"DFallon",
"BWomer",
"ADesimon",
"CBolton",
"MPisciot",
"SUppal",
"CMuff",
"MTulipan",
"JPolimen",
"MBissen",
"Lmunoz",
"BJohnson02",
"SValler",
"Gdoherty",
"Mvelazqu",
"Jedmisto",
"SCroal",
"Tnithian",
"LBelange",
"Kjenkins",
"Rfox",
"Phalvers",
"AMeyer",
"SSmith07",
"Jcullen",
"Maltieri",
"NSyrigos",
"BBauer",
"Agibbons",
"MHasan",
"Avornloc",
"Kkelly01",
"Redwards",
"Dparris",
"CConnoll",
"DMoody",
"CDodge",
"KRolland",
"BHoblos",
"WSeward",
"MParsons",
"BMiller01",
"Jbowen",
"SStupski",
"SCirrone",
"JThomas",
"RBindema",
"RSalamon",
"FOkonofu",
"DAndriol",
"WWong",
"SLazaro",
"GSherbur",
"TCunning")

$allusers = get-aduser -filter * -Properties name,SamAccountname,mail | select name,SamAccountname,mail
$output = @()

foreach($u in $allusers){
    foreach($f in $users){
        If($u.samaccountname -match $f){
            $output += $u.mail
        }
}
}

$output