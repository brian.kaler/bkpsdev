#Get-FullNamefromSamAccountName
#7/12/2019
#Written by Brian Kaler
#Gets the user's full name from Samaccountname provided.

import-module activedirectory
Function Get-FullNamefromSamaccountname{
    Param(
        [Parameter(Mandatory=$true)]
        [String[]]$users,
        [Parameter(Mandatory=$true)]
        [Object[]]$adusers
    )
    $report = @()
    foreach($u in $users){
        #If the line is blank, skip, otherwise get the aduser object
        if($u -eq $null){
            $report += "No AD User Found"
        }
        else {
            $myuser = Get-ADUser -Identity $u -ErrorAction SilentlyContinue -Properties name,givenname,surname,samaccountname -verbose
            $report += $myuser
        }
    }
    Return $report
}


$myusers = get-content "\\uswrdpt02\C$\temp\mypcaasuserlist.txt"
$myadusers = get-aduser -VERBOSE -filter * -Properties name,givenname,surname,samaccountname | ?{$_.name -ne $null} | select name,givenname,surname,samaccountname

$myreport = Get-FullNamefromSamaccountname -users $myusers -adusers $myadusers


