﻿
$mylist = Import-Excel c:\temp\Windows7UnknownLocations2.xlsx

$adusersmail = get-aduser -filter * -Properties Samaccountname,givenname,surname,mail,cn,wwwhomepage,description | select samaccountname,givenname,surname,mail,cn,wwwhomepage,description



$reportlist = @()
foreach($i in $mylist){
    foreach($user in $adusersmail){
        $calcuser = $user.samaccountname
        If($i."Primary User" -like $calcuser){
            $report = new-object -TypeName psobject
            $report | Add-Member -MemberType NoteProperty -Name "Primary User" -value $i."Primary User"
            $report | Add-Member -MemberType NoteProperty -Name "First Name" -value $user.givenname     
            $report | Add-Member -MemberType NoteProperty -Name "Last Name" -value $user.surname
            $report | Add-Member -MemberType NoteProperty -Name "Computername"  -value $i.Hostname            
            $report | Add-Member -MemberType NoteProperty -Name "EmailAddress" -value $user.mail
            $report | Add-Member -MemberType NoteProperty -Name "Canonical Name" -value $user.cn
            $report | Add-Member -MemberType NoteProperty -Name "Webpage" -value $user.wwwhomepage
            $report | Add-Member -MemberType NoteProperty -Name "Description" -value $user.Description
            $reportlist += $report
        }


    }

}

$reportlist | Export-Excel "C:\temp\Windows7Unknownlocationwithuser4.xlsx"