import-module activedirectory

$properties = @("GivenName","SN","EmailAddress","Department","departmentNumber")

$allusers = get-aduser -filter * -Properties GivenName,SN,EmailAddress,Department,departmentNumber
$output = @()

foreach($u in $allusers){
    $obj = New-Object -TypeName PSObject 
    $obj | Add-Member -MemberType NoteProperty -name "FirstName" -value $u.givenname
    $obj | Add-Member -MemberType NoteProperty -name "LastName" -value $u.sn
    $obj | Add-Member -MemberType NoteProperty -name "EmailAddress" -value $u.emailaddress
    $obj | Add-Member -MemberType NoteProperty -name "Department" -value $u.Department
    $obj | Add-Member -MemberType NoteProperty -Name "DepartmentNumber" -value $u.departmentnumber[0]
    $output += $obj
}

$output | export-excel C:\temp\adusersdepartmentreport.xlsx