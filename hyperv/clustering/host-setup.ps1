#Variables to edit
$vmdrive = "D:\"
$vmdir = join-path $vmdrive -childpath "vmmachine1"
$vhddir = join-path $vmdrive -ChildPath "vmstorage1"
$workspace = join-path $vmdrive -childpath "vmworkspace1"
$RMSave = New-TimeSpan -hours 1 -Minutes 30

#make directories
New-Item -path $vhddir,$vmdir,$workspace -erroraction 0 -type directory
#install features
install-windowsfeature -Name "Hyper-V" -IncludeAllSubFeature -IncludeManagementTools
Install-WindowsFeature -name "Failover-Clustering" -IncludeAllSubFeature -IncludeManagementTools
Install-WindowsFeature -name "Multipath-IO"

#configure hyper-v
Set-VMHost -VirtualHardDiskPath $VHDdir -VirtualMachinePath $vmdir -ResourceMeteringSaveInterval $RMSave
