﻿

$allnicinfo = Get-NetAdapterHardwareInfo | Select Name, InstanceID, InterfaceDescription, BusNumber, FunctionNumber

#determine how many adapters exist
$adapters = Get-NetAdapterHardwareInfo | select -ExpandProperty busnumber
$buscount = $adapters | measure-object -maximum | select Maximum

#get the nics
#switch($buscount.maximum){
#    1{
#    $NIC1 = $allnicinfo | where {($_.busnumber -eq "1") -and ($_.functionnumber -eq "0")}
#    $NIC2 = $allnicinfo | where {($_.busnumber -eq "1") -and ($_.functionnumber -eq "1")} 
#    $NIC3 = $allnicinfo | where {($_.busnumber -eq "1") -and ($_.functionnumber -eq "2")}
#    $NIC4 = $allnicinfo | where {($_.busnumber -eq "1") -and ($_.functionnumber -eq "3")}
#    }
#    2{
#    $NIC1 = $allnicinfo | where {($_.busnumber -eq "1") -and ($_.functionnumber -eq "0")}
#    $NIC2 = $allnicinfo | where {($_.busnumber -eq "1") -and ($_.functionnumber -eq "1")} 
#    $NIC3 = $allnicinfo | where {($_.busnumber -eq "2") -and ($_.functionnumber -eq "0")}
#    $NIC4 = $allnicinfo | where {($_.busnumber -eq "2") -and ($_.functionnumber -eq "1")}
#    }
#}

#get the nics (another approach)
$busunique = $adapters | select -Unique

foreach($bus in $busunique){
    $busfunction = Get-NetAdapterHardwareInfo | select -ExpandProperty functionnumber | measure
    for(0..$busfunction.maximum){
         $NIC1 = $allnicinfo | where {($_.busnumber -eq $busunique) -and ($_.functionnumber -eq "0")}
         $NIC2 = $allnicinfo | where {($_.busnumber -eq $busunique) -and ($_.functionnumber -eq "1")}
         $NIC3 = $allnicinfo | where {($_.busnumber -eq $busunique) -and ($_.functionnumber -eq "2")}
         $NIC4 = $allnicinfo | where {($_.busnumber -eq $busunique) -and ($_.functionnumber -eq "3")}
        }
}


#set labels
rename-netadapter $NIC1.Name "PORT1"
rename-netadapter $NIC2.Name "PORT2"
rename-netadapter $NIC3.Name "PORT3"
rename-netadapter $NIC4.Name "PORT4"

