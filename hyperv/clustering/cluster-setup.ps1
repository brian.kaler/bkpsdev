﻿## Collect hardware information, determine ports
$Nics = @(
    Get-NetAdapterHardwareInfo |
        Select-Object -Property @('Name', 'InstanceID', 'InterfaceDescription', 'BusNumber', 'FunctionNumber')
)

## Rename logic
For ($i = 0; $i -le $Nics.Count; $i++)
{
    $FunctionCount = (($Nics | Where-Object { $_.BusNumber -eq $i }).FunctionNumber | Measure-Object -Maximum).Maximum

    For ($j = 0; $j -le $FunctionCount; $j++)
    {
        $Nic = $Nics | Where-Object { $_.BusNumber -eq $i -and $_.FunctionNumber -eq $j }
        Rename-NetAdapter -Name $Nic.Name -NewName "B$i Port$($j + 1)" -PassThru
    }
}

#prompt user for which system to configure (1-3)
$sysid = read-host "Provide the system ID"
#get network adapters (they need to be preconfigured at this time)
$PORT1 = $Nics[0] | get-netadapter
$PORT2 = $Nics[1] | get-netadapter
$PORT3 = $Nics[2] | get-netadapter
$PORT4 = $Nics[3] | get-netadapter
$netmgmt = "192.168.2.5" + $sysid
$nethbeat = "10.249.0." + $sysid
$netmigrate = "10.250.0." + $sysid
$netbackup = "10.248.0." + $sysid
$mask255 = "255.255.255.0"
$dns1 = "192.168.2.4"
$dns2 = "192.168.2.7"
$gwmgmt = "192.168.2.2"
$gwhbeat = "10.249.0.254"
$gwmigrate = "10.250.0.254"
$gwbackup = "10.248.0.254"

#configure tcpip
start-process -filepath "netsh.exe" -argumentlist "interface ip set address $PORT1.InterfaceAlias static $netmgmt 255.255.255.0"
start-process -filepath "netsh.exe" -argumentlist "interface ip set address $PORT2.InterfaceAlias static $nethbeat 255.255.255.0"
start-process -filepath "netsh.exe" -argumentlist "interface ip set address $PORT3.InterfaceAlias static $netmigrate 255.255.255.0"
start-process -filepath "netsh.exe" -argumentlist "interface ip set address $PORT4.InterfaceAlias static $netbackup 255.255.255.0"

