﻿#New-Template
#Uses MDT to provision a new VM in vmware that is syspreped and readily converted to a template
#written by Brian Kaler
#8/1/2018


Param(
[Parameter(Mandatory)]
[String]$vCenterInstance,
[Parameter(Mandatory)]
[String]$TargetCluster,
[Parameter(Mandatory=$false)]
[String]$ISOPath = "[VMISO] New-Template\LiteTouchPE_x64.iso",
[Parameter(Mandatory)]
[String]$Id,
[Parameter(Mandatory)]
[String]$MDTBuildPath,
[Parameter(Mandatory=$false)]
[String]$VMName = ("vmwarebuild-{0:yyyy-MM-dd-HH-mm-ss}" -f (Get-Date)),
[Parameter(Mandatory=$false)]
[String]$TemplateName = ("template-{0:yyyy-MM-dd-HH-mm-ss}" -f (Get-Date)),
[Parameter(Mandatory)]
[String]$VMDatastore,
[Parameter(Mandatory=$false)]
[String]$VMCPU = 2,
[Parameter(Mandatory=$false)]
[String]$VMMemory = 4096,
[Parameter(Mandatory=$false)]
[String]$VMDiskSize = 40,
[Parameter(Mandatory=$false)]
[String]$GuestOS = "Windows",
[Parameter(Mandatory=$false)]
[String]$VMNetwork
)

# -----Import Modules-----
Write-Host "$(Get-Date -Format G) Importing Hyper-V 1.1 PowerShell Module"
Import-Module $env:windir\System32\WindowsPowerShell\v1.0\Modules\Hyper-V\1.1\Hyper-V.psd1
Write-Host "$(Get-Date -Format G) Importing MDT PowerShell Module"
Import-Module "$env:programfiles\Microsoft Deployment Toolkit\bin\MicrosoftDeploymentToolkit.psd1"
Write-Host "$(Get-Date -Format G) Importing Vmware Module"
Get-Module -ListAvailable VMware* | Import-Module | Out-Null

# ------Prep the MDT-------
    ## Setup MDT custom settings for VM auto deploy
    
    Write-Host ""
    Write-Host "$(Get-Date -Format G) Starting process for $Id"
    Write-Host ""
    Write-Host "$(Get-Date -Format G) Backing up current MDT CustomSettings.ini"

    Copy-Item $MdtBuildPath\Control\CustomSettings.ini $MdtBuildPath\Control\CustomSettings-backup.ini
    Start-Sleep -s 5

    Write-Host "$(Get-Date -Format G) Setting MDT CustomSettings.ini for Task Sequence ID: $Id"

    Add-Content $MdtBuildPath\Control\CustomSettings.ini ""
    Add-Content $MdtBuildPath\Control\CustomSettings.ini ""
    Add-Content $MdtBuildPath\Control\CustomSettings.ini "TaskSequenceID=$Id"
    Add-Content $MdtBuildPath\Control\CustomSettings.ini "SkipTaskSequence=YES"
    Add-Content $MdtBuildPath\Control\CustomSettings.ini "SkipComputerName=YES"

# ------Connect vcenter------
Connect-VIServer -Server $vCenterInstance -user $vCenterUser -Password $vCenterPass
# ------Make a VM------
$vmhost = Get-Cluster $TargetCluster | Get-VMHost | get-random
$newvm = new-vm -Name $VMName -Datastore (get-datastore $VMDatastore) -vmhost $vmhost -MemoryMB $VMMemory -NumCpu $VMCPU -DiskGB $VMDiskSize -CD -NetworkName $vmnetwork -guestid $GuestOS
# ------Attach the boot iso------
While ((get-vm -name $vmname).Powerstate -eq "PoweredOff") {
$cddrive = Get-CDDrive -VM $vmname
Set-CDDrive -cd $cddrive -IsoPath $ISOPath -StartConnected:$true -confirm:$false -ErrorAction:SilentlyContinue
# ------Power VM------
Start-VM $newvm
}


# ------Wait for VM to stop------
Write-Host "$(Get-Date -Format G) Waiting for $VmName to build"
While ((get-vm -name $vmname).Powerstate -eq "PoweredOn") {Start-Sleep -s 10}

# ------Make the VM a template------
$template = New-Template -vm $newvm -name $TemplateName -Datastore $VMDatastore -Location "Templates"

# ------Cleanup MDT ------
  Remove-Item $MdtBuildPath\Control\CustomSettings.ini
  Copy-Item $MdtBuildPath\Control\CustomSettings-backup.ini $MdtBuildPath\Control\CustomSettings.ini